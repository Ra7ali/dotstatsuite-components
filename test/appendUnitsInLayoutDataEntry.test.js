import { expect } from 'chai';
import { appendUnitsInLayoutDataEntry } from '../src/rules/src/table/units/appendUnitsInLayoutDataEntry';

const layoutData = [
  { dimension: { id: 'D1', __index: 2 }, value: { id: 'V', __index: 0 } },
  { dimension: { id: 'D2', __index: 0 }, value: { id: 'V', __index: 0 } },
  { dimension: { id: 'D3', __index: 4 }, value: { id: 'V', __index: 0 } }
];

const unitsSeries = {
  '0:x:0:x:x': {
    U1: { id: 'U1', value: { id: 'U1V' } },
    U2: { id: 'U2', value: { id: 'U2V' } },
    U3: { id: 'U3', value: { id: 'U3V' } }
  },
  '1:x:1:x:x': {
    U1: { id: 'U1', value: { id: 'U1V' } },
    U3: { id: 'U3', value: { id: 'U3V' } },
    U4: { id: 'U4', value: { id: 'U4V' } },
  },
};

describe('appendUnitsInLayoutDataEntry tests', () => {
  it('blank test', () => {
    expect(appendUnitsInLayoutDataEntry({
      display: 'label',
      unitDimension: {},
      unitsSeries: {},
      unitsDefinitionCodes: [],
      unitsIndexes: [],
      partialUnitSerieIndexes: []
    })([])).to.deep.equal([{ dimension: {}, value: { id: 'VAL', unitsLabel: '' } }]);
  });
  it('complete test', () => {
    expect(appendUnitsInLayoutDataEntry({
      display: 'code',
      unitDimension: { id: 'U' },
      unitsSeries,
      unitsDefinitionCodes: ['U3', 'U1', 'U4'],
      unitsIndexes: [1, 0],
      partialUnitSerieIndexes: ['x', 'x', 'x', 'x', 'x']
    })(layoutData)).to.deep.equal([
      { dimension: { id: 'D1', __index: 2 }, value: { id: 'V', __index: 0 } },
      { dimension: { id: 'D2', __index: 0 }, value: { id: 'V', __index: 0 } },
      { dimension: { id: 'D3', __index: 4 }, value: { id: 'V', __index: 0 } },
      { dimension: { id: 'U' }, value: { id: 'VAL', unitsLabel: 'U3V, U1V' } }
    ]);
  });
  it('rejectedValues test', () => {
    expect(appendUnitsInLayoutDataEntry({
      display: 'code',
      rejectedValueIds: ['U3V'],
      unitDimension: { id: 'U' },
      unitsSeries,
      unitsDefinitionCodes: ['U3', 'U1', 'U4'],
      unitsIndexes: [1, 0],
      partialUnitSerieIndexes: ['x', 'x', 'x', 'x', 'x']
    })(layoutData)).to.deep.equal([
        { dimension: { id: 'D1', __index: 2 }, value: { id: 'V', __index: 0 } },
        { dimension: { id: 'D2', __index: 0 }, value: { id: 'V', __index: 0 } },
        { dimension: { id: 'D3', __index: 4 }, value: { id: 'V', __index: 0 } },
        { dimension: { id: 'U' }, value: { id: 'VAL', unitsLabel: 'U1V' } }
    ]);
  })
});
