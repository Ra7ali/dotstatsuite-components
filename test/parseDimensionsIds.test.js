import { expect } from 'chai';
import { parseDimensionsIds } from '../src/rules/src/table/preparators/parseDimensionsIds';

describe('parseDimensionsIds', () => {
  it('basic test', () => {
    const dimensions = [
      {},
      { id: 'a' },
      { id: 'b', values: [] },
      { id: 'c', values: [{}] },
      { id: 'd', values: [{}, {}] },
      { id: 'e', values: [{}, {}, {}] },
      { id: 'f', values: [{}] }
    ];
    expect(parseDimensionsIds(dimensions)).to.deep.equal({
      oneValue: { c: 3, f: 6 },
      manyValues: { d: 4, e: 5 }
    });
  });
});
