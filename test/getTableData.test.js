import { expect } from 'chai';
import { getTableProps } from '../src/rules/src/table/factories/getTableData';

const data = {
  dimensions: { one: {}, many: {}, length: 0 },
  units: {
    headerUnits: [],
    unitsDefinitionCodes: [],
    unitsAttachmentSeriesIds: ['UNIT_MEASURE', 'UNIT_MULT', 'BASE_PER'],
    unitsSeries: {},
    unitDimension: { id: 'UNIT', name: 'units' }
  },
  observations: {},
  dataflowAttributes: {},
  dataflowName: undefined,
  seriesAttributes: [],
  seriesAttributesValues: {}
};

const data1 = {
  "dimensions": {
    "one": {
      "FREQ": {
        "id": "FREQ",
        "name": "Frequency of observation",
        "names": {
          "en": "Frequency of observation"
        },
        "keyPosition": 1,
        "roles": [
          "FREQ"
        ],
        "values": [
          {
            "id": "A",
            "order": 0,
            "name": "Annual",
            "names": {
              "en": "Annual"
            },
            "__indexPosition": 0,
            "attributes": {},
            "display": true
          }
        ],
        "role": "FREQ",
        "__index": 1,
        "display": true
      },
      "MEASURE": {
        "id": "MEASURE",
        "name": "Measure",
        "names": {
          "en": "Measure"
        },
        "keyPosition": 2,
        "roles": [
          "MEASURE"
        ],
        "values": [
          {
            "id": "GDP",
            "order": 0,
            "name": "GDP",
            "names": {
              "en": "GDP"
            },
            "__indexPosition": 0,
            "attributes": {},
            "display": true
          }
        ],
        "role": "MEASURE",
        "__index": 2,
        "display": true
      },
      "PRICE_BASE": {
        "id": "PRICE_BASE",
        "name": "Price Base",
        "names": {
          "en": "Price Base"
        },
        "keyPosition": 4,
        "roles": [
          "PRICE_BASE"
        ],
        "values": [
          {
            "id": "V",
            "order": 1,
            "name": "Current (V)",
            "names": {
              "en": "Current (V)"
            },
            "__indexPosition": 1,
            "attributes": {},
            "display": true
          }
        ],
        "role": "PRICE_BASE",
        "__index": 4,
        "display": true
      },
      "ADJUSTMENT": {
        "id": "ADJUSTMENT",
        "name": "Adjustment",
        "names": {
          "en": "Adjustment"
        },
        "keyPosition": 5,
        "roles": [
          "ADJUSTMENT"
        ],
        "values": [
          {
            "id": "SA",
            "order": 1,
            "name": "Seasonally adjusted",
            "names": {
              "en": "Seasonally adjusted"
            },
            "__indexPosition": 1,
            "attributes": {},
            "display": true
          }
        ],
        "role": "ADJUSTMENT",
        "__index": 5,
        "display": true
      },
      "TRANSFORMATION": {
        "id": "TRANSFORMATION",
        "name": "Transformation",
        "names": {
          "en": "Transformation"
        },
        "keyPosition": 6,
        "roles": [
          "TRANSFORMATION"
        ],
        "values": [
          {
            "id": "NT",
            "order": 0,
            "name": "Non-transformed",
            "names": {
              "en": "Non-transformed"
            },
            "__indexPosition": 0,
            "attributes": {},
            "display": true
          }
        ],
        "role": "TRANSFORMATION",
        "__index": 6,
        "display": true
      }
    },
    "many": {
      "REF_AREA": {
        "id": "REF_AREA",
        "name": "Reference area",
        "names": {
          "en": "Reference area"
        },
        "keyPosition": 0,
        "roles": [
          "REF_AREA"
        ],
        "values": [
          {
            "id": "AU",
            "name": "Australia",
            "__indexPosition": 12,
            "attributes": {},
            "__index": 0
          },
          {
            "id": "CA",
            "name": "Canada",
            "__indexPosition": 37,
            "attributes": {},
            "__index": 1
          },
          {
            "id": "_T",
            "name": "Total",
            "__indexPosition": 249,
            "attributes": {},
            "__index": 2
          },
          {
            "id": "BE",
            "name": "Belgium",
            "__indexPosition": 19,
            "attributes": {},
            "__index": 3
          }
        ],
        "role": "REF_AREA",
        "__index": 0
      },
      "UNIT_MEASURE": {
        "id": "UNIT_MEASURE",
        "name": "Unit of measure",
        "names": {
          "en": "Unit of measure"
        },
        "keyPosition": 3,
        "roles": [
          "UNIT_MEASURE"
        ],
        "values": [
          {
            "id": "AUD",
            "name": "Australien Dollar",
            "__indexPosition": 1,
            "attributes": {},
            "__index": 0
          },
          {
            "id": "CAD",
            "name": "Canadian Dollar",
            "__indexPosition": 3,
            "attributes": {},
            "__index": 1
          },
          {
            "id": "USDPPP",
            "name": "USD equivalent, PPP",
            "__indexPosition": 4,
            "attributes": {},
            "__index": 2
          },
          {
            "id": "EUR",
            "name": "Euro",
            "__indexPosition": 2,
            "attributes": {},
            "__index": 3
          }
        ],
        "role": "UNIT_MEASURE",
        "__index": 3
      },
      "TIME_PERIOD": {
        "id": "TIME_PERIOD",
        "name": "Time period",
        "names": {
          "en": "Time period"
        },
        "keyPosition": 7,
        "roles": [
          "TIME_PERIOD"
        ],
        "values": [
          {
            "id": "2000",
            "name": "2000",
            "__indexPosition": 946684800000,
            "attributes": {},
            "__index": 0
          },
          {
            "id": "2001",
            "name": "2001",
            "__indexPosition": 978307200000,
            "attributes": {},
            "__index": 1
          },
          {
            "id": "2002",
            "name": "2002",
            "__indexPosition": 1009843200000,
            "attributes": {},
            "__index": 2
          }
        ],
        "role": "TIME_PERIOD",
        "__index": 7
      }
    },
    "length": 8
  },
  "units": {
    "headerUnits": [],
    "unitDimension": { "id": "UNIT", "name": "units" },
    "unitsDefinitionCodes": [
      "UNIT_MEASURE",
      "PRICE_BASE",
      "ADJUSTMENT",
      "TRANSFORMATION",
      "UNIT_MULT",
      "BASE_YEAR"
    ],
    "unitsAttachmentSeriesIds": [
      "UNIT_MEASURE",
      "REF_AREA"
    ],
    "unitsSeries": {
      "0:x:x:0:x:x:x:x": {
        "UNIT_MEASURE": {
          "id": "UNIT_MEASURE",
          "name": "Unit of measure",
          "value": {
            "id": "AUD",
            "order": 1,
            "name": "Australien Dollar",
            "names": {
              "en": "Australien Dollar"
            },
            "__indexPosition": 1
          }
        },
        "UNIT_MULT": {
          "id": "UNIT_MULT",
          "name": "Unit multiplier",
          "value": {
            "id": "0",
            "order": 0,
            "name": "Units",
            "names": {
              "en": "Units"
            }
          }
        },
        "BASE_YEAR": {
          "id": "BASE_YEAR",
          "name": "Base Year",
          "value": {
            "id": "_Z",
            "order": 0,
            "name": "Not applicable",
            "names": {
              "en": "Not applicable"
            }
          }
        }
      },
      "1:x:x:1:x:x:x:x": {
        "UNIT_MEASURE": {
          "id": "UNIT_MEASURE",
          "name": "Unit of measure",
          "value": {
            "id": "CAD",
            "order": 3,
            "name": "Canadian Dollar",
            "names": {
              "en": "Canadian Dollar"
            },
            "__indexPosition": 3
          }
        },
        "UNIT_MULT": {
          "id": "UNIT_MULT",
          "name": "Unit multiplier",
          "value": {
            "id": "0",
            "order": 0,
            "name": "Units",
            "names": {
              "en": "Units"
            }
          }
        },
        "BASE_YEAR": {
          "id": "BASE_YEAR",
          "name": "Base Year",
          "value": {
            "id": "_Z",
            "order": 0,
            "name": "Not applicable",
            "names": {
              "en": "Not applicable"
            }
          }
        }
      },
      "2:x:x:2:x:x:x:x": {
        "UNIT_MEASURE": {
          "id": "UNIT_MEASURE",
          "name": "Unit of measure",
          "value": {
            "id": "USDPPP",
            "order": 4,
            "name": "USD equivalent, PPP",
            "names": {
              "en": "USD equivalent, PPP"
            },
            "__indexPosition": 4
          }
        },
        "UNIT_MULT": {
          "id": "UNIT_MULT",
          "name": "Unit multiplier",
          "value": {
            "id": "0",
            "order": 0,
            "name": "Units",
            "names": {
              "en": "Units"
            }
          }
        },
        "BASE_YEAR": {
          "id": "BASE_YEAR",
          "name": "Base Year",
          "value": {
            "id": "_Z",
            "order": 0,
            "name": "Not applicable",
            "names": {
              "en": "Not applicable"
            }
          }
        }
      },
      "3:x:x:3:x:x:x:x": {
        "UNIT_MEASURE": {
          "id": "UNIT_MEASURE",
          "name": "Unit of measure",
          "value": {
            "id": "EUR",
            "order": 2,
            "name": "Euro",
            "names": {
              "en": "Euro"
            },
            "__indexPosition": 2
          }
        },
        "UNIT_MULT": {
          "id": "UNIT_MULT",
          "name": "Unit multiplier",
          "value": {
            "id": "0",
            "order": 0,
            "name": "Units",
            "names": {
              "en": "Units"
            }
          }
        },
        "BASE_YEAR": {
          "id": "BASE_YEAR",
          "name": "Base Year",
          "value": {
            "id": "_Z",
            "order": 0,
            "name": "Not applicable",
            "names": {
              "en": "Not applicable"
            }
          }
        }
      }
    }
  },
  "observations": {
    "0:0:0:0:0:0:0:0": {
      "value": 162.06,
      "dimValuesIndexes": [
        "0",
        "0",
        "0",
        "0",
        "0",
        "0",
        "0",
        "0"
      ],
      "attrValuesIndexes": [
        0,
        0,
        0,
        0
      ],
      "attributes": {
        "OBS_STATUS": {
          "id": "OBS_STATUS",
          "name": "Observation status",
          "value": {
            "id": "A",
            "name": "Normal value"
          }
        }
      },
      "units": {
        "UNIT_MEASURE": {
          "id": "UNIT_MEASURE",
          "name": "Unit of measure",
          "value": {
            "id": "AUD",
            "order": 1,
            "name": "Australien Dollar",
            "names": {
              "en": "Australien Dollar"
            },
            "__indexPosition": 1
          }
        },
        "UNIT_MULT": {
          "id": "UNIT_MULT",
          "name": "Unit multiplier",
          "value": {
            "id": "0",
            "order": 0,
            "name": "Units",
            "names": {
              "en": "Units"
            }
          }
        },
        "BASE_YEAR": {
          "id": "BASE_YEAR",
          "name": "Base Year",
          "value": {
            "id": "_Z",
            "order": 0,
            "name": "Not applicable",
            "names": {
              "en": "Not applicable"
            }
          }
        }
      }
    },
    "0:0:0:0:0:0:0:1": {
      "value": 166.5,
      "dimValuesIndexes": [
        "0",
        "0",
        "0",
        "0",
        "0",
        "0",
        "0",
        "1"
      ],
      "attrValuesIndexes": [
        0,
        0,
        0,
        0
      ],
      "attributes": {
        "OBS_STATUS": {
          "id": "OBS_STATUS",
          "name": "Observation status",
          "value": {
            "id": "A",
            "name": "Normal value"
          }
        }
      },
      "units": {
        "UNIT_MEASURE": {
          "id": "UNIT_MEASURE",
          "name": "Unit of measure",
          "value": {
            "id": "AUD",
            "order": 1,
            "name": "Australien Dollar",
            "names": {
              "en": "Australien Dollar"
            },
            "__indexPosition": 1
          }
        },
        "UNIT_MULT": {
          "id": "UNIT_MULT",
          "name": "Unit multiplier",
          "value": {
            "id": "0",
            "order": 0,
            "name": "Units",
            "names": {
              "en": "Units"
            }
          }
        },
        "BASE_YEAR": {
          "id": "BASE_YEAR",
          "name": "Base Year",
          "value": {
            "id": "_Z",
            "order": 0,
            "name": "Not applicable",
            "names": {
              "en": "Not applicable"
            }
          }
        }
      }
    },
    "0:0:0:0:0:0:0:2": {
      "value": 170.94,
      "dimValuesIndexes": [
        "0",
        "0",
        "0",
        "0",
        "0",
        "0",
        "0",
        "2"
      ],
      "attrValuesIndexes": [
        0,
        0,
        0,
        0
      ],
      "attributes": {
        "OBS_STATUS": {
          "id": "OBS_STATUS",
          "name": "Observation status",
          "value": {
            "id": "A",
            "name": "Normal value"
          }
        }
      },
      "units": {
        "UNIT_MEASURE": {
          "id": "UNIT_MEASURE",
          "name": "Unit of measure",
          "value": {
            "id": "AUD",
            "order": 1,
            "name": "Australien Dollar",
            "names": {
              "en": "Australien Dollar"
            },
            "__indexPosition": 1
          }
        },
        "UNIT_MULT": {
          "id": "UNIT_MULT",
          "name": "Unit multiplier",
          "value": {
            "id": "0",
            "order": 0,
            "name": "Units",
            "names": {
              "en": "Units"
            }
          }
        },
        "BASE_YEAR": {
          "id": "BASE_YEAR",
          "name": "Base Year",
          "value": {
            "id": "_Z",
            "order": 0,
            "name": "Not applicable",
            "names": {
              "en": "Not applicable"
            }
          }
        }
      }
    },
    "1:0:0:1:0:0:0:0": {
      "value": 164.28,
      "dimValuesIndexes": [
        "1",
        "0",
        "0",
        "1",
        "0",
        "0",
        "0",
        "0"
      ],
      "attrValuesIndexes": [
        0,
        0,
        0,
        1
      ],
      "attributes": {
        "OBS_STATUS": {
          "id": "OBS_STATUS",
          "name": "Observation status",
          "value": {
            "id": "A",
            "name": "Normal value"
          }
        }
      },
      "units": {
        "UNIT_MEASURE": {
          "id": "UNIT_MEASURE",
          "name": "Unit of measure",
          "value": {
            "id": "CAD",
            "order": 3,
            "name": "Canadian Dollar",
            "names": {
              "en": "Canadian Dollar"
            },
            "__indexPosition": 3
          }
        },
        "UNIT_MULT": {
          "id": "UNIT_MULT",
          "name": "Unit multiplier",
          "value": {
            "id": "0",
            "order": 0,
            "name": "Units",
            "names": {
              "en": "Units"
            }
          }
        },
        "BASE_YEAR": {
          "id": "BASE_YEAR",
          "name": "Base Year",
          "value": {
            "id": "_Z",
            "order": 0,
            "name": "Not applicable",
            "names": {
              "en": "Not applicable"
            }
          }
        }
      }
    },
    "1:0:0:1:0:0:0:1": {
      "value": 168.72,
      "dimValuesIndexes": [
        "1",
        "0",
        "0",
        "1",
        "0",
        "0",
        "0",
        "1"
      ],
      "attrValuesIndexes": [
        0,
        0,
        0,
        1
      ],
      "attributes": {
        "OBS_STATUS": {
          "id": "OBS_STATUS",
          "name": "Observation status",
          "value": {
            "id": "A",
            "name": "Normal value"
          }
        }
      },
      "units": {
        "UNIT_MEASURE": {
          "id": "UNIT_MEASURE",
          "name": "Unit of measure",
          "value": {
            "id": "CAD",
            "order": 3,
            "name": "Canadian Dollar",
            "names": {
              "en": "Canadian Dollar"
            },
            "__indexPosition": 3
          }
        },
        "UNIT_MULT": {
          "id": "UNIT_MULT",
          "name": "Unit multiplier",
          "value": {
            "id": "0",
            "order": 0,
            "name": "Units",
            "names": {
              "en": "Units"
            }
          }
        },
        "BASE_YEAR": {
          "id": "BASE_YEAR",
          "name": "Base Year",
          "value": {
            "id": "_Z",
            "order": 0,
            "name": "Not applicable",
            "names": {
              "en": "Not applicable"
            }
          }
        }
      }
    },
    "1:0:0:1:0:0:0:2": {
      "value": 173.16,
      "dimValuesIndexes": [
        "1",
        "0",
        "0",
        "1",
        "0",
        "0",
        "0",
        "2"
      ],
      "attrValuesIndexes": [
        0,
        0,
        0,
        1
      ],
      "attributes": {
        "OBS_STATUS": {
          "id": "OBS_STATUS",
          "name": "Observation status",
          "value": {
            "id": "A",
            "name": "Normal value"
          }
        }
      },
      "units": {
        "UNIT_MEASURE": {
          "id": "UNIT_MEASURE",
          "name": "Unit of measure",
          "value": {
            "id": "CAD",
            "order": 3,
            "name": "Canadian Dollar",
            "names": {
              "en": "Canadian Dollar"
            },
            "__indexPosition": 3
          }
        },
        "UNIT_MULT": {
          "id": "UNIT_MULT",
          "name": "Unit multiplier",
          "value": {
            "id": "0",
            "order": 0,
            "name": "Units",
            "names": {
              "en": "Units"
            }
          }
        },
        "BASE_YEAR": {
          "id": "BASE_YEAR",
          "name": "Base Year",
          "value": {
            "id": "_Z",
            "order": 0,
            "name": "Not applicable",
            "names": {
              "en": "Not applicable"
            }
          }
        }
      }
    },
    "2:0:0:2:0:0:0:0": {
      "value": 160.95,
      "dimValuesIndexes": [
        "2",
        "0",
        "0",
        "2",
        "0",
        "0",
        "0",
        "0"
      ],
      "attrValuesIndexes": [
        0,
        0,
        0,
        2
      ],
      "attributes": {
        "OBS_STATUS": {
          "id": "OBS_STATUS",
          "name": "Observation status",
          "value": {
            "id": "A",
            "name": "Normal value"
          }
        }
      },
      "units": {
        "UNIT_MEASURE": {
          "id": "UNIT_MEASURE",
          "name": "Unit of measure",
          "value": {
            "id": "USDPPP",
            "order": 4,
            "name": "USD equivalent, PPP",
            "names": {
              "en": "USD equivalent, PPP"
            },
            "__indexPosition": 4
          }
        },
        "UNIT_MULT": {
          "id": "UNIT_MULT",
          "name": "Unit multiplier",
          "value": {
            "id": "0",
            "order": 0,
            "name": "Units",
            "names": {
              "en": "Units"
            }
          }
        },
        "BASE_YEAR": {
          "id": "BASE_YEAR",
          "name": "Base Year",
          "value": {
            "id": "_Z",
            "order": 0,
            "name": "Not applicable",
            "names": {
              "en": "Not applicable"
            }
          }
        }
      }
    },
    "2:0:0:2:0:0:0:1": {
      "value": 165.39,
      "dimValuesIndexes": [
        "2",
        "0",
        "0",
        "2",
        "0",
        "0",
        "0",
        "1"
      ],
      "attrValuesIndexes": [
        0,
        0,
        0,
        2
      ],
      "attributes": {
        "OBS_STATUS": {
          "id": "OBS_STATUS",
          "name": "Observation status",
          "value": {
            "id": "A",
            "name": "Normal value"
          }
        }
      },
      "units": {
        "UNIT_MEASURE": {
          "id": "UNIT_MEASURE",
          "name": "Unit of measure",
          "value": {
            "id": "USDPPP",
            "order": 4,
            "name": "USD equivalent, PPP",
            "names": {
              "en": "USD equivalent, PPP"
            },
            "__indexPosition": 4
          }
        },
        "UNIT_MULT": {
          "id": "UNIT_MULT",
          "name": "Unit multiplier",
          "value": {
            "id": "0",
            "order": 0,
            "name": "Units",
            "names": {
              "en": "Units"
            }
          }
        },
        "BASE_YEAR": {
          "id": "BASE_YEAR",
          "name": "Base Year",
          "value": {
            "id": "_Z",
            "order": 0,
            "name": "Not applicable",
            "names": {
              "en": "Not applicable"
            }
          }
        }
      }
    },
    "2:0:0:2:0:0:0:2": {
      "value": 169.83,
      "dimValuesIndexes": [
        "2",
        "0",
        "0",
        "2",
        "0",
        "0",
        "0",
        "2"
      ],
      "attrValuesIndexes": [
        0,
        0,
        0,
        2
      ],
      "attributes": {
        "OBS_STATUS": {
          "id": "OBS_STATUS",
          "name": "Observation status",
          "value": {
            "id": "A",
            "name": "Normal value"
          }
        }
      },
      "units": {
        "UNIT_MEASURE": {
          "id": "UNIT_MEASURE",
          "name": "Unit of measure",
          "value": {
            "id": "USDPPP",
            "order": 4,
            "name": "USD equivalent, PPP",
            "names": {
              "en": "USD equivalent, PPP"
            },
            "__indexPosition": 4
          }
        },
        "UNIT_MULT": {
          "id": "UNIT_MULT",
          "name": "Unit multiplier",
          "value": {
            "id": "0",
            "order": 0,
            "name": "Units",
            "names": {
              "en": "Units"
            }
          }
        },
        "BASE_YEAR": {
          "id": "BASE_YEAR",
          "name": "Base Year",
          "value": {
            "id": "_Z",
            "order": 0,
            "name": "Not applicable",
            "names": {
              "en": "Not applicable"
            }
          }
        }
      }
    },
    "3:0:0:3:0:0:0:0": {
      "value": 163.17,
      "dimValuesIndexes": [
        "3",
        "0",
        "0",
        "3",
        "0",
        "0",
        "0",
        "0"
      ],
      "attrValuesIndexes": [
        0,
        0,
        0,
        3
      ],
      "attributes": {
        "OBS_STATUS": {
          "id": "OBS_STATUS",
          "name": "Observation status",
          "value": {
            "id": "A",
            "name": "Normal value"
          }
        }
      },
      "units": {
        "UNIT_MEASURE": {
          "id": "UNIT_MEASURE",
          "name": "Unit of measure",
          "value": {
            "id": "EUR",
            "order": 2,
            "name": "Euro",
            "names": {
              "en": "Euro"
            },
            "__indexPosition": 2
          }
        },
        "UNIT_MULT": {
          "id": "UNIT_MULT",
          "name": "Unit multiplier",
          "value": {
            "id": "0",
            "order": 0,
            "name": "Units",
            "names": {
              "en": "Units"
            }
          }
        },
        "BASE_YEAR": {
          "id": "BASE_YEAR",
          "name": "Base Year",
          "value": {
            "id": "_Z",
            "order": 0,
            "name": "Not applicable",
            "names": {
              "en": "Not applicable"
            }
          }
        }
      }
    },
    "3:0:0:3:0:0:0:1": {
      "value": 167.61,
      "dimValuesIndexes": [
        "3",
        "0",
        "0",
        "3",
        "0",
        "0",
        "0",
        "1"
      ],
      "attrValuesIndexes": [
        0,
        0,
        0,
        3
      ],
      "attributes": {
        "OBS_STATUS": {
          "id": "OBS_STATUS",
          "name": "Observation status",
          "value": {
            "id": "A",
            "name": "Normal value"
          }
        }
      },
      "units": {
        "UNIT_MEASURE": {
          "id": "UNIT_MEASURE",
          "name": "Unit of measure",
          "value": {
            "id": "EUR",
            "order": 2,
            "name": "Euro",
            "names": {
              "en": "Euro"
            },
            "__indexPosition": 2
          }
        },
        "UNIT_MULT": {
          "id": "UNIT_MULT",
          "name": "Unit multiplier",
          "value": {
            "id": "0",
            "order": 0,
            "name": "Units",
            "names": {
              "en": "Units"
            }
          }
        },
        "BASE_YEAR": {
          "id": "BASE_YEAR",
          "name": "Base Year",
          "value": {
            "id": "_Z",
            "order": 0,
            "name": "Not applicable",
            "names": {
              "en": "Not applicable"
            }
          }
        }
      }
    },
    "3:0:0:3:0:0:0:2": {
      "value": 172.05,
      "dimValuesIndexes": [
        "3",
        "0",
        "0",
        "3",
        "0",
        "0",
        "0",
        "2"
      ],
      "attrValuesIndexes": [
        0,
        0,
        0,
        3
      ],
      "attributes": {
        "OBS_STATUS": {
          "id": "OBS_STATUS",
          "name": "Observation status",
          "value": {
            "id": "A",
            "name": "Normal value"
          }
        }
      },
      "units": {
        "UNIT_MEASURE": {
          "id": "UNIT_MEASURE",
          "name": "Unit of measure",
          "value": {
            "id": "EUR",
            "order": 2,
            "name": "Euro",
            "names": {
              "en": "Euro"
            },
            "__indexPosition": 2
          }
        },
        "UNIT_MULT": {
          "id": "UNIT_MULT",
          "name": "Unit multiplier",
          "value": {
            "id": "0",
            "order": 0,
            "name": "Units",
            "names": {
              "en": "Units"
            }
          }
        },
        "BASE_YEAR": {
          "id": "BASE_YEAR",
          "name": "Base Year",
          "value": {
            "id": "_Z",
            "order": 0,
            "name": "Not applicable",
            "names": {
              "en": "Not applicable"
            }
          }
        }
      }
    }
  },
  "dataflowAttributes": {},
  "dataflowName": "National Accounts (UoM)",
  "seriesAttributes": [],
  "seriesAttributesValues": {}
}

const layout = {
  "sections": [],
  "rows": [
    "REF_AREA",
    "UNIT_MEASURE"
  ],
  "header": [
    "TIME_PERIOD"
  ]
};

describe('getTableData tests', () => {
  it('blan test', () => {
    expect(getTableProps(
      data,
      { header: [], rows: [], sections: [] },
      'label',
      {
        "flags": [],
        "footnotes": [],
      },
      0,
      false
    )).to.deep.equal({
      cells: {},
      headerData: [],
      sectionsData: [],
      layout: { header: [], rows: [], sections: [] }
    });
  });
});
