import { expect } from 'chai';
import { getNoDisplayAnnotationsIndexes } from '../src/rules/src/table/preparators/getNoDisplayAnnotationsIndexes';

describe('getNoDisplayAnnotationsIndexes test', () => {
  it('basic test', () => {
    const annotations = [
      {},
      { random: 'test' },
      { type: 'wrong' },
      { type: 'NOT_DISPLAYED', text: 'correct case' },
      { type: 'NOT_DISPLAYED', message: 'other one' },
      { other: 'random' },
      { type: 'NOT_DISPLAYED', information: 'one last' }
    ];

    expect(getNoDisplayAnnotationsIndexes(annotations)).to.deep.equal([3, 4, 6]);
  });
});
