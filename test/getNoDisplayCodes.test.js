import { expect } from 'chai';
import { getNoDisplayCodes } from '../src/rules/src/table/preparators/getNoDisplayCodes';

const annotations = [
  {},
  { title: '' },
  { title: 'Toto' },
  { title: 'Toto=Africa' },
  { title: 'JM_Jarre=Oxygen+Equinoxe' },
  { title: 'Toto,JM_Jarre' },
  { title: 'Toto,JM_Jarre=Oxygen+Equinoxe' },
  { title: 'Toto=Africa,JM_Jarre=Oxygen+Equinoxe' }
];

const noDisplayIndexes = [0, 1, 2, 3, 4, 5, 6, 7];

describe('getNoDisplayCodes tests', () => {
  it('no indexes match', () => {
    expect(getNoDisplayCodes({
      noDisplayIndexes,
      annotations,
      datasetIndexes: [8, 9]
    })).to.deep.equal({});
  });
  it('no title', () => {
    expect(getNoDisplayCodes({
      noDisplayIndexes,
      annotations,
      datasetIndexes: [0, 1]
    })).to.deep.equal({});
  });
  it('empty title', () => {
    expect(getNoDisplayCodes({
      noDisplayIndexes,
      annotations,
      datasetIndexes: [1, 2]
    })).to.deep.equal({});
  });
  it('dimension', () => {
    expect(getNoDisplayCodes({
      noDisplayIndexes,
      annotations,
      datasetIndexes: [2]
    })).to.deep.equal({ Toto: { id: 'Toto' } });
  });
  it('dimension value', () => {
    expect(getNoDisplayCodes({
      noDisplayIndexes,
      annotations,
      datasetIndexes: [3, 4, 5]
    })).to.deep.equal({ Toto: { id: 'Toto', values: { Africa: 'Africa' } } });
  });
  it('dimension values', () => {
    expect(getNoDisplayCodes({
      noDisplayIndexes,
      annotations,
      datasetIndexes: [4, 5]
    })).to.deep.equal({ JM_Jarre: { id: 'JM_Jarre', values: { Oxygen: 'Oxygen', Equinoxe: 'Equinoxe' } } });
  });
  it('dimensions', () => {
    expect(getNoDisplayCodes({
      noDisplayIndexes,
      annotations,
      datasetIndexes: [5, 9, 10]
    })).to.deep.equal({ Toto: { id: 'Toto' }, JM_Jarre: { id: 'JM_Jarre' } });
  });
  it('dimension plus dimension values', () => {
    expect(getNoDisplayCodes({
      noDisplayIndexes,
      annotations,
      datasetIndexes: [18, 6, 2]
    })).to.deep.equal({
      Toto: { id: 'Toto' },
      JM_Jarre: { id: 'JM_Jarre', values: { Oxygen: 'Oxygen', Equinoxe: 'Equinoxe' } }
    });
  });
  it('dimension value plus dimension values', () => {
    expect(getNoDisplayCodes({
      noDisplayIndexes,
      annotations,
      datasetIndexes: [7, 8, 9]
    })).to.deep.equal({
      Toto: { id: 'Toto', values: { Africa: 'Africa' } },
      JM_Jarre: { id: 'JM_Jarre', values: { Oxygen: 'Oxygen', Equinoxe: 'Equinoxe' } }
    });
  });
});
