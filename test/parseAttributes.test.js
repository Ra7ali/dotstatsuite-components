import { expect } from 'chai';
import { parseAttributes } from '../src/rules/src/table/preparators/parseAttributes';
import { getDisplay } from '../src/rules/src/table/preparators/getDisplay';

const noDisplayIndexes = [0, 5, 10];
const notDisplayedCodes = { a17: { id: 'a17' }, a18: { values: { a18v2: 'a18v2' } } };


const attributes = [
  { id: 'a0', annotations: [0, 4, 9], relationship: { primaryMeasure: 'OBS_VALUE' }, values: [{ id: 'a0v1' }] }, // no display
  { id: 'a1', relationship: { primaryMeasure: 'OBS_VALUE' } }, // no values
  { id: 'a2', relationship: { primaryMeasure: 'OBS_VALUE' }, values: [] }, // empty values
  { id: 'a3', values: [{ id: 'a3v1' }] }, // no relationship definition
  { id: 'a4', relationship: {}, values: [{ id: 'a4v1' }] }, // empty relationship
  { id: 'a5', relationship: { primaryMeasure: 'WRONG' }, values: [{ id: 'a5v1' }] }, // incorrect primaryMeasure relationship
  { id: 'a6', relationship: { primaryMeasure: 'OBS_VALUE' }, values: [{ id: 'a6v1' }] }, // correct observations attribute
  { id: 'a7', relationship: { dimensions: [] }, values: [{ id: 'a7v1' }] }, // empty dimensions relationship
  { id: 'a8', relationship: { dimensions: ['d1'] }, values: [{ id: 'a8v1' }] }, // single 'one value' dimension relationship
  { id: 'a9', relationship: { dimensions: ['d4'] }, values: [{ id: 'a9v1' }] }, // single 'many values' dimension relationship
  { id: 'a10', relationship: { dimensions: ['d1', 'd2'] }, values: [{ id: 'a10v1' }] }, // many 'single value' dimensions realtionship
  { id: 'a11', relationship: { dimensions: ['d1', 'd2', 'd4'] }, values: [{ id: 'a11v1' }] }, // one 'many values' and many 'single value' dimensions relationship
  { id: 'a12', relationship: { dimensions: ['d5', 'd6'] }, values: [{ id: 'a12v1' }] }, // many 'many values' dimensions relationship
  { id: 'a13', relationship: { dimensions: ['d4', 'd1', 'd6'] }, values: [{ id: 'a13v1' }] }, // many 'many values' and one 'single value' dimensions relationship
  { id: 'a14', relationship: { dimensions: ['d1'] }, values: [{ id: 'a14v1' }] }, // not recognised as footnote
  { id: 'a15', relationship: { dimensions: ['d3'] }, values: [{ id: 'a15v1' }] }, // recognised as prefscale
  { id: 'a16', relationship: { dimensions: ['d6'] }, values: [{ id: 'a16v1' }] }, // recognised as decimals 
  { id: 'a17', relationship: { primaryMeasure: 'OBS_VALUE' }, values: [{ id: 'a17v1' }] }, // blacklisted
  { id: 'a18', relationship: { primaryMeasure: 'OBS_VALUE' }, values: [{ id: 'a18v1' }, { id: 'a18v2' }] }, // blacklisted value
  { id: 'a19', relationship: { none: {} }, values: [{ id: 'a19v1' }] }, // dataset relationship
  { id: 'a20', relationship: { primaryMeasure: 'OBS_VALUE' }, values: [{ id: 'a20v1' }] } // units attribute
];

const customAttributes = {
  prefscale: 'a15',
  decimals: 'a16',
  units: ['d7', 'a20']
};

const parsedDimensionsIds = {
  oneValue: { 'd1': 1, 'd2': 2, 'd3': 3 },
  manyValues: { 'd4': 4, 'd5': 5, 'd6': 6 },
};

describe('parseAttributes test', () => {
  it('complete case', () => {
    expect(parseAttributes({
      attributes, parsedDimensionsIds, customAttributes, getDisplay: getDisplay({ noDisplayIndexes, notDisplayedCodes })
    })).to.deep.equal({
      attributesIndexedByIds: {
        a6: { id: 'a6', index: 6, relationship: { primaryMeasure: 'OBS_VALUE' }, values: [{ id: 'a6v1' }] },
        a8: { id: 'a8', index: 8, relationship: { dimensions: ['d1'] }, values: [{ id: 'a8v1' }] },
        a9: { id: 'a9', index: 9, relationship: { dimensions: ['d4'] }, values: [{ id: 'a9v1' }], targetIndex: 4 },
        a10: { id: 'a10', index: 10, relationship: { dimensions: ['d1', 'd2'] }, values: [{ id: 'a10v1' }] },
        a11: { id: 'a11', index: 11, relationship: { dimensions: ['d1', 'd2', 'd4'] }, values: [{ id: 'a11v1' }], targetIndex: 4 },
        a12: { id: 'a12', index: 12, relationship: { dimensions: ['d5', 'd6'] }, values: [{ id: 'a12v1' }], targetIndexes: { '5': 5, '6': 6 } },
        a13: { id: 'a13', index: 13, relationship: { dimensions: ['d4', 'd1', 'd6'] }, values: [{ id: 'a13v1' }], targetIndexes: { '4': 4, '6': 6 } },
        a14: { id: 'a14', index: 14, relationship: { dimensions: ['d1'] }, values: [{ id: 'a14v1' }] },
        a15: { id: 'a15', index: 15, relationship: { dimensions: ['d3'] }, values: [{ id: 'a15v1' }] },
        a16: { id: 'a16', index: 16, relationship: { dimensions: ['d6'] }, values: [{ id: 'a16v1' }] },
        a18: { id: 'a18', index: 18, relationship: { primaryMeasure: 'OBS_VALUE' }, values: [{ id: 'a18v1' }, null] },
        a19: { id: 'a19', index: 19, relationship: { none: {} }, values: [{ id: 'a19v1' }] }
      },
      attributesIdsIndexedByTargets: {
        observations: ['a6', 'a18'],
        oneValueDimensions: { d1: ['a8', 'a14'] },
        manyValuesDimensions: ['a9', 'a11'],
        dataflow: ['a10', 'a19'],
        series: ['a12', 'a13'],
        prefscale: ['a15'],
        decimals: ['a16'],
      }
    });
  });
});
