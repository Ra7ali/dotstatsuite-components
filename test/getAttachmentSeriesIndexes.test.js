import { expect } from 'chai';
import { getAttachmentSeriesIndexes } from '../src/rules/src/table/units/getAttachmentSeriesIndexes';

const dimensions = {
  D0: { id: 'D0', index: 0 },
  D1: { id: 'D1', index: 1 },
  D2: { id: 'D2', index: 2 },
  D3: { id: 'D3', index: 3 },
  D4: { id: 'D4', index: 4 },
  D5: { id: 'D5', index: 5 }, 
};

const unitsArtefacts = {
  dimensions: { series: { D0: { id: 'D0' }, D1: { id: 'D1', header: true }, D6: { id: 'D6' } } },
  attributes: { series: {
    A1: { id: 'A1', relationship: { dimensions: ['D3', 'D2'] } },
    A2: { id: 'A2' }, A3: { id: 'A3', relationship: { primaryMeasure: 'OBS_VALUE' } }
  } }
};

describe('getAttachmentSeriesIndexes test', () => {
  it('blank test', () => {
    expect(getAttachmentSeriesIndexes({
      dimensions: {},
      unitsArtefacts: {
        dimensions:  { series: {} },
        attributes: { series: {} }
      }
    })).to.deep.equal([]);
  });
  it('basic test', () => {
    expect(getAttachmentSeriesIndexes({ dimensions, unitsArtefacts }))
      .to.deep.equal([0, 3, 2]);
  });
});
