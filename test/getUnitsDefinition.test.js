import { expect } from 'chai';
import { getUnitsDefinition } from '../src/rules/src/table/units/getUnitsDefinition';

const data = {
  data: {
    dataSets: [{
      annotations: [0, 3]
    }],
    structure: {
      annotations: [
        { type: 'A0' },
        { trash: true },
        { type: 'A1', title: 'C0,C1,C2' },
        { type: 'A2', title: 'C3,C4' },
      ]
    }
  }
};

describe('getUnitsDefinition tests', () => {
  it('no annotation id provided', () => {
    expect(getUnitsDefinition({ data, defaultCodes: ['C5', 'C6'], annotationsDefinitionCodes: {} }))
      .to.deep.equal(['C5', 'C6']);
  });
  it('no codes in annotation', () => {
    expect(getUnitsDefinition({ data, defaultCodes: ['C5', 'C6'], annotationsDefinitionCodes: { concepts: ['A0'] } }))
      .to.deep.equal(['C5', 'C6']);
  });
  it('not a dataset annotation', () => {
    expect(getUnitsDefinition({ data, defaultCodes: ['C5', 'C6'], annotationsDefinitionCodes: { concepts: ['A1'] } }))
      .to.deep.equal(['C5', 'C6']);
  });
  it('basic case', () => {
    expect(getUnitsDefinition({ data, defaultCodes: ['C5', 'C6'], annotationsDefinitionCodes: { concepts: ['A2'] } }))
      .to.deep.equal(['C3', 'C4']);
  });
});