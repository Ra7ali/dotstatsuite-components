import { expect } from 'chai';
import { getSortedLayoutData } from '../src/rules/src/table/factories/getSortedLayoutData';

const d1 = { id: 'D1', name: 'D1', __index: 0 };
const d1v1 = { id: 'D1V1', name: 'D1V1', __index: 0 };
const d1v2 = { id: 'D1V2', name: 'D1V2', __index: 1 };
const d1v3 = { id: 'D1V3', name: 'D1V3', __index: 2 };
const d1v1Layout = { dimension: d1, value: d1v1 };
const d1v2Layout = { dimension: d1, value: d1v2 };
const d1v3Layout = { dimension: d1, value: d1v3 };
const dimension1 = { ...d1, values: [d1v1, d1v2, d1v3] };
const reordedDimension1 = { ...d1, values: [d1v2, d1v3, d1v1] };

const d2 = { id: 'D2', name: 'D2', __index: 1 };
const d2v1 = { id: 'D2V1', name: 'D2V1', __index: 0 };
const d2v2 = { id: 'D2V2', name: 'D2V2', __index: 1 };
const d2v3 = { id: 'D2V3', name: 'D2V3', __index: 2 };
const d2v1Layout = { dimension: d2, value: d2v1 };
const d2v2Layout = { dimension: d2, value: d2v2 };
const d2v3Layout = { dimension: d2, value: d2v3 };
const dimension2 = { ...d2, values: [d2v1, d2v2, d2v3] };
const reordedDimension2 = { ...d2, values: [d2v3, d2v1, d2v2] };

const d3 = { id: 'D3', name: 'D3', __index: 2 };
const d3v1 = { id: 'D3V1', name: 'D3V1', __index: 0 };
const d3v2 = { id: 'D3V2', name: 'D3V2', __index: 1 };
const d3v3 = { id: 'D3V3', name: 'D3V3', __index: 2 };
const d3v1Layout = { dimension: d3, value: d3v1 };
const d3v2Layout = { dimension: d3, value: d3v2 };
const d3v3Layout = { dimension: d3, value: d3v3 };
const dimension3 = { ...d3, values: [d3v1, d3v2, d3v3] };
const reordedDimension3 = { ...d3, values: [d3v3, d3v2, d3v1] };

const simpleLayout = {
  header: [dimension1],
  rows: [dimension2],
  sections: [dimension3]
};

const emptyHeadersLayout = {
  header: [],
  rows: [dimension2, dimension1],
  sections: [dimension3]
};

const emptySectionsLayout = {
  header: [dimension1],
  rows: [dimension3, dimension2],
  sections: [],
};

const reordedSimpleLayout = {
  header: [reordedDimension1],
  rows: [reordedDimension2],
  sections: [reordedDimension3]
};

const onlyRowsLayout = {
  header: [],
  rows: [reordedDimension2, reordedDimension3, reordedDimension1],
  sections: []
};

const onlyHeaderLayout = {
  header: [reordedDimension2, reordedDimension3, reordedDimension1],
  rows: [],
  sections: []
};

const observations = {
  '0:0:0': [],
  '0:0:1': [],
  '0:0:2': [],
  '0:1:0': [],
  '0:1:1': [],
  '0:1:2': [],
  '0:2:0': [],
  '0:2:1': [],
  '0:2:2': [],
  '1:0:0': [],
  '1:0:1': [],
  '1:0:2': [],
  '1:1:0': [],
  '1:1:1': [],
  '1:1:2': [],
  '1:2:0': [],
  '1:2:1': [],
  '1:2:2': [],
  '2:0:0': [],
  '2:0:1': [],
  '2:0:2': [],
  '2:1:0': [],
  '2:1:1': [],
  '2:1:2': [],
  '2:2:0': [],
  '2:2:1': [],
  '2:2:2': [],
};

const partialObservations = {
  '0:0:0': [],
  '0:0:2': [],
  '0:1:0': [],
  '0:1:2': [],
  '0:2:2': [],
  '2:0:0': [],
  '2:0:2': [],
  '2:1:0': [],
  '2:1:2': [],
  '2:2:2': [],
};

describe('getSortedLayoutData tests', () => {
  it('simple basic case', () => {
    const headerData = [[d1v1Layout], [d1v2Layout], [d1v3Layout]];

    const sectionsData = [
      [
        [d3v1Layout],
        [[d2v1Layout], [d2v2Layout], [d2v3Layout]]
      ],
      [
        [d3v2Layout],
        [[d2v1Layout], [d2v2Layout], [d2v3Layout]]
      ],
      [
        [d3v3Layout],
        [[d2v1Layout], [d2v2Layout], [d2v3Layout]]
      ]
    ];

    expect(getSortedLayoutData(simpleLayout, observations)).to.deep.equal({ headerData, sectionsData });
  });

  it('no sections case', () => {
    const headerData = [[d1v1Layout], [d1v2Layout], [d1v3Layout]];

    const sectionsData = [
      [
        [],
        [
          [d3v1Layout, d2v1Layout],
          [d3v1Layout, d2v2Layout],
          [d3v1Layout, d2v3Layout],
          [d3v2Layout, d2v1Layout],
          [d3v2Layout, d2v2Layout],
          [d3v2Layout, d2v3Layout],
          [d3v3Layout, d2v1Layout],
          [d3v3Layout, d2v2Layout],
          [d3v3Layout, d2v3Layout]
        ]
      ]
    ];

    expect(getSortedLayoutData(emptySectionsLayout, observations)).to.deep.equal({ headerData, sectionsData });
  });

  it('no headers case', () => {
    const headerData = [[]];

    const sectionsData = [
      [
        [d3v1Layout],
        [
          [d2v1Layout, d1v1Layout],
          [d2v1Layout, d1v2Layout],
          [d2v1Layout, d1v3Layout],
          [d2v2Layout, d1v1Layout],
          [d2v2Layout, d1v2Layout],
          [d2v2Layout, d1v3Layout],
          [d2v3Layout, d1v1Layout],
          [d2v3Layout, d1v2Layout],
          [d2v3Layout, d1v3Layout]
        ]
      ],
      [
        [d3v2Layout],
        [
          [d2v1Layout, d1v1Layout],
          [d2v1Layout, d1v2Layout],
          [d2v1Layout, d1v3Layout],
          [d2v2Layout, d1v1Layout],
          [d2v2Layout, d1v2Layout],
          [d2v2Layout, d1v3Layout],
          [d2v3Layout, d1v1Layout],
          [d2v3Layout, d1v2Layout],
          [d2v3Layout, d1v3Layout]
        ]
      ],
      [
        [d3v3Layout],
        [
          [d2v1Layout, d1v1Layout],
          [d2v1Layout, d1v2Layout],
          [d2v1Layout, d1v3Layout],
          [d2v2Layout, d1v1Layout],
          [d2v2Layout, d1v2Layout],
          [d2v2Layout, d1v3Layout],
          [d2v3Layout, d1v1Layout],
          [d2v3Layout, d1v2Layout],
          [d2v3Layout, d1v3Layout]
        ]
      ],
    ];

    expect(getSortedLayoutData(emptyHeadersLayout, observations)).to.deep.equal({ headerData, sectionsData });
  });

  it('reorded dimensions case', () => {
    const headerData = [[d1v2Layout], [d1v3Layout], [d1v1Layout]];

    const sectionsData = [
      [
        [d3v3Layout],
        [[d2v3Layout], [d2v1Layout], [d2v2Layout]]
      ],
      [
        [d3v2Layout],
        [[d2v3Layout], [d2v1Layout], [d2v2Layout]]
      ],
      [
        [d3v1Layout],
        [[d2v3Layout], [d2v1Layout], [d2v2Layout]]
      ]
    ];

    expect(getSortedLayoutData(reordedSimpleLayout, observations)).to.deep.equal({ headerData, sectionsData });
  });

  it('partial case', () => {
    const headerData = [[d1v1Layout], [d1v3Layout]];

    const sectionsData = [
      [
        [d3v1Layout],
        [[d2v1Layout], [d2v2Layout]]
      ],
      [
        [d3v3Layout],
        [[d2v1Layout], [d2v2Layout], [d2v3Layout]]
      ]
    ];

    expect(getSortedLayoutData(simpleLayout, partialObservations)).to.deep.equal({ headerData, sectionsData });
  });

  it('all reorded dimensions in rows', () => {
    const headerData = [[]];

    const sectionsData = [[
      [],
      [
        [d2v3Layout, d3v3Layout, d1v2Layout],
        [d2v3Layout, d3v3Layout, d1v3Layout],
        [d2v3Layout, d3v3Layout, d1v1Layout],
        [d2v3Layout, d3v2Layout, d1v2Layout],
        [d2v3Layout, d3v2Layout, d1v3Layout],
        [d2v3Layout, d3v2Layout, d1v1Layout],
        [d2v3Layout, d3v1Layout, d1v2Layout],
        [d2v3Layout, d3v1Layout, d1v3Layout],
        [d2v3Layout, d3v1Layout, d1v1Layout],        
        [d2v1Layout, d3v3Layout, d1v2Layout],
        [d2v1Layout, d3v3Layout, d1v3Layout],
        [d2v1Layout, d3v3Layout, d1v1Layout],
        [d2v1Layout, d3v2Layout, d1v2Layout],
        [d2v1Layout, d3v2Layout, d1v3Layout],
        [d2v1Layout, d3v2Layout, d1v1Layout],
        [d2v1Layout, d3v1Layout, d1v2Layout],
        [d2v1Layout, d3v1Layout, d1v3Layout],
        [d2v1Layout, d3v1Layout, d1v1Layout],
        [d2v2Layout, d3v3Layout, d1v2Layout],
        [d2v2Layout, d3v3Layout, d1v3Layout],
        [d2v2Layout, d3v3Layout, d1v1Layout],
        [d2v2Layout, d3v2Layout, d1v2Layout],
        [d2v2Layout, d3v2Layout, d1v3Layout],
        [d2v2Layout, d3v2Layout, d1v1Layout],
        [d2v2Layout, d3v1Layout, d1v2Layout],
        [d2v2Layout, d3v1Layout, d1v3Layout],
        [d2v2Layout, d3v1Layout, d1v1Layout]
      ]
    ]];

    expect(getSortedLayoutData(onlyRowsLayout, observations)).to.deep.equal({ headerData, sectionsData });
  });

  it('all reorded dimensions in header', () => {
    const headerData = [
      [d2v3Layout, d3v3Layout, d1v2Layout],
      [d2v3Layout, d3v3Layout, d1v3Layout],
      [d2v3Layout, d3v3Layout, d1v1Layout],
      [d2v3Layout, d3v2Layout, d1v2Layout],
      [d2v3Layout, d3v2Layout, d1v3Layout],
      [d2v3Layout, d3v2Layout, d1v1Layout],
      [d2v3Layout, d3v1Layout, d1v2Layout],
      [d2v3Layout, d3v1Layout, d1v3Layout],
      [d2v3Layout, d3v1Layout, d1v1Layout],        
      [d2v1Layout, d3v3Layout, d1v2Layout],
      [d2v1Layout, d3v3Layout, d1v3Layout],
      [d2v1Layout, d3v3Layout, d1v1Layout],
      [d2v1Layout, d3v2Layout, d1v2Layout],
      [d2v1Layout, d3v2Layout, d1v3Layout],
      [d2v1Layout, d3v2Layout, d1v1Layout],
      [d2v1Layout, d3v1Layout, d1v2Layout],
      [d2v1Layout, d3v1Layout, d1v3Layout],
      [d2v1Layout, d3v1Layout, d1v1Layout],
      [d2v2Layout, d3v3Layout, d1v2Layout],
      [d2v2Layout, d3v3Layout, d1v3Layout],
      [d2v2Layout, d3v3Layout, d1v1Layout],
      [d2v2Layout, d3v2Layout, d1v2Layout],
      [d2v2Layout, d3v2Layout, d1v3Layout],
      [d2v2Layout, d3v2Layout, d1v1Layout],
      [d2v2Layout, d3v1Layout, d1v2Layout],
      [d2v2Layout, d3v1Layout, d1v3Layout],
      [d2v2Layout, d3v1Layout, d1v1Layout]
    ];

    const sectionsData = [
      [
        [],
        [[]]
      ]
    ];

    expect(getSortedLayoutData(onlyHeaderLayout, observations)).to.deep.equal({ headerData, sectionsData });
  });

  it('reorded dimensions with partial observations', () => {
    const headerData = [[d1v3Layout], [d1v1Layout]];

    const sectionsData = [
      [
        [d3v3Layout],
        [[d2v3Layout], [d2v1Layout], [d2v2Layout]]
      ],
      [
        [d3v1Layout],
        [[d2v1Layout], [d2v2Layout]]
      ]
    ];

    expect(getSortedLayoutData(reordedSimpleLayout, partialObservations)).to.deep.equal({ headerData, sectionsData });
  });

  it('all reorded dimensions in header with partial observations', () => {
    const headerData = [
      [d2v3Layout, d3v3Layout, d1v3Layout],
      [d2v3Layout, d3v3Layout, d1v1Layout],
      [d2v1Layout, d3v3Layout, d1v3Layout],
      [d2v1Layout, d3v3Layout, d1v1Layout],
      [d2v1Layout, d3v1Layout, d1v3Layout],
      [d2v1Layout, d3v1Layout, d1v1Layout],
      [d2v2Layout, d3v3Layout, d1v3Layout],
      [d2v2Layout, d3v3Layout, d1v1Layout],
      [d2v2Layout, d3v1Layout, d1v3Layout],
      [d2v2Layout, d3v1Layout, d1v1Layout]
    ];

    const sectionsData = [
      [
        [],
        [[]]
      ]
    ];

    expect(getSortedLayoutData(onlyHeaderLayout, partialObservations)).to.deep.equal({ headerData, sectionsData });
  });
});
