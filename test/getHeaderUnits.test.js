import { expect } from 'chai';
import { getHeaderUnits } from '../src/rules/src/table/units/getHeaderUnits';

describe('getHeaderUnits tests', () => {
  it('blank test', () => {
    expect(getHeaderUnits({
      unitsArtefacts: {
        attributes: { header: {} },
        dimensions: { header: {} }
      },
      unitsDefinitionCodes: ['D1', 'C8', 'A2']
    })).to.deep.equal([]);
  });
  it('basic test', () => {
    expect(getHeaderUnits({
      unitsArtefacts: {
        attributes: { header: {
          A1: { id: 'A1', values: [{ id: 'V1' }] },
          A2: { id: 'A2', values: [{ id: 'V1' }] },
          A3: { id: 'A3', values: [{ id: 'RA' }] },
        } },
        dimensions: { header: {
          D1: { id: 'D1', values: [{ id: 'V1' }] },
          D2: { id: 'D2', values: [{ id: 'RD' }] }
        } }
      },
      rejectedValues: ['RA', 'RD'],
      unitsDefinitionCodes: ['D1', 'C8', 'A2']
    })).to.deep.equal([
      { id: 'D1', value: { id: 'V1' } },
      { id: 'A2', value: { id: 'V1' } }
    ]);
  });
});
