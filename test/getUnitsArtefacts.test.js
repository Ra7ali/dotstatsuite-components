import { expect } from 'chai';
import { getUnitsArtefacts } from '../src/rules/src/table/units/getUnitsArtefacts';
import { getDisplay } from '../src/rules/src/table/preparators/getDisplay';

const noDisplayIndexes = [0, 1];
const _getDisplay = getDisplay({ noDisplayIndexes });

const data = {
  structure: {
    attributes: {
      dataSet: [
        { id: 'A0', values: [] },
        { id: 'A1', values: [{ id: 'V1' }] },
        { id: 'A2', values: [{ id: 'V1' }, { id: 'V2' }] },
      ],
      observation: [
        { id: 'A3', values: [] },
        { id: 'A4', relationship: { dimensions: ['D2'] }, values: [{ id: 'V1' }] },
        { id: 'A5', relationship: { dimensions: ['D2'] }, values: [{ id: 'V1' }, { id: 'V2' }] },
        { id: 'A6', relationship: { dimensions: ['D1'] }, values: [{ id: 'V1' }] },
        { id: 'A7', relationship: { dimensions: ['D1', 'D2'] }, values: [{ id: 'V1' }] },
        { id: 'A8', relationship: { primaryMeasure: 'OBS_VALUE' }, values: [{ id: 'V1' }, { id: 'V2' }], annotations: [2, 3, 4] },
        { id: 'A9', relationship: { primaryMeasure: 'OBS_VALUE' }, values: [{ id: 'V1' }, { id: 'V2' }], annotations: [0, 5] },
      ]
    },
    dimensions: {
      observation: [
        { id: 'D0', values: [] },
        { id: 'D1', values: [{ id: 'V1' }] },
        { id: 'D2', values: [{ id: 'V1' }, { id: 'V2' }] },
      ]
    },
  }
};

describe('getUnitsArtefacts tests', () => {
  it('no data', () => {
    expect(getUnitsArtefacts({ data: { data: undefined } })).to.deep.equal({
      attributes: { header: {}, series: {} },
      dimensions: { header: {}, series: {} },
    });
  });
  it('no units definition codes', () => {
    expect(getUnitsArtefacts({ data: { data } })).to.deep.equal({
      attributes: { header: {}, series: {} },
      dimensions: { header: {}, series: {} },
    });
  });
  it('full header', () => {
    expect(getUnitsArtefacts({ data: { data }, unitsDefinitionCodes: ['A1', 'D1'], getDisplay: _getDisplay })).to.deep.equal({
      attributes: { header: { A1: { id: 'A1', datasetLevel: true, values: [{ id: 'V1' }] } }, series: {} },
      dimensions: { header: { D1: { id: 'D1', header: true, index: 1, values: [{ id: 'V1' }] } }, series: {} },
    });
  });
  it('full series', () => {
    expect(getUnitsArtefacts({ data: { data }, unitsDefinitionCodes: ['A4', 'A5', 'D2'], getDisplay: _getDisplay })).to.deep.equal({
      attributes: {
        series: {
          A4: { id: 'A4', index: 1, relationship: { dimensions: ['D2'] }, values: [{ id: 'V1' }] },
          A5: { id: 'A5', index: 2, relationship: { dimensions: ['D2'] }, values: [{ id: 'V1' }, { id: 'V2' }] }
        },
        header: {}
      },
      dimensions: { series: { D2: { id: 'D2', index: 2, values: [{ id: 'V1' }, { id: 'V2' }] } }, header: {} },
    });
  });
  it('header dimensions with series', () => {
    expect(getUnitsArtefacts({ data: { data }, unitsDefinitionCodes: ['A4', 'A5', 'D1', 'D2'], getDisplay: _getDisplay })).to.deep.equal({
      attributes: {
        series: {
          A4: { id: 'A4', index: 1, relationship: { dimensions: ['D2'] }, values: [{ id: 'V1' }] },
          A5: { id: 'A5', index: 2, relationship: { dimensions: ['D2'] }, values: [{ id: 'V1' }, { id: 'V2' }] }
        },
        header: {}
      },
      dimensions: {
        series: {
          D1: { id: 'D1', index: 1, header: true, values: [{ id: 'V1' }] },
          D2: { id: 'D2', index: 2, values: [{ id: 'V1' }, { id: 'V2' }] }
        },
        header: {},
      }
    });
  });
  it('header attributes with series', () => {
    expect(getUnitsArtefacts({ data: { data }, unitsDefinitionCodes: ['A1', 'A4', 'A5', 'D2'], getDisplay: _getDisplay })).to.deep.equal({
      attributes: {
        series: {
          A1: { id: 'A1', datasetLevel: true, values: [{ id: 'V1' }] },
          A4: { id: 'A4', index: 1, relationship: { dimensions: ['D2'] }, values: [{ id: 'V1' }] },
          A5: { id: 'A5', index: 2, relationship: { dimensions: ['D2'] }, values: [{ id: 'V1' }, { id: 'V2' }] }
        },
        header: {}
      },
      dimensions: { series: { D2: { id: 'D2', index: 2, values: [{ id: 'V1' }, { id: 'V2' }] } }, header: {} },
    });
  });
  it('all attached dimensions single value makes a header', () => {
    expect(getUnitsArtefacts({ data: { data }, unitsDefinitionCodes: ['A6'], getDisplay: _getDisplay })).to.deep.equal({
      attributes: { header: { A6: { id: 'A6', index: 3, relationship: { dimensions: [] }, values: [{ id: 'V1' }] }, }, series: {} },
      dimensions: { header: {}, series: {} },
    });
  });
  it('series attributes keeps only multi values dimensions in attachment', () => {
    expect(getUnitsArtefacts({ data: { data }, unitsDefinitionCodes: ['A7'], getDisplay: _getDisplay })).to.deep.equal({
      attributes: { series: { A7: { id: 'A7', index: 4, relationship: { dimensions: ['D2'] }, values: [{ id: 'V1' }] }, }, header: {} },
      dimensions: { header: {}, series: {} },
    });
  });
  it('no display annotations', () => {
    expect(getUnitsArtefacts({ data: { data }, unitsDefinitionCodes: ['A8', 'A9'], getDisplay: _getDisplay })).to.deep.equal({
      attributes: { header: {}, series: { A8: { id: 'A8', observations: true, index: 5, relationship: { primaryMeasure: 'OBS_VALUE' }, values: [{ id: 'V1' }, { id: 'V2' }], annotations: [2, 3, 4] } } },
      dimensions: { header: {}, series: {} },
    });
  });
});
