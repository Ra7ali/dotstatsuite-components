import { expect } from 'chai';
import { getSubtitleFlags } from '../src/rules/src/header';

describe('getSubtitleFlags tests', () => {
  it('blank test', () => {
    expect(getSubtitleFlags({ customAttributes: {}, dimensions: {}, display: null, units: {} })).to.deep.equal([]);
  });
  it('no flags test', () => {
    expect(getSubtitleFlags({
      customAttributes: {
        flags: ['f1', 'f2']
      },
      dimensions: {
        one: {
          a: { id: 'a', __index: 0, attributes: {}, values: [{ id: 'a1' }] },
          b: { id: 'b', __index: 0, attributes: {}, values: [{ id: 'b1' }] },
        }
      },
      display: 'code',
      units: { values: [] }
    })).to.deep.equal([]);
  });
  it('simple test', () => {
    expect(getSubtitleFlags({
      customAttributes: {},
      dimensions: {
        one: {
          a: {
            id: 'a', __index: 0,
            values: [{ id: 'a1', attributes: { f1: { id: 'f1', value: { id: 'v' } }, f2: { id: 'f2', value: { id: 'v' } }} }]
          },
          b: { id: 'b', __index: 0, values: [{ id: 'b1', attributes: { f1: { id: 'f1', value: { id: 'v' } } } }] },
        }
      },
      display: 'code',
      units: { values: [] }
    })).to.deep.equal([
      { header: 'a:', label: 'a1', sub: [{ label: 'f1: v' }, { label: 'f2: v' }] },
      { header: 'b:', label: 'b1', sub: [{ label: 'f1: v' }] }
    ]);
  })
});
