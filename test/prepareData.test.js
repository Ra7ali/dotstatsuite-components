import { expect } from 'chai';
import { prepareData } from '../src/rules/src/table/preparators/prepareData';

describe('prepareData tests', () => {
  it('blank test', () => {
    expect(prepareData(
      { data: undefined },
      {},
      { defaultCodes: [], annotationsDefinitionCodes: {} }
    )).to.deep.equal({
      dimensions: { one: {}, many: {}, length: 0 },
      units: {
        headerUnits: [],
        rejectedValueIds: [],
        unitsDefinitionCodes: [],
        unitsAttachmentSeriesIds: [],
        unitsSeries: {},
      },
      observations: {},
      dataflowAttributes: {},
      dataflowName: undefined,
      seriesAttributes: [],
      seriesAttributesValues: {}
    });
  });
});
