import { expect } from 'chai';
import getTimelineFrequency from '../src/rules/src/factories/timeline-frequency';

describe('getTimelineFrequency tests', () => {
  it('annual test', () => {
    const dimensions = [
      { id: 'random'},
      { id: 'time', role: 'TIME', values: [{ id: '2015' }, { id: '2016' }] }
    ]
    expect(getTimelineFrequency({ dimensions, attributes: [] })).to.equal('annual');
  });
  it('quarterly test', () => {
    const dimensions = [
      { id: 'random'},
      { id: 'time', role: 'TIME', values: [{ id: '2015-Q1' }, { id: '2016-Q4' }] }
    ]
    expect(getTimelineFrequency({ dimensions, attributes: [] })).to.equal('quarterly');
  });
  it('monthly test', () => {
    const dimensions = [
      { id: 'random'},
      { id: 'time', role: 'TIME', values: [{ id: '2015-01' }, { id: '2016-12' }] }
    ]
    expect(getTimelineFrequency({ dimensions, attributes: [] })).to.equal('monthly');
  });
});
