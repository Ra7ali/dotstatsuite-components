import { expect } from 'chai';
import { refineLayoutHierarchy } from '../src/rules/src/table/factories/refineLayoutHierarchy';

const values = [
  { id: '1', name: '1', parents: [] },
  { id: '1.1', name: '1.1', parents: ['1'] },
  { id: '1.1.1', name: '1.1.1', parents: ['1', '1.1'] },
  { id: '1.1.1.1', name: '1.1.1.1', parents: ['1', '1.1', '1.1.1'] },
  { id: '2', name: '2', parents: [] },
  { id: '3', name: '3', parents: [] }
];

describe('refineLayoutHierarchy tests', () => {
  it('one missing parent', () => {
    const sections = [[[], [
      [{}, {}, { value: values[0] }],
      [{}, {}, { value: values[2] }],
      [{}, {}, { value: values[3] }],
      [{}, {}, { value: values[4] }],
      [{}, {}, { value: values[5] }],
    ]]];
    expect(refineLayoutHierarchy(sections, { values })).to.deep.equal([[[], [
      [{}, {}, { value: values[0] }],
      [{}, {}, { value: { id: '1.1.1', name: '1.1 > 1.1.1', parents: ['1'] } }],
      [{}, {}, { value: { id: '1.1.1.1', name: '1.1.1.1', parents: ['1', '1.1.1'] } }],
      [{}, {}, { value: values[4] }],
      [{}, {}, { value: values[5] }],
    ]]]);
  });
  it('many missing parents consecutives', () => {
    const sections = [[[], [
      [{}, {}, { value: values[0] }],
      [{}, {}, { value: values[3] }],
      [{}, {}, { value: values[4] }],
      [{}, {}, { value: values[5] }],
    ]]];
    expect(refineLayoutHierarchy(sections, { values })).to.deep.equal([[[], [
      [{}, {}, { value: values[0] }],
      [{}, {}, { value: { id: '1.1.1.1', name: '1.1 > 1.1.1 > 1.1.1.1', parents: ['1'] } }],
      [{}, {}, { value: values[4] }],
      [{}, {}, { value: values[5] }],
    ]]]);
  });
  it('holes in the tree', () => {
    const sections = [[[], [
      [{}, {}, { value: values[1] }],
      [{}, {}, { value: values[3] }],
      [{}, {}, { value: values[4] }],
      [{}, {}, { value: values[5] }],
    ]]];
    expect(refineLayoutHierarchy(sections, { values })).to.deep.equal([[[], [
      [{}, {}, { value: { id: '1.1', name: '1 > 1.1', parents: [] } }],
      [{}, {}, { value: { id: '1.1.1.1', name: '1.1.1 > 1.1.1.1', parents: ['1.1'] } }],
      [{}, {}, { value: values[4] }],
      [{}, {}, { value: values[5] }],
    ]]]);
  });
  it('no missing parents', () => {
    const sections = [[[], [
      [{}, {}, { value: values[0] }],
      [{}, {}, { value: values[1] }],
      [{}, {}, { value: values[2] }],
      [{}, {}, { value: values[3] }],
      [{}, {}, { value: values[4] }],
      [{}, {}, { value: values[5] }],
    ]]];
    expect(refineLayoutHierarchy(sections, { values })).to.deep.equal(sections);
  });
  it('no hierarchy', () => {
    const values = [{ id: '1' }, { id: '2' }, { id: '3' }, { id: '4' }];
    const sections = [[[], [
      [{}, {}, {}, {}, { value: values[0] }],
      [{}, {}, {}, {}, { value: values[1] }],
      [{}, {}, {}, {}, { value: values[2] }],
      [{}, {}, {}, {}, { value: values[3] }],
    ]]];
    expect(refineLayoutHierarchy(sections, { values })).to.deep.equal(sections);
  });
});
