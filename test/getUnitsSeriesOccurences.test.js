import { expect } from 'chai';
import { getUnitsSeriesOccurences } from '../src/rules/src/table/units/getUnitsSeriesOccurences';
import { intersection } from 'ramda';


const attachmentSeriesIndexes = [0, 1];
const observations = {
  0: {
    dimValuesIndexes: ['0', '0', '0', '0'],
    attrValuesIndexes: ['0', null, '0'],
    key: '0:0:0:0',
    units: {
      A: { value: { index: 0 } },
      B: { value: { index: 0 } },
      C: { value: { index: 0 } }
    }
  },
  1: {
    dimValuesIndexes: ['0', '0', '1', '0'],
    attrValuesIndexes: ['0', null, '1'],
    key: '0:0:1:0',
    units: {
      A: { value: { index: 0 } },
      B: { value: { index: 0 } },
      C: { value: { index: 1 } }
    }
  },
  2: {
    dimValuesIndexes: ['1', '0', '0', '0'],
    attrValuesIndexes: ['1', null, '0'],
    key: '1:0:0:0',
    units: {
      A: { value: { index: 1 } },
      B: { value: { index: 1 } },
      C: { value: { index: 0 } }
    }
  },
  3: {
    dimValuesIndexes: ['1', '0', '1', '0'],
    attrValuesIndexes: ['1', null, '0'],
    key: '1:0:1:0',
    units: {
      A: { value: { index: 1 } },
      B: { value: { index: 1 } },
      C: { value: { index: 0 } }
    }
  },
  4: {
    dimValuesIndexes: ['2', '0', '0', '0'],
    attrValuesIndexes: ['0', null, '0'],
    key: '2:0:0:0',
    units: {
      A: { value: { index: 2 } },
      B: { value: { index: 0 } },
      C: { value: { index: 0 } }
    }
  }
};

describe('getUnitsSeriesOccurences tests', () => {
  it('blank test', () => {
    expect(getUnitsSeriesOccurences({
      attachmentSeriesIndexes: []
    })({})).to.deep.equal({ rawUnitsSeries: {}, obs: {} });
  });
  it('basic test', () => {
    expect(getUnitsSeriesOccurences({ attachmentSeriesIndexes })(observations))
      .to.deep.equal({
        rawUnitsSeries: {
          '0:0:x:x': { A: [0, 0], B: [0, 0], C: [0, 1], observations: 2 },
          '1:0:x:x': { A: [1, 1], B: [1, 1], C: [0, 0], observations: 2 },
          '2:0:x:x': { A: [2], B: [0], C: [0], observations: 1 }
        },
        obs: {
          '0:0:0:0': {
            dimValuesIndexes: ['0', '0', '0', '0'],
            attrValuesIndexes: ['0', null, '0'],
            key: '0:0:0:0',
            units: {
              A: { value: { index: 0 } },
              B: { value: { index: 0 } },
              C: { value: { index: 0 } },
              serieKey: '0:0:x:x'
            }
          },
          '0:0:1:0': {
            dimValuesIndexes: ['0', '0', '1', '0'],
            attrValuesIndexes: ['0', null, '1'],
            key: '0:0:1:0',
            units: {
              A: { value: { index: 0 } },
              B: { value: { index: 0 } },
              C: { value: { index: 1 } },
              serieKey: '0:0:x:x'
            }
          },
          '1:0:0:0': {
            dimValuesIndexes: ['1', '0', '0', '0'],
            attrValuesIndexes: ['1', null, '0'],
            key: '1:0:0:0',
            units: {
              A: { value: { index: 1 } },
              B: { value: { index: 1 } },
              C: { value: { index: 0 } },
              serieKey: '1:0:x:x'
            }
          },
          '1:0:1:0': {
            dimValuesIndexes: ['1', '0', '1', '0'],
            attrValuesIndexes: ['1', null, '0'],
            key: '1:0:1:0',
            units: {
              A: { value: { index: 1 } },
              B: { value: { index: 1 } },
              C: { value: { index: 0 } },
              serieKey: '1:0:x:x'
            }
          },
          '2:0:0:0': {
            dimValuesIndexes: ['2', '0', '0', '0'],
            attrValuesIndexes: ['0', null, '0'],
            key: '2:0:0:0',
            units: {
              A: { value: { index: 2 } },
              B: { value: { index: 0 } },
              C: { value: { index: 0 } },
              serieKey: '2:0:x:x'
            }
          }
        }
      })
  });
});
