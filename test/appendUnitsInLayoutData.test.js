import { expect } from 'chai';
import { appendUnitsInLayoutData } from '../src/rules/src/table/units/appendUnitsInLayoutData';

const headerData = [
  [
    { dimension: { id: 'H1', __index: 0 }, value: { id: 'H1V1', __index: 0 } },
    { dimension: { id: 'H2', __index: 2 }, value: { id: 'H2V1', __index: 0 } },
  ],[
    { dimension: { id: 'H1', __index: 0 }, value: { id: 'H1V2', __index: 1 } },
    { dimension: { id: 'H2', __index: 2 }, value: { id: 'H2V2', __index: 1 } }
  ],
];

const sectionsData = [
  [
    [
      { dimension: { id: 'S1', __index: 1 }, value: { id: 'S1V1', __index: 0 } },
      { dimension: { id: 'S2', __index: 3 }, value: { id: 'S2V1', __index: 0 } },
    ],
    [
      [
        { dimension: { id: 'R1', __index: 4 }, value: { id: 'R1V1', __index: 0 } },
        { dimension: { id: 'R2', __index: 5 }, value: { id: 'R1V1', __index: 0 } }
      ],
      [
        { dimension: { id: 'R1', __index: 4 }, value: { id: 'R1V1', __index: 1 } },
        { dimension: { id: 'R2', __index: 5 }, value: { id: 'R1V1', __index: 1 } }
      ]
    ]
  ],
  [
    [
      { dimension: { id: 'S1', __index: 1 }, value: { id: 'S1V1', __index: 1 } },
      { dimension: { id: 'S2', __index: 3 }, value: { id: 'S2V1', __index: 1 } },
    ],
    [
      [
        { dimension: { id: 'R1', __index: 4 }, value: { id: 'R1V1', __index: 0 } },
        { dimension: { id: 'R2', __index: 5 }, value: { id: 'R1V1', __index: 0 } }
      ],
      [
        { dimension: { id: 'R1', __index: 4 }, value: { id: 'R1V1', __index: 1 } },
        { dimension: { id: 'R2', __index: 5 }, value: { id: 'R1V1', __index: 1 } }
      ]
    ]
  ],
];

const unitsSeries = {
  "0:x:0:x:x:x": { U1: { id: 'U1', value: { id: 'U1V1' } } },
  "1:x:1:x:x:x": { U1: { id: 'U1', value: { id: 'U1V2' } } },
  "x:0:x:0:x:x": { U2: { id: 'U2', value: { id: 'U2V1' } } },
  "x:1:x:1:x:x": { U2: { id: 'U2', value: { id: 'U2V2' } } },
  "x:x:x:x:0:0": { U3: { id: 'U3', value: { id: 'U3V1' } } },
  "x:x:x:x:1:1": { U3: { id: 'U3', value: { id: 'U3V2' } } },
  "x:0:x:x:0:0": { U4: { id: 'U4', value: { id: 'U4V1' } } },
  "x:1:x:x:1:1": { U4: { id: 'U4', value: { id: 'U4V2' } } }
};

describe('appendUnitsInLayoutData tests', () => {
  it('blank test', () => {
    expect(appendUnitsInLayoutData({
      display: 'label',
      unitDimension: {},
      unitsDisplay: 'header',
      unitsSeries: {},
      unitsDefinitionCodes: [],
      unitsIndexes: {}, // { header: [], sections: [], rows: [] }
      partialUnitSerieIndexes: [],
      layoutIds: {}
    })({})).to.deep.equal({});
  });
  it('header test', () => {
    expect(appendUnitsInLayoutData({
      display: 'code',
      unitDimension: { id: 'U' },
      unitsDisplay: 'header',
      unitsSeries,
      unitsDefinitionCodes: ['U1'],
      unitsIndexes: { header: [0, 1] }, // { header: [], sections: [], rows: [] }
      partialUnitSerieIndexes: ['x', 'x', 'x', 'x', 'x', 'x'],
      layoutIds: {}
    })({ headerData })).to.deep.equal({
      headerData: [
        [
          { dimension: { id: 'H1', __index: 0 }, value: { id: 'H1V1', __index: 0 } },
          { dimension: { id: 'H2', __index: 2 }, value: { id: 'H2V1', __index: 0 } },
          { dimension: { id: 'U' }, value: { id: 'VAL', unitsLabel: 'U1V1' } },
        ],[
          { dimension: { id: 'H1', __index: 0 }, value: { id: 'H1V2', __index: 1 } },
          { dimension: { id: 'H2', __index: 2 }, value: { id: 'H2V2', __index: 1 } },
          { dimension: { id: 'U' }, value: { id: 'VAL', unitsLabel: 'U1V2' } },
        ],
      ]
    });
  });
  it('sections test', () => {
    expect(appendUnitsInLayoutData({
      display: 'code',
      unitDimension: { id: 'U' },
      unitsDisplay: 'sections',
      unitsSeries,
      unitsDefinitionCodes: ['U2'],
      unitsIndexes: { sections: [0, 1] }, // { header: [], sections: [], rows: [] }
      partialUnitSerieIndexes: ['x', 'x', 'x', 'x', 'x', 'x'],
      layoutIds: {},
    })({ sectionsData })).to.deep.equal({
      sectionsData: [
        [
          [
            { dimension: { id: 'S1', __index: 1 }, value: { id: 'S1V1', __index: 0 } },
            { dimension: { id: 'S2', __index: 3 }, value: { id: 'S2V1', __index: 0 } },
            { dimension: { id: 'U' }, value: { id: 'VAL', unitsLabel: 'U2V1' } },
          ],
          [
            [
              { dimension: { id: 'R1', __index: 4 }, value: { id: 'R1V1', __index: 0 } },
              { dimension: { id: 'R2', __index: 5 }, value: { id: 'R1V1', __index: 0 } }
            ],
            [
              { dimension: { id: 'R1', __index: 4 }, value: { id: 'R1V1', __index: 1 } },
              { dimension: { id: 'R2', __index: 5 }, value: { id: 'R1V1', __index: 1 } }
            ]
          ]
        ],
        [
          [
            { dimension: { id: 'S1', __index: 1 }, value: { id: 'S1V1', __index: 1 } },
            { dimension: { id: 'S2', __index: 3 }, value: { id: 'S2V1', __index: 1 } },
            { dimension: { id: 'U' }, value: { id: 'VAL', unitsLabel: 'U2V2' } },
          ],
          [
            [
              { dimension: { id: 'R1', __index: 4 }, value: { id: 'R1V1', __index: 0 } },
              { dimension: { id: 'R2', __index: 5 }, value: { id: 'R1V1', __index: 0 } }
            ],
            [
              { dimension: { id: 'R1', __index: 4 }, value: { id: 'R1V1', __index: 1 } },
              { dimension: { id: 'R2', __index: 5 }, value: { id: 'R1V1', __index: 1 } }
            ]
          ]
        ],
      ]
    });
  });
  it('rows test', () => {
    expect(appendUnitsInLayoutData({
      display: 'code',
      unitDimension: { id: 'U' },
      unitsDisplay: 'rows',
      unitsSeries,
      unitsDefinitionCodes: ['U3'],
      unitsIndexes: { rows: [0, 1], sections: [] }, // { header: [], sections: [], rows: [] }
      partialUnitSerieIndexes: ['x', 'x', 'x', 'x', 'x', 'x'],
      layoutIds: { sections: ['S1', 'S2'] }
    })({ sectionsData })).to.deep.equal({
      sectionsData: [
        [
          [
            { dimension: { id: 'S1', __index: 1 }, value: { id: 'S1V1', __index: 0 } },
            { dimension: { id: 'S2', __index: 3 }, value: { id: 'S2V1', __index: 0 } }
          ],
          [
            [
              { dimension: { id: 'R1', __index: 4 }, value: { id: 'R1V1', __index: 0 } },
              { dimension: { id: 'R2', __index: 5 }, value: { id: 'R1V1', __index: 0 } },
              { dimension: { id: 'U' }, value: { id: 'VAL', unitsLabel: 'U3V1' } },
            ],
            [
              { dimension: { id: 'R1', __index: 4 }, value: { id: 'R1V1', __index: 1 } },
              { dimension: { id: 'R2', __index: 5 }, value: { id: 'R1V1', __index: 1 } },
              { dimension: { id: 'U' }, value: { id: 'VAL', unitsLabel: 'U3V2' } },
            ]
          ]
        ],
        [
          [
            { dimension: { id: 'S1', __index: 1 }, value: { id: 'S1V1', __index: 1 } },
            { dimension: { id: 'S2', __index: 3 }, value: { id: 'S2V1', __index: 1 } },
          ],
          [
            [
              { dimension: { id: 'R1', __index: 4 }, value: { id: 'R1V1', __index: 0 } },
              { dimension: { id: 'R2', __index: 5 }, value: { id: 'R1V1', __index: 0 } },
              { dimension: { id: 'U' }, value: { id: 'VAL', unitsLabel: 'U3V1' } },
            ],
            [
              { dimension: { id: 'R1', __index: 4 }, value: { id: 'R1V1', __index: 1 } },
              { dimension: { id: 'R2', __index: 5 }, value: { id: 'R1V1', __index: 1 } },
              { dimension: { id: 'U' }, value: { id: 'VAL', unitsLabel: 'U3V2' } },
            ]
          ]
        ],
      ]
    });
  });
  it('rows and sections test', () => {
    expect(appendUnitsInLayoutData({
      display: 'code',
      unitDimension: { id: 'U' },
      unitsDisplay: 'rows',
      unitsSeries,
      unitsDefinitionCodes: ['U4'],
      unitsIndexes: { rows: [0, 1], sections: [0] }, // { header: [], sections: [], rows: [] }
      partialUnitSerieIndexes: ['x', 'x', 'x', 'x', 'x', 'x'],
      layoutIds: { sections: ['S1', 'S2'] }
    })({ sectionsData })).to.deep.equal({
      sectionsData: [
        [
          [
            { dimension: { id: 'S1', __index: 1 }, value: { id: 'S1V1', __index: 0 } },
            { dimension: { id: 'S2', __index: 3 }, value: { id: 'S2V1', __index: 0 } }
          ],
          [
            [
              { dimension: { id: 'R1', __index: 4 }, value: { id: 'R1V1', __index: 0 } },
              { dimension: { id: 'R2', __index: 5 }, value: { id: 'R1V1', __index: 0 } },
              { dimension: { id: 'U' }, value: { id: 'VAL', unitsLabel: 'U4V1' } },
            ],
            [
              { dimension: { id: 'R1', __index: 4 }, value: { id: 'R1V1', __index: 1 } },
              { dimension: { id: 'R2', __index: 5 }, value: { id: 'R1V1', __index: 1 } },
              { dimension: { id: 'U' }, value: { id: 'VAL', unitsLabel: '' } },
            ]
          ]
        ],
        [
          [
            { dimension: { id: 'S1', __index: 1 }, value: { id: 'S1V1', __index: 1 } },
            { dimension: { id: 'S2', __index: 3 }, value: { id: 'S2V1', __index: 1 } },
          ],
          [
            [
              { dimension: { id: 'R1', __index: 4 }, value: { id: 'R1V1', __index: 0 } },
              { dimension: { id: 'R2', __index: 5 }, value: { id: 'R1V1', __index: 0 } },
              { dimension: { id: 'U' }, value: { id: 'VAL', unitsLabel: '' } },
            ],
            [
              { dimension: { id: 'R1', __index: 4 }, value: { id: 'R1V1', __index: 1 } },
              { dimension: { id: 'R2', __index: 5 }, value: { id: 'R1V1', __index: 1 } },
              { dimension: { id: 'U' }, value: { id: 'VAL', unitsLabel: 'U4V2' } },
            ]
          ]
        ],
      ]
    });
  });
});
