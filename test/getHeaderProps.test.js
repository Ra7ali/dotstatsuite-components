import { expect } from 'chai';
import { getHeaderProps } from '../src/rules/src';

describe('getHeaderProps test', () => {
  it('subtitle display rejection', () => {
    const dimensions = [
      { id: '0', display: true, values: [{ id: '0.1', attributes: {}, display: true }] },
      { id: '1', display: false, values: [{ id: '1.1', attributes: {}, display: true }] },
      { id: '2', display: true, values: [{ id: '2.1', attributes: {}, display: false }] },
    ];

    expect(getHeaderProps({
      oneValueDimensions: dimensions,
      dataflowName: '',
      dataflowCode: 'DF',
      dataflowAttributes: {},
      display: 'code',
      customAttributes: {},
    })).to.deep.equal({ title: { label: 'DF', flags: [] }, subtitle: [{ label: ['0', '0.1'], flags: [] }], uprs: null })
  });
  it('correct flags format', () => {
    const dimensions = [
      {
        id: '0', name: 'd0', display: true, values: [
          { id: '0.1', name: 'v1', attributes: {}, display: true, attributes: { A1: { id: 'A2', name: 'a2', value: { id: 'V', name: 'v' } }, } }
        ] },
      { id: '1', name: 'd1', display: false, values: [{ id: '1.1', attributes: {}, display: true }] },
      { id: '2', name: 'd2', display: true, values: [{ id: '2.1', attributes: {}, display: false }] },
    ];

    expect(getHeaderProps({
      oneValueDimensions: dimensions,
      dataflowName: 'dataflow',
      dataflowCode: 'DF',
      dataflowAttributes: {
        A1: { id: 'A1', name: 'a1', value: { id: 'V', name: 'v' } },
      },
      display: 'label',
      customAttributes: { footnotes: [] },
    })).to.deep.equal({
      title: { label: 'dataflow', flags: [{ label: 'a1: v' }] },
      subtitle: [{ label: ['d0', 'v1'], flags: [{ label: 'a2: v' }] }],
      uprs: null });
  })
});
