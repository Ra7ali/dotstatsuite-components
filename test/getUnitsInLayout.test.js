import { expect } from 'chai';
import { getUnitsInLayout } from '../src/rules/src/table/units/getUnitsinLayout';

describe('getUnitsInLayout tests', () => {
  it('blank test', () => {
    expect(getUnitsInLayout({
      layoutIds: { header: [], sections: [], rows: [] },
      unitsAttachmentSeriesIds: [],
      unitsDefinitionCodes: [],
      unitsSeries: {}
    })).to.deep.equal({
      unitsLevelDisplay: 'cells',
      unitsAttachmentIndexesInLayout: { header: [], sections: [], rows: [] },
      unitsIndexesInLayout: { header: [], sections: [], rows: [] },
    });
  });
  it('header case', () => {
    expect(getUnitsInLayout({
      layoutIds: { header: ['H1', 'H2', 'H3'], sections: ['S1', 'S2', 'S3'], rows: ['R1', 'R2', 'R3'] },
      unitsAttachmentSeriesIds: ['H1', 'H3'],
      unitsDefinitionCodes: ['H1'],
      unitsSeries: { not: 'empty' }
    })).to.deep.equal({
      unitsLevelDisplay: 'header',
      unitsAttachmentIndexesInLayout: { header: [0, 2], sections: [], rows: [] },
      unitsIndexesInLayout: { header: [0], sections: [], rows: [] },
    });
  });
  it('sections case', () => {
    expect(getUnitsInLayout({
      layoutIds: { header: ['H1', 'H2', 'H3'], sections: ['S1', 'S2', 'S3'], rows: ['R1', 'R2', 'R3'] },
      unitsAttachmentSeriesIds: ['S2', 'S3'],
      unitsDefinitionCodes: ['S2', 'S3'],
      unitsSeries: { not: 'empty' }
    })).to.deep.equal({
      unitsLevelDisplay: 'sections',
      unitsAttachmentIndexesInLayout: { header: [], sections: [1, 2], rows: [] },
      unitsIndexesInLayout: { header: [], sections: [1, 2], rows: [] },
    });
  });
  it('only rows case', () => {
    expect(getUnitsInLayout({
      layoutIds: { header: ['H1', 'H2', 'H3'], sections: ['S1', 'S2', 'S3'], rows: ['R1', 'R2', 'R3'] },
      unitsAttachmentSeriesIds: ['R2'],
      unitsDefinitionCodes: [],
      unitsSeries: { not: 'empty' }
    })).to.deep.equal({
      unitsLevelDisplay: 'rows',
      unitsAttachmentIndexesInLayout: { header: [], sections: [], rows: [1] },
      unitsIndexesInLayout: { header: [], sections: [], rows: [] },
    });
  });
  it('rows and sections case', () => {
    expect(getUnitsInLayout({
      layoutIds: { header: ['H1', 'H2', 'H3'], sections: ['S1', 'S2', 'S3'], rows: ['R1', 'R2', 'R3'] },
      unitsAttachmentSeriesIds: ['R2', 'S1', 'S3'],
      unitsDefinitionCodes: ['R2', 'S1'],
      unitsSeries: { not: 'empty' }
    })).to.deep.equal({
      unitsLevelDisplay: 'rows',
      unitsAttachmentIndexesInLayout: { header: [], sections: [0, 2], rows: [1] },
      unitsIndexesInLayout: { header: [], sections: [0], rows: [1] },
    });
  });
  it('mixed case', () => {
    expect(getUnitsInLayout({
      layoutIds: { header: ['H1', 'H2', 'H3'], sections: ['S1', 'S2', 'S3'], rows: ['R1', 'R2', 'R3'] },
      unitsAttachmentSeriesIds: ['R2', 'S1', 'H3'],
      unitsDefinitionCodes: [],
      unitsSeries: { not: 'empty' }
    })).to.deep.equal({
      unitsLevelDisplay: 'cells',
      unitsAttachmentIndexesInLayout: { header: [], sections: [], rows: [] },
      unitsIndexesInLayout: { header: [], sections: [], rows: [] },
    });
  });
});
