import { expect } from 'chai';
import { refineDimSeriesUnits } from '../src/rules/src/table/units/refineDimSeriesUnits';

describe('refineDimSeriesUnits tests', () => {
  it('blank test', () => {
    expect(refineDimSeriesUnits({})({})).to.deep.equal({});
  });
  it('complete test', () => {
    expect(refineDimSeriesUnits({
      unitsArtefacts: {
        A: { id: 'A', name: 'a', values: [{ id: 'a1' }] },
        B: { id: 'B', name: 'b', values: [{ id: 'b1' }, { id: 'b2' }] },
        C: { id: 'C', name: 'c', values: [{ id: 'c1' }, { id: 'c2' }] },
      },
      rejectedValueIds: ['b1', 'c2']
    })({
      s1: { observations: 3, A: [0, 0], B: [0, 1, 0], C: [0, 0, 0] },
      s2: { observations: 2, A: [0, 0], B: [0, 0], C: [2, 2] }
    })).to.deep.equal({
      s1: { C: { id: 'C', name: 'c', value: { id: 'c1' } } },
      s2: { A: { id: 'A', name: 'a', value: { id: 'a1' } } }
    });
  });
});
