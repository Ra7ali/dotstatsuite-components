import { expect } from 'chai';
import { getConfirmedSeriesAttributesIds } from '../src/rules/src/table/factories/getConfirmedSeriesAttributesIds';

const layoutIds = {
  header: ['d1', 'd2', 'd3'],
  sections: ['d4', 'd5', 'd6'],
  rows: ['d7', 'd8', 'd9']
};

const attributes = [
  { id: 'a1' }, // no target dimensions
  { id: 'a2', relationship: { dimensions: ['toto', 'africa'] } }, // non valid dimensions
  { id: 'a3', relationship: { dimensions: [] } }, // empty dimensions
  { id: 'a4', relationship: { dimensions: ['d2', 'd3'] } }, // header dimensions
  { id: 'a5', relationship: { dimensions: ['d4', 'd6'] } }, // sections dimensions
  { id: 'a6', relationship: { dimensions: ['d7', 'd8', 'd9'] } }, // rows dimensions
  { id: 'a7', relationship: { dimensions: ['d1', 'd4'] } }, // header and sections
  { id: 'a8', relationship: { dimensions: ['d2', 'd9'] } }, // header and rows
  { id: 'a9', relationship: { dimensions: ['d5', 'd8'] } }, // sections and rows
  { id: 'a10', relationship: { dimensions: ['d3', 'd4', 'd8'] } }, // header sections and rows
];

describe('getConfirmedSeriesAttributesIds test', () => {
  it('complete case', () => {
    expect(getConfirmedSeriesAttributesIds(attributes, layoutIds)).to.deep.equal(['a4', 'a5', 'a6', 'a9']);
  });
});
