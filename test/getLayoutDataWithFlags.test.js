import { expect } from 'chai';
import { getLayoutDataWithFlags } from '../src/rules/src/table/factories/getLayoutWithFlags';

const customAttributes = { flags: ['a1', 'a2'] };

const layoutData = {
  headerData: [
    [{ dimension: { id: 'COL1', __index: 0 }, value: { id: 'COL1V1', __index: 0 } }, { dimension: { id: 'COL2', __index: 1 }, value: { id: 'COL2V1', __index: 0 } }],
    [{ dimension: { id: 'COL1', __index: 0 }, value: { id: 'COL1V2', __index: 1 } }, { dimension: { id: 'COL2', __index: 1 }, value: { id: 'COL2V1', __index: 0 } }]
  ],
  sectionsData: [
    [
      [{ dimension: { id: 'SEC', __index: 2 }, value: { id: 'SECV1', __index: 0 } }],
      [
        [{ dimension: { id: 'ROW1', __index: 3 }, value: { id: 'ROW1V1', __index: 0 } }, { dimension: { id: 'ROW2', __index: 4 }, value: { id: 'ROW2V1', __index: 0 } }],
        [{ dimension: { id: 'ROW1', __index: 3 }, value: { id: 'ROW1V1', __index: 0 } }, { dimension: { id: 'ROW2', __index: 4 }, value: { id: 'ROW2V2', __index: 1 } }],
        [{ dimension: { id: 'ROW1', __index: 3 }, value: { id: 'ROW1V1', __index: 0 } }, { dimension: { id: 'ROW2', __index: 4 }, value: { id: 'ROW2V3', __index: 2 } }],
      ]
    ],
    [
      [{ dimension: { id: 'SEC', __index: 2 }, value: { id: 'SECV2', __index: 1 } }],
      [
        [{ dimension: { id: 'ROW1', __index: 3 }, value: { id: 'ROW1V1', __index: 0 } }, { dimension: { id: 'ROW2', __index: 4 }, value: { id: 'ROW2V1', __index: 0 } }],
        [{ dimension: { id: 'ROW1', __index: 3 }, value: { id: 'ROW1V1', __index: 0 } }, { dimension: { id: 'ROW2', __index: 4 }, value: { id: 'ROW2V2', __index: 1 } }],
        [{ dimension: { id: 'ROW1', __index: 3 }, value: { id: 'ROW1V1', __index: 0 } }, { dimension: { id: 'ROW2', __index: 4 }, value: { id: 'ROW2V3', __index: 2 } }],
      ]
    ]
  ]
};

const attributesValues = {
  a: { attributes: { a1: { id: 'A1', value: { id: 'A1VAL' } }, a3: { id: 'A3', value: { id: 'A3VAL' } } }, serie: ['0', '0', 'x', 'x', 'x', 'x', 'x'] },
  b: { attributes: { a4: { id: 'A4', value: { id: 'A4VAL' } } }, serie: ['x', 'x', '1', 'x', '1', 'x', 'x'] }
};

const expected = {
  headerData: [
    { data: [{ dimension: { label: 'COL1', flags: [] }, value: { label: 'COL1V1', flags: [] } }, { dimension: { label: 'COL2', flags: [] }, value: { label: 'COL2V1', flags: [] } }], key: '0:0', flags: [{ code: 'A1VAL', label: 'A1: A1VAL' }, { label: 'A3: A3VAL' }] },
    { data: [{ dimension: { label: 'COL1', flags: [] }, value: { label: 'COL1V2', flags: [] } }, { dimension: { label: 'COL2', flags: [] }, value: { label: 'COL2V1', flags: [] } }], key: '1:0', flags: [] }
  ],
  sectionsData: [
    [
      { data: [{ dimension: { label: 'SEC', flags: [] }, value: { label: 'SECV1', flags: [] } }], key: '0', flags: [] },
      [
        { data: [{ dimension: { label: 'ROW1', flags: [] }, value: { label: 'ROW1V1', flags: [] } }, { dimension: { label: 'ROW2', flags: [] }, value: { label: 'ROW2V1', flags: [] } }], key: '0:0', flags: [] },
        { data: [{ dimension: { label: 'ROW1', flags: [] }, value: { label: 'ROW1V1', flags: [] } }, { dimension: { label: 'ROW2', flags: [] }, value: { label: 'ROW2V2', flags: [] } }], key: '0:1', flags: [] },
        { data: [{ dimension: { label: 'ROW1', flags: [] }, value: { label: 'ROW1V1', flags: [] } }, { dimension: { label: 'ROW2', flags: [] }, value: { label: 'ROW2V3', flags: [] } }], key: '0:2', flags: [] },
      ]
    ],
    [
      { data: [{ dimension: { label: 'SEC', flags: [] }, value: { label: 'SECV2', flags: [] } }], key: '1', flags: [] },
      [
        { data: [{ dimension: { label: 'ROW1', flags: [] }, value: { label: 'ROW1V1', flags: [] } }, { dimension: { label: 'ROW2', flags: [] }, value: { label: 'ROW2V1', flags: [] } }], key: '0:0', flags: [] },
        { data: [{ dimension: { label: 'ROW1', flags: [] }, value: { label: 'ROW1V1', flags: [] } }, { dimension: { label: 'ROW2', flags: [] }, value: { label: 'ROW2V2', flags: [] } }], key: '0:1', flags: [{ label: 'A4: A4VAL' }] },
        { data: [{ dimension: { label: 'ROW1', flags: [] }, value: { label: 'ROW1V1', flags: [] } }, { dimension: { label: 'ROW2', flags: [] }, value: { label: 'ROW2V3', flags: [] } }], key: '0:2', flags: [] },
      ]
    ]
  ]
};

describe('getLayoutDataWithFlags test', () => {
  it('empty', () => {
    expect(getLayoutDataWithFlags({}, 'label', {})({ headerData: [], sectionsData: [] })).to.deep.equal({ headerData: [], sectionsData: [] });
  });
  it('multi cases', () => {
    expect(getLayoutDataWithFlags(attributesValues, 'code', customAttributes)(layoutData)).to.deep.equal(expected);
  });
});