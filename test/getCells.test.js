import { expect } from 'chai';
import { getCells } from '../src/rules/src/table/factories/getCells';
import * as R from 'ramda';

const customAttributes = {
  flags: ['FL1', 'FL2'],
  footnotes: ['FT1', 'FT2', 'FT3']
};

const data = {
  observations: {
    a: { value: 2, units: {} },
    b: { value: 0, units: {}, attributes: { T: { id: 'T', name: 'Toto', value: { id: 'A', name: 'Africa' } } } },
    c: { value: 10, units: {}, prefscale: 'Z' },
    d: { value: 5,  units: {}, prefscale: '0' },
    e: { value: 8.3,  units: {}, prefscale: '-1' },
    f: { value: 25.62,  units: {}, prefscale: '3' },
    g: { value: 3.7,  units: {}, decimals: 'AB' },
    h: { value: 21.1234567,  units: {}, decimals: '3' },
    i: { value: 20.1,  units: {}, decimals: '5' },
    j: { value: 19.9,  units: {}, decimals: '0' },
    k: { value: 16.35,  units: {}, decimals: '2', prefscale: '1' },
    l: { value: 155062.56,  units: {}, decimals: '3' },
    m: { value: 155062.56,  units: {}, decimals: '0' },
    n: { value: null, units: {} },
  }
};

describe('getCells tests', () => {
  it('value format tests', () => {
    expect(getCells(data, 'label', customAttributes)).to.deep.equal({
      a: { value: '2', intValue: 2, flags: [] },
      b: { value: '0', intValue: 0, flags: [{ label: 'Toto: Africa' }] },
      c: { value: '10', intValue: 10, flags: [] },
      d: { value: '5', intValue: 5, flags: [] },
      e: { value: '83', intValue: 8.3, flags: [] },
      f: { value: '0.02562', intValue: 25.62, flags: [] },
      g: { value: '3.7', intValue: 3.7, flags: [] },
      h: { value: '21.123', intValue: 21.1234567, flags: [] },
      i: { value: '20.10000', intValue: 20.1, flags: [] },
      j: { value: '20', intValue: 19.9, flags: [] },
      k: { value: '1.64', intValue: 16.35, flags: [] },
      l: { value: '155,062.560', intValue: 155062.56, flags: [] },
      m: { value: '155,063', intValue: 155062.56, flags: [] },
      n: { value: '..', intValue: null, flags: [] },
    });
  });
  it('flags display name', () => {
    expect(getCells({
      observations: {
        a: {
          value: 33,
          attributes: {
            FL2: { id: 'FL2', name: 'Flag 2', value: { id: 'FL2.1', name: 'Flag 2 Value 1' } },
            FT1: { id: 'FT1', name: 'Footnote 1', value: { id: 'FT1.1', name: 'Footnote 1 Value 1' } },
            FT3: { id: 'FT3', name: 'Footnote 3', value: { id: 'FT3.3', name: 'Footnote 3 Value 3' } },
          }
        }
      }
    }, 'label', customAttributes,
    { unitsDefinitionCodes: [], unitsSeries: {}, unitsDisplay: 'cells', unitDimension: {} })).to.deep.equal({
      a: {
        value: '33',
        intValue: 33,
        flags: [
          { code: 'FL2.1', label: 'Flag 2: Flag 2 Value 1' },
          { label: 'Footnote 1: Footnote 1 Value 1' },
          { label: 'Footnote 3: Footnote 3 Value 3' }
        ]
      }
    })
  });
  it('flags display both', () => {
    expect(getCells({
      observations: {
        a: {
          value: 33,
          attributes: {
            FL2: { id: 'FL2', name: 'Flag 2', value: { id: 'FL2.1', name: 'Flag 2 Value 1' } },
            FT1: { id: 'FT1', name: 'Footnote 1', value: { id: 'FT1.1', name: 'Footnote 1 Value 1' } },
            FT3: { id: 'FT3', name: 'Footnote 3', value: { id: 'FT3.3', name: 'Footnote 3 Value 3' } },
          }
        }
      }
    }, 'both', customAttributes)).to.deep.equal({
      a: {
        value: '33',
        intValue: 33,
        flags: [
          { code: 'FL2.1', label: '(FL2) Flag 2: (FL2.1) Flag 2 Value 1' },
          { label: '(FT1) Footnote 1: (FT1.1) Footnote 1 Value 1' },
          { label: '(FT3) Footnote 3: (FT3.3) Footnote 3 Value 3' }
        ]
      }
    })
  });
});
