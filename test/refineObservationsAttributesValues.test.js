import { expect } from 'chai';
import { refineObservationsAttributesValues } from '../src/rules/src/table/preparators/refineObservationsAttributesValues';


const attributesByIds = {
  a: { id: 'a', values: [{ id: 'a1' }] },
  b: { id: 'b', values: [{ id: 'b1' }, { id: 'b2', annotations: [1, 3, 5] }] }
};

const attributesOccurences = {
  o1: { a: [0, 0, 0, 0, 0], b: [3, 3, 3, 3, 3, 3, 3] },
  o2: { a: [0, 0, 0, null], b: [] },
  o3: { b: [0, 0] },
  o4: { a: [0], b: [1, 1, 1], c: [4, 4, 4, 4, 4] },
};

describe('refineObservationsAttributesValues test', () => {
  it('valid plus non existin value case', () => {
    expect(
      refineObservationsAttributesValues(attributesByIds)({ a: [0, 0, 0, 0, 0], b: [3, 3, 3, 3, 3, 3, 3] })
    ).to.deep.equal({ a: { id: 'a', value: { id: 'a1' } } });
  });
  it('different indexes and no index case', () => {
    expect(
      refineObservationsAttributesValues(attributesByIds)({ a: [0, 0, 0, null], b: [] })
    ).to.deep.equal({});
  });
  it('valids plus unknown atrtibute case', () => {
    expect(
      refineObservationsAttributesValues(attributesByIds)({ a: [0], b: [1, 1, 1], c: [4, 4, 4, 4, 4] })
    ).to.deep.equal({ a: { id: 'a', value: { id: 'a1' } }, b: { id: 'b', value: { id: 'b2' } } });
  });
});
