import { expect } from 'chai';
import { parseObservations } from '../src/rules/src/table/preparators/parseObservations';
import { getDisplay } from '../src/rules/src/table/preparators/getDisplay';

const observationsAttributes = {
  a: { index: 0, values: [{ id: 'a0' }, { id: 'a1' }] },
  b: { index: 1, id: 'b', name: 'B', values: [{ id: 'b0' }] },
};

const formatAttributes = {
  f1: { index: 2 },
  f2: { index: 3, values: [{ id: 'f2.1' }, null] },
};


const observations = {
  '0:0:0:0:0': [22, 0, 0, 0, 0, 0],
  '0:0:0:1:0': [17, 1, 0, 1, 1, 0],
  '1:0:0:0:0': [55, 2, 0, null, null, 0],
  '2:0:0:0:0': [35, 2, 0, null, null, null],
  '3:0:0:0:0': [35, 2, 0, null, null, null],
};

const unitsArtefacts = {
  dimensions: { series: { D0: { id: 'D0', index: 0, values: [{ id: 'V0' }, { id: 'V1' }, { id: 'V2' }, { id: 'V3' }] } } },
  attributes: { series: { A4: { id: 'A4', index: 4, values: [{ id: 'V0' }] } } }
};

describe('parseObservations test', () => {
  it('complete case', () => {
    expect(parseObservations(
      observations,
      observationsAttributes,
      formatAttributes,
      unitsArtefacts,
      getDisplay({ noDisplayIndexes: [], notDisplayedCodes: { D0: { values: { V3: 'V3' } } } })
    )).to.deep.equal({
        '0:0:0:0:0': {
          key: '0:0:0:0:0',
          value: 22,
          units: { D0: { id: 'D0', value: { id: 'V0', index: 0 } }, A4: { id: 'A4', value: { id: 'V0', index: 0 } } },
          attrValuesIndexes: [0, 0, 0, 0, 0],
          dimValuesIndexes: ['0', '0', '0', '0', '0'],
          attributes: {
            a: { value: { id: 'a0' } },
            b: { id: 'b', name: 'B', value: { id: 'b0' } }
          },
          f1: null,
          f2: 'f2.1'
        },
        '0:0:0:1:0': {
          key: '0:0:0:1:0',
          value: 17,
          units: { D0: { id: 'D0', value: { id: 'V0', index: 0 } }, A4: { id: 'A4', value: { id: 'V0', index: 0 } } },
          attrValuesIndexes: [1, 0, 1, 1, 0],
          dimValuesIndexes: ['0', '0', '0', '1', '0'],
          attributes: {
            a: { value: { id: 'a1' } },
            b: { id: 'b', name: 'B', value: { id: 'b0' } }
          },
          f1: null,
          f2: null
        },
        '1:0:0:0:0': {
          key: '1:0:0:0:0',
          value: 55,
          units: { D0: { id: 'D0', value: { id: 'V1', index: 1 } }, A4: { id: 'A4', value: { id: 'V0', index: 0 } } },
          attrValuesIndexes: [2, 0, null, null, 0],
          dimValuesIndexes: ['1', '0', '0', '0', '0'],
          attributes: {
            b: { id: 'b', name: 'B', value: { id: 'b0' } }
          },
          f1: null,
          f2: null
        },
        '2:0:0:0:0': {
          key: '2:0:0:0:0',
          value: 35,
          units: { D0: { id: 'D0', value: { id: 'V2', index: 2 } }, A4: null },
          attrValuesIndexes: [2, 0, null, null, null],
          dimValuesIndexes: ['2', '0', '0', '0', '0'],
          attributes: {
            b: { id: 'b', name: 'B', value: { id: 'b0' } }
          },
          f1: null,
          f2: null,
        },
        '3:0:0:0:0': {
          key: '3:0:0:0:0',
          value: 35,
          units: { D0: null, A4: null },
          attrValuesIndexes: [2, 0, null, null, null],
          dimValuesIndexes: ['3', '0', '0', '0', '0'],
          attributes: {
            b: { id: 'b', name: 'B', value: { id: 'b0' } }
          },
          f1: null,
          f2: null
        }
      });
  });
});
