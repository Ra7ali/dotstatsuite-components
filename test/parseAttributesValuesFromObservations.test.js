import { expect } from 'chai';
import { parseAttributesValuesFromObservations } from '../src/rules/src/table/preparators/parseAttributesValuesFromObservations';
import { getDimensionsAttributesRegisters } from '../src/rules/src/table/preparators/getDimensionsAttributesRegisters';

const attributesIdsIndexedByTargets = {
  manyValuesDimensions: ['a1', 'a3'],
  series: ['a2', 'a4']
};

const attributesIndexedByIds = {
  a1: { id: 'a1', index: 0, targetIndex: 3 },
  a2: { id: 'a2', index: 1, targetIndexes: { '0': 0, '1': 1 } },
  a3: { id: 'a3', index: 2, targetIndex: 2 },
  a4: { id: 'a4', index: 3, targetIndexes: { '1': 1, '4': 4 } }
};

const observations = {
  a: { dimValuesIndexes: [0, 0, 0, 0, 0], attrValuesIndexes: [0, 3, 0, 0] },
  b: { dimValuesIndexes: [0, 0, 0, 0, 1], attrValuesIndexes: [0, 3, 1, 2] },
  c: { dimValuesIndexes: [0, 0, 1, 0, 0], attrValuesIndexes: [0, 3, 5, null] },
  d: { dimValuesIndexes: [0, 0, 1, 0, 1], attrValuesIndexes: [0, 3, 0, 2] },
};

const attributesRegisters = getDimensionsAttributesRegisters(
  attributesIdsIndexedByTargets,
  attributesIndexedByIds
);

describe('parseAttributesValuesFromObservations test', () => {
  it('test', () => {
    expect(parseAttributesValuesFromObservations(observations, attributesRegisters))
      .to.deep.equal({
        series: {
          '0:0:x:x:x': { a2: [3, 3, 3, 3] },
          'x:0:x:x:0': { a4: [0, null] },
          'x:0:x:x:1': { a4: [2, 2] }
        },
        dimensions: {
          '3:0': { a1: [0, 0, 0, 0] },
          '2:0': { a3: [0, 1] },
          '2:1': { a3: [5, 0] }
        }
      });
  });
});
