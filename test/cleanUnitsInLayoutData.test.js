import { expect } from 'chai';
import { cleanUnitsInLayoutData } from '../src/rules/src/table/units/cleanUnitsInLayoutData';

const headerData = [
  { data: [{ id: 0 }, { id: 1 }, { dimension: { label: 'U' }, value: { label: null, unitsLabel: 'U1' } }] },
  { data: [{ id: 0 }, { id: 1 }, { dimension: { label: 'U' }, value: { label: 'toto', unitsLabel: 'U2' } }] },
  { data: [{ id: 0 }, { id: 1 }, { dimension: { label: 'U' }, value: { unitsLabel: 'U3' } }] },
];

const sectionsData1 = [
  [
    { data: [{ id: 0 }, { id: 1 }, { value: { unitsLabel: 'Units S1' } }] },
    [{ data: [{ id: 'r1' }, { id: 'r2' }] }]
  ],
  [
    { data: [{ id: 2 }, { id: 3 }, { value: { unitsLabel: 'Units S2' } }] },
    [{ data: [{ id: 'r1' }, { id: 'r2' }] }]
  ]
];

const sectionsData2 = [
  [
    { data: [{ id: 0 }, { id: 1 }] },
    [
      { data: [{ id: 'r1' }, { id: 'r2' }, { value: { unitsLabel: 'Units R1' } }] },
      { data: [{ id: 'r1' }, { id: 'r2' }, { value: { unitsLabel: 'Units R2' } }] },
    ]
  ],
  [
    { data: [{ id: 2 }, { id: 3 }] },
    [
      { data: [{ id: 'r1' }, { id: 'r2' }, { value: { unitsLabel: 'Units R3' } }] },
      { data: [{ id: 'r1' }, { id: 'r2' }, { value: { unitsLabel: 'Units R4' } }] },
    ]
  ]
];

describe('cleanUnitsInLayoutData tests', () => {
  it('header test', () => {
    expect(cleanUnitsInLayoutData({ unitsDisplay: 'header', unitsLayoutIndexes: { header: [1] } })({ headerData }))
      .to.deep.equal({
        headerData: [
          { data: [{ id: 0 }, { dimension: { label: 'U' }, value: { label: 'U1', unitsLabel: 'U1' } }] },
          { data: [{ id: 0 }, { dimension: { label: 'U' }, value: { label: 'U2', unitsLabel: 'U2' } }] },
          { data: [{ id: 0 }, { dimension: { label: 'U' }, value: { label: 'U3', unitsLabel: 'U3' } }] },
        ]
      })
  });
  it('sections test', () => {
    expect(cleanUnitsInLayoutData({ unitsDisplay: 'sections', unitsLayoutIndexes: { sections: [0] } })({ sectionsData: sectionsData1 }))
      .to.deep.equal({
        sectionsData: [
          [
            { data: [{ id: 1 }, { value: { label: 'Units S1', unitsLabel: 'Units S1' } }] },
            [{ data: [{ id: 'r1' }, { id: 'r2' }] }]
          ],
          [
            { data: [{ id: 3 }, { value: { label: 'Units S2', unitsLabel: 'Units S2' } }] },
            [{ data: [{ id: 'r1' }, { id: 'r2' }] }]
          ]
        ]
      })
  });
  it('rows test', () => {
    expect(cleanUnitsInLayoutData({ unitsDisplay: 'rows', unitsLayoutIndexes: { rows: [0] } })({ sectionsData: sectionsData2 }))
      .to.deep.equal({
        sectionsData: [
          [
            { data: [{ id: 0 }, { id: 1 }] },
            [
              { data: [{ id: 'r2' }, { value: { label: 'Units R1', unitsLabel: 'Units R1' } }] },
              { data: [{ id: 'r2' }, { value: { label: 'Units R2', unitsLabel: 'Units R2' } }] },
            ]
          ],
          [
            { data: [{ id: 2 }, { id: 3 }] },
            [
              { data: [{ id: 'r2' }, { value: { label: 'Units R3', unitsLabel: 'Units R3' } }] },
              { data: [{ id: 'r2' }, { value: { label: 'Units R4', unitsLabel: 'Units R4' } }] },
            ]
          ]
        ]
      })
  });
});
