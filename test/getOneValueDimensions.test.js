import { expect } from 'chai';
import { getOneValueDimensions } from '../src/rules/src/table/preparators/getOneValueDimensions';
import { getDisplay } from '../src/rules/src/table/preparators/getDisplay';

const noDisplayIndexes = [0, 2, 4];

const notDisplayedCodes = { '5': { id: '5' }, '6': { id: '6', values: { '6.1': '6.1' } } }

describe('getOneValueDimensions tests', () => {
  it('no display indexes test', () => {
    const dimensions = [
      { id: '0', values: [{ id: '0.1' }] },
      { id: '1', annotations: [1, 3, 5], values: [{ id: '1.1' }] },
      { id: '2', annotations: [0, 3, 5], values: [{ id: '2.1' }] },
      { id: '3', values: [{ id: '3.1', annotations: [7, 8] }] },
      { id: '4', values: [{ id: '4.1', annotations: [2, 7] }] },
      { id: '5', values: [{ id: '5.1' }] },
      { id: '6', values: [{ id: '6.1' }] },
    ];

    expect(getOneValueDimensions(dimensions, {}, [], getDisplay({ noDisplayIndexes, notDisplayedCodes }))).to.deep.equal([
      { id: '0', display: true, values: [{ id: '0.1', attributes: {}, display: true }] },
      { id: '1', annotations: [1, 3, 5], display: true, values: [{ id: '1.1', attributes: {}, display: true }] },
      { id: '2', annotations: [0, 3, 5], display: false, values: [{ id: '2.1', attributes: {}, display: true }] },
      { id: '3', display: true, values: [{ id: '3.1', annotations: [7, 8], attributes: {}, display: true }] },
      { id: '4', display: true, values: [{ id: '4.1', annotations: [2, 7], attributes: {}, display: false }] },
      { id: '5', display: false, values: [{ id: '5.1', attributes: {}, display: true }] },
      { id: '6', display: true, values: [{ id: '6.1', attributes: {}, display: false }] },
    ]);
  });
});
