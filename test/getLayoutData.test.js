import { expect } from 'chai';
import { getLayoutData } from '../src/rules/src/table/factories/getLayoutData';

const formatLayoutData = (d, v) => ({ dimension: { __index: d }, value: { __index: v } });

const emptyHeadersLayout1 = {
  header: [],
  sections: [],
  rows: [
    { __index: 0, values: [{ __index: 0 }, { __index: 1 }, { __index: 2 }] },
    { __index: 1, values: [{ __index: 0 }, { __index: 1 }, { __index: 2 }] },
    { __index: 2, values: [{ __index: 0 }, { __index: 1 }, { __index: 2 }] },
  ]
};

const emptyRowsLayout = {
  header: [
    { __index: 0, values: [{ __index: 0 }, { __index: 1 }, { __index: 2 }] },
    { __index: 1, values: [{ __index: 0 }, { __index: 1 }, { __index: 2 }] },
    { __index: 2, values: [{ __index: 0 }, { __index: 1 }, { __index: 2 }] },
  ],
  sections: [],
  rows: [],
};

const simpleLayout = {
  header: [{ __index: 0, values: [{ __index: 0 }, { __index: 1 }, { __index: 2 }] }],
  sections: [{ __index: 1, values: [{ __index: 0 }, { __index: 1 }, { __index: 2 }] }],
  rows: [{ __index: 2, values: [{ __index: 0 }, { __index: 1 }, { __index: 2 }] }]
};

const noSectionsLayout = {
  header: [
    { __index: 0, values: [{ __index: 0 }, { __index: 1 }, { __index: 2 }] },
    { __index: 1, values: [{ __index: 0 }, { __index: 1 }, { __index: 2 }] }
  ],
  sections: [],
  rows: [{ __index: 2, values: [{ __index: 0 }, { __index: 1 }, { __index: 2 }] }],
};

const expectedSimpleLayoutData = {
  headerData: [
    [formatLayoutData(0, 0)],
    [formatLayoutData(0, 1)],
    [formatLayoutData(0, 2)]
  ],
  sectionsData: [
    [
      [formatLayoutData(1, 0)],
      [
        [formatLayoutData(2, 0)],
        [formatLayoutData(2, 1)],
        [formatLayoutData(2, 2)]
      ]
    ],
    [
      [formatLayoutData(1, 1)],
      [
        [formatLayoutData(2, 0)],
        [formatLayoutData(2, 1)],
        [formatLayoutData(2, 2)]
      ]
    ],
    [
      [formatLayoutData(1, 2)],
      [
        [formatLayoutData(2, 0)],
        [formatLayoutData(2, 1)],
        [formatLayoutData(2, 2)]
      ]
    ]
  ]
};

const expectedRefinedAfterEmptyRowsClean = {
  headerData: [
    [formatLayoutData(0, 0), formatLayoutData(1, 0)],
    [formatLayoutData(0, 0), formatLayoutData(1, 1)],
    [formatLayoutData(0, 0), formatLayoutData(1, 2)],
    [formatLayoutData(0, 1), formatLayoutData(1, 0)],
    [formatLayoutData(0, 1), formatLayoutData(1, 1)],
    [formatLayoutData(0, 1), formatLayoutData(1, 2)],
  ],
  sectionsData: [
    [
      [],
      [
        [formatLayoutData(2, 0)],
      ]
    ]
  ]
};

const expectedRefinedAfterEmptyHeaderClean = {
  headerData: [
    [formatLayoutData(0, 0)],
    [formatLayoutData(0, 2)]
  ],
  sectionsData: [
    [
      [formatLayoutData(1, 0)],
      [
        [formatLayoutData(2, 0)],
        [formatLayoutData(2, 1)],
        [formatLayoutData(2, 2)]
      ]
    ],
    [
      [formatLayoutData(1, 1)],
      [
        [formatLayoutData(2, 0)],
        [formatLayoutData(2, 1)],
        [formatLayoutData(2, 2)]
      ]
    ]
  ]
};

const expectedEmptyHeadersLayout1 = {
  headerData: [[]],
  sectionsData: [
    [
      [],
      [
        [formatLayoutData(0, 0), formatLayoutData(1, 0), formatLayoutData(2, 0)],
        [formatLayoutData(0, 0), formatLayoutData(1, 0), formatLayoutData(2, 1)],
        [formatLayoutData(0, 0), formatLayoutData(1, 0), formatLayoutData(2, 2)],
        [formatLayoutData(0, 0), formatLayoutData(1, 1), formatLayoutData(2, 0)],
        [formatLayoutData(0, 0), formatLayoutData(1, 1), formatLayoutData(2, 1)],
        [formatLayoutData(0, 0), formatLayoutData(1, 1), formatLayoutData(2, 2)],
        [formatLayoutData(0, 0), formatLayoutData(1, 2), formatLayoutData(2, 0)],
        [formatLayoutData(0, 0), formatLayoutData(1, 2), formatLayoutData(2, 1)],
        [formatLayoutData(0, 0), formatLayoutData(1, 2), formatLayoutData(2, 2)],
        [formatLayoutData(0, 1), formatLayoutData(1, 0), formatLayoutData(2, 0)],
        [formatLayoutData(0, 1), formatLayoutData(1, 0), formatLayoutData(2, 1)],
        [formatLayoutData(0, 1), formatLayoutData(1, 0), formatLayoutData(2, 2)],
        [formatLayoutData(0, 1), formatLayoutData(1, 1), formatLayoutData(2, 0)],
        [formatLayoutData(0, 1), formatLayoutData(1, 1), formatLayoutData(2, 1)],
        [formatLayoutData(0, 1), formatLayoutData(1, 1), formatLayoutData(2, 2)],
        [formatLayoutData(0, 1), formatLayoutData(1, 2), formatLayoutData(2, 0)],
        [formatLayoutData(0, 1), formatLayoutData(1, 2), formatLayoutData(2, 1)],
        [formatLayoutData(0, 1), formatLayoutData(1, 2), formatLayoutData(2, 2)],
        [formatLayoutData(0, 2), formatLayoutData(1, 0), formatLayoutData(2, 0)],
        [formatLayoutData(0, 2), formatLayoutData(1, 0), formatLayoutData(2, 1)],
      ],
    ]
  ]
};

const expectedEmptyRowsLayoutData = {
  headerData: [
    [formatLayoutData(0, 0), formatLayoutData(1, 0), formatLayoutData(2, 0)],
    [formatLayoutData(0, 0), formatLayoutData(1, 0), formatLayoutData(2, 1)],
    [formatLayoutData(0, 0), formatLayoutData(1, 0), formatLayoutData(2, 2)],
    [formatLayoutData(0, 0), formatLayoutData(1, 1), formatLayoutData(2, 0)],
    [formatLayoutData(0, 0), formatLayoutData(1, 1), formatLayoutData(2, 1)],
    [formatLayoutData(0, 0), formatLayoutData(1, 1), formatLayoutData(2, 2)],
    [formatLayoutData(0, 0), formatLayoutData(1, 2), formatLayoutData(2, 0)],
    [formatLayoutData(0, 0), formatLayoutData(1, 2), formatLayoutData(2, 1)],
    [formatLayoutData(0, 0), formatLayoutData(1, 2), formatLayoutData(2, 2)],
    [formatLayoutData(0, 1), formatLayoutData(1, 0), formatLayoutData(2, 0)],
    [formatLayoutData(0, 1), formatLayoutData(1, 0), formatLayoutData(2, 1)],
    [formatLayoutData(0, 1), formatLayoutData(1, 0), formatLayoutData(2, 2)],
    [formatLayoutData(0, 1), formatLayoutData(1, 1), formatLayoutData(2, 0)],
    [formatLayoutData(0, 1), formatLayoutData(1, 1), formatLayoutData(2, 1)],
    [formatLayoutData(0, 1), formatLayoutData(1, 1), formatLayoutData(2, 2)],
    [formatLayoutData(0, 1), formatLayoutData(1, 2), formatLayoutData(2, 0)],
    [formatLayoutData(0, 1), formatLayoutData(1, 2), formatLayoutData(2, 1)],
    [formatLayoutData(0, 1), formatLayoutData(1, 2), formatLayoutData(2, 2)],
    [formatLayoutData(0, 2), formatLayoutData(1, 0), formatLayoutData(2, 0)],
    [formatLayoutData(0, 2), formatLayoutData(1, 0), formatLayoutData(2, 1)],
  ],
  sectionsData: [[[], [[]]]],
};

const observations1 = {
  '0:0:0': [], '0:0:1': [], '0:0:2': [],
  '0:1:0': [], '0:1:1': [], '0:1:2': [],
  '0:2:0': [], '0:2:1': [], '0:2:2': [],
  '1:0:0': [], '1:0:1': [], '1:0:2': [],
  '1:1:0': [], '1:1:1': [], '1:1:2': [],
  '1:2:0': [], '1:2:1': [], '1:2:2': [],
  '2:0:0': [], '2:0:1': [], '2:0:2': [],
  '2:1:0': [], '2:1:1': [], '2:1:2': [],
  '2:2:0': [], '2:2:1': [], '2:2:2': [],
};

const partialObservations1 = {
  '0:0:0': [], '0:0:1': [], '0:0:2': [],
  '0:1:0': [], '0:1:1': [], '0:1:2': [],
  '0:2:0': [], '0:2:1': [], '0:2:2': [],
  '1:2:0': [], '1:2:1': [], '1:2:2': [],
  '2:0:0': [], '2:0:1': [], '2:0:2': [],
  '2:1:0': [], '2:1:1': [], '2:1:2': [],
  '2:2:0': [], '2:2:1': [], '2:2:2': [],
};

const partialObservations2 = {
  '0:0:0': [],
  '0:1:0': [],
  '0:2:0': [],
  '1:0:0': [],
  '1:1:0': [],
  '1:2:0': [],
  '2:0:0': [], '2:0:1': [], '2:0:2': [],
  '2:1:0': [], '2:1:1': [], '2:1:2': [],
  '2:2:0': [], '2:2:1': [], '2:2:2': [],
};

const mixedDimensionOrderHeader = {
  header: [
    { __index: 2, values: [{ __index: 0 }, { __index: 1 }, { __index: 2 }] },
    { __index: 0, values: [{ __index: 0 }, { __index: 1 }, { __index: 2 }] },
  ],
  sections: [],
  rows: [{ __index: 1, values: [{ __index: 0 }, { __index: 1 }, { __index: 2 }] }],
};

const expectedRefinedAfterEmptyRowsClean2 = {
  headerData: [
    [formatLayoutData(2, 0), formatLayoutData(0, 0)],
    [formatLayoutData(2, 0), formatLayoutData(0, 1)],
    [formatLayoutData(2, 0), formatLayoutData(0, 2)],
    [formatLayoutData(2, 1), formatLayoutData(0, 0)],
    [formatLayoutData(2, 1), formatLayoutData(0, 1)],
    [formatLayoutData(2, 1), formatLayoutData(0, 2)],
  ],
  sectionsData: [
    [
      [],
      [
        [formatLayoutData(1, 1)],
      ]
    ]
  ]
};

const partialObservations3 = {
  '0:0:2': [],
  '0:1:0': [], '0:1:1': [], '0:1:2': [],
  '0:2:2': [],
  '1:0:2': [],
  '1:1:0': [], '1:1:1': [], '1:1:2': [],
  '1:2:2': [],
  '2:0:2': [],
  '2:1:0': [], '2:1:1': [], '2:1:2': [],
  '2:2:2': [],
};

describe('getLayoutData tests', () => {
  it('no changes under limit', () => {
    expect(
      getLayoutData({
        layout: simpleLayout,
        observations: observations1,
        limit: 30,
      })
    ).to.deep.equal(expectedSimpleLayoutData);
  });
  it('no headers', () => {
    expect(
      getLayoutData({
        layout: emptyHeadersLayout1,
        observations: observations1,
        limit: 20
      })
    ).to.deep.equal(expectedEmptyHeadersLayout1);
  });
  it('no rows', () => {
    expect(
      getLayoutData({
        layout: emptyRowsLayout,
        observations: observations1,
        limit: 20
      })
    ).to.deep.equal(expectedEmptyRowsLayoutData);
  });
  it('some empty headers to clean after cut', () => {
    expect(
      getLayoutData({
        layout: simpleLayout,
        observations: partialObservations1,
        limit: 20,
      })
    ).to.deep.equal(expectedRefinedAfterEmptyHeaderClean);
  });
  it('multiple empty rows to clean after cut', () => {
    expect(
      getLayoutData({
        layout: noSectionsLayout,
        observations: partialObservations2,
        limit: 20,
      })
    ).to.deep.equal(expectedRefinedAfterEmptyRowsClean);
  });
  it('multiple empty rows to clean after cut with mixed dimensions order in header', () => {
    expect(
      getLayoutData({
        layout: mixedDimensionOrderHeader,
        observations: partialObservations3,
        limit: 20,
      })
    ).to.deep.equal(expectedRefinedAfterEmptyRowsClean2);
  });
});
