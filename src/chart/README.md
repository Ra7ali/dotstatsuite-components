# charts

The component scopes the options and styles of the oecd for visualisation charts.

## usage

```javascript
import { Chart } from '@sis-cc/dotstatsuite-components';

<Chart data={barData()} options={options} type="BarChart" config={config} />
```

## api

#### data `<object>`
Have a look to `test/mocks` to see expect data in details.

#### options `<object>`
Same options as the chart engine, but some options will be overriden by oecd defaults.

##### responsiveness
Dimensions are provided throught `options`: `{base: { width, height} }`.

There are 3 usecases:
* no dimension provided, chart is resizable and takes the size of its container
* single dimension provided, chart behaves like no dimension for the unprovided dimension and
limit chart with the provided dimension except if the container is too small
* dimensions provided, chart behaves like the previous case for its dimensions

#### config `<object>`
```javascript
{
  read: 'http://rcw-pp.prelive.oecd.org',
  write: 'http://vs-webdev-1:89/chart/generic',
}
```

#### type `<string>` (ie 'BarChart')
Have a look to [d3-charts](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-d3-charts) to have an up-to-date list of available charts.
