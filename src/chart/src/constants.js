export const REDUCED_THRESHOLD = 160;
export const FOCUS_COLORS = {
  highlightColors: ['#E73741', '#0F8FD9', '#993484', '#DF521E', '#719E24', '#E1B400', '#32A674', '#0B68AF'],
  baselineColors: ['#0B1E2D']
};
