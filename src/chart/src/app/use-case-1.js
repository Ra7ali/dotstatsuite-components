import React, { Component } from 'react';
import { render } from 'react-dom';
import OecdChart from '../';
import rowData from '../../test/mocks/row';
import barData from '../../test/mocks/bar-format';
import rowDataF from '../../test/mocks/row-format';
import scatterData from '../../test/mocks/scatter-format';
import symbolData from '../../test/mocks/symbol-format';
import timelineData from '../../test/mocks/timeline-format';
import stackedData from '../../test/mocks/stacked-format';

export default ({ config, style, options }) => {
  return (
    <div style={style}>
      <OecdChart options={options} config={config} type="StackedBarChart" data={stackedData()} />
    </div>
  );
};
