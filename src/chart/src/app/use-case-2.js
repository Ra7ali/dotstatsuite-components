import React, { Component } from 'react';
import { render } from 'react-dom';
import OecdChart from '../';
import rowData from '../../test/mocks/row';
import barData from '../../test/mocks/bar';
import scatterData from '../../test/mocks/scatter';
import timeData from '../../test/mocks/gpp-time';

export default ({ config, style, options }) => {
  const style1 = {
    ...style,
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  };

  const style2 = {
    width: 500,
    height: 500,
    padding: 10,
  };

  return (
    <div style={style1}>
      <div style={style2}>
        <OecdChart options={options} config={config} type="RowChart" data={rowData()} />
      </div>
      <div style={style2}>
        <OecdChart options={options} config={config} type="BarChart" data={barData()} />
      </div>
      <div style={style2}>
        <OecdChart options={options} config={config} type="ScatterChart" data={scatterData()} />
      </div>
      <div style={style2}>
        <OecdChart options={options} config={config} type="TimelineChart" data={timeData()} />
      </div>
    </div>
  );
};
