import React from 'react';
import { map } from 'lodash';
import Chart from '../';
import barData from '../../test/mocks/bar';
import rowData from '../../test/mocks/row';
import scatterData from '../../test/mocks/scatter';
import timeData from '../../test/mocks/time';
import gppBarData from '../../test/mocks/gpp-bar';
import pisaSymbolData from '../../test/mocks/pisa-symbol';
import gppSymbolData from '../../test/mocks/gpp-symbol';
import gppTimeData from '../../test/mocks/gpp-time';
import stacked from '../../test/mocks/stacked';
import choro from '../../test/mocks/choro';

const gppRowData = () => {
  const data = gppBarData();
  return ({
    ...data,
    series: [{
      datapoints: map(
        data.series[0].datapoints,
        dp => ({ ...dp, x: dp.y, y: dp.x })
      )
    }]
  });
};

const highlightGppTimeData = () => {
  const data = gppTimeData();
  let series = data.series;
  series[0] = { ...series[0], highlightIndex: 0};
  series[2] = { ...series[2], highlightIndex: 1};
  series[5] = { ...series[5], baselineIndex: 0};
  return ({
    ...data,
    series,
    share: {
      focused: {
        highlight: [
          { label: series[0].category },
          { label: series[2].category }
        ],
        baseline: [
          { label: series[5].category }
        ]
      }
    }
  })
};

const highlightGppSymbolData = () => {
  const data = gppSymbolData();
  let datapoints = data.series[0].datapoints;
  datapoints[0] = { ...datapoints[0], highlightIndex: 0};
  datapoints[2] = { ...datapoints[2], highlightIndex: 1};
  datapoints[5] = { ...datapoints[5], baselineIndex: 0};
  return ({
    ...data,
    series: [{ ...data.series[0], datapoints }],
  })
};

export default ({ config }) => (
  <div className="app legacy">
    <div className="app-chart">
      <span className="app-note">Ordinal Max Offset Vertical (Bar + VerticalSymbol)</span>
      <Chart data={barData()} options={{ base: { width: 600, height: 600 } }} type="BarChart" config={config} />
    </div> 
    <div className="app-chart">
      <span className="app-note">Ordinal Max Offset Vertical (Bar + VerticalSymbol)</span>
      <Chart data={gppBarData()} options={{ base: { width: 600, height: 600 } }} type="BarChart" config={config} />
    </div>   
    <div className="app-chart">
      <span className="app-note">Ordinal Max Offset Vertical (Bar + VerticalSymbol)</span>
      <Chart data={gppBarData()} options={{ base: { width: 600, height: 630 } }} type="BarChart" config={config} />
    </div>
    <div className="app-chart">
      <span className="app-note">Ordinal Max Offset Horizontal (Row + HorizontalSymbol)</span>
      <Chart data={rowData()} options={{ base: { width: 600, height: 700 } }} type="RowChart" config={config} />
    </div>
    <div className="app-chart">
      <span className="app-note">Ordinal Max Offset Horizontal (Row + HorizontalSymbol)</span>
      <Chart data={gppRowData()} options={{ base: { width: 600, height: 700 } }} type="RowChart" config={config} />
    </div>
    <div className="app-chart">
      <span className="app-note">Ordinal Max Offset Horizontal (Row + HorizontalSymbol)</span>
      <Chart data={gppRowData()} options={{ base: { width: 700, height: 700 } }} type="RowChart" config={config} />
    </div>
    <div className="app-chart">
      <span className="app-note">Ordinal Overlap Vertical (Bar + VerticalSymbol)</span>
      <Chart data={pisaSymbolData()} options={{ base: { width: 840, height: 430 } }} type="VerticalSymbolChart" config={config} />
    </div>
    <div className="app-chart">
      <span className="app-note">Ordinal Overlap Vertical (Bar + VerticalSymbol)</span>
      <Chart data={pisaSymbolData()} options={{ base: { width: 850, height: 430 } }} type="VerticalSymbolChart" config={config} />
    </div>
    <div className="app-chart">
      <span className="app-note">Ordinal Overlap Horizontal (Row + HorizontalSymbol)</span>
      <Chart data={pisaSymbolData()} options={{ base: { width: 500, height: 680 } }} type="HorizontalSymbolChart" config={config} />
    </div>
    <div className="app-chart">
      <span className="app-note">Ordinal Overlap Horizontal (Row + HorizontalSymbol)</span>
      <Chart data={pisaSymbolData()} options={{ base: { width: 500, height: 730 } }} type="HorizontalSymbolChart" config={config} />
    </div>
    <div className="app-chart">
      <span className="app-note">Ordinal MinSize Vertical (Bar + VerticalSymbol)</span>
      <Chart data={gppSymbolData()} options={{ base: { width: 550, height: 430 } }} type="VerticalSymbolChart" config={config} />
    </div>
    <div className="app-chart">
      <span className="app-note">Ordinal MinSize Vertical (Bar + VerticalSymbol)</span>
      <Chart data={gppSymbolData()} options={{ base: { width: 540, height: 430 } }} type="VerticalSymbolChart" config={config} />
    </div>
    <div className="app-chart">
      <span className="app-note">Ordinal MinSize Horizontal (Row + HorizontalSymbol)</span>
      <Chart data={gppSymbolData()} options={{ base: { width: 400, height: 680 } }} type="HorizontalSymbolChart" config={config} />
    </div>
    <div className="app-chart">
      <span className="app-note">Ordinal MinSize Horizontal (Row + HorizontalSymbol)</span>
      <Chart data={gppSymbolData()} options={{ base: { width: 400, height: 670 } }} type="HorizontalSymbolChart" config={config} />
    </div>
    <div className="app-chart">
      <span className="app-note">Linear MinSize (All)</span>
      <Chart data={scatterData()} options={{ base: { width: 150, height: 430 } }} type="ScatterChart" config={config} />
    </div>
    <div className="app-chart">
      <span className="app-note">Linear MinSize (All)</span>
      <Chart data={scatterData()} options={{ base: { width: 300, height: 420 } }} type="ScatterChart" config={config} />
    </div>
    <div className="app-chart">
      <span className="app-note">Linear MinSize (All)</span>
      <Chart data={scatterData()} options={{ base: { width: 150, height: 420 } }} type="ScatterChart" config={config} />
    </div>
    <div className="app-chart">
      <span className="app-note">Linear MinSize (All)</span>
      <Chart data={scatterData()} options={{ base: { width: 300, height: 600 } }} type="ScatterChart" config={config} />
    </div>
    <div className="app-chart">
      <span className="app-note">Data Visibility Horizontal (Timeline + HorizontalSymbol)</span>
      <Chart data={gppTimeData()} options={{ base: { width: 550, height: 430 } }} type="TimelineChart" config={config} />
    </div>
    <div className="app-chart">
      <span className="app-note">Data Visibility Horizontal (Timeline + HorizontalSymbol)</span>
      <Chart data={gppTimeData()} options={{ base: { width: 550, height: 230 } }} type="TimelineChart" config={config} />
    </div>
    <div className="app-chart">
      <span className="app-note">Data Visibility Horizontal (Timeline + HorizontalSymbol)</span>
      <Chart data={highlightGppTimeData()} options={{ base: { width: 600, height: 230 } }} type="TimelineChart" config={config} />
    </div>
    <div className="app-chart">
      <span className="app-note">Data Visibility Vertical (VerticalSymbol)</span>
      <Chart data={gppSymbolData()} options={{ base: { width: 550, height: 430 } }} type="VerticalSymbolChart" config={config} />
    </div>
    <div className="app-chart">
      <span className="app-note">Data Visibility Vertical (VerticalSymbol)</span>
      <Chart data={gppSymbolData()} options={{ base: { width: 100, height: 430 } }} type="VerticalSymbolChart" config={config} />
    </div>
    <div className="app-chart">
      <span className="app-note">Data Visibility Vertical (VerticalSymbol)</span>
      <Chart data={highlightGppSymbolData()} options={{ base: { width: 100, height: 430 } }} type="VerticalSymbolChart" config={config} />
    </div>
    <div className="app-chart">
      <span className="app-note">Stacked Bar Chart</span>
      <Chart data={stacked()} options={{ base: { width: 800, height: 730 } }} type="StackedBarChart" config={config} />
    </div>
    <div className="app-chart">
      <span className="app-note">Choropleth Chart (large, horizontal legend)</span>
      <Chart data={choro} options={{ base: { width: 400, height: 530 }, map: { scale: 1000 }, serie: { choropleth: { domain: 'Purples', divisions: 7 } } }} type="ChoroplethChart" config={config} />
    </div>
    <div className="app-chart">
      <span className="app-note">Choropleth Chart (reduced, vertical legend)</span>
      <Chart data={choro} options={{ base: { width: 350, height: 750 }, map: { scale: 1000 }, serie: { choropleth: { divisions: 7 } } }} type="ChoroplethChart" config={config} />
    </div>
  </div>
);
