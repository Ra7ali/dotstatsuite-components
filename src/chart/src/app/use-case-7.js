import React, { Component } from 'react';
import { render } from 'react-dom';
import OecdChart from '../';
import rowData from '../../test/mocks/row';
import scatterData from '../../test/mocks/scatter';

const options = {
  axis: {
    x: {
      font: {
        family: 'Comic Sans MS'
      }
    },
    y: {
      font: {
        family: 'Comic Sans MS'
      }
    },
  }
};

const usecaseConfig = {
  fonts: {
    header: {
      subtitle: {
        color: 'red',
        fontFamily: 'Comic sans MS'
      },
      title: {
        color: 'green',
        fontFamily: 'Comic sans MS',
      },
      uprs: {
        color: 'purple',
        fontFamily: 'Comic sans MS'
      },
      tooltip: {
        fontFamily: 'Comic sans MS',
        backgroundColor: 'yellow',
        color: 'blue'
      }
    },
    chart: {
      axisLegend: {
        fontFamily: 'Comic sans MS',
      },
      chartLegend: {
        fontFamily: 'Comic sans MS',
      },
      tooltip: {
        primary: {
          fontFamily: 'Comic sans MS',
          fontSize: 20
        },
        secondary: {
          fontFamily: 'Comic sans MS',
          fontSize: 20
        }
      }
    },
    footer: {
      copyright: {
        fontFamily: 'Comic sans MS'
      },
      source: {
        fontFamily: 'Comic sans MS'
      }
    }
  }
};

export default ({ config, style }) => {
  return (
    <div style={style}>
      <OecdChart options={options} config={{ ...config, ...usecaseConfig }} type="RowChart" data={rowData()} />
      <OecdChart options={options} config={{ ...config, ...usecaseConfig }} type="ScatterChart" data={scatterData()} />
    </div>
  );
};
