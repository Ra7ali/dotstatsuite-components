import React, { Component } from 'react';
import { render } from 'react-dom';
import OecdChart from '../';
import rowData from '../../test/mocks/row';
import barData from '../../test/mocks/bar';

export default ({ config, style, options, direction = 'row' }) => {
  const isRow = direction === 'row';

  const style1 = {
    ...style,
    display: 'flex',
    flexDirection: direction,
  };

  const style2 = {
    height: isRow ? '100%' : 300,
    width: isRow ? 700 : '100%',
    [isRow ? 'paddingRight' : 'paddingBottom']: 20,
  };

  return (
    <div style={style1}>
      <div style={style2}>
        <OecdChart options={options} config={config} type="BarChart" data={barData()} />
      </div>
      <OecdChart options={options} config={config} type="RowChart" data={rowData()} />
    </div>
  );
};
