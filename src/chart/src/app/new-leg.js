import React from 'react';
import { map } from 'lodash';
import Chart from '../';
import barData from '../../test/mocks/bar';
import rowData from '../../test/mocks/row';
import scatterData from '../../test/mocks/scatter';
import timeData from '../../test/mocks/time';
import gppBarData from '../../test/mocks/gpp-bar';
import pisaSymbolData from '../../test/mocks/pisa-symbol';
import gppSymbolData from '../../test/mocks/gpp-symbol';
import gppTimeData from '../../test/mocks/gpp-time';
import stacked from '../../test/mocks/stacked';
import choro from '../../test/mocks/choro';

const gppRowData = () => {
  const data = gppBarData();
  return ({
    ...data,
    series: [{
      datapoints: map(
        data.series[0].datapoints,
        dp => ({ ...dp, x: dp.y, y: dp.x })
      )
    }]
  });
};

const highlightGppTimeData = () => {
  const data = gppTimeData();
  let series = data.series;
  series[0] = { ...series[0], highlightIndex: 0};
  series[2] = { ...series[2], highlightIndex: 1};
  series[5] = { ...series[5], baselineIndex: 0};
  return ({
    ...data,
    series,
    share: {
      focused: {
        highlight: [
          { label: series[0].category },
          { label: series[2].category }
        ],
        baseline: [
          { label: series[5].category }
        ]
      }
    }
  })
};

const highlightGppSymbolData = () => {
  const data = gppSymbolData();
  let datapoints = data.series[0].datapoints;
  datapoints[0] = { ...datapoints[0], highlightIndex: 0};
  datapoints[2] = { ...datapoints[2], highlightIndex: 1};
  datapoints[5] = { ...datapoints[5], baselineIndex: 0};
  return ({
    ...data,
    series: [{ ...data.series[0], datapoints }],
  })
};

export default ({ config }) => (
  <div className="app">
    <div className="app-chart">
      <span className="app-note">Ordinal Max Offset Vertical (Bar + VerticalSymbol)</span>
      <div className="app-chart-wrapper" style={{ width: 600, height: 620 }}>
        <Chart data={barData()} type="BarChart" config={config} />
      </div>
    </div> 
    <div className="app-chart">
      <span className="app-note">Ordinal Max Offset Vertical (Bar + VerticalSymbol)</span>
      <div className="app-chart-wrapper" style={{ width: 600, height: 620 }}>
        <Chart data={gppBarData()} type="BarChart" config={config} />
      </div>
    </div>
    <div className="app-chart">
      <span className="app-note">Ordinal Max Offset Vertical (Bar + VerticalSymbol)</span>
      <div className="app-chart-wrapper" style={{ width: 600, height: 650 }}>
        <Chart data={gppBarData()} type="BarChart" config={config} />
      </div>
    </div>
    <div className="app-chart">
      <span className="app-note">Ordinal Max Offset Horizontal (Row + HorizontalSymbol)</span>
      <div className="app-chart-wrapper" style={{ width: 600, height: 720 }}>
      <Chart data={rowData()} type="RowChart" config={config} />
      </div>
    </div>
    <div className="app-chart">
      <span className="app-note">Ordinal Max Offset Horizontal (Row + HorizontalSymbol)</span>
      <div className="app-chart-wrapper" style={{ width: 600, height: 720 }}>
        <Chart data={gppRowData()} type="RowChart" config={config} />
      </div>
    </div>
    <div className="app-chart">
      <span className="app-note">Ordinal Max Offset Horizontal (Row + HorizontalSymbol)</span>
      <div className="app-chart-wrapper" style={{ width: 700, height: 720 }}>
        <Chart data={gppRowData()} type="RowChart" config={config} />
      </div>
    </div>
    <div className="app-chart">
      <span className="app-note">Ordinal Overlap Vertical (Bar + VerticalSymbol)</span>
      <div className="app-chart-wrapper" style={{ width: 840, height: 450 }}>
        <Chart data={pisaSymbolData()} type="VerticalSymbolChart" config={config} />
      </div>
    </div>
    <div className="app-chart">
      <span className="app-note">Ordinal Overlap Vertical (Bar + VerticalSymbol)</span>
      <div className="app-chart-wrapper" style={{ width: 850, height: 450 }}>
        <Chart data={pisaSymbolData()} type="VerticalSymbolChart" config={config} />
      </div>
    </div>
    <div className="app-chart">
      <span className="app-note">Ordinal Overlap Horizontal (Row + HorizontalSymbol)</span>
      <div className="app-chart-wrapper" style={{ width: 500, height: 700 }}>
        <Chart data={pisaSymbolData()} type="HorizontalSymbolChart" config={config} />
      </div>
    </div>
    <div className="app-chart">
      <span className="app-note">Ordinal Overlap Horizontal (Row + HorizontalSymbol)</span>
      <div className="app-chart-wrapper" style={{ width: 500, height: 750 }}>
        <Chart data={pisaSymbolData()} type="HorizontalSymbolChart" config={config} />
      </div>
    </div>
    <div className="app-chart">
      <span className="app-note">Ordinal MinSize Vertical (Bar + VerticalSymbol)</span>
      <div className="app-chart-wrapper" style={{ width: 550, height: 450 }}>
        <Chart data={gppSymbolData()} type="VerticalSymbolChart" config={config} />
      </div>
    </div>
    <div className="app-chart">
      <span className="app-note">Ordinal MinSize Vertical (Bar + VerticalSymbol)</span>
      <div className="app-chart-wrapper" style={{ width: 540, height: 450 }}>
        <Chart data={gppSymbolData()} type="VerticalSymbolChart" config={config} />
      </div>
    </div>
    <div className="app-chart">
      <span className="app-note">Ordinal MinSize Horizontal (Row + HorizontalSymbol)</span>
      <div className="app-chart-wrapper" style={{ width: 400, height: 700 }}>
        <Chart data={gppSymbolData()} type="HorizontalSymbolChart" config={config} />
      </div>
    </div>
    <div className="app-chart">
      <span className="app-note">Ordinal MinSize Horizontal (Row + HorizontalSymbol)</span>
      <div className="app-chart-wrapper" style={{ width: 400, height: 690 }}>
        <Chart data={gppSymbolData()} type="HorizontalSymbolChart" config={config} />
      </div>
    </div>
    <div className="app-chart">
      <span className="app-note">Linear MinSize (All)</span>
      <div className="app-chart-wrapper" style={{ width: 150, height: 450 }}>
        <Chart data={scatterData()} type="ScatterChart" config={config} />
      </div>
    </div>
    <div className="app-chart">
      <span className="app-note">Linear MinSize (All)</span>
      <div className="app-chart-wrapper" style={{ width: 300, height: 440 }}>
        <Chart data={scatterData()} type="ScatterChart" config={config} />
      </div>
    </div>
    <div className="app-chart">
      <span className="app-note">Linear MinSize (All)</span>
      <div className="app-chart-wrapper" style={{ width: 150, height: 440 }}>
        <Chart data={scatterData()} type="ScatterChart" config={config} />
      </div>
    </div>
    <div className="app-chart">
      <span className="app-note">Linear MinSize (All)</span>
      <div className="app-chart-wrapper" style={{ width: 300, height: 620 }}>
        <Chart data={scatterData()} type="ScatterChart" config={config} />
      </div>
    </div>
    <div className="app-chart">
      <span className="app-note">Data Visibility Horizontal (Timeline + HorizontalSymbol)</span>
      <div className="app-chart-wrapper" style={{ width: 550, height: 450 }}>
        <Chart data={gppTimeData()} type="TimelineChart" config={config} />
      </div>
    </div>
    <div className="app-chart">
      <span className="app-note">Data Visibility Horizontal (Timeline + HorizontalSymbol)</span>
      <div className="app-chart-wrapper" style={{ width: 550, height: 250 }}>
        <Chart data={gppTimeData()} type="TimelineChart" config={config} />
      </div>
    </div>
    <div className="app-chart">
      <span className="app-note">Data Visibility Horizontal (Timeline + HorizontalSymbol)</span>
      <div className="app-chart-wrapper" style={{ width: 600, height: 250 }}>
        <Chart data={highlightGppTimeData()} type="TimelineChart" config={config} />
      </div>
    </div>
    <div className="app-chart">
      <span className="app-note">Data Visibility Vertical (VerticalSymbol)</span>
      <div className="app-chart-wrapper" style={{ width: 550, height: 450 }}>
        <Chart data={gppSymbolData()} type="VerticalSymbolChart" config={config} />
      </div>
    </div>
    <div className="app-chart">
      <span className="app-note">Data Visibility Vertical (VerticalSymbol)</span>
      <div className="app-chart-wrapper" style={{ width: 100, height: 450 }}>
        <Chart data={gppSymbolData()} type="VerticalSymbolChart" config={config} />
      </div>
    </div>
    <div className="app-chart">
      <span className="app-note">Data Visibility Vertical (VerticalSymbol)</span>
      <div className="app-chart-wrapper" style={{ width: 100, height: 450 }}>
        <Chart data={highlightGppSymbolData()} type="VerticalSymbolChart" config={config} />
      </div>
    </div>
    <div className="app-chart">
      <span className="app-note">Stacked Bar Chart</span>
      <div className="app-chart-wrapper" style={{ width: 800, height: 750 }}>
        <Chart data={stacked()} type="StackedBarChart" config={config} />
      </div>
    </div>
    <div className="app-chart">
      <span className="app-note">Choropleth Chart (large, horizontal legend)</span>
      <div className="app-chart-wrapper" style={{ width: 440, height: 550 }}>
        <Chart data={choro} options={{ map: { scale: 1000 }, serie: { choropleth: { domain: 'Purples', divisions: 7 } } }} type="ChoroplethChart" config={config} />
      </div>
    </div>
    <div className="app-chart">
      <span className="app-note">Choropleth Chart (reduced, vertical legend)</span>
      <div className="app-chart-wrapper" style={{ width: 350, height: 770 }}>
        <Chart data={choro} options={{ map: { scale: 1000 }, serie: { choropleth: { divisions: 7 } } }} type="ChoroplethChart" config={config} />
      </div>
    </div>
  </div>
);
