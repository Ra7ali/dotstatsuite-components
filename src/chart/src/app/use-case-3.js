import React, { Component } from 'react';
import { render } from 'react-dom';
import OecdChart from '../';
import rowData from '../../test/mocks/row';
import barData from '../../test/mocks/bar';
import scatterData from '../../test/mocks/scatter';

export default ({ config, style, options }) => {
  const style1 = {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  };

  const style4 = {
    width: 1000,
    height: 400,
    display: 'flex',
    flexDirection: 'row',
  };

  const style2 = {
    width: 1000,
    height: 400,
    padding: 10,
  };

  const style3 = {
    width: 500,
    height: 400,
    padding: 10,
  };

  return (
    <div style={style1}>
      <div style={style2}>
        <OecdChart options={options} config={config} type="RowChart" data={rowData()} />
      </div>
      <div style={style4}>
        <div style={style3}>
          <OecdChart options={options} config={config} type="BarChart" data={barData()} />
        </div>
        <div style={style3}>
          <OecdChart options={options} config={config} type="ScatterChart" data={scatterData()} />
        </div>
      </div>
    </div>
  );
};
