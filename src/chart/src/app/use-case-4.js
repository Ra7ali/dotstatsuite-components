import React, { Component } from 'react';
import { render } from 'react-dom';
import OecdChart from '../';
import rowData from '../../test/mocks/row';
import barData from '../../test/mocks/bar';
import scatterData from '../../test/mocks/scatter';
import timeData from '../../test/mocks/gpp-time';

export default ({ config, style, options }) => {
  const style1 = {
    ...style,
    display: 'flex',
    flexDirection: 'column',
  };

  const style2 = {
    display: 'flex',
    flexDirection: 'row',
    height: '100%',
  };

  const style3 = {
    height: '100%',
    width: '50%',
    padding: 10,
  }

  return (
    <div style={style1}>
      <div style={style2}>
        <div style={style3}>
          <OecdChart options={options} config={config} type="RowChart" data={rowData()} />
        </div>
        <div style={style3}>
          <OecdChart options={options} config={config} type="BarChart" data={barData()} />
        </div>
      </div>
      <div style={style2}>
        <div style={style3}>
          <OecdChart options={options} config={config} type="ScatterChart" data={scatterData()} />
        </div>
        <div style={style3}>
          <OecdChart options={options} config={config} type="TimelineChart" data={timeData()} />
        </div>
      </div>
    </div>
  );
};
