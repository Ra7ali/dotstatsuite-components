import React, { Component } from 'react';
import { render } from 'react-dom';
import OecdChart from '../';
import rowData from '../../test/mocks/row';

export default ({ config, style, options = {} }) => {
  const style1 = {
    ...style,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  };

  const style2 = {
    ...options.base,
  };

  const options1 = {
    ...options,
    base: {
      ...options.base,
      width: 500,
      height: 500,
    },
  }

  return (
    <div style={style1}>
      <div style={style2}>
        <OecdChart options={options1} config={config} type="RowChart" data={rowData()} />
      </div>
    </div>
  );
};
