import { get, isFunction, map, round } from 'lodash';
import { format } from 'date-fns';
import * as R from 'ramda';

export default (tooltipFonts, type, options) => {
  const primaryFontFamily = get(tooltipFonts, 'primary.fontFamily', 'bernino-sans-narrow-regular');
  const primaryFontSize = get(tooltipFonts, 'primary.fontSize', 12);
  const secondaryFontFamily = get(tooltipFonts, 'secondary.fontFamily', 'bernino-sans-bold');
  const secondaryFontSize = get(tooltipFonts, 'secondary.fontSize', 16);

  const primaryFonts = (color) => (`
    font-size: ${primaryFontSize}px;
    font-family: '${primaryFontFamily}';
    color: ${color};
    padding: 5px;
    border: 1px solid ${color};
    background: white;
    opacity: .8;
  `);

  const secondaryFonts = `font-family: '${secondaryFontFamily}'; font-size: ${secondaryFontSize}px; font-weight: bold`;

  switch (type) {
    case 'BarChart':
      return ({
        serie: {
          tooltip: {
            layout: (serie, datum, color) => (`
              <div style="${primaryFonts(color)}">
                <div style="text-align: right;">
                  ${datum.x}
                </div>
                <div style="text-align: right; ${secondaryFonts}">
                  ${datum.formatedValue || round(datum.y, 2)}
                </div>
              </div>
            `)
          }
        }
      });
    case 'RowChart':
      return ({
        serie: {
          tooltip: {
            layout: (serie, datum, color) => (`
              <div style="${primaryFonts(color)}">
                <div style="text-align: right;">
                  ${datum.y}
                </div>
                <div style="text-align: right; ${secondaryFonts}">
                  ${datum.formatedValue || round(datum.x, 2)}
                </div>
              </div>
            `)
          }
        }
      });
    case 'ScatterChart':
      return ({
        serie: {
          tooltip: {
            layout: (serie, datum, color) => {
              return (`
                <div style="${primaryFonts(color)}">
                  <div style="text-align: right;">
                    ${map(get(datum, 'dimensionValues', {}), 'name').join(' ')}
                  </div>
                  <div style="text-align: right;">
                    ${get(serie, 'dimensionValues.x.name')}
                    <span style="${secondaryFonts}">
                      ${datum.xFormat || round(datum.x, 2)}
                    </span>
                  </div>
                  <div style="text-align: right;">
                    ${get(serie, 'dimensionValues.y.name')}
                    <span style="${secondaryFonts}">
                      ${datum.yFormat || round(datum.y, 2)}
                    </span>
                  </div>
                </div>
              `)
            }
          }
        }
      });
    case 'HorizontalSymbolChart':
      return ({
        serie: {
          tooltip: {
            layout: (serie, datum, color) => (`
              <div style="${primaryFonts(color)}">
                <div style="text-align: right;">
                  ${datum.y}
                  <br />
                  ${datum.symbolValue}
                </div>
                <div style="text-align: right; ${secondaryFonts}">
                  ${datum.formatedValue || round(datum.x, 2)}
                </div>
              </div>
            `)
          }
        }
      });
    case 'VerticalSymbolChart':
      return ({
        serie: {
          tooltip: {
            layout: (serie, datum, color) => (`
              <div style="${primaryFonts(color)}">
                <div style="text-align: right;">
                  ${datum.x}
                  <br />
                  ${datum.symbolValue}
                </div>
                <div style="text-align: right; ${secondaryFonts}">
                  ${datum.formatedValue || round(datum.y, 2)}
                </div>
              </div>
            `)
          }
        }
      });
    case 'TimelineChart':
      const optionTimeFormatter = R.path(['axis', 'x', 'format', 'proc'], options);
      const timeFormatter = R.when(
        R.pipe(R.is(Function), R.not),
        R.always(date => format(date, 'MMM YY'))
      )(optionTimeFormatter);
      return ({
        serie: {
          tooltip: {
            layout: (serie, datum, color) => (`
              <div style="${primaryFonts(color)}">
                <div style="text-align: right;">
                  ${map(get(datum, 'dimensionValues', {}), 'name').join('<br />')}
                </div>
                <div style="text-align: right;">
                  ${timeFormatter(datum.x)}
                </div>
                <div style="text-align: right; ${secondaryFonts}">
                  ${datum.formatedValue || round(datum.y, 2)}
                </div>
              </div>
            `)
          }
        }
      });
    case 'StackedBarChart':
      return ({
        serie: {
          tooltip: {
            layout: (serie, datum, color) => (`
              <div style="${primaryFonts(color)}">
                <div style="text-align: right;">
                  ${datum.x}
                </div>
                <div style="text-align: right;">
                  ${datum.layerLabel}
                </div>
                <div style="text-align: right; font-size: ${secondaryFontSize}px;">
                  Value:
                  <span style="${secondaryFonts}">
                    ${isFunction(datum.formater) ? datum.formater(datum.height) : round(datum.height, 2)}
                  </span>
                </div>
                <div style="text-align: right; font-size: ${secondaryFontSize}px;">
                  Cumulated:
                  <span style="${secondaryFonts}">
                    ${isFunction(datum.formater) ? datum.formater(datum.y) : round(datum.y, 2)}
                  </span>
                </div>
              </div>
            `)
          }
        }
      });
    case 'ChoroplethChart':
      return ({
        serie: {
          tooltip: {
            layout: (serie, datum, color) => (`
              <div style="${primaryFonts(color)}">
                <div style="text-align: right;">
                  ${get(datum, 'properties.label', '')}
                </div>
                <div style="text-align: right;">
                  ${get(datum, 'properties.category', '')}
                </div>
                <div style="text-align: right; ${secondaryFonts}">
                  ${round(get(datum, 'properties.value', null), 2)}
                </div>
              </div>
            `)
          }
        }
      });
    default:
      return ({});
  }
}
