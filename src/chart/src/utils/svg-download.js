import { isEmpty, each, get } from 'lodash';

const SVG_CHARTS = 'rcw-chart__chart';

export const svgDownload = () => {
  const doctype = '<?xml version="1.0" standalone="no"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">';

  window.URL = (window.URL || window.webkitURL);
  const body = document.body;

  const prefix = {
    xmlns: 'http://www.w3.org/2000/xmlns/',
    xlink: 'http://www.w3.org/1999/xlink',
    svg: 'http://www.w3.org/2000/svg'
  }

  function getStyles(doc) {
    let styles = '';
    const styleSheets = doc.styleSheets;

    if (styleSheets) {
      each(styleSheets, styleSheet => {
        processStyleSheet(styleSheet);
      })
    }

    function processStyleSheet(ss) {
      if (ss.cssRules) {
        each(get(ss, 'cssRules', {}), cssRule => {
          const rule = cssRule;
          if (rule.type === 3) {
            // Import Rule
            processStyleSheet(rule.styleSheet);
          }
          else {
            // hack for illustrator crashing on descendent selectors
            if (rule.selectorText) {
              if (rule.selectorText.indexOf('>') === -1) {
                styles += '\n' + rule.cssText;
              }
            }
          }
        })
      }
    }
    return styles;
  };

  function getSources(doc, styles) {
    let svgInfo = [];
    const svgs = doc.getElementsByClassName(SVG_CHARTS);

    styles = (styles === undefined) ? '' : styles;

    each(svgs, svg => {

      svg.setAttribute('version', '1.1');

      //<defs> useful ? 
      const defsEl = document.createElement('defs');
      svg.insertBefore(defsEl, svg.firstChild); 
      let styleEl = document.createElement('style')
      defsEl.appendChild(styleEl);
      styleEl.setAttribute('type', 'text/css');

      // removing attributes so they aren't doubled up
      svg.removeAttribute('xmlns');
      svg.removeAttribute('xlink');

      // These are needed for the svg
      if (!svg.hasAttributeNS(prefix.xmlns, 'xmlns')) {
        svg.setAttributeNS(prefix.xmlns, 'xmlns', prefix.svg);
      }

      if (!svg.hasAttributeNS(prefix.xmlns, 'xmlns:xlink')) {
        svg.setAttributeNS(prefix.xmlns, 'xmlns:xlink', prefix.xlink);
      }
      const source = (new XMLSerializer()).serializeToString(svg).replace('</style>', '<![CDATA[' + styles + ']]></style>');
      const rect = svg.getBoundingClientRect();
      svgInfo.push({
        top: rect.top,
        left: rect.left,
        width: rect.width,
        height: rect.height,
        class: svg.getAttribute('class'),
        id: svg.getAttribute('id'),
        childElementCount: svg.childElementCount,
        source: [doctype + source]
      });
    });
    return svgInfo;
  };

  function download(sources) {
    each(sources, source => {
      let blob = new Blob(source.source, { 'type' : 'text\/xml' });
      //if IE
      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveBlob(blob, 'chart.svg');
      }
      //other browsers
      else {
        const url = window.URL.createObjectURL(blob);
  
        const a = document.createElement('a');
        body.appendChild(a);
        a.setAttribute('class', 'svg-crowbar');
        a.setAttribute('download', 'chart.svg');
        a.setAttribute('href', url);
        a.style['display'] = 'none';
        a.click();
        setTimeout(() => {
          blob = null;
          window.URL.revokeObjectURL(url);
        }, 10);
      }
    })
  };

  function initialize() {
    let documents = [window.document];
    let  SVGSources = [];

    each(documents, doc => {
      let styles = getStyles(doc);
      let newSources = getSources(doc, styles);
      
      // because of prototype on NYT pages
      each(newSources, source => {
        SVGSources = [ ...newSources ];
      })
    })

    if (isEmpty(SVGSources)) {
      alert('No chart available');
    }
    else {
      download(SVGSources);
    } 
  }

  initialize();
};