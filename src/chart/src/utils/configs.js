import { round, map, get, last, cloneDeep } from 'lodash';
import { FOCUS_COLORS } from '../constants';

export const axis = () => ({
  thickness: 0,
  font: {
    family: 'bernino-sans-narrow-regular',
    color: '#575757',
    size: 12
  },
  linear: {
    pivot: {
      step: 1,
      color: 'white'
    }
  }
});

export const grid = {
  color: 'white',
  baselines: [0]
}

export const common = {
  axis: {
    x: axis(),
    y: axis(),
  },
  grid: {
    x: { ...grid },
    y: { ...grid }
  },
  background: { color: '#DBEBF2' },
  serie: {
    colors: ['#8EA4B1'],
    overColors: ['#39617D'],
    highlightColors: FOCUS_COLORS.highlightColors,
    baselineColors: FOCUS_COLORS.baselineColors,
    annotation: {
      font: {
        family: 'bernino-sans-bold',
        size: 11
      }
    }
  }
};

export const BarChart = {
  base: {
    padding: { top: 20 },
    innerPadding: { left: 20, right: 20, bottom: 10 }
  },
  axis: {
    x: {
      tick: { thickness: 0, size: 0 },
      format: { proc: datum => datum },
      ordinal: { gap: .3, padding: .3 }
    },
    y: {
      padding: 10,
      tick: { thickness: 0 },
      font: { baseline: 'ideographic' },
      linear: {
        pivot: { value: 0 }
      }
    }
  },
  grid: {
    x: { thickness: 0 }
  }
};

export const RowChart = {
  base: {
    padding: { top: 10, left: 10, right: 20 },
  },
  axis: {
    y: {
      tick: { thickness: 0, size: 0 },
      format: { proc: datum => datum },
      ordinal: { gap: .3, padding: .3, minDisplaySize: 300 }
    },
    x: {
      padding: 10,
      tick: { thickness: 0 },
      orient: 'top',
      linear: {
        pivot: { value: 0 }
      }
    }
  },
  grid: {
    y: { thickness: 0 }
  }
};

export const ScatterChart = {
  base: {
    padding: { top: 40, right: 10 }
  },
  axis: {
    x: {
      tick: { thickness: 0, size: 0 },
    },
    y: {
      padding: 10,
      tick: { thickness: 0 },
      font: { baseline: 'ideographic' }
    }
  },
  serie: {
    annotation: {
      display: 'never',
      format: {
        datapoint: {
          proc: (datum) => map(get(datum, 'dimensionValues', []), 'name').join(' ')
        }
      }
    }
  }
};

export const LineChart = {
  base: {
    isAnnotated: true,
    padding: { top: 20 },
    innerPadding: { left: 40, right: 40 }
  },
  axis: {
    x: {
      tick: { size: -6, color: 'white' }
    },
    y: {
      padding: 10,
      tick: { thickness: 0 },
      font: { baseline: 'ideographic' }
    }
  },
  grid: {
    x: { thickness: 0 }
  },
  serie: {
    annotation: {
      format: {
        serie: {
          proc: (serie) => {
            const dp = last(get(serie, 'datapoints', []));
            return `${map(get(dp, 'dimensionValues', {}), 'name').join(' ')} (${round(dp.y, 2)})`;
          },
          maxWidth: 120
        },
        datapoint: { pattern: '.2f' }
      }
    }
  }
};

export const symbolMarkers = () => {
  const paths = {
    circle: size => {
      const r = Math.sqrt(size) / 2;
      return `M0,${r} A${r},${r} 0 1,1 0,${-r} A${r},${r} 0 1,1 0,${r} Z`;
    },
    square: size => {
      const r = Math.sqrt(size) / 2;
      return `M${-r},${-r} L${r},${-r} ${r},${r} ${-r},${r} Z`;
    },
    cross: size => {
      const r = Math.sqrt(size) / 2;
      return `M${-r},${r} L${r},${-r} M${r},${r} L${-r},${-r}`;
    },
    'triangle': size => {
      const rx = Math.sqrt(size) / Math.sqrt(3);
      const ry = Math.sqrt(size) / 2;
      return `M0,${-ry} L${rx},${ry} ${-rx},${ry} Z`;
    }
  }
  return ([
    { style: { stroke: '#39617D', fill: '#39617D' }, path: paths.circle },
    { style: { stroke: '#39617D', fill: '#DBEBF2' }, rotate: 45, path: paths.square },
    { style: { stroke: '#39617D', fill: '#39617D', strokeWidth: 2 }, path: paths.cross },
    { style: { stroke: '#39617D', fill: '#DBEBF2' }, path: paths.square },
    { style: { stroke: '#39617D', fill: '#39617D' }, path: paths.triangle }
  ]);
}

export const HorizontalSymbolChart = {
  base: {
    padding: { top: 10, left: 10, right: 20 },
  },
  axis: {
    y: {
      tick: { thickness: 0, size: 5 },
      format: { proc: datum => datum },
      ordinal: { minDisplaySize: 300 }
    },
    x: {
      padding: 10,
      tick: { thickness: 0 },
      orient: 'top',
    }
  },
  grid: {
    y: { thickness: 0 }
  },
  serie: {
    symbol: {
      markers: symbolMarkers(),
      markerDefaultSize: 60,
      markerDefaultStrokeWidth: 1,
      gapMinMaxColor: '#607D8B',
      gapMinMaxThickness: 1,
      gapAxisMinColor: 'white',
      gapAxisMinThickness: 1
    }
  }
};

export const VerticalSymbolChart = {
  base: {
    padding: { top: 20 },
    innerPadding: { left: 20, right: 20, bottom: 10 }
  },
  axis: {
    x: {
      tick: { thickness: 0, size: 0 },
      format: { proc: datum => datum },
    },
    y: {
      padding: 10,
      tick: { thickness: 0 },
      font: { baseline: 'ideographic' },
    }
  },
  grid: {
    x: { thickness: 0 }
  },
  serie: {
    symbol: {
      markers: symbolMarkers(),
      markerDefaultSize: 60,
      markerDefaultStrokeWidth: 1,
      gapMinMaxColor: '#607D8B',
      gapMinMaxThickness: 1,
      gapAxisMinColor: 'white',
      gapAxisMinThickness: 1
    }
  }
};

export const TimelineChart = {
  base: {
    isAnnotated: false,
    padding: { top: 20 },
    innerPadding: { left: 40, right: 40 }
  },
  axis: {
    x: {
      format: { isTime: true, pattern: '%b %y' },
      tick: { size: -6, minorSize: -2, thickness: 2, minorThickness: 1, color: 'white' }
    },
    y: {
      padding: 10,
      tick: { thickness: 0 },
      font: { baseline: 'ideographic' }
    }
  },
  grid: {
    x: { thickness: 0 }
  },
  serie: {
    annotation: {
      format: {
        serie: {
          proc: (serie) => {
            const dp = last(get(serie, 'datapoints', []));
            return `${map(get(dp, 'dimensionValues', {}), 'name').join(' ')} (${round(dp.y, 2)})`;
          },
          maxWidth: 120
        },
        datapoint: { pattern: '.2f' }
      }
    }
  }
};

export const StackedBarChart = {
  base: {
    padding: { top: 20 },
    innerPadding: { left: 20, right: 20, bottom: 10 }
  },
  axis: {
    x: {
      tick: { thickness: 0, size: 0 },
      format: { proc: datum => datum },
      ordinal: { gap: .3, padding: .3 }
    },
    y: {
      padding: 10,
      tick: { thickness: 0 },
      font: { baseline: 'ideographic' },
      linear: {
        pivot: { value: 0 }
      }
    }
  },
  grid: {
    x: { thickness: 0 }
  },
  serie: {
    colors: ['#607D8B'],
    stacked: {
      strokeThickness: 1,
      strokeColor: '#F8FAFC',
    }
  }
};

export const ChoroplethChart = {
  serie: {
    choropleth: {
      divisions: 4,
      domain: 'YlorRd',
      invert: false,
      labelDisplay: 'none',
    },
  }
}
