import { get, includes, identity, merge } from 'lodash';
import compose from 'lodash.compose';
import numeral from 'numeral';
import * as configs from './configs';
import timelineFrequencyAndChoroplethConfig from './frequencies';
import tooltipOptions from './tooltip-config';

export const isScatter = (type) => type === 'ScatterChart';
export const isStackedBar = (type) => type === 'StackedBarChart';
export const isChoropleth = (type) => type === 'ChoroplethChart';
export const hasFocusLegend = (type) => includes(['ScatterChart', 'TimelineChart'], type);
export const isX = (axis) => axis === 'x';
export const getSeries = (data) => get(data, 'series', []);
const getDimensionValueName = (axis) => (data) => get(data, `[0].dimensionValues.${axis}.name`);
export const getAxisLabel = (axis) => (data) => compose(getDimensionValueName(axis), getSeries)(data);
//export const getChartClass = (type) => /SymbolChart/.test(type) ? 'SymbolChart' : type;
export const getChartClass = (type) => type;
export const isSymbol = (type) => /SymbolChart/.test(type);
export const getSymbolValues = (data) => get(data, 'series[0].symbolValues', []);

export const linearTicksFormat = (type) => {
  const proc =  d => numeral(d).format('0,0.[00]');
  const formatAxis = (key) => ({
    axis: {
      [key]: {
        format: {
          proc
        }
      }
    }
  });

  const formatAnnotation = ({
    serie: {
      annotation: {
        format: {
          datapoint: {
            proc
          }
        }
      }
    }
  });
  
  switch (type) {
    case 'BarChart': case 'VerticalSymbolChart': case 'TimelineChart': case 'StackedBarChart':
      return { ...formatAxis('y'), ...formatAnnotation };
    case 'RowChart': case 'HorizontalSymbolChart':
      return { ...formatAxis('x'), ...formatAnnotation };
    case 'ScatterChart':
      return merge(formatAxis('x'), formatAxis('y'));
  }
};

export const getOptions = (options, data, type, config) => {
  const chartClass = getChartClass(type);
  return merge(
    { chartClass },
    configs.common,
    configs[type],
    tooltipOptions(get(config, 'fonts.chart.tooltip', {}), type, options),
    linearTicksFormat(type),
    timelineFrequencyAndChoroplethConfig(chartClass, data.frequency, options)
  );
};
