import { filter, get, map } from 'lodash';

const isFocused = (obj) => {
  const highlightIndex = get(obj, 'highlightIndex', -1);
  const baselineIndex = get(obj, 'baselineIndex', -1);
  return ((highlightIndex > -1) || (baselineIndex > -1));
};

const sampleFilterSeries = (series) => (
  map(
    series,
    serie => {
      const _datapoints = filter(serie.datapoints, dp => isFocused(dp));
      return { ...serie, datapoints: _datapoints };
    }
  )
);

const lineFilterSeries = (series) => (filter(series, serie => isFocused(serie)));

export default (series, type, options) => {
  const height = options.base.height;
  const width = options.base.width;
  if (type === 'HorizontalSymbolChart') {
    if (height < 160) {
      return sampleFilterSeries(series);
    }
  }
  else if (type === 'TimelineChart') {
    if (height < 160) {
      return lineFilterSeries(series);
    }
  }
  else if ( type === 'VerticalSymbolChart') {
    if (width < 160) {
      return sampleFilterSeries(series);
    }
  }
  return series;
};