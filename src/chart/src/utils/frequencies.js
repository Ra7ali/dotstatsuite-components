import { divide, get, includes, isNumber, merge } from 'lodash';

const getQuarter = (datum) => {
  const month = datum.getMonth();
  return `Q${divide(month, 3) + 1}`;
}

const timelineConfigToOptions = (frequency, options) => {
  const step = get(options, 'axis.x.linear.step', 1);
  let freq = null;
  let pattern = null;
  let _step = null;
  let proc = null;
  if (frequency === 'annual') {
    freq = 'year';
    pattern = '%Y';
    _step = 1;
  }
  else if (frequency === 'quarterly') {
    freq = 'month';
    proc = datum => `${getQuarter(datum)} ${datum.getFullYear()}`,
    _step = 3;
  }
  else if (frequency === 'monthly') {
    freq = 'month';
    pattern = '%b %y';
    _step = 1;
  }
  return ({
    freq,
    pattern,
    proc,
    step: _step * step
  });
}

const checkDivisions = (divisions) => (!isNumber(divisions) || divisions < 1 || divisions > 7) ? 4 : divisions;

const checkDomain = (domain) => !includes(['Blues', 'Reds', 'Purples', 'RdYlGn', 'YlorRd'], domain) ? 'YlorRd' : domain;

export default (chartClass, frequency, options = {}) => {
  if (chartClass !== 'TimelineChart' && chartClass !== 'ChoroplethChart')  return options;

  const { freq, pattern, proc, step } = timelineConfigToOptions(frequency, options);

  return ({
    ...options,
    axis: {
      ...options.axis,
      x: {
        ...get(options, 'axis.x'),
        format: merge({ pattern, proc }, get(options, 'axis.x.format', {})),
        linear: {
          ...get(options, 'axis.x.linear'),
          frequency: freq,
          step
        }
      }
    },
    serie: {
      ...options.serie,
      choropleth: {
        ...get(options, 'serie.choropleth'),
        domain: checkDomain(get(options, 'serie.choropleth.domain')),
        divisions: checkDivisions(get(options, 'serie.choropleth.divisions')),
      }
    }
  });
}
