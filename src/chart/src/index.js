import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { get, floor, isNaN, gt, lt, min, isEmpty, max, pick } from 'lodash';
import Header from './components/header';
import { REDUCED_THRESHOLD } from './constants';
import sizeMe from 'react-sizeme';
import { isSymbol, isStackedBar } from './utils';
import '../../assets/chart/variables.less';
import { Chart, Container, Footer } from './glamorous_components';

//--------------------------------------------------------------------------------------------------
export class OecdChart extends Component {
  constructor(props){
    super(props);

    this.state = { deltaHeight: NaN, getInitialized: false };

    this.getInitialOptions = () => (type) => (options) => {
      this.setState({ options, getInitialized: true });
      this.getInitialOptionsInstance = null;
    }

    this.getInitialOptionsInstance = this.getInitialOptions();
  }

  componentDidMount() {
    this.setState({ getInitialized: true });
  }

  componentDidUpdate() {
    if (this.state.getInitialized) {
      this.setState({
        deltaHeight: this.headerEl.clientHeight + this.footerEl.clientHeight,
        getInitialized: false,
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    this.getInitialOptionsInstance = this.getInitialOptions();
  }

  render() {
    const { width, height, config, data, options, type, translations } = this.props;

    const isReduced = width >= REDUCED_THRESHOLD;

    let chartSize;
    if (!isNaN(this.state.deltaHeight)) {
      chartSize = { width, height: max([0, height - this.state.deltaHeight - 2]) }; // borders -_-
    }

    const getInitialOptions = this.getInitialOptionsInstance
      ? this.getInitialOptionsInstance(type)
      : null;
    return (
      <Container height={height} width={width}>
        <div ref={(headerEl) => this.headerEl = headerEl}>
          <Header
            {...pick(this.props, ['data', 'config', 'options', 'type', 'translations', 'width'])}
          />
        </div>
        <div>
          { isEmpty(chartSize)
            ? null
            : <Chart
                config={config}
                data={data}
                getInitialOptions={getInitialOptions}
                type={type}
                options={{...options, base: { ...get(options, 'base'), ...chartSize }}}
                width={width}
              />
          }
        </div>
        <div ref={(footerEl) => this.footerEl = footerEl}>
          <Footer
            {...pick(this.props, ['type', 'data', 'config', 'width'])}
            options={isSymbol(type) || isStackedBar(type) ? this.state.options : options}
          />
        </div>
      </Container>
    );
  }
}

OecdChart.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
  data: PropTypes.object,
  options: PropTypes.object,
  type: PropTypes.string,
  config: PropTypes.object,
  translations: PropTypes.object,
};

//--------------------------------------------------------------------------------------------------
const ResponsiveOecdChart = sizeMe({monitorWidth: true, monitorHeight: true})(
  ({ size, options, ...rest }) => {
    const setDimension = (size, options, dimensionKey) => {
      const oDimension = get(options, `base.${dimensionKey}`, NaN);
      const sDimension = floor(get(size, dimensionKey));

      // if dimension is in options then from options else from size
      return !oDimension || isNaN(oDimension) ? sDimension : oDimension;
    };

    const width = setDimension(size, options, 'width');
    const height = setDimension(size, options, 'height');

    if (!width || isNaN(width) || !height || isNaN(height)) {
      return <span>{`invalid dimension(s) for oecd chart: w: ${width} x h: ${height}`}</span>;
    }

    return (
      <div style={{height: '100%'}}>
        <OecdChart
          {...rest}
          width={width}
          height={height}
          options={options}
        />
      </div>
    );
  }
);

ResponsiveOecdChart.propTypes = {
  options: PropTypes.object,
  size: PropTypes.object,
};

//--------------------------------------------------------------------------------------------------
export default (props) => (
  <div style={{overflow: 'hidden', width: '100%', height: '100%'}}>
    <ResponsiveOecdChart {...props} />
  </div>
);
