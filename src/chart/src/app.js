import React from 'react';
import { Tab2, Tabs2, Intent, Icon, Tooltip, Position } from '@blueprintjs/core';
import UseCase1 from './app/use-case-1';
import UseCase2 from './app/use-case-2';
import UseCase3 from './app/use-case-3';
import UseCase4 from './app/use-case-4';
import UseCase5 from './app/use-case-5';
import UseCase6 from './app/use-case-6';
import UseCase7 from './app/use-case-7';
import Legacy from './app/leg';
import NewLegacy from './app/new-leg';
import Chart from '.';
import symbol from '../test/mocks/pisa-symbol';
import stacked from '../test/mocks/stacked';
import choro from '../test/mocks/choro';
import '../../assets/chart/app.less';

const AppTab = ({ label, iconName }) => (
  <Tooltip content={label} position={Position.RIGHT}>
    <Icon iconName={iconName} />
    {/*<span style={{paddingLeft: 10}}>{label}</span>*/}
  </Tooltip>
);

export default () => {
  const config = {
    read: 'http://vs-dotstattest.main.oecd.org/FrontEndDemo/teamcity/component-viewer/prototype/oecd',
    write: 'http://vs-webdev-1:89/chart',
    logo: 'https://upload.wikimedia.org/wikipedia/en/thumb/0/0d/OECD_logo_new.svg/200px-OECD_logo_new.svg.png',
    owner: '©OECD',
    terms: { label: 'Terms & Conditions', link: 'https://www.oecd.org/termsandconditions/' },
  };

  const style = {
    height: 'calc(100vh - 70px)',
  };

  const options = {
    axis: {
      x: {
        linear: {
          step: 5,
          max: 20,
        },
      },
      y: {
        linear: {
          step: 5,
          max: 20,
        },
      },
    },
  };

  return (
    <div style={{padding: 10}}>
      <Tabs2 renderActiveTabPanelOnly>
        <Tab2
          id="uc1"
          title={<AppTab label="full screen resizable chart" iconName="fullscreen" />}
          panel={<UseCase1 config={config} style={style} options={options} />}
        />
        <Tab2
          id="uc2"
          title={<AppTab label="responsive fixed slot dashboard" iconName="grid-view" />}
          panel={<UseCase2 config={config} style={style} options={options} />}
        />
        <Tab2
          id="uc3"
          title={<AppTab label="fixed slot dashboard" iconName="page-layout" />}
          panel={<UseCase3 config={config} style={style} options={options} />}
        />
        <Tab2
          id="uc4"
          title={<AppTab label="full screen dashboard" iconName="new-grid-item" />}
          panel={<UseCase4 config={config} style={style} options={options} />}
        />
        <Tab2
          id="uc5r"
          title={<AppTab label="mixed row dashboard" iconName="add-column-right" />}
          panel={<UseCase5 config={config} style={style} options={options} />}
        />
        <Tab2
          id="uc5c"
          title={<AppTab label="mixed column dashboard" iconName="add-row-bottom" />}
          panel={<UseCase5 config={config} style={style} options={options} direction="column" />}
        />
        <Tab2
          id="uc6"
          title={<AppTab label="fixed chart" iconName="symbol-square" />}
          panel={<UseCase6 config={config} style={style} options={options} />}
        />
        <Tab2
          id="leg"
          title={<AppTab label="legacy should still work like a charm" iconName="comparison" />}
          panel={<Legacy config={config} />}
        />
        <Tab2
          id="newleg"
          title={<AppTab label="rework of legacy" iconName="heat-grid" />}
          panel={<NewLegacy config={config} />}
        />
        <Tab2
          id="symbol"
          title={<AppTab label="symbol chart" iconName="waterfall-chart" />}
          panel={
            <Chart
              type="HorizontalSymbolChart"
              data={symbol()}
              config={config}
              style={style}
              options={options}
            />
          }
        />
        <Tab2
          id="stacked"
          title={<AppTab label="stacked bar chart" iconName="stacked-chart" />}
          panel={
            <Chart
              type="StackedBarChart"
              data={stacked()}
              config={config}
              style={style}
              options={options}
            />
          }
        />
        <Tab2
          id="choropleth"
          title={<AppTab label="choropleth chart" iconName="path-search" />}
          panel={
            <Chart
              data={choro}
              options={{
                map: { scale: 1000 },
                serie: { choropleth: { domain: 'Purples', divisions: 7 } }
              }}
              type="ChoroplethChart"
              config={config}
              style={style}
            />
          }
        />
        <Tab2
          id="uc7"
          title={<AppTab label="fonts configuration" iconName="fullscreen" />}
          panel={<UseCase7 config={config} style={style} />}
        />
      </Tabs2>
    </div>
  );
};
