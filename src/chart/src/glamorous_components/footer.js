import React from 'react';
import PropTypes from 'prop-types';
import { get, isEmpty } from 'lodash';
import SisFooter from '@sis-cc/dotstatsuite-ui-footer';
import FocusLegend from '../components/focus-legend';
import SymbolLegend from '../components/symbol-legend';
import StackedLegend from '../components/stacked-legend';
import ChoroLegend from '../components/choro-legend';
import { isSymbol, isStackedBar, isChoropleth } from '../utils';
import { AxisLegend } from '../glamorous_components';
import glamorous from 'glamorous';

const GSisFooter = glamorous(SisFooter)({
  margin: 0,
  padding: '5px 0',
});

const Footer = ({ type, data, options, config, width }) => {
  const logo = get(config, 'logo', null);
  const source = {
    link: get(data, 'footnotes.source'),
    label: get(data, 'footnotes.sourceLabel'),
  };
  const owner = get(config, 'owner', null);
  const terms = {
    label: get(config, 'terms.label', null),
    link: get(config, 'terms.link', null),
  };

  const symbols = get(options, 'serie.symbol'); // TOC triggered !
  const fonts = get(config, 'fonts.chart.chartLegend', {});
  const symbolLegend = isSymbol(type) && symbols
    ? <SymbolLegend
        data={data}
        fonts={fonts}
        symbols={symbols}
        type={type}
      />
    : null;

  const layerSeries = get(options, 'serie.stacked.layerSeries', []);
  const stackedLegend = isStackedBar && !isEmpty(layerSeries)
    ? <StackedLegend
        fonts={fonts}
        layerSeries={layerSeries}
        type={type}
      />
    : null;

  const choroLegend = isChoropleth
    ? <ChoroLegend
        data={data}
        fonts={fonts}
        options={options}
        type={type}
        width={width}
      />
    : null;

  return (
    <GSisFooter
      fonts={get(config, 'fonts.footer', {})}
      logo={logo}
      source={source}
      owner={owner}
      terms={terms}
      legend={
        <div>
          <AxisLegend axis="x" data={data} fonts={get(config, 'fonts.chart.axisLegend', {})} type={type} />
          <FocusLegend data={data} fonts={fonts} type={type} width={width} />
          {symbolLegend}
          {stackedLegend}
          {choroLegend}
        </div>
      }
      width={width}
    />
  );
};

Footer.propTypes = {
  width: PropTypes.number,
  data: PropTypes.object,
  options: PropTypes.object,
  type: PropTypes.string,
  config: PropTypes.object,
};

export default Footer;
