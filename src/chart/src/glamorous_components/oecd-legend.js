import glamorous from 'glamorous';

export const OecdChartLegend = glamorous.div({
  display: 'flex',
  flexDirection: 'row',
  flexWrap: 'wrap',
  fontFamily: 'bernino-sans-narrow-regular',
  fontSize: 14,
  justifyContent: 'flex-start',
}, ({ fonts={} }) => ({
  ...fonts
}));

export const OecdChartLegendEntry = glamorous.div({
  alignItems: 'center',
  display: 'flex',
  flexDirection: 'row',
  flexWrap: 'wrap',
  marginRight: 5,
  marginLeft: 5,
}); 

export const StyledSvg = glamorous.svg({
  marginRight: 5,
  marginLeft: 5,
})