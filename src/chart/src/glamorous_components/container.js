import glamorous from 'glamorous';

export default glamorous.div({
  borderColor: '#0297C9',
  borderBottomWidth: 1,
  borderLeftWidth: 0,
  borderTopWidth: 1,
  borderRightWidth: 0,
  borderStyle: 'solid',
  '& svg text::selection': {
    background: 'none'
  },
}, ({ height, width }) => ({
  height,
  width,
}));
