import React from 'react';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';
import { isScatter, getAxisLabel } from '../utils';

const XSpan = glamorous.span({
  color: '#7A7A7A',
  fontFamily: 'bernino-sans-narrow-bold',
  fontSize: 12,
}, ({ fonts={} }) => ({
  ...fonts
}));

const YSpan = glamorous(XSpan)({
  left: 10,
  position: 'absolute',
  top: 10,
});

const AxisLegend = ({ axis, data, fonts, type }) => {
  if (!isScatter(type))
    return null;
  switch (axis) {
    case 'x':
      return <XSpan fonts={fonts}>→ {getAxisLabel('x')(data)}</XSpan>;
    case 'y':
      return <YSpan fonts={fonts}>↑ {getAxisLabel('y')(data)}</YSpan>;
    default:
      return null;
  }
};

AxisLegend.propTypes = {
  axis: PropTypes.string.isRequired,
  data: PropTypes.object.isRequired,
  fonts: PropTypes.object,
  type: PropTypes.string.isRequired,
};

export default AxisLegend;
