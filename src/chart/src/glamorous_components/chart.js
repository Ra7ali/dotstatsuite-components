import React from 'react';
import PropTypes from 'prop-types';
import * as charts from '../../../bridge-d3-react/src';
import { get, isNumber } from 'lodash';
import { getChartClass, getOptions } from '../utils';
import filterSeriesRegardingDimensions from '../utils/filterSeries';
import { AxisLegend } from '.';
import { isScatter } from '../utils';
import glamorous from 'glamorous';

const Container = glamorous.div({
  backgroundColor: 'none',
  overflow: 'hidden',
  position: 'relative',
  direction: 'ltr !important',
}, ({ type }) => {
  if (!isScatter(type))
    return ({});
  return ({
    '& .rcw-chart__chart__line--x:last-of-type': {
      visibility: 'hidden',
    },
    '& .rcw-chart__chart__line--y:last-of-type': {
      visibility: 'hidden',
    },
    '& .rcw-chart__chart__axis--x': {
      '& .tick:last-of-type': {
        visibility: 'hidden',
      }
    },
    '& .rcw-chart__chart__axis--y': {
      '& .tick:last-of-type': {
        visibility: 'hidden',
      }
    },
  });
});

const Chart = ({ config, data, getInitialOptions, options, type, width }) => {
  const chartOptions = getOptions(options, data, type, config);
  const filteredSeries = filterSeriesRegardingDimensions(data.series, type, chartOptions);
  const ChartClass = charts[getChartClass(type)];

  return (
    <Container type={type}>
      <ChartClass
        data={filteredSeries}
        getInitialOptions={getInitialOptions}
        options={chartOptions}
      />
      {
        (isNumber(width) && width < 370)
        ? null
        : <AxisLegend axis="y" data={data} fonts={get(config, 'fonts.chart.axisLegend', {})} type={type} />
      }
    </Container>
  );
};

Chart.propTypes = {
  config: PropTypes.object,
  data: PropTypes.object,
  getInitialOptions: PropTypes.func,
  options: PropTypes.object,
  type: PropTypes.string
};

export default Chart;
