import React from 'react';
import { each, get, reduce, size, split } from 'lodash';
import { hasFocusLegend } from '../utils';
import { FOCUS_COLORS, REDUCED_THRESHOLD } from '../constants';
import { OecdChartLegend, OecdChartLegendEntry, StyledSvg } from '../glamorous_components';

const circle = (color) => (
  <circle cx='7' cy='7' r='4' stroke={color} fill={color} />
);

const line = (color) => (
  <line x1='0' y1='7' x2='11' y2='7' stroke={color} strokeWidth='2' />
);

const marker = (type, color) => {
  if (type === 'ScatterChart') {
    return circle(color);
  }
  else if (type === 'TimelineChart') {
    return line(color);
  }
  return null;
}

const indKey = (key) => {
  if (key === 'baseline') {
    return 10;
  }
  else if (key === 'highlight') {
    return 20;
  }
  return 0;
} 

const legendLabel = (type, selectionLabel) => {
  if (type === 'ScatterChart') {
    const [dimensionName, dimensionValueName] = split(selectionLabel, ' - ');
    return(dimensionValueName);
  }
  return selectionLabel;
}

const Legend = ({ data, fonts, type, width }) => {
  if (!(hasFocusLegend(type)) || (width < REDUCED_THRESHOLD))
    return null;

  const baseline = get(data, 'share.focused.baseline', []);
  const highlight = get(data, 'share.focused.highlight', []);
  // stupid, but it's for having baseline first in the legend

  const legend = reduce(
    { baseline, highlight },
    (memo, selection, key) => {
      if (size(selection) > size(FOCUS_COLORS[`${key}Colors`]))
        return memo;
      each(selection,
        (entry, index) => {
          const color = FOCUS_COLORS[`${key}Colors`][index];
          const label = legendLabel(type, entry.label);
          if (label !== 'Series') {
            memo.push(
              <OecdChartLegendEntry style={{color}} key={indKey(key) + index}>
                <StyledSvg width='12' height='14'>
                  {marker(type, color)}  
                </StyledSvg>
                {label}
              </OecdChartLegendEntry>
            );
          }
        }
      );
      return memo;
    },
    []
  );

  return (
    <OecdChartLegend fonts={fonts}>
      {legend}
    </OecdChartLegend>
  )
}

export default Legend;