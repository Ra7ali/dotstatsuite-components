import React from 'react';
import PropTypes from 'prop-types';
import { get, map, round, size } from 'lodash';
import { getSymbolValues, isSymbol } from '../utils';
import { OecdChartLegend, OecdChartLegendEntry, StyledSvg } from '../glamorous_components';

const SymbolLegend = ({ data, fonts, symbols, type }) => {
  if (!isSymbol(type) || !symbols) return null;
  const symbolValues = getSymbolValues(data);
  const nbSymbols = size(symbolValues);
  const symbolMarkers = symbols.markers;
  const nbMarkers = size(symbolMarkers);
  const markerSize = symbols.markerDefaultSize;
  const markerDefaultStrokeWidth = symbols.markerDefaultStrokeWidth;
  const markerHeight = round(Math.sqrt(markerSize)) + 8;
  const legend = map(symbolValues, (symbolValue, _index) => {
    const index = _index % nbMarkers;
    const rotate = get(symbolMarkers[index], 'rotate', 0);
    const markerFill = symbolMarkers[index].style.fill === '#DBEBF2' ?
      'white' : symbolMarkers[index].style.fill;
    return (
      <OecdChartLegendEntry key={_index} >
        <StyledSvg width={`${markerHeight}`} height={`${markerHeight}`} >
          <path
            d={symbolMarkers[index].path(markerSize)}
            transform={`translate(${markerHeight / 2}, ${markerHeight / 2}) rotate(${rotate})`}
            fill={markerFill}
            stroke={symbolMarkers[index].style.stroke}
            strokeWidth={get(symbolMarkers[index], 'style.strokeWidth', markerDefaultStrokeWidth)} />
        </StyledSvg>
        {symbolValue}
      </OecdChartLegendEntry>
    );
  });

  return (
    <OecdChartLegend fonts={fonts}>
      {legend}
    </OecdChartLegend>
  );
}

export default SymbolLegend;
