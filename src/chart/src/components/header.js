import React from 'react';
import PropTypes from 'prop-types';
import { get } from 'lodash';
import SisHeader from '@sis-cc/dotstatsuite-ui-header';
import Share from '../../../share/src';
import { getShareData } from '../../../rules/src';

const Header = ({ data, config, options, type, translations, width }) => {
  const toggleInfoRequest = get(config, 'toggleInfoRequest', null);
  const meta = {
    title: get(data, 'title'),
    subtitle: get(data, 'subtitle'),
    width: get(options, 'base.width', NaN),
    height: get(options, 'base.height', NaN),
  };
  const shareData = getShareData(data, options, config, type);

  return (
    <SisHeader
      disclaimer={data.disclaimer}
      fonts={get(config, 'fonts.header', {})}
      subtitle={data.subtitle}
      title={data.title}
      toggleInfoRequest={toggleInfoRequest}
      uprs={data.uprs}
      width={width}
    >
      <Share data={shareData} config={config} meta={meta} translations={translations} type={type} />
    </SisHeader>
  );
};

Header.propTypes = {
  data: PropTypes.object,
  options: PropTypes.object,
  type: PropTypes.string,
  config: PropTypes.object,
  translations: PropTypes.object,
};

export default Header;