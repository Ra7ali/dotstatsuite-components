import React from 'react';
import PropTypes from 'prop-types';
import { get, merge, round } from 'lodash';
import { ChoroplethLegend } from '../../../bridge-d3-react/src';
import { getOptions } from '../utils';

const ChoroLegend = ({ type, options, data, width }) => {
  if (type !== 'ChoroplethChart') return null;

  const orient = width >= 400 ? 'bottom': 'left';
  const chroWidth = orient === 'left' ? 50 : 300;
  const height = orient === 'left' ? 200 : 30;
  const margin = orient === 'left' ? { top: 7, bottom: 7 } : { left: 15, right: 15 };

  const legendOptions = merge(
    getOptions(options, data, type),
    {
      legend: {
        choropleth: {
          width: chroWidth,
          height,
          margin,
          axis: {
            thickness: 0,
            orient,
            font: {
              family: 'bernino-sans-narrow-regular',
              color: '#575757',
              size: 10
            },
            tick: {
              size: 5,
              thickness: 0
            },
            format: {
              proc: d => round(d, 2)
            }
          }
        }
      }
    }
  );

  return (
    <ChoroplethLegend options={legendOptions} data={data.series} />
  );
}

ChoroLegend.propTypes = {
  width: PropTypes.number,
  data: PropTypes.object,
  options: PropTypes.object,
  type: PropTypes.string
};

export default ChoroLegend;
