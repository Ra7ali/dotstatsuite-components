import React from 'react';
import { get, map } from 'lodash';
import { OecdChartLegend, OecdChartLegendEntry, StyledSvg } from '../glamorous_components';


const Legend = ({ fonts, layerSeries, type }) => {
  if ((type !== 'StackedBarChart') || (!layerSeries)) {
    return null;
  }
  const legend = map(
    layerSeries,
    (layer, index) => (
      <OecdChartLegendEntry key={index}>
        <StyledSvg width='21' height='14'>
          <rect x='0' y='0' width='21' height='14' fill={get(layer, `baseColor`, null)} />
        </StyledSvg>
        {get(layer, 'label', '')}
      </OecdChartLegendEntry>
    )
  );
  return (
    <OecdChartLegend fonts={fonts}>{legend}</OecdChartLegend>
  );
}

export default Legend;
