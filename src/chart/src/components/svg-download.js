import React from 'react';
import { Button, Classes } from '@blueprintjs/core';
import { svgDownload } from '../utils/svg-download';

const SvgDownload = () => (
  <Button className={Classes.MINIMAL} onClick={svgDownload} iconName="download" />
);

export default SvgDownload;
