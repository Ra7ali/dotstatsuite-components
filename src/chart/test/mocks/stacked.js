export default () => ({
  footnotes: {
    source: 'http://dotstat.oecd.org/',
    sourceLabel: 'source'
  },
  series: [
  {
      datapoints: [
        { x: 'Serie 1', y: [21, 24, 63, 11, 11] },
        { x: 'Serie 2', y: [34, null, 15, 23, 9] },
        { x: 'Serie 3', y: [55, 65, 32, 35, 7] },
        { x: 'Serie 4', y: [62, 82, 27, 21, 13] },
        { x: 'Serie 5', y: [10, 73, 54, 24, 4], highlightIndex: 0 },
        { x: 'Serie 6', y: [0, 16, 92, 32, 9] },
        { x: 'Serie 7', y: [32, 53, 18, 34, 12] },
        { x: 'Serie 8', y: [-68, 53, 41, 21, -7] },
        { x: 'Serie 9', y: [42, 46, 83, 13, 15] },
        { x: 'Serie 10', y: [28, 38, 22, 17, 7], baselineIndex: 0 },
        { x: 'Serie 11', y: [36, 52, 62, 23, 14] },
        { x: 'Serie 12', y: [-29, -64, -33, -39, -10], highlightIndex: 1 }
      ],
      layerSeries: [
        { id: '1', label: 'layer 1' },
        { id: '2', label: 'layer 2' },
        { id: '3', label: 'layer 3' },
        { id: '4', label: 'layer 4' },
        { id: '5', label: 'layer 5' }
      ]
    }
  ],
  subtitle: ['Mean score 2015, Annual incl. OECD, all countries'],
  title: 'Science performance (PISA)'
});