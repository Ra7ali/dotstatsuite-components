export default () => ({
  title: 'Science performance (PISA)',
  subtitle: ['Mean score 2015, Annual incl. OECD, all countries'],
  footnotes: {
    source: 'http://data.oecd.org/',
    sourceLabel: 'data.oecd.org',
  },
  series: [
  {
    "datapoints": [
      {
        "value": -1.70548071959404,
        "category": "Total manufacturing, s.a. - Australia",
        "y": -1.70548071959404,
        "x": "Total manufacturing, s.a. - Australia",
        "key": "PRMNTO01 - AUS",
        "formatedValue": "-1.7054807",
        "highlightIndex": 0,
      },
      {
        "value": -0.958831808585503,
        "category": "Industrial production, s.a. - Finland",
        "y": -0.958831808585503,
        "x": "Industrial production, s.a. - Finland",
        "key": "PRINTO01 - FIN",
        "formatedValue": "-0.9588318"
      },
      {
        "value": -0.938640709195226,
        "category": "Total manufacturing, s.a. - Finland",
        "y": -0.938640709195226,
        "x": "Total manufacturing, s.a. - Finland",
        "key": "PRMNTO01 - FIN",
        "formatedValue": "-0.9386407"
      },
      {
        "value": -0.734931326089172,
        "category": "Industrial production, s.a. - Canada",
        "y": -0.734931326089172,
        "x": "Industrial production, s.a. - Canada",
        "key": "PRINTO01 - CAN",
        "formatedValue": "-0.7349313"
      },
      {
        "value": -0.0160655474335108,
        "category": "Industrial production, s.a. - Belgium",
        "y": -0.0160655474335108,
        "x": "Industrial production, s.a. - Belgium",
        "key": "PRINTO01 - BEL",
        "formatedValue": "-0.0160655"
      },
      {
        "value": 0.177647331428157,
        "category": "Total manufacturing, s.a. - Belgium",
        "y": 0.177647331428157,
        "x": "Total manufacturing, s.a. - Belgium",
        "key": "PRMNTO01 - BEL",
        "formatedValue": "0.1776473"
      },
      {
        "value": 0.178223109790082,
        "category": "Total manufacturing, s.a. - Canada",
        "y": 0.178223109790082,
        "x": "Total manufacturing, s.a. - Canada",
        "key": "PRMNTO01 - CAN",
        "formatedValue": "0.1782231"
      },
      {
        "value": 0.230172987500003,
        "category": "Industrial production, s.a. - Chile",
        "y": 0.230172987500003,
        "x": "Industrial production, s.a. - Chile",
        "key": "PRINTO01 - CHL",
        "formatedValue": "0.230173"
      },
      {
        "value": 0.286348587500551,
        "category": "Total manufacturing, s.a. - Chile",
        "y": 0.286348587500551,
        "x": "Total manufacturing, s.a. - Chile",
        "key": "PRMNTO01 - CHL",
        "formatedValue": "0.2863486"
      },
      {
        "value": 0.292360493199452,
        "category": "Industrial production, s.a. - Estonia",
        "y": 0.292360493199452,
        "x": "Industrial production, s.a. - Estonia",
        "key": "PRINTO01 - EST",
        "formatedValue": "0.2923605"
      },
      {
        "value": 1.18005263064255,
        "category": "Industrial production, s.a. - Australia",
        "y": 1.18005263064255,
        "x": "Industrial production, s.a. - Australia",
        "key": "PRINTO01 - AUS",
        "formatedValue": "1.1800526"
      },
      {
        "value": 1.18634492777014,
        "category": "Industrial production, s.a. - Denmark",
        "y": 1.18634492777014,
        "x": "Industrial production, s.a. - Denmark",
        "key": "PRINTO01 - DNK",
        "formatedValue": "1.1863449"
      },
      {
        "value": 1.4055978394304,
        "category": "Total manufacturing, s.a. - Estonia",
        "y": 1.4055978394304,
        "x": "Total manufacturing, s.a. - Estonia",
        "key": "PRMNTO01 - EST",
        "formatedValue": "1.4055978"
      },
      {
        "value": 1.92806480043649,
        "category": "Total manufacturing, s.a. - France",
        "y": 1.92806480043649,
        "x": "Total manufacturing, s.a. - France",
        "key": "PRMNTO01 - FRA",
        "formatedValue": "1.9280648"
      },
      {
        "value": 1.96360436882936,
        "category": "Industrial production, s.a. - France",
        "y": 1.96360436882936,
        "x": "Industrial production, s.a. - France",
        "key": "PRINTO01 - FRA",
        "formatedValue": "1.9636044"
      },
      {
        "value": 2.11995066296641,
        "category": "Industrial production, s.a. - Austria",
        "y": 2.11995066296641,
        "x": "Industrial production, s.a. - Austria",
        "key": "PRINTO01 - AUT",
        "formatedValue": "2.1199507"
      },
      {
        "value": 2.36545969532268,
        "category": "Total manufacturing, s.a. - Austria",
        "y": 2.36545969532268,
        "x": "Total manufacturing, s.a. - Austria",
        "key": "PRMNTO01 - AUT",
        "formatedValue": "2.3654597"
      },
      {
        "value": 2.47915590644139,
        "category": "Total manufacturing, s.a. - Denmark",
        "y": 2.47915590644139,
        "x": "Total manufacturing, s.a. - Denmark",
        "key": "PRMNTO01 - DNK",
        "formatedValue": "2.4791559"
      },
      {
        "value": 4.62318401937048,
        "category": "Industrial production, s.a. - Czech Republic",
        "y": 4.62318401937048,
        "x": "Industrial production, s.a. - Czech Republic",
        "key": "PRINTO01 - CZE",
        "formatedValue": "4.623184"
      },
      {
        "value": 6.01902272562258,
        "category": "Total manufacturing, s.a. - Czech Republic",
        "y": 6.01902272562258,
        "x": "Total manufacturing, s.a. - Czech Republic",
        "key": "PRMNTO01 - CZE",
        "formatedValue": "6.0190227"
      }
    ]
  }
]
});