export default () => ({
  title: 'Science performance (PISA)',
  subtitle: ['Mean score 2015, Annual incl. OECD, all countries'],
  footnotes: {
    source: 'http://data.oecd.org/',
    sourceLabel: 'data.oecd.org',
  },
  series: [
    { datapoints: [
        { category: 'Hungary', values: [5.5] },
        { category: 'Sweden', values: [5.6] },
        { category: 'Estonia', values: [5.7] },
        { category: 'Finland', values: [5.8] },
        { category: 'Belgium', values: [5.9] },
        { category: 'Turkey', values: [6] },
        { category: 'France', values: [6.5] },
        { category: 'Poland', values: [6.6] },
        { category: 'Slovenia', values: [6.8] },
        { category: 'European Union', values: [7.4], baselineIndex: 0 },
        { category: 'Euro area (18 countries)', values: [8.2] },
        { category: 'Ireland', values: [8.7], highlightIndex: 0 },
        { category: 'Italy', values: [8.9] },
        { category: 'Slovak Republic', values: [9.9] },
        { category: 'Portugal', values: [10.9] },
        { category: 'South Africa', values: [12.3], highlightIndex: 1 },
        { category: 'Spain (2 digits)', values: [16.86] }
      ],
      symbolValues: ['test1', 'test2', 'test3']
    }
  ]
});
