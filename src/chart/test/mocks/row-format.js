export default () => ({
  title: 'Science performance (PISA)',
  subtitle: ['Mean score 2015, Annual incl. OECD, all countries'],
  footnotes: {
    source: 'http://data.oecd.org/',
    sourceLabel: 'data.oecd.org',
  },
  series: [
  {
    "datapoints": [
      {
        "value": 6.01902272562258,
        "category": "Total manufacturing, s.a. - Czech Republic",
        "x": 6.01902272562258,
        "y": "Total manufacturing, s.a. - Czech Republic",
        "key": "PRMNTO01 - CZE",
        "formatedValue": "6.0190227"
      },
      {
        "value": 4.62318401937048,
        "category": "Industrial production, s.a. - Czech Republic",
        "x": 4.62318401937048,
        "y": "Industrial production, s.a. - Czech Republic",
        "key": "PRINTO01 - CZE",
        "formatedValue": "4.623184"
      },
      {
        "value": 2.47915590644139,
        "category": "Total manufacturing, s.a. - Denmark",
        "x": 2.47915590644139,
        "y": "Total manufacturing, s.a. - Denmark",
        "key": "PRMNTO01 - DNK",
        "formatedValue": "2.4791559"
      },
      {
        "value": 2.36545969532268,
        "category": "Total manufacturing, s.a. - Austria",
        "x": 2.36545969532268,
        "y": "Total manufacturing, s.a. - Austria",
        "key": "PRMNTO01 - AUT",
        "formatedValue": "2.3654597"
      },
      {
        "value": 2.11995066296641,
        "category": "Industrial production, s.a. - Austria",
        "x": 2.11995066296641,
        "y": "Industrial production, s.a. - Austria",
        "key": "PRINTO01 - AUT",
        "formatedValue": "2.1199507"
      },
      {
        "value": 1.96360436882936,
        "category": "Industrial production, s.a. - France",
        "x": 1.96360436882936,
        "y": "Industrial production, s.a. - France",
        "key": "PRINTO01 - FRA",
        "formatedValue": "1.9636044"
      },
      {
        "value": 1.92806480043649,
        "category": "Total manufacturing, s.a. - France",
        "x": 1.92806480043649,
        "y": "Total manufacturing, s.a. - France",
        "key": "PRMNTO01 - FRA",
        "formatedValue": "1.9280648"
      },
      {
        "value": 1.4055978394304,
        "category": "Total manufacturing, s.a. - Estonia",
        "x": 1.4055978394304,
        "y": "Total manufacturing, s.a. - Estonia",
        "key": "PRMNTO01 - EST",
        "formatedValue": "1.4055978"
      },
      {
        "value": 1.18634492777014,
        "category": "Industrial production, s.a. - Denmark",
        "x": 1.18634492777014,
        "y": "Industrial production, s.a. - Denmark",
        "key": "PRINTO01 - DNK",
        "formatedValue": "1.1863449"
      },
      {
        "value": 1.18005263064255,
        "category": "Industrial production, s.a. - Australia",
        "x": 1.18005263064255,
        "y": "Industrial production, s.a. - Australia",
        "key": "PRINTO01 - AUS",
        "formatedValue": "1.1800526"
      },
      {
        "value": 0.292360493199452,
        "category": "Industrial production, s.a. - Estonia",
        "x": 0.292360493199452,
        "y": "Industrial production, s.a. - Estonia",
        "key": "PRINTO01 - EST",
        "formatedValue": "0.2923605"
      },
      {
        "value": 0.286348587500551,
        "category": "Total manufacturing, s.a. - Chile",
        "x": 0.286348587500551,
        "y": "Total manufacturing, s.a. - Chile",
        "key": "PRMNTO01 - CHL",
        "formatedValue": "0.2863486"
      },
      {
        "value": 0.230172987500003,
        "category": "Industrial production, s.a. - Chile",
        "x": 0.230172987500003,
        "y": "Industrial production, s.a. - Chile",
        "key": "PRINTO01 - CHL",
        "formatedValue": "0.230173"
      },
      {
        "value": 0.178223109790082,
        "category": "Total manufacturing, s.a. - Canada",
        "x": 0.178223109790082,
        "y": "Total manufacturing, s.a. - Canada",
        "key": "PRMNTO01 - CAN",
        "formatedValue": "0.1782231"
      },
      {
        "value": 0.177647331428157,
        "category": "Total manufacturing, s.a. - Belgium",
        "x": 0.177647331428157,
        "y": "Total manufacturing, s.a. - Belgium",
        "key": "PRMNTO01 - BEL",
        "formatedValue": "0.1776473"
      },
      {
        "value": -0.0160655474335108,
        "category": "Industrial production, s.a. - Belgium",
        "x": -0.0160655474335108,
        "y": "Industrial production, s.a. - Belgium",
        "key": "PRINTO01 - BEL",
        "formatedValue": "-0.0160655"
      },
      {
        "value": -0.734931326089172,
        "category": "Industrial production, s.a. - Canada",
        "x": -0.734931326089172,
        "y": "Industrial production, s.a. - Canada",
        "key": "PRINTO01 - CAN",
        "formatedValue": "-0.7349313"
      },
      {
        "value": -0.938640709195226,
        "category": "Total manufacturing, s.a. - Finland",
        "x": -0.938640709195226,
        "y": "Total manufacturing, s.a. - Finland",
        "key": "PRMNTO01 - FIN",
        "formatedValue": "-0.9386407"
      },
      {
        "value": -0.958831808585503,
        "category": "Industrial production, s.a. - Finland",
        "x": -0.958831808585503,
        "y": "Industrial production, s.a. - Finland",
        "key": "PRINTO01 - FIN",
        "formatedValue": "-0.9588318"
      },
      {
        "value": -1.70548071959404,
        "category": "Total manufacturing, s.a. - Australia",
        "x": -1.70548071959404,
        "y": "Total manufacturing, s.a. - Australia",
        "key": "PRMNTO01 - AUS",
        "formatedValue": "-1.7054807"
      }
    ]
  }
]
});