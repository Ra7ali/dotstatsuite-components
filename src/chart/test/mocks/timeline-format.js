export default () => ({
  title: 'Science performance (PISA)',
  subtitle: ['Mean score 2015, Annual incl. OECD, all countries'],
  footnotes: {
    source: 'http://data.oecd.org/',
    sourceLabel: 'data.oecd.org',
  },
  frequency: 'quarterly',
  series: [
  {
    "datapoints": [
      {
        "x": "2014-12-31T23:00:00.000Z",
        "y": 0.609639177804482,
        "formatedValue": "0.6096392",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "AUS": {
            "id": "AUS",
            "name": "Australia"
          }
        }
      },
      {
        "x": "2015-03-31T22:00:00.000Z",
        "y": -0.886440440846983,
        "formatedValue": "-0.8864404",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "AUS": {
            "id": "AUS",
            "name": "Australia"
          }
        }
      },
      {
        "x": "2015-06-30T22:00:00.000Z",
        "y": 1.75180816521674,
        "formatedValue": "1.7518082",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "AUS": {
            "id": "AUS",
            "name": "Australia"
          }
        }
      },
      {
        "x": "2015-09-30T22:00:00.000Z",
        "y": -0.501823810381974,
        "formatedValue": "-0.5018238",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "AUS": {
            "id": "AUS",
            "name": "Australia"
          }
        }
      }
    ],
    "category": "Industrial production, s.a. - Australia",
    "key": "PRINTO01 - AUS"
  },
  {
    "datapoints": [
      {
        "x": "2014-12-31T23:00:00.000Z",
        "y": 1.74346201743464,
        "formatedValue": "1.743462",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "AUT": {
            "id": "AUT",
            "name": "Austria"
          }
        }
      },
      {
        "x": "2015-03-31T22:00:00.000Z",
        "y": -0.428396572827401,
        "formatedValue": "-0.4283966",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "AUT": {
            "id": "AUT",
            "name": "Austria"
          }
        }
      },
      {
        "x": "2015-06-30T22:00:00.000Z",
        "y": 0.215119852489256,
        "formatedValue": "0.2151199",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "AUT": {
            "id": "AUT",
            "name": "Austria"
          }
        }
      },
      {
        "x": "2015-09-30T22:00:00.000Z",
        "y": 0.306654400490624,
        "formatedValue": "0.3066544",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "AUT": {
            "id": "AUT",
            "name": "Austria"
          }
        }
      }
    ],
    "category": "Industrial production, s.a. - Austria",
    "key": "PRINTO01 - AUT"
  },
  {
    "datapoints": [
      {
        "x": "2014-12-31T23:00:00.000Z",
        "y": -0.580457916801026,
        "formatedValue": "-0.5804579",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "BEL": {
            "id": "BEL",
            "name": "Belgium"
          }
        }
      },
      {
        "x": "2015-03-31T22:00:00.000Z",
        "y": 0.0973078170613029,
        "formatedValue": "0.0973078",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "BEL": {
            "id": "BEL",
            "name": "Belgium"
          }
        }
      },
      {
        "x": "2015-06-30T22:00:00.000Z",
        "y": 0.453661697990936,
        "formatedValue": "0.4536617",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "BEL": {
            "id": "BEL",
            "name": "Belgium"
          }
        }
      },
      {
        "x": "2015-09-30T22:00:00.000Z",
        "y": 1.32258064516129,
        "formatedValue": "1.3225806",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "BEL": {
            "id": "BEL",
            "name": "Belgium"
          }
        }
      }
    ],
    "category": "Industrial production, s.a. - Belgium",
    "key": "PRINTO01 - BEL"
  },
  {
    "datapoints": [
      {
        "x": "2014-12-31T23:00:00.000Z",
        "y": -0.674970340629221,
        "formatedValue": "-0.6749703",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "CAN": {
            "id": "CAN",
            "name": "Canada"
          }
        }
      },
      {
        "x": "2015-03-31T22:00:00.000Z",
        "y": -1.55133353288431,
        "formatedValue": "-1.5513335",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "CAN": {
            "id": "CAN",
            "name": "Canada"
          }
        }
      },
      {
        "x": "2015-06-30T22:00:00.000Z",
        "y": 1.19169388970324,
        "formatedValue": "1.1916939",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "CAN": {
            "id": "CAN",
            "name": "Canada"
          }
        }
      },
      {
        "x": "2015-09-30T22:00:00.000Z",
        "y": -0.58460613961239,
        "formatedValue": "-0.5846061",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "CAN": {
            "id": "CAN",
            "name": "Canada"
          }
        }
      }
    ],
    "category": "Industrial production, s.a. - Canada",
    "key": "PRINTO01 - CAN"
  },
  {
    "datapoints": [
      {
        "x": "2014-12-31T23:00:00.000Z",
        "y": 1.59668835008869,
        "formatedValue": "1.5966884",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "CZE": {
            "id": "CZE",
            "name": "Czech Republic"
          }
        }
      },
      {
        "x": "2015-03-31T22:00:00.000Z",
        "y": 1.13504074505239,
        "formatedValue": "1.1350407",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "CZE": {
            "id": "CZE",
            "name": "Czech Republic"
          }
        }
      },
      {
        "x": "2015-06-30T22:00:00.000Z",
        "y": -0.374100719424463,
        "formatedValue": "-0.3741007",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "CZE": {
            "id": "CZE",
            "name": "Czech Republic"
          }
        }
      },
      {
        "x": "2015-09-30T22:00:00.000Z",
        "y": 0.77989601386481,
        "formatedValue": "0.779896",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "CZE": {
            "id": "CZE",
            "name": "Czech Republic"
          }
        }
      }
    ],
    "category": "Industrial production, s.a. - Czech Republic",
    "key": "PRINTO01 - CZE"
  },
  {
    "datapoints": [
      {
        "x": "2014-12-31T23:00:00.000Z",
        "y": 0.0962155227710083,
        "formatedValue": "0.0962155",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "DNK": {
            "id": "DNK",
            "name": "Denmark"
          }
        }
      },
      {
        "x": "2015-03-31T22:00:00.000Z",
        "y": 2.14674783723163,
        "formatedValue": "2.1467478",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "DNK": {
            "id": "DNK",
            "name": "Denmark"
          }
        }
      },
      {
        "x": "2015-06-30T22:00:00.000Z",
        "y": -1.09786700125469,
        "formatedValue": "-1.097867",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "DNK": {
            "id": "DNK",
            "name": "Denmark"
          }
        }
      },
      {
        "x": "2015-09-30T22:00:00.000Z",
        "y": -3.23501427212179,
        "formatedValue": "-3.2350143",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "DNK": {
            "id": "DNK",
            "name": "Denmark"
          }
        }
      }
    ],
    "category": "Industrial production, s.a. - Denmark",
    "key": "PRINTO01 - DNK"
  },
  {
    "datapoints": [
      {
        "x": "2014-12-31T23:00:00.000Z",
        "y": -1.06082036775106,
        "formatedValue": "-1.0608204",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "FIN": {
            "id": "FIN",
            "name": "Finland"
          }
        }
      },
      {
        "x": "2015-03-31T22:00:00.000Z",
        "y": -0.107219442458899,
        "formatedValue": "-0.1072194",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "FIN": {
            "id": "FIN",
            "name": "Finland"
          }
        }
      },
      {
        "x": "2015-06-30T22:00:00.000Z",
        "y": 0.536672629695886,
        "formatedValue": "0.5366726",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "FIN": {
            "id": "FIN",
            "name": "Finland"
          }
        }
      },
      {
        "x": "2015-09-30T22:00:00.000Z",
        "y": 0.21352313167261,
        "formatedValue": "0.2135231",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "FIN": {
            "id": "FIN",
            "name": "Finland"
          }
        }
      }
    ],
    "category": "Industrial production, s.a. - Finland",
    "key": "PRINTO01 - FIN"
  },
  {
    "datapoints": [
      {
        "x": "2014-12-31T23:00:00.000Z",
        "y": 1.12608827187469,
        "formatedValue": "1.1260883",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "FRA": {
            "id": "FRA",
            "name": "France"
          }
        }
      },
      {
        "x": "2015-03-31T22:00:00.000Z",
        "y": 0.302486371493158,
        "formatedValue": "0.3024864",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "FRA": {
            "id": "FRA",
            "name": "France"
          }
        }
      },
      {
        "x": "2015-06-30T22:00:00.000Z",
        "y": 0.762220381110196,
        "formatedValue": "0.7622204",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "FRA": {
            "id": "FRA",
            "name": "France"
          }
        }
      },
      {
        "x": "2015-09-30T22:00:00.000Z",
        "y": 0.266403552047352,
        "formatedValue": "0.2664036",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "FRA": {
            "id": "FRA",
            "name": "France"
          }
        }
      }
    ],
    "category": "Industrial production, s.a. - France",
    "key": "PRINTO01 - FRA"
  },
  {
    "datapoints": [
      {
        "x": "2014-12-31T23:00:00.000Z",
        "y": -1.43976236982363,
        "formatedValue": "-1.4397624",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "CHL": {
            "id": "CHL",
            "name": "Chile"
          }
        }
      },
      {
        "x": "2015-03-31T22:00:00.000Z",
        "y": 5.09368342961992,
        "formatedValue": "5.0936834",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "CHL": {
            "id": "CHL",
            "name": "Chile"
          }
        }
      },
      {
        "x": "2015-06-30T22:00:00.000Z",
        "y": -1.89124404695679,
        "formatedValue": "-1.891244",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "CHL": {
            "id": "CHL",
            "name": "Chile"
          }
        }
      },
      {
        "x": "2015-09-30T22:00:00.000Z",
        "y": -1.04018078102806,
        "formatedValue": "-1.0401808",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "CHL": {
            "id": "CHL",
            "name": "Chile"
          }
        }
      }
    ],
    "category": "Industrial production, s.a. - Chile",
    "key": "PRINTO01 - CHL"
  },
  {
    "datapoints": [
      {
        "x": "2014-12-31T23:00:00.000Z",
        "y": -0.797209765819618,
        "formatedValue": "-0.7972098",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "EST": {
            "id": "EST",
            "name": "Estonia"
          }
        }
      },
      {
        "x": "2015-03-31T22:00:00.000Z",
        "y": 0.452034153691591,
        "formatedValue": "0.4520342",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "EST": {
            "id": "EST",
            "name": "Estonia"
          }
        }
      },
      {
        "x": "2015-06-30T22:00:00.000Z",
        "y": -1.27499999999999,
        "formatedValue": "-1.275",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "EST": {
            "id": "EST",
            "name": "Estonia"
          }
        }
      },
      {
        "x": "2015-09-30T22:00:00.000Z",
        "y": -1.5193719929096,
        "formatedValue": "-1.519372",
        "dimensionValues": {
          "PRINTO01": {
            "id": "PRINTO01",
            "name": "Industrial production, s.a."
          },
          "EST": {
            "id": "EST",
            "name": "Estonia"
          }
        }
      }
    ],
    "category": "Industrial production, s.a. - Estonia",
    "key": "PRINTO01 - EST"
  },
  {
    "datapoints": [
      {
        "x": "2014-12-31T23:00:00.000Z",
        "y": -1.01419102000824,
        "formatedValue": "-1.014191",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "CAN": {
            "id": "CAN",
            "name": "Canada"
          }
        }
      },
      {
        "x": "2015-03-31T22:00:00.000Z",
        "y": -0.547861522877197,
        "formatedValue": "-0.5478615",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "CAN": {
            "id": "CAN",
            "name": "Canada"
          }
        }
      },
      {
        "x": "2015-06-30T22:00:00.000Z",
        "y": 0.986296044319164,
        "formatedValue": "0.986296",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "CAN": {
            "id": "CAN",
            "name": "Canada"
          }
        }
      },
      {
        "x": "2015-09-30T22:00:00.000Z",
        "y": -0.00500458111654567,
        "formatedValue": "-0.0050046",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "CAN": {
            "id": "CAN",
            "name": "Canada"
          }
        }
      }
    ],
    "category": "Total manufacturing, s.a. - Canada",
    "key": "PRMNTO01 - CAN"
  },
  {
    "datapoints": [
      {
        "x": "2014-12-31T23:00:00.000Z",
        "y": -0.385019250962552,
        "formatedValue": "-0.3850193",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "FIN": {
            "id": "FIN",
            "name": "Finland"
          }
        }
      },
      {
        "x": "2015-03-31T22:00:00.000Z",
        "y": -0.562192550948708,
        "formatedValue": "-0.5621926",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "FIN": {
            "id": "FIN",
            "name": "Finland"
          }
        }
      },
      {
        "x": "2015-06-30T22:00:00.000Z",
        "y": -0.353356890459349,
        "formatedValue": "-0.3533569",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "FIN": {
            "id": "FIN",
            "name": "Finland"
          }
        }
      },
      {
        "x": "2015-09-30T22:00:00.000Z",
        "y": 0.780141843971616,
        "formatedValue": "0.7801418",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "FIN": {
            "id": "FIN",
            "name": "Finland"
          }
        }
      }
    ],
    "category": "Total manufacturing, s.a. - Finland",
    "key": "PRMNTO01 - FIN"
  },
  {
    "datapoints": [
      {
        "x": "2014-12-31T23:00:00.000Z",
        "y": -0.21896383186705,
        "formatedValue": "-0.2189638",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "AUS": {
            "id": "AUS",
            "name": "Australia"
          }
        }
      },
      {
        "x": "2015-03-31T22:00:00.000Z",
        "y": -0.46240056428544,
        "formatedValue": "-0.4624006",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "AUS": {
            "id": "AUS",
            "name": "Australia"
          }
        }
      },
      {
        "x": "2015-06-30T22:00:00.000Z",
        "y": -0.566906814692331,
        "formatedValue": "-0.5669068",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "AUS": {
            "id": "AUS",
            "name": "Australia"
          }
        }
      },
      {
        "x": "2015-09-30T22:00:00.000Z",
        "y": -1.96777131092371,
        "formatedValue": "-1.9677713",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "AUS": {
            "id": "AUS",
            "name": "Australia"
          }
        }
      }
    ],
    "category": "Total manufacturing, s.a. - Australia",
    "key": "PRMNTO01 - AUS"
  },
  {
    "datapoints": [
      {
        "x": "2014-12-31T23:00:00.000Z",
        "y": 0.575677620532518,
        "formatedValue": "0.5756776",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "EST": {
            "id": "EST",
            "name": "Estonia"
          }
        }
      },
      {
        "x": "2015-03-31T22:00:00.000Z",
        "y": -0.977820176484621,
        "formatedValue": "-0.9778202",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "EST": {
            "id": "EST",
            "name": "Estonia"
          }
        }
      },
      {
        "x": "2015-06-30T22:00:00.000Z",
        "y": -0.91522157996146,
        "formatedValue": "-0.9152216",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "EST": {
            "id": "EST",
            "name": "Estonia"
          }
        }
      },
      {
        "x": "2015-09-30T22:00:00.000Z",
        "y": -0.0486144871171718,
        "formatedValue": "-0.0486145",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "EST": {
            "id": "EST",
            "name": "Estonia"
          }
        }
      }
    ],
    "category": "Total manufacturing, s.a. - Estonia",
    "key": "PRMNTO01 - EST"
  },
  {
    "datapoints": [
      {
        "x": "2014-12-31T23:00:00.000Z",
        "y": -1.48011100832563,
        "formatedValue": "-1.480111",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "BEL": {
            "id": "BEL",
            "name": "Belgium"
          }
        }
      },
      {
        "x": "2015-03-31T22:00:00.000Z",
        "y": 1.0015649452269,
        "formatedValue": "1.0015649",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "BEL": {
            "id": "BEL",
            "name": "Belgium"
          }
        }
      },
      {
        "x": "2015-06-30T22:00:00.000Z",
        "y": 1.11558723272388,
        "formatedValue": "1.1155872",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "BEL": {
            "id": "BEL",
            "name": "Belgium"
          }
        }
      },
      {
        "x": "2015-09-30T22:00:00.000Z",
        "y": -0.704872816426587,
        "formatedValue": "-0.7048728",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "BEL": {
            "id": "BEL",
            "name": "Belgium"
          }
        }
      }
    ],
    "category": "Total manufacturing, s.a. - Belgium",
    "key": "PRMNTO01 - BEL"
  },
  {
    "datapoints": [
      {
        "x": "2014-12-31T23:00:00.000Z",
        "y": -1.4397623698236,
        "formatedValue": "-1.4397624",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "CHL": {
            "id": "CHL",
            "name": "Chile"
          }
        }
      },
      {
        "x": "2015-03-31T22:00:00.000Z",
        "y": 5.09368342961989,
        "formatedValue": "5.0936834",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "CHL": {
            "id": "CHL",
            "name": "Chile"
          }
        }
      },
      {
        "x": "2015-06-30T22:00:00.000Z",
        "y": -1.89124404695682,
        "formatedValue": "-1.891244",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "CHL": {
            "id": "CHL",
            "name": "Chile"
          }
        }
      },
      {
        "x": "2015-09-30T22:00:00.000Z",
        "y": -1.04018078102803,
        "formatedValue": "-1.0401808",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "CHL": {
            "id": "CHL",
            "name": "Chile"
          }
        }
      }
    ],
    "category": "Total manufacturing, s.a. - Chile",
    "key": "PRMNTO01 - CHL"
  },
  {
    "datapoints": [
      {
        "x": "2014-12-31T23:00:00.000Z",
        "y": 1.80097680097681,
        "formatedValue": "1.8009768",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "AUT": {
            "id": "AUT",
            "name": "Austria"
          }
        }
      },
      {
        "x": "2015-03-31T22:00:00.000Z",
        "y": 0.0899550224887536,
        "formatedValue": "0.089955",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "AUT": {
            "id": "AUT",
            "name": "Austria"
          }
        }
      },
      {
        "x": "2015-06-30T22:00:00.000Z",
        "y": 0.808867585380489,
        "formatedValue": "0.8088676",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "AUT": {
            "id": "AUT",
            "name": "Austria"
          }
        }
      },
      {
        "x": "2015-09-30T22:00:00.000Z",
        "y": 0.1188707280832,
        "formatedValue": "0.1188707",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "AUT": {
            "id": "AUT",
            "name": "Austria"
          }
        }
      }
    ],
    "category": "Total manufacturing, s.a. - Austria",
    "key": "PRMNTO01 - AUT"
  },
  {
    "datapoints": [
      {
        "x": "2014-12-31T23:00:00.000Z",
        "y": 0.763582966226142,
        "formatedValue": "0.763583",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "DNK": {
            "id": "DNK",
            "name": "Denmark"
          }
        }
      },
      {
        "x": "2015-03-31T22:00:00.000Z",
        "y": 1.98192946662781,
        "formatedValue": "1.9819295",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "DNK": {
            "id": "DNK",
            "name": "Denmark"
          }
        }
      },
      {
        "x": "2015-06-30T22:00:00.000Z",
        "y": 0.342955130037158,
        "formatedValue": "0.3429551",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "DNK": {
            "id": "DNK",
            "name": "Denmark"
          }
        }
      },
      {
        "x": "2015-09-30T22:00:00.000Z",
        "y": -3.01908288236968,
        "formatedValue": "-3.0190829",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "DNK": {
            "id": "DNK",
            "name": "Denmark"
          }
        }
      }
    ],
    "category": "Total manufacturing, s.a. - Denmark",
    "key": "PRMNTO01 - DNK"
  },
  {
    "datapoints": [
      {
        "x": "2014-12-31T23:00:00.000Z",
        "y": 0.743804409459331,
        "formatedValue": "0.7438044",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "FRA": {
            "id": "FRA",
            "name": "France"
          }
        }
      },
      {
        "x": "2015-03-31T22:00:00.000Z",
        "y": 0.725069527214925,
        "formatedValue": "0.7250695",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "FRA": {
            "id": "FRA",
            "name": "France"
          }
        }
      },
      {
        "x": "2015-06-30T22:00:00.000Z",
        "y": 0.650823390198221,
        "formatedValue": "0.6508234",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "FRA": {
            "id": "FRA",
            "name": "France"
          }
        }
      },
      {
        "x": "2015-09-30T22:00:00.000Z",
        "y": 0.502922830737064,
        "formatedValue": "0.5029228",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "FRA": {
            "id": "FRA",
            "name": "France"
          }
        }
      }
    ],
    "category": "Total manufacturing, s.a. - France",
    "key": "PRMNTO01 - FRA"
  },
  {
    "datapoints": [
      {
        "x": "2014-12-31T23:00:00.000Z",
        "y": 1.69635284139101,
        "formatedValue": "1.6963528",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "CZE": {
            "id": "CZE",
            "name": "Czech Republic"
          }
        }
      },
      {
        "x": "2015-03-31T22:00:00.000Z",
        "y": 1.91826522101751,
        "formatedValue": "1.9182652",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "CZE": {
            "id": "CZE",
            "name": "Czech Republic"
          }
        }
      },
      {
        "x": "2015-06-30T22:00:00.000Z",
        "y": -0.109110747408626,
        "formatedValue": "-0.1091107",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "CZE": {
            "id": "CZE",
            "name": "Czech Republic"
          }
        }
      },
      {
        "x": "2015-09-30T22:00:00.000Z",
        "y": 1.33806663025671,
        "formatedValue": "1.3380666",
        "dimensionValues": {
          "PRMNTO01": {
            "id": "PRMNTO01",
            "name": "Total manufacturing, s.a."
          },
          "CZE": {
            "id": "CZE",
            "name": "Czech Republic"
          }
        }
      }
    ],
    "category": "Total manufacturing, s.a. - Czech Republic",
    "key": "PRMNTO01 - CZE"
  }
]
});
