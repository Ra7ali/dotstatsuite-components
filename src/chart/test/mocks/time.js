export default () => ({
  title: 'Science performance (PISA)',
  subtitle: ['Mean score 2015, Annual incl. OECD, all countries'],
  footnotes: {
    source: 'http://data.oecd.org/',
    sourceLabel: 'data.oecd.org',
  },
  series: [
    {
      datapoints: [
        {
          x: new Date(2010, 0, 1), y: 10,
          // relevant dimension values that are used for x and y
          // used for annotation and tooltip
          dimensionValues: {
            LOCATION: { id: 'AUS', name: 'Australia' },
            TIME_PERIOD: { id: '2010', name: '2010' }
          }
        },
        {
          x: new Date(2011, 0, 1), y: 8,
          dimensionValues: {
            LOCATION: { id: 'AUS', name: 'Australia' },
            TIME_PERIOD: { id: '2011', name: '2011' }
          }
        },
        {
          x: new Date(2012, 0, 1), y: 9,
          dimensionValues: {
            LOCATION: { id: 'AUS', name: 'Australia' },
            TIME_PERIOD: { id: '2012', name: '2012' }
          }
        },
        {
          x: new Date(2013, 0, 1), y: 10.5,
          dimensionValues: {
            LOCATION: { id: 'AUS', name: 'Australia' },
            TIME_PERIOD: { id: '2013', name: '2013' }
          }
        },
        {
          x: new Date(2014, 0, 1), y: 12.77789987899,
          dimensionValues: {
            LOCATION: { id: 'AUS', name: 'Australia' },
            TIME_PERIOD: { id: '2014', name: '2014' }
          }
        }
      ],
      // in line, natural focus is on series and not datapoints
      // expected value is an index (Number)
      // baseline overrides highlight is index is equal
      highlightIndex: 0, baselineIndex: -1
    }
  ]
});
