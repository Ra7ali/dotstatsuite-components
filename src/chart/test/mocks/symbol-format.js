export default () => ({
  title: 'Science performance (PISA)',
  subtitle: ['Mean score 2015, Annual incl. OECD, all countries'],
  footnotes: {
    source: 'http://data.oecd.org/',
    sourceLabel: 'data.oecd.org',
  },
  series: [
  {
    "datapoints": [
      {
        "category": "Czech Republic",
        "formatedValues": [
          "4.623184",
          "6.0190227"
        ],
        "values": [
          4.62318401937048,
          6.01902272562258
        ],
        "key": "CZE"
      },
      {
        "category": "Austria",
        "formatedValues": [
          "2.1199507",
          "2.3654597"
        ],
        "values": [
          2.11995066296641,
          2.36545969532268
        ],
        "key": "AUT"
      },
      {
        "category": "France",
        "formatedValues": [
          "1.9636044",
          "1.9280648"
        ],
        "values": [
          1.96360436882936,
          1.92806480043649
        ],
        "key": "FRA"
      },
      {
        "category": "Denmark",
        "formatedValues": [
          "1.1863449",
          "2.4791559"
        ],
        "values": [
          1.18634492777014,
          2.47915590644139
        ],
        "key": "DNK"
      },
      {
        "category": "Australia",
        "formatedValues": [
          "1.1800526",
          "-1.7054807"
        ],
        "values": [
          1.18005263064255,
          -1.70548071959404
        ],
        "key": "AUS"
      },
      {
        "category": "Estonia",
        "formatedValues": [
          "0.2923605",
          "1.4055978"
        ],
        "values": [
          0.292360493199452,
          1.4055978394304
        ],
        "key": "EST"
      },
      {
        "category": "Chile",
        "formatedValues": [
          "0.230173",
          "0.2863486"
        ],
        "values": [
          0.230172987500003,
          0.286348587500551
        ],
        "key": "CHL"
      },
      {
        "category": "Belgium",
        "formatedValues": [
          "-0.0160655",
          "0.1776473"
        ],
        "values": [
          -0.0160655474335108,
          0.177647331428157
        ],
        "key": "BEL"
      },
      {
        "category": "Canada",
        "formatedValues": [
          "-0.7349313",
          "0.1782231"
        ],
        "values": [
          -0.734931326089172,
          0.178223109790082
        ],
        "key": "CAN"
      },
      {
        "category": "Finland",
        "formatedValues": [
          "-0.9588318",
          "-0.9386407"
        ],
        "values": [
          -0.958831808585503,
          -0.938640709195226
        ],
        "key": "FIN"
      }
    ],
    "symbolValues": [
      "Industrial production, s.a.",
      "Total manufacturing, s.a."
    ]
  }
]
});
