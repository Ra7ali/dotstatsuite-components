export default () => ({
  "title": "Growth previous period - Annual - 2015",
  "subtitle": ["this is a subtitle"],
  "series": [
    {
      "datapoints": [
        {
          "value": -1.70548071959404,
          "category": "Total manufacturing, s.a. - Australia",
          "y": -1.70548071959404,
          "x": "Total manufacturing, s.a. - Australia"
        },
        {
          "value": -0.958831808585503,
          "category": "Industrial production, s.a. - Finland",
          "y": -0.958831808585503,
          "x": "Industrial production, s.a. - Finland"
        },
        {
          "value": -0.938640709195226,
          "category": "Total manufacturing, s.a. - Finland",
          "y": -0.938640709195226,
          "x": "Total manufacturing, s.a. - Finland"
        },
        {
          "value": -0.734931326089172,
          "category": "Industrial production, s.a. - Canada",
          "y": -0.734931326089172,
          "x": "Industrial production, s.a. - Canada"
        },
        {
          "value": -0.0160655474335108,
          "category": "Industrial production, s.a. - Belgium",
          "y": -0.0160655474335108,
          "x": "Industrial production, s.a. - Belgium"
        },
        {
          "value": 0.177647331428157,
          "category": "Total manufacturing, s.a. - Belgium",
          "y": 0.177647331428157,
          "x": "Total manufacturing, s.a. - Belgium"
        },
        {
          "value": 0.178223109790082,
          "category": "Total manufacturing, s.a. - Canada",
          "y": 0.178223109790082,
          "x": "Total manufacturing, s.a. - Canada"
        },
        {
          "value": 0.230172987500003,
          "category": "Industrial production, s.a. - Chile",
          "y": 0.230172987500003,
          "x": "Industrial production, s.a. - Chile"
        },
        {
          "value": 0.286348587500551,
          "category": "Total manufacturing, s.a. - Chile",
          "y": 0.286348587500551,
          "x": "Total manufacturing, s.a. - Chile"
        },
        {
          "value": 0.292360493199452,
          "category": "Industrial production, s.a. - Estonia",
          "y": 0.292360493199452,
          "x": "Industrial production, s.a. - Estonia"
        },
        {
          "value": 1.18005263064255,
          "category": "Industrial production, s.a. - Australia",
          "y": 1.18005263064255,
          "x": "Industrial production, s.a. - Australia"
        },
        {
          "value": 1.18634492777014,
          "category": "Industrial production, s.a. - Denmark",
          "y": 1.18634492777014,
          "x": "Industrial production, s.a. - Denmark"
        },
        {
          "value": 1.4055978394304,
          "category": "Total manufacturing, s.a. - Estonia",
          "y": 1.4055978394304,
          "x": "Total manufacturing, s.a. - Estonia"
        },
        {
          "value": 1.92806480043649,
          "category": "Total manufacturing, s.a. - France",
          "y": 1.92806480043649,
          "x": "Total manufacturing, s.a. - France"
        },
        {
          "value": 1.96360436882936,
          "category": "Industrial production, s.a. - France",
          "y": 1.96360436882936,
          "x": "Industrial production, s.a. - France"
        },
        {
          "value": 2.11995066296641,
          "category": "Industrial production, s.a. - Austria",
          "y": 2.11995066296641,
          "x": "Industrial production, s.a. - Austria"
        },
        {
          "value": 2.36545969532268,
          "category": "Total manufacturing, s.a. - Austria",
          "y": 2.36545969532268,
          "x": "Total manufacturing, s.a. - Austria"
        },
        {
          "value": 2.47915590644139,
          "category": "Total manufacturing, s.a. - Denmark",
          "y": 2.47915590644139,
          "x": "Total manufacturing, s.a. - Denmark"
        },
        {
          "value": 4.62318401937048,
          "category": "Industrial production, s.a. - Czech Republic",
          "y": 4.62318401937048,
          "x": "Industrial production, s.a. - Czech Republic"
        },
        {
          "value": 6.01902272562258,
          "category": "Total manufacturing, s.a. - Czech Republic",
          "y": 6.01902272562258,
          "x": "Total manufacturing, s.a. - Czech Republic"
        }
      ]
    }
  ],
  "footnotes": {
    "source": "http://dotstat.oecd.org/",
    "sourceLabel": "source"
  }
});