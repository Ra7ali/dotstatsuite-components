export default () => ({
  "title": "Mathematics performance (PISA) - Mean score - Annual - 2015",
  "subtitle": ['Mean score 2015, Annual incl. OECD, all countries'],
  "series": [
    {
      "datapoints": [
        {
          "category": "Brazil",
          "values": [
            385,
            370
          ]
        },
        {
          "category": "Indonesia",
          "values": [
            385,
            387
          ]
        },
        {
          "category": "Peru",
          "values": [
            391,
            382
          ]
        },
        {
          "category": "Colombia",
          "values": [
            395,
            384
          ]
        },
        {
          "category": "Mexico",
          "values": [
            412,
            404
          ]
        },
        {
          "category": "Turkey",
          "values": [
            423,
            418
          ]
        },
        {
          "category": "Chile",
          "values": [
            432,
            413
          ]
        },
        {
          "category": "Greece",
          "values": [
            454,
            454
          ]
        },
        {
          "category": "United States",
          "values": [
            474,
            465
          ]
        },
        {
          "category": "Israel",
          "values": [
            474,
            466
          ]
        },
        {
          "category": "Slovak Republic",
          "values": [
            478,
            472
          ]
        },
        {
          "category": "Hungary",
          "values": [
            481,
            473
          ]
        },
        {
          "category": "Latvia",
          "values": [
            481,
            483
          ]
        },
        {
          "category": "Iceland",
          "values": [
            487,
            489
          ]
        },
        {
          "category": "Luxembourg",
          "values": [
            491,
            480
          ]
        },
        {
          "category": "Sweden",
          "values": [
            493,
            495
          ]
        },
        {
          "category": "Spain",
          "values": [
            494,
            478
          ]
        },
        {
          "category": "OECD - Average",
          "values": [
            494,
            486
          ]
        },
        {
          "category": "Czech Republic",
          "values": [
            496,
            489
          ]
        },
        {
          "category": "France",
          "values": [
            496,
            490
          ]
        },
        {
          "category": "Australia",
          "values": [
            497,
            491
          ]
        },
        {
          "category": "Portugal",
          "values": [
            497,
            487
          ]
        },
        {
          "category": "Russia",
          "values": [
            497,
            491
          ]
        },
        {
          "category": "United Kingdom",
          "values": [
            498,
            487
          ]
        },
        {
          "category": "New Zealand",
          "values": [
            499,
            491
          ]
        },
        {
          "category": "Italy",
          "values": [
            500,
            480
          ]
        },
        {
          "category": "Norway",
          "values": [
            501,
            503
          ]
        },
        {
          "category": "Finland",
          "values": [
            507,
            515
          ]
        },
        {
          "category": "Austria",
          "values": [
            510,
            483
          ]
        },
        {
          "category": "Poland",
          "values": [
            510,
            499
          ]
        },
        {
          "category": "Ireland",
          "values": [
            512,
            495
          ]
        },
        {
          "category": "Slovenia",
          "values": [
            512,
            508
          ]
        },
        {
          "category": "Netherlands",
          "values": [
            513,
            511
          ]
        },
        {
          "category": "Belgium",
          "values": [
            514,
            500
          ]
        },
        {
          "category": "Germany",
          "values": [
            514,
            498
          ]
        },
        {
          "category": "Denmark",
          "values": [
            516,
            506
          ]
        },
        {
          "category": "Canada",
          "values": [
            520,
            511
          ]
        },
        {
          "category": "Korea",
          "values": [
            521,
            528
          ]
        },
        {
          "category": "Estonia",
          "values": [
            522,
            517
          ]
        },
        {
          "category": "Switzerland",
          "values": [
            527,
            515
          ]
        },
        {
          "category": "Japan",
          "values": [
            539,
            525
          ]
        },
        {
          "category": "Macau, China",
          "values": [
            540,
            548
          ]
        },
        {
          "category": "Chinese Taipei",
          "values": [
            545,
            539
          ]
        },
        {
          "category": "Hong Kong, China",
          "values": [
            549,
            547
          ]
        },
        {
          "category": "Singapore",
          "values": [
            564,
            564
          ]
        }
      ],
      "symbolValues": [
        "Boys",
        "Girls"
      ]
    }
  ],
  "footnotes": {
    "source": "http://dotstat.oecd.org/",
    "sourceLabel": "source"
  }
});