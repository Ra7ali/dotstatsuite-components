export default () => ({
  title: 'Science performance (PISA)',
  subtitle: ['Mean score 2015, Annual incl. OECD, all countries'],
  footnotes: {
    source: 'http://data.oecd.org/',
    sourceLabel: 'data.oecd.org',
  },
  series: [
  {
    "dimensionValues": {
      "x": {
        "id": "PRINTO01",
        "name": "Industrial production, s.a."
      },
      "y": {
        "id": "PRMNTO01",
        "name": "Total manufacturing, s.a."
      }
    },
    "datapoints": [
      {
        "dimensionValues": {
          "LOCATION": {
            "id": "AUT",
            "name": "Austria"
          }
        },
        "x": 2.11995066296641,
        "xFormat": "2.1199507",
        "y": 2.36545969532268,
        "yFormat": "2.3654597"
      },
      {
        "dimensionValues": {
          "LOCATION": {
            "id": "BEL",
            "name": "Belgium"
          }
        },
        "x": -0.0160655474335108,
        "xFormat": "-0.0160655",
        "y": 0.177647331428157,
        "yFormat": "0.1776473"
      },
      {
        "dimensionValues": {
          "LOCATION": {
            "id": "CAN",
            "name": "Canada"
          }
        },
        "x": -0.734931326089172,
        "xFormat": "-0.7349313",
        "y": 0.178223109790082,
        "yFormat": "0.1782231"
      },
      {
        "dimensionValues": {
          "LOCATION": {
            "id": "CZE",
            "name": "Czech Republic"
          }
        },
        "x": 4.62318401937048,
        "xFormat": "4.623184",
        "y": 6.01902272562258,
        "yFormat": "6.0190227"
      },
      {
        "dimensionValues": {
          "LOCATION": {
            "id": "DNK",
            "name": "Denmark"
          }
        },
        "x": 1.18634492777014,
        "xFormat": "1.1863449",
        "y": 2.47915590644139,
        "yFormat": "2.4791559"
      },
      {
        "dimensionValues": {
          "LOCATION": {
            "id": "FIN",
            "name": "Finland"
          }
        },
        "x": -0.958831808585503,
        "xFormat": "-0.9588318",
        "y": -0.938640709195226,
        "yFormat": "-0.9386407"
      },
      {
        "dimensionValues": {
          "LOCATION": {
            "id": "FRA",
            "name": "France"
          }
        },
        "x": 1.96360436882936,
        "xFormat": "1.9636044",
        "y": 1.92806480043649,
        "yFormat": "1.9280648"
      },
      {
        "dimensionValues": {
          "LOCATION": {
            "id": "CHL",
            "name": "Chile"
          }
        },
        "x": 0.230172987500003,
        "xFormat": "0.230173",
        "y": 0.286348587500551,
        "yFormat": "0.2863486"
      },
      {
        "dimensionValues": {
          "LOCATION": {
            "id": "EST",
            "name": "Estonia"
          }
        },
        "x": 0.292360493199452,
        "xFormat": "0.2923605",
        "y": 1.4055978394304,
        "yFormat": "1.4055978"
      },
      {
        "dimensionValues": {
          "LOCATION": {
            "id": "AUS",
            "name": "Australia"
          }
        },
        "x": 1.18005263064255,
        "xFormat": "1.1800526",
        "y": -1.70548071959404,
        "yFormat": "-1.7054807"
      }
    ]
  }
]
});
