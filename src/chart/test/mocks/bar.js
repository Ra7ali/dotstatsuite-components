export default () => ({
  title: 'Science performance (PISA)',
  subtitle: ['Mean score 2015, Annual incl. OECD, all countries'],
  footnotes: {
    source: 'http://data.oecd.org/',
    sourceLabel: 'data.oecd.org',
  },
  series: [
    { datapoints: [
        {x: 'Hungary', y: 5.5},
        {x: 'Sweden', y: 5.6},
        {x: 'Estonia', y: 5.7},
        {x: 'Finland', y: 5.8},
        {x: 'Belgium', y: 5.9},
        {x: 'Turkey', y: 6},
        {x: 'France', y: 6.5},
        {x: 'Poland', y: 6.6},
        {x: 'Slovenia', y: 6.8},
        {x: 'European Union', y: 7.4, baselineIndex: 0},
        {x: 'Euro area (18 countries)', y: 8.2},
        {x: 'Ireland', y: 8.7, highlightIndex: 0},
        {x: 'Italy', y: 8.9},
        {x: 'Slovak Republic', y: 9.9},
        {x: 'Portugal', y: 10.9},
        {x: 'South Africa', y: 12.3, highlightIndex: 1},
        {x: 'Spain (2 digits)', y: 16.86 }
      ]
    }
  ]
});
