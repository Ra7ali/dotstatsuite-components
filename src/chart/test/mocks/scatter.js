export default () => ({
  title: 'Science performance (PISA)',
  subtitle: ['Mean score 2015, Annual incl. OECD, all countries'],
  footnotes: {
    source: 'http://data.oecd.org/',
    sourceLabel: 'data.oecd.org',
  },
  // dealing with collections is more generic and compliant with D3
  // but scatter will only deal with the first serie
  series: [
    {
      // dimension values could come from different dimensions
      dimensionValues: {
        x: { id: 'PRINTO01', name: 'Industrial production, s.a.' },
        y: { id: 'GP', name: 'Growth previous period' },
      },
      datapoints: [
        { // scatter scales are linear, expected type for x and y is Number
          x: 1, y: 2,
          // relevant dimension values that are used for x and y
          // used for annotation and tooltip
          dimensionValues: {
            LOCATION: { id: 'AUS', name: 'Australia' }
          },
          // expected value is an index (Number)
          // baseline overrides highlight is index is equal
          baselineIndex: 0
        },
        { // normal and a lot of decimals (should be rounded at 2)
          x: 3.84651916954654, y: 6,
          dimensionValues: {
            LOCATION: { id: 'BEL', name: 'Belgium' }
          }
        },
        { // multiple dimension values
          x: 2, y: 2,
          dimensionValues: {
            LOCATION: { id: 'CAN', name: 'Canada' }
          },
          highlightIndex: 0
        },
        { // highlighted and 1 decimal, should stay as is
          x: 1, y: 5.2,
          dimensionValues: {
            LOCATION: { id: 'FRA', name: 'France' }
          },
          highlightIndex: 1
        },
        { // yAxis Label conflict
          x: 0.25, y: 6,
          dimensionValues: {
            LOCATION: { id: 'FIN', name: 'Finland' }
          },
          highlightIndex: 2
        },
        { // right edge
          x: 6, y: 3,
          dimensionValues: {
            LOCATION: { id: 'POL', name: 'Poland' }
          },

        },
        { // negative plot to show pivot lines
          x: -3.8, y: -3.2,
          dimensionValues: {
            LOCATION: { id: 'ITA', name: 'Italy' }
          },
          highlightIndex: 3,
        }
      ]
    }
  ],
  share: {
    focused: {
      baseline: [
        { label: 'Country - Australia', category: 'Country - Australia' }
      ],
      highlight: [
        { label: 'Country - Canada', category: 'Country - Canada' },
        { label: 'Country - France', category: 'Country - France' },
        { label: 'Country - Finland', category: 'Country - Finland' },
        { label: 'Country - Italy', category: 'Country - Italy' }
      ],
    }
  }
});
