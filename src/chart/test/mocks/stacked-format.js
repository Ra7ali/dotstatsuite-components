export default () => ({
  title: 'Science performance (PISA)',
  subtitle: ['Mean score 2015, Annual incl. OECD, all countries'],
  footnotes: {
    source: 'http://data.oecd.org/',
    sourceLabel: 'data.oecd.org',
  },
  series: [
  {
    "layerSeries": [
      {
        "id": "PRINTO01",
        "label": "Industrial production, s.a."
      },
      {
        "id": "PRMNTO01",
        "label": "Total manufacturing, s.a."
      }
    ],
    "datapoints": [
      {
        "category": "Czech Republic",
        "formaters": [
          null,
          null
        ],
        "formatedValues": [
          "4.623184",
          "6.0190227"
        ],
        "index": 4,
        "x": "Czech Republic",
        "y": [
          4.62318401937048,
          6.01902272562258
        ],
        "key": "CZE"
      },
      {
        "formaters": [
          null,
          null
        ],
        "category": "Austria",
        "formatedValues": [
          "2.1199507",
          "2.3654597"
        ],
        "index": 1,
        "x": "Austria",
        "y": [
          2.11995066296641,
          2.36545969532268
        ],
        "key": "AUT"
      },
      {
        "formaters": [
          null,
          null
        ],
        "category": "France",
        "formatedValues": [
          "1.9636044",
          "1.9280648"
        ],
        "index": 7,
        "x": "France",
        "y": [
          1.96360436882936,
          1.92806480043649
        ],
        "key": "FRA"
      },
      {
        "formaters": [
          null,
          null
        ],
        "category": "Denmark",
        "formatedValues": [
          "1.1863449",
          "2.4791559"
        ],
        "index": 5,
        "x": "Denmark",
        "y": [
          1.18634492777014,
          2.47915590644139
        ],
        "key": "DNK"
      },
      {
        "formaters": [
          null,
          null
        ],
        "category": "Estonia",
        "formatedValues": [
          "0.2923605",
          "1.4055978"
        ],
        "index": 9,
        "x": "Estonia",
        "y": [
          0.292360493199452,
          1.4055978394304
        ],
        "key": "EST"
      },
      {
        "formaters": [
          null,
          null
        ],
        "category": "Australia",
        "formatedValues": [
          "1.1800526",
          "-1.7054807"
        ],
        "index": 0,
        "x": "Australia",
        "y": [
          1.18005263064255,
          -1.70548071959404
        ],
        "key": "AUS"
      },
      {
        "formaters": [
          null,
          null
        ],
        "category": "Chile",
        "formatedValues": [
          "0.230173",
          "0.2863486"
        ],
        "index": 8,
        "x": "Chile",
        "y": [
          0.230172987500003,
          0.286348587500551
        ],
        "key": "CHL"
      },
      {
        "formaters": [
          null,
          null
        ],
        "category": "Canada",
        "formatedValues": [
          "-0.7349313",
          "0.1782231"
        ],
        "index": 3,
        "x": "Canada",
        "y": [
          -0.734931326089172,
          0.178223109790082
        ],
        "key": "CAN"
      },
      {
        "formaters": [
          null,
          null
        ],
        "category": "Belgium",
        "formatedValues": [
          "-0.0160655",
          "0.1776473"
        ],
        "index": 2,
        "x": "Belgium",
        "y": [
          -0.0160655474335108,
          0.177647331428157
        ],
        "key": "BEL"
      },
      {
        "formaters": [
          null,
          null
        ],
        "category": "Finland",
        "formatedValues": [
          "-0.9588318",
          "-0.9386407"
        ],
        "index": 6,
        "x": "Finland",
        "y": [
          -0.958831808585503,
          -0.938640709195226
        ],
        "key": "FIN"
      }
    ]
  }
]
});
