import series from './map-choro';
module.exports = {
  title: 'United Kingdom Map',
  subtitle: ['random data'],
  footnotes: {
    source: 'http://data.oecd.org/',
    sourceLabel: 'data.oecd.org',
  },
  series
}  