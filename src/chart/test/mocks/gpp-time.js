export default () => ({
  "title": "Growth previous period - Quarterly",
  "subtitle": ["Mean score 2015, Annual incl. OECD, all countries"],
  "series": [
    {
      highlightIndex: 1,
      "datapoints": [
        {
          "x": "2014-12-31T23:00:00.000Z",
          "y": 4.609639177804482,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "AUS",
              "name": "Australia"
            },
            "TIME_PERIOD": {
              "id": "2015-Q1",
              "name": "Q1-2015"
            }
          }
        },
        {
          "x": "2015-03-31T22:00:00.000Z",
          "y": 1.886440440846983,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "AUS",
              "name": "Australia"
            },
            "TIME_PERIOD": {
              "id": "2015-Q2",
              "name": "Q2-2015"
            }
          }
        },
        {
          "x": "2015-06-30T22:00:00.000Z",
          "y": 12.75180816521674,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "AUS",
              "name": "Australia"
            },
            "TIME_PERIOD": {
              "id": "2015-Q3",
              "name": "Q3-2015"
            }
          }
        },
        {
          "x": "2015-09-30T22:00:00.000Z",
          "y": 13.501823810381974,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "AUS",
              "name": "Australia"
            },
            "TIME_PERIOD": {
              "id": "2015-Q4",
              "name": "Q4-2015"
            }
          }
        }
      ],
      "category": "Industrial production, s.a. - Australia"
    },
    {
      baselineIndex: 0,
      "datapoints": [
        {
          "x": "2014-12-31T23:00:00.000Z",
          "y": 5.74346201743464,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "AUT",
              "name": "Austria"
            },
            "TIME_PERIOD": {
              "id": "2015-Q1",
              "name": "Q1-2015"
            }
          }
        },
        {
          "x": "2015-03-31T22:00:00.000Z",
          "y": 6.428396572827401,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "AUT",
              "name": "Austria"
            },
            "TIME_PERIOD": {
              "id": "2015-Q2",
              "name": "Q2-2015"
            }
          }
        },
        {
          "x": "2015-06-30T22:00:00.000Z",
          "y": 8.215119852489256,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "AUT",
              "name": "Austria"
            },
            "TIME_PERIOD": {
              "id": "2015-Q3",
              "name": "Q3-2015"
            }
          }
        },
        {
          "x": "2015-09-30T22:00:00.000Z",
          "y": 10.306654400490624,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "AUT",
              "name": "Austria"
            },
            "TIME_PERIOD": {
              "id": "2015-Q4",
              "name": "Q4-2015"
            }
          }
        }
      ],
      "category": "Industrial production, s.a. - Austria"
    },
    {
      "datapoints": [
        {
          "x": "2014-12-31T23:00:00.000Z",
          "y": 7.580457916801026,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "BEL",
              "name": "Belgium"
            },
            "TIME_PERIOD": {
              "id": "2015-Q1",
              "name": "Q1-2015"
            }
          }
        },
        {
          "x": "2015-03-31T22:00:00.000Z",
          "y": 9.0973078170613029,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "BEL",
              "name": "Belgium"
            },
            "TIME_PERIOD": {
              "id": "2015-Q2",
              "name": "Q2-2015"
            }
          }
        },
        {
          "x": "2015-06-30T22:00:00.000Z",
          "y": 10.453661697990936,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "BEL",
              "name": "Belgium"
            },
            "TIME_PERIOD": {
              "id": "2015-Q3",
              "name": "Q3-2015"
            }
          }
        },
        {
          "x": "2015-09-30T22:00:00.000Z",
          "y": 17.32258064516129,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "BEL",
              "name": "Belgium"
            },
            "TIME_PERIOD": {
              "id": "2015-Q4",
              "name": "Q4-2015"
            }
          }
        }
      ],
      "category": "Industrial production, s.a. - Belgium"
    },
    {
      "datapoints": [
        {
          "x": "2014-12-31T23:00:00.000Z",
          "y": 1.674970340629221,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "CAN",
              "name": "Canada"
            },
            "TIME_PERIOD": {
              "id": "2015-Q1",
              "name": "Q1-2015"
            }
          }
        },
        {
          "x": "2015-03-31T22:00:00.000Z",
          "y": 12.55133353288431,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "CAN",
              "name": "Canada"
            },
            "TIME_PERIOD": {
              "id": "2015-Q2",
              "name": "Q2-2015"
            }
          }
        },
        {
          "x": "2015-06-30T22:00:00.000Z",
          "y": 11.9169388970324,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "CAN",
              "name": "Canada"
            },
            "TIME_PERIOD": {
              "id": "2015-Q3",
              "name": "Q3-2015"
            }
          }
        },
        {
          "x": "2015-09-30T22:00:00.000Z",
          "y": 12.58460613961239,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "CAN",
              "name": "Canada"
            },
            "TIME_PERIOD": {
              "id": "2015-Q4",
              "name": "Q4-2015"
            }
          }
        }
      ],
      "category": "Industrial production, s.a. - Canada"
    },
    {
      "datapoints": [
        {
          "x": "2014-12-31T23:00:00.000Z",
          "y": 18.59668835008869,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "CZE",
              "name": "Czech Republic"
            },
            "TIME_PERIOD": {
              "id": "2015-Q1",
              "name": "Q1-2015"
            }
          }
        },
        {
          "x": "2015-03-31T22:00:00.000Z",
          "y": 11.13504074505239,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "CZE",
              "name": "Czech Republic"
            },
            "TIME_PERIOD": {
              "id": "2015-Q2",
              "name": "Q2-2015"
            }
          }
        },
        {
          "x": "2015-06-30T22:00:00.000Z",
          "y": 10.374100719424463,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "CZE",
              "name": "Czech Republic"
            },
            "TIME_PERIOD": {
              "id": "2015-Q3",
              "name": "Q3-2015"
            }
          }
        },
        {
          "x": "2015-09-30T22:00:00.000Z",
          "y": 8.77989601386481,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "CZE",
              "name": "Czech Republic"
            },
            "TIME_PERIOD": {
              "id": "2015-Q4",
              "name": "Q4-2015"
            }
          }
        }
      ],
      "category": "Industrial production, s.a. - Czech Republic"
    },
    {
      "datapoints": [
        {
          "x": "2014-12-31T23:00:00.000Z",
          "y": 3.0962155227710083,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "DNK",
              "name": "Denmark"
            },
            "TIME_PERIOD": {
              "id": "2015-Q1",
              "name": "Q1-2015"
            }
          }
        },
        {
          "x": "2015-03-31T22:00:00.000Z",
          "y": 9.14674783723163,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "DNK",
              "name": "Denmark"
            },
            "TIME_PERIOD": {
              "id": "2015-Q2",
              "name": "Q2-2015"
            }
          }
        },
        {
          "x": "2015-06-30T22:00:00.000Z",
          "y": 10.09786700125469,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "DNK",
              "name": "Denmark"
            },
            "TIME_PERIOD": {
              "id": "2015-Q3",
              "name": "Q3-2015"
            }
          }
        },
        {
          "x": "2015-09-30T22:00:00.000Z",
          "y": 11.23501427212179,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "DNK",
              "name": "Denmark"
            },
            "TIME_PERIOD": {
              "id": "2015-Q4",
              "name": "Q4-2015"
            }
          }
        }
      ],
      "category": "Industrial production, s.a. - Denmark"
    },
    {
      "datapoints": [
        {
          "x": "2014-12-31T23:00:00.000Z",
          "y": -1.06082036775106,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "FIN",
              "name": "Finland"
            },
            "TIME_PERIOD": {
              "id": "2015-Q1",
              "name": "Q1-2015"
            }
          }
        },
        {
          "x": "2015-03-31T22:00:00.000Z",
          "y": -0.107219442458899,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "FIN",
              "name": "Finland"
            },
            "TIME_PERIOD": {
              "id": "2015-Q2",
              "name": "Q2-2015"
            }
          }
        },
        {
          "x": "2015-06-30T22:00:00.000Z",
          "y": 0.536672629695886,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "FIN",
              "name": "Finland"
            },
            "TIME_PERIOD": {
              "id": "2015-Q3",
              "name": "Q3-2015"
            }
          }
        },
        {
          "x": "2015-09-30T22:00:00.000Z",
          "y": 0.21352313167261,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "FIN",
              "name": "Finland"
            },
            "TIME_PERIOD": {
              "id": "2015-Q4",
              "name": "Q4-2015"
            }
          }
        }
      ],
      "category": "Industrial production, s.a. - Finland"
    },
    {
      "datapoints": [
        {
          "x": "2014-12-31T23:00:00.000Z",
          "y": 1.12608827187469,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "FRA",
              "name": "France"
            },
            "TIME_PERIOD": {
              "id": "2015-Q1",
              "name": "Q1-2015"
            }
          }
        },
        {
          "x": "2015-03-31T22:00:00.000Z",
          "y": 0.302486371493158,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "FRA",
              "name": "France"
            },
            "TIME_PERIOD": {
              "id": "2015-Q2",
              "name": "Q2-2015"
            }
          }
        },
        {
          "x": "2015-06-30T22:00:00.000Z",
          "y": 0.762220381110196,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "FRA",
              "name": "France"
            },
            "TIME_PERIOD": {
              "id": "2015-Q3",
              "name": "Q3-2015"
            }
          }
        },
        {
          "x": "2015-09-30T22:00:00.000Z",
          "y": 0.266403552047352,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "FRA",
              "name": "France"
            },
            "TIME_PERIOD": {
              "id": "2015-Q4",
              "name": "Q4-2015"
            }
          }
        }
      ],
      "category": "Industrial production, s.a. - France"
    },
    {
      "datapoints": [
        {
          "x": "2014-12-31T23:00:00.000Z",
          "y": -1.43976236982363,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "CHL",
              "name": "Chile"
            },
            "TIME_PERIOD": {
              "id": "2015-Q1",
              "name": "Q1-2015"
            }
          }
        },
        {
          "x": "2015-03-31T22:00:00.000Z",
          "y": 5.09368342961992,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "CHL",
              "name": "Chile"
            },
            "TIME_PERIOD": {
              "id": "2015-Q2",
              "name": "Q2-2015"
            }
          }
        },
        {
          "x": "2015-06-30T22:00:00.000Z",
          "y": -1.89124404695679,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "CHL",
              "name": "Chile"
            },
            "TIME_PERIOD": {
              "id": "2015-Q3",
              "name": "Q3-2015"
            }
          }
        },
        {
          "x": "2015-09-30T22:00:00.000Z",
          "y": -1.04018078102806,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "CHL",
              "name": "Chile"
            },
            "TIME_PERIOD": {
              "id": "2015-Q4",
              "name": "Q4-2015"
            }
          }
        }
      ],
      "category": "Industrial production, s.a. - Chile"
    },
    {
      "datapoints": [
        {
          "x": "2014-12-31T23:00:00.000Z",
          "y": -0.797209765819618,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "EST",
              "name": "Estonia"
            },
            "TIME_PERIOD": {
              "id": "2015-Q1",
              "name": "Q1-2015"
            }
          }
        },
        {
          "x": "2015-03-31T22:00:00.000Z",
          "y": 0.452034153691591,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "EST",
              "name": "Estonia"
            },
            "TIME_PERIOD": {
              "id": "2015-Q2",
              "name": "Q2-2015"
            }
          }
        },
        {
          "x": "2015-06-30T22:00:00.000Z",
          "y": -1.27499999999999,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "EST",
              "name": "Estonia"
            },
            "TIME_PERIOD": {
              "id": "2015-Q3",
              "name": "Q3-2015"
            }
          }
        },
        {
          "x": "2015-09-30T22:00:00.000Z",
          "y": -1.5193719929096,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRINTO01",
              "name": "Industrial production, s.a."
            },
            "LOCATION": {
              "id": "EST",
              "name": "Estonia"
            },
            "TIME_PERIOD": {
              "id": "2015-Q4",
              "name": "Q4-2015"
            }
          }
        }
      ],
      "category": "Industrial production, s.a. - Estonia"
    },
    {
      "datapoints": [
        {
          "x": "2014-12-31T23:00:00.000Z",
          "y": -1.01419102000824,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRMNTO01",
              "name": "Total manufacturing, s.a."
            },
            "LOCATION": {
              "id": "CAN",
              "name": "Canada"
            },
            "TIME_PERIOD": {
              "id": "2015-Q1",
              "name": "Q1-2015"
            }
          }
        },
        {
          "x": "2015-03-31T22:00:00.000Z",
          "y": -0.547861522877197,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRMNTO01",
              "name": "Total manufacturing, s.a."
            },
            "LOCATION": {
              "id": "CAN",
              "name": "Canada"
            },
            "TIME_PERIOD": {
              "id": "2015-Q2",
              "name": "Q2-2015"
            }
          }
        },
        {
          "x": "2015-06-30T22:00:00.000Z",
          "y": 0.986296044319164,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRMNTO01",
              "name": "Total manufacturing, s.a."
            },
            "LOCATION": {
              "id": "CAN",
              "name": "Canada"
            },
            "TIME_PERIOD": {
              "id": "2015-Q3",
              "name": "Q3-2015"
            }
          }
        },
        {
          "x": "2015-09-30T22:00:00.000Z",
          "y": -0.00500458111654567,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRMNTO01",
              "name": "Total manufacturing, s.a."
            },
            "LOCATION": {
              "id": "CAN",
              "name": "Canada"
            },
            "TIME_PERIOD": {
              "id": "2015-Q4",
              "name": "Q4-2015"
            }
          }
        }
      ],
      "category": "Total manufacturing, s.a. - Canada"
    },
    {
      "datapoints": [
        {
          "x": "2014-12-31T23:00:00.000Z",
          "y": -0.385019250962552,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRMNTO01",
              "name": "Total manufacturing, s.a."
            },
            "LOCATION": {
              "id": "FIN",
              "name": "Finland"
            },
            "TIME_PERIOD": {
              "id": "2015-Q1",
              "name": "Q1-2015"
            }
          }
        },
        {
          "x": "2015-03-31T22:00:00.000Z",
          "y": -0.562192550948708,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRMNTO01",
              "name": "Total manufacturing, s.a."
            },
            "LOCATION": {
              "id": "FIN",
              "name": "Finland"
            },
            "TIME_PERIOD": {
              "id": "2015-Q2",
              "name": "Q2-2015"
            }
          }
        },
        {
          "x": "2015-06-30T22:00:00.000Z",
          "y": -0.353356890459349,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRMNTO01",
              "name": "Total manufacturing, s.a."
            },
            "LOCATION": {
              "id": "FIN",
              "name": "Finland"
            },
            "TIME_PERIOD": {
              "id": "2015-Q3",
              "name": "Q3-2015"
            }
          }
        },
        {
          "x": "2015-09-30T22:00:00.000Z",
          "y": 0.780141843971616,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRMNTO01",
              "name": "Total manufacturing, s.a."
            },
            "LOCATION": {
              "id": "FIN",
              "name": "Finland"
            },
            "TIME_PERIOD": {
              "id": "2015-Q4",
              "name": "Q4-2015"
            }
          }
        }
      ],
      "category": "Total manufacturing, s.a. - Finland"
    },
    {
      "datapoints": [
        {
          "x": "2014-12-31T23:00:00.000Z",
          "y": -0.21896383186705,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRMNTO01",
              "name": "Total manufacturing, s.a."
            },
            "LOCATION": {
              "id": "AUS",
              "name": "Australia"
            },
            "TIME_PERIOD": {
              "id": "2015-Q1",
              "name": "Q1-2015"
            }
          }
        },
        {
          "x": "2015-03-31T22:00:00.000Z",
          "y": -0.46240056428544,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRMNTO01",
              "name": "Total manufacturing, s.a."
            },
            "LOCATION": {
              "id": "AUS",
              "name": "Australia"
            },
            "TIME_PERIOD": {
              "id": "2015-Q2",
              "name": "Q2-2015"
            }
          }
        },
        {
          "x": "2015-06-30T22:00:00.000Z",
          "y": -0.566906814692331,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRMNTO01",
              "name": "Total manufacturing, s.a."
            },
            "LOCATION": {
              "id": "AUS",
              "name": "Australia"
            },
            "TIME_PERIOD": {
              "id": "2015-Q3",
              "name": "Q3-2015"
            }
          }
        },
        {
          "x": "2015-09-30T22:00:00.000Z",
          "y": -1.96777131092371,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRMNTO01",
              "name": "Total manufacturing, s.a."
            },
            "LOCATION": {
              "id": "AUS",
              "name": "Australia"
            },
            "TIME_PERIOD": {
              "id": "2015-Q4",
              "name": "Q4-2015"
            }
          }
        }
      ],
      "category": "Total manufacturing, s.a. - Australia"
    },
    {
      highlightIndex: 0,
      "datapoints": [
        {
          "x": "2014-12-31T23:00:00.000Z",
          "y": 0.575677620532518,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRMNTO01",
              "name": "Total manufacturing, s.a."
            },
            "LOCATION": {
              "id": "EST",
              "name": "Estonia"
            },
            "TIME_PERIOD": {
              "id": "2015-Q1",
              "name": "Q1-2015"
            }
          }
        },
        {
          "x": "2015-03-31T22:00:00.000Z",
          "y": -0.977820176484621,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRMNTO01",
              "name": "Total manufacturing, s.a."
            },
            "LOCATION": {
              "id": "EST",
              "name": "Estonia"
            },
            "TIME_PERIOD": {
              "id": "2015-Q2",
              "name": "Q2-2015"
            }
          }
        },
        {
          "x": "2015-06-30T22:00:00.000Z",
          "y": -0.91522157996146,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRMNTO01",
              "name": "Total manufacturing, s.a."
            },
            "LOCATION": {
              "id": "EST",
              "name": "Estonia"
            },
            "TIME_PERIOD": {
              "id": "2015-Q3",
              "name": "Q3-2015"
            }
          }
        },
        {
          "x": "2015-09-30T22:00:00.000Z",
          "y": -0.0486144871171718,
          "dimensionValues": {
            "SUBJECT": {
              "id": "PRMNTO01",
              "name": "Total manufacturing, s.a."
            },
            "LOCATION": {
              "id": "EST",
              "name": "Estonia"
            },
            "TIME_PERIOD": {
              "id": "2015-Q4",
              "name": "Q4-2015"
            }
          }
        }
      ],
      "category": "Total manufacturing, s.a. - Estonia"
    },
  ],
  "footnotes": {
    "source": "http://dotstat.oecd.org/",
    "sourceLabel": "source"
  },
  "frequency": "quarterly"
});