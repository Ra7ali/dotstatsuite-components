# share

The component scopes the share feature of the oecd. This is a deprecated component not used in application of the .Stat suite.

## usage

```javascript
import { Share } from '@sis-cc/dotstatsuite-components';

<div>
  <span>this sharable component</span>
  <h2>header</h2>
  <Share config={config} data={data} meta={meta} />
  <div>...component...</div>
  <h2>footer</h2>
</div>
```

## api

#### config `<object>`
```javascript
{
  read: 'http://rcw-pp.prelive.oecd.org',
  write: 'http://vs-webdev-1:89/chart/generic',
}
```

#### data `<object>` data of underlying component
```javascript
{ /* depends on the component */ }
```

#### meta `<object>` subset of data used by oecd-share
```javascript
{
  title: 'Science performance (PISA)',
  subtitle: 'Boys / Girls, Mean score 2015, Annual incl. OECD, all countries',
  width: 800,
  height: 400,
}
```