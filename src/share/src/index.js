import { get, has, isArray, isFunction } from 'lodash';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Classes, Spinner } from '@blueprintjs/core';
import Select from 'react-select';
import { stringify } from 'query-string';
import '@blueprintjs/core/dist/blueprint.css';
import ShareIcon from '../../assets/share/share-icon.png';
import t4 from './t4';
import 'react-select/dist/react-select.css';
import { refineShareData } from '../../rules/src';
import {
  Dialog,
  DialogBody,
  ShareHeader,
  ShareMain,
  ShareMedia,
  ShareOptions,
  ShareOut,
  ShareSide,
} from './glamorous_components';

const SUBTITLE_SEPARATOR = ' \u25cf ';

class Share extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isFetching: false,
      isSharing: false,
      hasFailed: false,
      shareMode: 'snapshot',
      permanentUri: '',
      hasFormatSelection: get(props.config, 'hasT4Formats', false),
      selectedFormat: 'default',
      sharedData: null,
      shareResponse: null,
    };
    this.onShare = this.onShare.bind(this);
    this.onClose = this.onClose.bind(this);
    this.onChangeShareMode = this.onChangeShareMode.bind(this);
    this.onChangeFormat = this.onChangeFormat.bind(this);
    this.onOutput = this.onOutput.bind(this);
    this.configIsValid = (config) => has(config, 'read') && has(config, 'write');
  }

  onShare() {
    if (!this.configIsValid(this.props.config)) return;

    this.setState({ isFetching: true });
    const requestBody = {
      data: refineShareData(this.state.shareMode)(this.props.data),
      share: this.state.shareMode,
      type: this.props.type,
    };
    fetch(this.props.config.write, {
      method: 'POST',
      mode: 'cors',
      body: JSON.stringify(requestBody),
    }).then((res) => {
        this.setState({ isFetching: false });
        if (res.ok) {
          try {
            return res.json();
          } catch (err) {
            console.log(err);
            this.setState({ hasFailed: true });
          }
        } else {
          console.log(`${res.statusText} (${res.status})`);
          this.setState({ hasFailed: true });
        }
      }).then(responseJson => {
        const onShareGenerated = get(this.props, 'config.onShareGenerated');
        if (responseJson && isFunction(onShareGenerated)) {
          onShareGenerated({ responseJson, requestBody });
        }
        return responseJson;
      }).then((json) => {
        const permanentUri = `${this.props.config.read}?id=${json.id}`;

        // NaN is not falsy for get
        const _width = get(this.props, 'meta.width', NaN);
        const width = isNaN(_width) ? '100%' : _width;
        const _height = get(this.props, 'meta.height', NaN);
        const height = isNaN(_height) ? '100%' : _height;

        this.setState({
          permanentUri,
          embedCode:
`<iframe src="${permanentUri}" width="${width}" height="${height}" style="border: none;" allowfullscreen="true">
  <a href="${permanentUri}" rel="noopener noreferrer" target="_blank">${this.props.meta.title}</a>
</iframe>`,
          isSharing: true,
          selectedFormat: 'default',
          sharedData: requestBody,
          shareResponse: json,
        });
      });
  }

  onClose() {
    this.setState({ isSharing: false });
  }

  onChangeShareMode(event) {
    this.setState({ shareMode: event.target.value }, this.onShare);
  }

  onChangeFormat({ value }) {
    const width = get(t4, `values.${value}.width`, '100%');
    const height = get(t4, `values.${value}.height`, '100%');
    const embedCode =
`<iframe src="${this.state.permanentUri}" width="${width}" height="${height}" style="border: none;" allowfullscreen="true">
  <a rel="noopener noreferrer" href="${this.state.permanentUri}" target="_blank">${this.props.meta.title}</a>
</iframe>`;
    this.setState({ selectedFormat: value, embedCode })
  }

  onOutput(media) {
    let mediaHandler = null;
    switch (media) {
      case 'facebook':
        const uf = stringify({ u: this.state.permanentUri });
        mediaHandler = () => window.open(`https://www.facebook.com/sharer/sharer.php?${uf}`, '_blank');
        break;
      case 'twitter':
        const ut = stringify({ text: `${this.props.meta.title}\n${this.state.permanentUri}` })
        mediaHandler = () => window.open(`https://twitter.com/intent/tweet?${ut}`, '_blank');
        break;
      case 'email':
        const subject = encodeURIComponent(this.props.meta.title);
        const body = encodeURIComponent(`${this.props.meta.title}\n${this.state.permanentUri}`);
        mediaHandler = () => window.location.href = `mailto:?subject=${subject}&body=${body}`;
        break;
      case 'preview':
        //https://developer.mozilla.org/fr/docs/Web/API/Window/open
        mediaHandler = () => window.open(
          this.state.permanentUri,
          'preview',
          `resizable=yes,scrollbars=yes,status=yes`
        );
    }

    return () => {
      if (isFunction(mediaHandler)) {
        const onMediaShare = get(this.props, 'config.onMediaShare');
        if (isFunction(onMediaShare)) {
          const { sharedData, shareResponse } = this.state;
          onMediaShare({ media, sharedData, shareResponse });
        }
        mediaHandler();
      }
    };
  }

  render() {
    if (!this.configIsValid(this.props.config)) return null;

    const spinner = <Spinner className={Classes.SMALL} />;
    const permanentUrl = this.state.isFetching
      ? spinner
      : <input className={`${Classes.INPUT} ${Classes.FILL}`} onChange={() => {}}type="text" value={this.state.permanentUri} />;
    const embedCode = this.state.isFetching
      ? spinner
      : <textarea className={`${Classes.INPUT} ${Classes.FILL}`} value={this.state.embedCode} onChange={() => {}}></textarea>;
    const source = get(this.props.data, 'data.share.source');
    const disableLatest = !(source && (source !== 'no source'));

    if (this.state.isFetching && !(this.state.isSharing)) return spinner;

    let subtitle = get(this.props, 'meta.subtitle');

    return (
      <div>
        <button
          className={`${Classes.BUTTON} ${Classes.MINIMAL}`}
          onClick={this.onShare}
        >
          <img src={ShareIcon} />
        </button>
        <Dialog isOpen={this.state.isSharing} onClose={this.onClose}>
          <DialogBody>
            <ShareSide>
              <h2>
                { get(this.props.translations, 'share.title', 'Your selection for sharing:') }
              </h2>
              <div>
                <h3>{this.props.meta.title}</h3>
                <h4>
                  {isArray(subtitle) ? subtitle.join(SUBTITLE_SEPARATOR) : subtitle}
                </h4>
              </div>
              <ShareOptions
                disabledLatest={disableLatest}
                latestLabel={get(this.props.translations, 'share.latest.available.data', 'Latest available data for the period defined')}
                onChange={this.onChangeShareMode}
                selectedValue={this.state.shareMode}
                snapshotLabel={get(this.props.translations, 'share.snapshot', 'Snapshot of data for the period defined (data will not change even if updated on the site)')}
              />
            </ShareSide>
            <ShareMain>
              <ShareHeader
                close={this.onClose}
                label={get(this.props.translations, 'share.sharing.options', 'Sharing options:')}
              />
              <ShareMedia clickCallback={this.onOutput} />
              <ShareOut
                clickCallback={this.onOutput}
                embedCode={embedCode}
                hasSelection={this.state.hasFormatSelection}
                options={t4.options}
                select={this.onChangeFormat}
                selectedValue={this.state.selectedFormat}
                translations={this.props.translations}
                url={permanentUrl}
              />
            </ShareMain>
          </DialogBody>
        </Dialog>
      </div>
    );
  }
};

Share.propTypes = {
  config: PropTypes.object,
  data: PropTypes.object,
  meta: PropTypes.object,
  type: PropTypes.string
};

export default Share;
