import glamorous from 'glamorous';

export default glamorous.div({
  flexGrow: 3,
  padding: 20,
});
