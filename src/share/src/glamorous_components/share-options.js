import React from 'react';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';
import { Classes, Radio, RadioGroup } from '@blueprintjs/core';


const ShareOptions = (props) => { 

  return (
    <div>
      <RadioGroup
        onChange={props.onChange}
        selectedValue={props.selectedValue}
      >
        <Radio
          label={props.snapshotLabel}
          value="snapshot"
        />
        <Radio
          label={props.latestLabel}
          value="latest"
          disabled={props.disabledLatest}
        />
      </RadioGroup>
    </div>
  );
};

ShareOptions.propTypes = {
  disabledLatest: PropTypes.bool.isRequired,
  latestLabel: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  selectedValue: PropTypes.string.isRequired,
  snapshotLabel: PropTypes.string.isRequired,
};

export default ShareOptions;
