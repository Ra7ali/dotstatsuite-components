import { Dialog } from '@blueprintjs/core';
import glamorous from 'glamorous';

export default glamorous(Dialog)({
  borderRadius: 0,
  margin: '0 !important',
  padding: '0 !important',
  top: '10% !important',
  width: '80% !important',
  '& h2': {
    fontSize: 24,
    marginBottom: 20,
  },
  '& h3': {
    color: '#0b71b0',
    fontSize: 16,
    fontWeight: 'normal',
    marginBottom: 20,
  },
  '& h4': {
    fontSize: 14,
    fontWeight: 'normal',
    marginBottom: 40,
  },
  '& textarea': {
    fontSize: 11,
    height: '120px !important',
  }
});
