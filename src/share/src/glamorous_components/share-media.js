import React from 'react';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';
import { Button, Classes } from '@blueprintjs/core';

const Container = glamorous.div({
  marginBottom: 12,
  [`& ${Classes.BUTTON}`]: {
    margin: 0
  }
});

const GButton = glamorous(Button)({
  marginRight: 10
})

const ShareMedia = ({ clickCallback }) => { 

  return (
    <Container>
      <GButton className={Classes.INTENT_PRIMARY} onClick={clickCallback('facebook')}>
        Facebook
      </GButton>
      <GButton className={Classes.INTENT_PRIMARY} onClick={clickCallback('twitter')}>
        Twitter
      </GButton>
      <GButton className={Classes.INTENT_PRIMARY} onClick={clickCallback('email')}>
        E-Mail
      </GButton>
    </Container>
  );
};

ShareMedia.propTypes = {
  clickCallback: PropTypes.func.isRequired
};

export default ShareMedia;
