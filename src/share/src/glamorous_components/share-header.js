import React from 'react';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';
import { Button, Classes } from '@blueprintjs/core';

const Container = glamorous.div({
  display: 'flex',
  justifyContent: 'space-between'
});

const ShareHeader= ({ close, label }) => { 

  return (
    <Container>
      <h2>
        {label}
      </h2>
      <Button className={Classes.MINIMAL} onClick={close} iconName="cross" />
    </Container>
  );
};

ShareHeader.propTypes = {
  close: PropTypes.func.isRequired,
  label: PropTypes.string.isRequired,
};

export default ShareHeader;
