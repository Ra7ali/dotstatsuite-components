import glamorous from 'glamorous';

export default glamorous.div({
  display: 'flex',
  flexDirection: 'row',
  margin: 0,
});
