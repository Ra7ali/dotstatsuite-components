import React from 'react';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';
import { Button, Classes } from '@blueprintjs/core';
import Select from 'react-select';
import { get } from 'lodash';

const ShareOut = (props) => { 

  return (
    <div>
      <label className={Classes.LABEL}>
        {get(props.translations, 'share.permanent.url', 'Permanent URL') }
        {props.url}
        <p className={Classes.TEXT_MUTED}>
          {get(props.translations, 'share.copy.url', 'Copy the URL to open this chart with all your selections.:')}
        </p>
      </label>
      <label className={Classes.LABEL}>
        { get(props.translations, 'share.embed.code', 'Embed code') }
        {
          props.hasSelection ?
            <Select
              clearable={false}
              onChange={props.select}
              options={props.options}
              value={props.selectedValue}
            />
            : null
        }
        {props.embedCode}
        <p className={Classes.TEXT_MUTED}>
          {get(props.translations, 'share.use.code.to.embed.visualisation', 'Use this code to embed the visualisation into your website.')}
        </p>
      </label>
      <Button onClick={props.clickCallback('preview')}>
        {get(props.translations, 'share.preview.embedded', 'Preview Embedding')}
      </Button>
    </div>
  );
};

ShareOut.propTypes = {
  clickCallback: PropTypes.func.isRequired,
  embedCode: PropTypes.element.isRequired,
  hasSelection: PropTypes.bool.isRequired,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
    })
  ),
  select: PropTypes.func.isRequired,
  selectedValue: PropTypes.string,
  translations: PropTypes.object,
  url: PropTypes.element.isRequired,
};

export default ShareOut;
