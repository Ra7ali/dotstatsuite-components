import glamorous from 'glamorous';

export default glamorous.div({
  backgroundColor: '#deeaf1',
  padding: 20,
  width: '40%',
});
