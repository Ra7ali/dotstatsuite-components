import React from 'react';
import Share from '.';

const config = {
  read: 'http://rcw-pp.prelive.oecd.org',
  write: 'http://dotstatcor-dev1.main.oecd.org/FEDev1ShareIntranetService',
  hasT4Formats: true
};

const meta = {
  title: 'Science performance (PISA)',
  subtitle: ['Boys / Girls', 'Mean score 2015', 'Annual incl. OECD', 'all countries'],
};

const data = {};

const latestData = { data: { share: { source: 'https://www.google.com' } } };
const type = '';

export default () => (
  <div style={{display: 'flex', justifyContent: 'space-around'}}>
    <div style={{backgroundColor: '#9093CA'}}>
      <span>this is a sharable component</span>
      <h2>header</h2>
      <div><Share config={config} data={data} meta={meta} type={type} /></div>
      <div>...component...</div>
      <h2>footer</h2>
    </div>
    <div style={{backgroundColor: '#9093CA'}}>
      <span>this is a sharable component with latest mode</span>
      <h2>header</h2>
      <div><Share config={config} data={latestData} meta={meta} type={type} /></div>
      <div>...component...</div>
      <h2>footer</h2>
    </div>
    <div style={{backgroundColor: '#62C9C8'}}>
      <span>this is a sharable component without a config to be shared!</span>
      <h2>header</h2>
      <div><Share data={data} meta={meta} type={type} /></div>
      <div>...component...</div>
      <h2>footer</h2>
    </div>
  </div>
);
