# rules

The library scopes business logic regarding sdmx parsing and refining. This library is responsible for generating table and charts data series from sdmx data.

## usage

```javascript
import { rules } from '@sis-cc/dotstatsuite-components';

rules.extractSdmxArtefacts(data);
```

## misc

Top-level methods are memoized to have a good performance.  
Factory methods have been extracted from a component and are not yet memoized.

The memoization has been used to make the library easy to use, parsing methods can be heavy
regarding data and being forced to use them carefully generally lead to complexity.

## api

TODO