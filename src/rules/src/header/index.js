import * as R from 'ramda';
import { dimensionValueDisplay, parseDisplay, getFlags } from '../';
import { DEFAULT_REJECTED_SUBTITLE_IDS } from '../constants';

export { getDefaultTitle } from './getDefaultTitle';
export { getDefaultSubtitle } from './getDefaultSubtitle';
export { getSubtitleFlags } from './getSubtitleFlags';
export { getHeaderUnits } from './getHeaderUnits';
export { getTitleFlags } from './getTitleFlags';

export const getHeaderProps = ({
  dataflowName,
  dataflowCode,
  dataflowAttributes,
  oneValueDimensions,
  display,
  customAttributes,
  units // { id, name, values }
}) => {
  const _display = parseDisplay(display);
  const formatter = dimensionValueDisplay(_display);
  const _getFlags = getFlags(customAttributes, formatter, {});

  const title = {
    label: formatter({ id: dataflowCode, name: dataflowName }),
    flags: _getFlags({ attributes: dataflowAttributes })
  };

  const unitsValuesIds = R.pipe(R.propOr([], 'values'), R.pluck('id'))(units);

  const formattedUnits = R.converge(
    (unit, values) => R.isEmpty(values) ? null : [unit, values],
    [
      dimensionValueDisplay(_display),
      R.pipe(
        R.propOr([], 'values'),
        R.map(R.pipe(R.propOr({}, 'value'), dimensionValueDisplay(_display))),
        R.join(', ')
      )
    ]
  )(units);

  const subtitle = R.pipe(
    R.values,
    R.sortBy(R.prop('__index')),
    R.reduce(
      (acc, dim) => {
        if (!R.propOr(true, 'display', dim)) {
          return acc;
        }
        if (R.includes(dim.id, unitsValuesIds)) {
          return acc;
        }
        const value = R.path(['values', 0], dim);

        if (R.includes(value.id, DEFAULT_REJECTED_SUBTITLE_IDS) || !R.propOr(true, 'display', value)) {
          return acc;
        }

        const flags = _getFlags({ attributes: R.propOr({}, 'attributes', value) });

        return R.append({
          label: [formatter(dim), formatter(value)],
          flags
        }, acc);
      },
      []
    )
  )(oneValueDimensions);
  return ({ title, subtitle, uprs: formattedUnits });
};
