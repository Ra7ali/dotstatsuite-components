import * as R from 'ramda';
import { dimensionValueDisplay, parseDisplay } from '../';

export const getDefaultTitle = ({ dataflow, display }) =>  {
  const _display = parseDisplay(display);
  const formatter = dimensionValueDisplay(_display);
  return formatter(dataflow);
};
