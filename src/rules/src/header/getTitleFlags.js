import * as R from 'ramda';
import { parseDisplay, dimensionValueDisplay, getFlags } from '../';

export const getTitleFlags = ({ customAttributes, dataflowAttributes, display }) => {
  const _display = parseDisplay(display);
  const formatter = dimensionValueDisplay(_display);
  return getFlags(customAttributes, formatter, {})({ attributes: dataflowAttributes });
};
