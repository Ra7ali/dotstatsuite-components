import * as R from 'ramda';
import dateFns from 'date-fns';
import fr from 'date-fns/locale/fr';
import en from 'date-fns/locale/en';
import es from 'date-fns/locale/es';
import ar from 'date-fns/locale/ar';
import it from 'date-fns/locale/it';

const isValidDate = period =>
  R.and(dateFns.isDate, date => R.not(R.equals(date.getTime(date), NaN)))(new Date(period));

const dec = R.ifElse(R.gte(0), R.identity, R.dec);
const parseDate = date => dateFns.parse(date);
const addMonths = month => date => dateFns.addMonths(date, dec(month));
const slicerFrequency = R.pipe(R.last, R.slice(1, Infinity));

const getDate = (fn, last) => sdmxDate => {
  if (R.isEmpty(R.head(sdmxDate))) return undefined;
  const year = R.head(sdmxDate);
  const hasAdded = last(sdmxDate);
  return R.pipe(
    parseDate,
    fn(hasAdded),
  )(year);
};

const parseDateFromSdmxDate = (sdmxDate) => R.pipe(
  R.split('-'),
  R.ifElse(
    R.pipe(R.last, R.head, R.equals('M')),
    getDate(addMonths, slicerFrequency),
    R.always(sdmxDate)
  )
)(sdmxDate)

const getLocale = R.cond([
  [R.equals('fr'), R.always(fr)],
  [R.equals('es'), R.always(es)],
  [R.equals('ar'), R.always(ar)],
  [R.equals('it'), R.always(it)],
  [R.T, R.always(en)]
])

const dateFnsFormat = (date, format, locale) => dateFns.format(date, format, { locale: getLocale(locale) })

const formaterPeriod = (locale, format) => date => {
  if (R.is(String)(date)) {
    if (R.pipe(R.split('-'), R.length, R.equals(2), R.not)(date)) return date
    return dateFnsFormat(date, format, locale);
  }
  return dateFnsFormat(date, format, locale);
}

export const getTimePeriodLabel = (locale, format = 'YYYY MMM') => R.pipe(
  R.ifElse(R.isNil, R.always(''), R.identity),
  parseDateFromSdmxDate,
  R.ifElse(
    isValidDate,
    formaterPeriod(locale, format),
    R.identity
  )
);
