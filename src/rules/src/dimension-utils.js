import { includes, isEmpty, isNil, get, has, reduce } from 'lodash';
import { DEFAULT_MAJOR_DIMENSIONS } from './constants';

export const isFreqDimension = (id, MAJOR_DIMENSIONS = DEFAULT_MAJOR_DIMENSIONS) => (
  includes(get(MAJOR_DIMENSIONS, 'FREQ.dimension.id', []), id)
);

export const isFreqAttribute = (id, MAJOR_DIMENSIONS = DEFAULT_MAJOR_DIMENSIONS) => (
  includes(get(MAJOR_DIMENSIONS, 'FREQ.attribute.id', []), id)
);

export const isAreaDimension = (id, MAJOR_DIMENSIONS = DEFAULT_MAJOR_DIMENSIONS) => (
  includes(get(MAJOR_DIMENSIONS, 'AREA.dimension.id', []), id)
);

export const isTimeDimension = (id, MAJOR_DIMENSIONS = DEFAULT_MAJOR_DIMENSIONS) => (
  includes(get(MAJOR_DIMENSIONS, 'TIME.dimension.id', []), id)
);

export const dimensionValueDisplay = (display) => (dimensionValue) => {
  const id = get(dimensionValue, 'id', '');
  const name = get(dimensionValue, 'name', '');
  switch(display) {
    case 'label':
      return isEmpty(name) ? `[${id}]` : name;
    case 'code':
      return id;
    case 'both':
      if (isNil(id) || isEmpty(id)) {
        return name;
      }
      return `(${id}) ${name}`
    default:
      return null;
  }
};

export const dimensionValueDisplayAt = (dimension, index, display) => {
  const dimensionValue = get(dimension, `values[${Number(index)}]`, {});
  return dimensionValueDisplay(display)(dimensionValue);
};

export const categoryDisplay = (splitObservationKey, dimensions, dimensionsWithValuesIndexedById, rejectedId, display) => {
  return reduce(
    splitObservationKey,
    (memo, dimensionValueIndex, dimensionIndex) => {
      const dimension = get(dimensions, `[${dimensionIndex}]`, {});
      if (has(dimensionsWithValuesIndexedById, dimension.id) && dimension.id !== rejectedId) {
        memo.push(dimensionValueDisplayAt(dimension, dimensionValueIndex, display));
      }
      return memo;
    },
    []
  ).join(' - ');
};