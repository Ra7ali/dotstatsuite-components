import * as R from 'ramda';
import dateFns from 'date-fns';
import {
  getRelationnalAnnotations, setAnnotationsLayout, setIndexPositions, getPriorityIndexPosition,
  getIsHidden, hiddenFormat,
} from './sdmx-data';
import { getTimePeriodLabel } from './date';
import { isTimeDimension } from './dimension-utils';

export const dataTransformer = (dataNew, options = {}) => {
  const getLanguage = R.pipe(R.pathOr('', ['meta', 'contentLanguages']), R.head());
  const locale = getLanguage(dataNew);

  const getProp = (prop, locale) => R.pathOr(null,[prop, locale]);
  const getLocalisedProp = R.curry((prop, collection) => getProp(prop, locale)(collection));
  const getValues = R.map((value) => ({ ...value, name: getLocalisedProp('names', value) }));
  const getValuesEnhanced = (locale, annotations, isTimeDimension) => R.map((value) => {
    const indexPositions =  R.pipe(
      getRelationnalAnnotations(R.propOr([],'annotations')(value)),
      setIndexPositions(locale),
      getPriorityIndexPosition(R.prop('order')(value))
    )(annotations);

    return ({
      ...value,
      id: R.when(
        R.always(isTimeDimension),
        R.replace(/M/g, '')
      )(value.id),
      name: R.ifElse(
        R.always(isTimeDimension),
        R.pipe(getLocalisedProp('names'), getTimePeriodLabel(locale, R.prop('timeFormat')(options))),
        getLocalisedProp('names')
      )(value),
      ...indexPositions,
      ...(isTimeDimension ? { '__indexPosition': dateFns.getTime(new Date(R.prop('start', value))) } : {})
    });
  })

  const getDataSets = R.pathOr({},['data', 'dataSets']);
  const dataSets = getDataSets(dataNew);

  const getStructure = R.pathOr({},['data', 'structure']);
  const structure = getStructure(dataNew);

//--------------------------------------------------------------------------------------------Header
  const getMeta = R.propOr({}, 'meta');
  const meta = getMeta(dataNew);

  const getSender = R.propOr({}, 'sender');
  const sender = getSender(meta);

  const resHeader = {
    ...meta,
    sender: {...sender, name: getLocalisedProp('names', sender)},
  };

//---------------------------------------------------------------------------------------Annotations
  const getAnnotations = R.propOr([], 'annotations');
  const annotations = getAnnotations(structure);

  const dataSetsAnnotations = getRelationnalAnnotations(
    R.pathOr([],[0, 'annotations'])(dataSets)
  )(annotations);

  const hiddenIds = R.pipe(
    R.find(R.propEq('type', 'NOT_DISPLAYED')),
    R.propOr('', 'title'),
    R.ifElse(R.isEmpty, R.always([]), R.split(','))
  )(annotations);

//----------------------------------------------------------------------------------------Attributes
  const getAttributes = R.propOr({}, 'attributes');
  const attributes = getAttributes(structure);

  const getAttrObservations = R.propOr([], 'observation');
  const attrObservations = getAttrObservations(attributes);

  const getAttrDataset = R.propOr([], 'dataset')
  const attrDataset = getAttrDataset(attributes);

  const mergeAttr = R.concat(attrDataset, attrObservations);

  const resAttrObservations = R.map((observation) => ({
    ...observation,
    name: getLocalisedProp('names', observation),
    values: getValues(observation.values),
    roles: !R.isNil(observation.role) ? R.head(observation.role) : null,
  }), mergeAttr);

  const resAttributes = {
    ...attributes,
    observation: resAttrObservations,
  }

//----------------------------------------------------------------------------------------Dimensions
  const getDimensions = R.propOr({}, 'dimensions');
  const dimensions = getDimensions(structure);

  const getDimObservations = R.propOr([], 'observation');
  const dimObservations = getDimObservations(dimensions);

  const getObservations = locale => R.reduce((acc, observation) => {
    const id = R.prop('id')(observation);
    const dimAnnotations = getRelationnalAnnotations(R.propOr([],'annotations')(observation))(annotations)
    const isHidden = R.includes(id)(hiddenIds) ? hiddenFormat : getIsHidden(dimAnnotations);
    return ({
      observation: R.append({
        ...observation,
        name: getLocalisedProp('names', observation),
        values: getValuesEnhanced(locale, annotations, isTimeDimension(id))(R.propOr([],'values')(observation)),
        role: R.isNil(observation.roles) ? null : R.head(observation.roles),
        ...isHidden,
      })(acc.observation),
      dimensionsLayout: setAnnotationsLayout(id, acc.dimensionsLayout)(dimAnnotations),
    });
  }, { observation: [], dimensionsLayout: {}});

//-----------------------------------------------------------------------------------------Structure
  const { dimensionsLayout, observation } = getObservations(locale)(dimObservations);
  const resStructure = {
    ...structure,
    name: getLocalisedProp('names', structure),
    annotations,
    attributes: resAttributes,
    dimensions: { observation },
  };

//------------------------------------------------------------------------------------------------------Layout
  const dataSetsLayout = R.reduce((acc, dataSetsAnnotation) => {
    const title = R.propOr('', 'title')(dataSetsAnnotation);
    if (R.isEmpty(title)) return acc;
    return setAnnotationsLayout(title, acc)([dataSetsAnnotation])
  }, {})(dataSetsAnnotations);

  return ({ data: { header: resHeader, dataSets, structure: resStructure }, layout: R.mergeRight(dimensionsLayout, dataSetsLayout)});
};
