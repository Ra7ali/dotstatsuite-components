import { is, isEmpty, isNil, map, not, of, path, pipe, prop, length, split, when } from 'ramda';
import {
  defaultSubtitle,
  extractSdmxArtefacts,
  headerUprsLabel,
  parseDisplay,
  uprAttributes,
  uprAttributesWithOneRelevantValue,
} from '../';

import { dimensionValueDisplay } from '../dimension-utils';

export const initialState = {
  subtitle: undefined,
  title: undefined,
  sourceLabel: undefined,
};

export const informationStateFromNewProps = (props, state) => {
  const { id } = extractSdmxArtefacts(props.data);
  if (id !== state.id) {
    return initialState;
  }
  return ({});
};

const getDataflowCode = (url) => {
  if (isNil(url) || isEmpty(url)) {
    return null;
  }
  const match = url.match(/[\w\/]+\/data\/([^\/]+)\//);
  if (!is(Array, match)) {
    return null;
  }
  //v8 case: 'AGENCY,CODE,VERSION'
  const id = match[1];
  const splitId = split(',', id);
  if ((length(splitId) === 1)) {
    return splitId[0];
  }
  return splitId[1];
};

const defaultTitle = (props, display) => {
  const name = path(['data', 'structure', 'name'], props);
  const code = getDataflowCode(props.sdmxUrl);
  return dimensionValueDisplay(display)({ id: code, name });
};

const defaultSourceLabel = (props, title) => {
  const propsLabel = path(['source', 'label'], props);
  return (isNil(propsLabel) || isEmpty(propsLabel))
    ? title : propsLabel;
}

export const toHeader = (props, state) => { 
  const display = parseDisplay(props.display);
  const stateTitle = prop('title', state);
  const stateSubtitle = prop('subtitle', state);
  const stateSourceLabel = prop('sourceLabel', state);
  const propsSourceLink = path(['source', 'link'], props);
  const title = (isNil(stateTitle) || isEmpty(stateTitle))
    ? defaultTitle(props, display) : stateTitle;
  const subtitle = (isNil(stateSubtitle) || isEmpty(stateSubtitle))
    ? defaultSubtitle(props.data, display) : stateSubtitle;
  const sourceLink = (isNil(propsSourceLink) || isEmpty(propsSourceLink))
    ? props.sdmxUrl : propsSourceLink;
  const sourceLabel = (isNil(stateSourceLabel) || isEmpty(stateSourceLabel))
    ? defaultSourceLabel(props, title) : stateSourceLabel;
  return ({
    footnotes: { source: sourceLink, sourceLabel },
    title: { label: title },
    subtitle: pipe(
      when(pipe(is(Array), not), of),
      map(entry => ({ label: entry }))
    )(subtitle),
    uprs: pipe(
      extractSdmxArtefacts,
      ({ attributes }) => uprAttributes(attributes),
      uprAttributesWithOneRelevantValue,
      headerUprsLabel(display),
    )(props.data)
  });
};
