import * as R from 'ramda';
import { extractSdmxArtefacts, focus } from '../';
import { BAR, ROW, SCATTER, TIMELINE, TYPES } from '../constants';
import { toSingularity, hasFocus } from './utils';

const sampleFocusTypes = { [BAR]: BAR, [ROW]: ROW, [TIMELINE]: TIMELINE };

const focusComparator = (type) => {
  switch(type) {
    case SCATTER:
      return (op1, op2) => {
        const v1 = R.prop('value', op1);
        const v2 = R.prop('value', op2);
        return ((R.prop('dimensionId', v1) === R.prop('dimensionId', v2))
          && (R.prop('dimensionValueId', v1) === R.prop('dimensionValueId', v2)));
      };
    default:
      return (op1, op2) => op1.value === op2.value;
  }
};

export const focusOptions = (props, state, otherFocus, singularity) => {
  if (!hasFocus(props.type)) {
    return [];
  }
  const options = focus(
    props.data,
    props.type,
    singularity,
    props.display
  );
  return R.differenceWith(
    focusComparator(props.type),
    options,
    R.prop(otherFocus, state)
  );
};

export const onChangeFocusSelection = (_value) => {
  const value = R.isNil(_value) ? [] : _value;
  return (R.is(Array, value) ? value : [value]);
};

const toKey = (type) => (entry) => {
  switch(type) {
    case SCATTER:
      const value = R.prop('value', entry);
      return `${R.prop('dimensionId', value)}${R.prop('dimensionValueId', value)}`; //to change with an hash
    default:
      return R.prop('value', entry);
  }
};

const filterFocus = (focus, keyedOptions, type) => R.reduce(
  (acc, entry) => {
    const entryKey = toKey(type)(entry);
    if (R.has(entryKey, keyedOptions)) {
      return R.append(R.prop(entryKey, keyedOptions), acc);
    }
    return acc;
  },
  [],
  focus
)

export const focusStateFromNewProps = ({ data, display, type }, singularity, state) => {
  const { id } = extractSdmxArtefacts(data);
  const highlightState = R.propOr([], 'highlight', state);
  const baselineState = R.propOr([], 'baseline', state);
  const options = focus(data, type, singularity, display);
  const keyedOptions = R.indexBy(toKey(type), options);
  return ({
    baseline: filterFocus(baselineState, keyedOptions, R.prop('type', state)),
    highlight: filterFocus(highlightState, keyedOptions, R.prop('type', state)),
  });
};

/*
  This props/state parser which will be exposed in highlight/baseline properties,
  only deals for Bar and Row Chart (no chart dimension singularity involved).
  For other chart types, their own chart dimension singularity parser will handle focus management.
*/

export const sampleFocusStateFromNewProps = (props, state) => {
  if (!R.has(props.type, sampleFocusTypes)) {
    return ({}); //no changes 
  }
  return focusStateFromNewProps(props, null, state);
};