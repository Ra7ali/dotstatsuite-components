import * as R from 'ramda';
import {
  extractSdmxArtefacts,
  focus,
  header,
  prepareData,
  series,
  timelineFrequency
} from '../';
import {
  H_SYMBOL,
  PERCENT,
  SCATTER,
  STACKED,
  TYPES,
  TIMELINE,
  V_SYMBOL,
  VALUES
} from '../constants';
import {
  onChangeFocusSelection,
  focusOptions,
  sampleFocusStateFromNewProps,
} from './focus';
import {
  isScatter,
  onChangeScatterDim,
  onChangeScatterVal,
  scatterStateFromNewProps,
  scatterStateValueToOption,
  scatterValueOptions,
  toScatterSingularity,
} from './scatter';
import {
  isSymbol,
  onChangeSymbolDim,
  symbolStateFromNewProps,
  toSymbolSingularity,
} from './symbol';
import {
  isStacked,
  onChangeStackedDimension,
  onChangeStackedMode,
  stackedModeOptions,
  stackedStateFromNewProps,
  toStackedSingularity
} from './stacked';
import {
  linearStateFromNewProps,
  toProperties as toLinearProperties
} from './linear';
import {
  informationStateFromNewProps,
  intitialState as informationInitialState,
  toHeader,
} from './information';
import {
  chartDimensionOptions,
  chartDimensionToOption,
  hasFocus,
  isChart,
  isInt,
  isNaturalInt,
  optionParser,
} from './utils';
import { getInformationsStateFromNewProps } from './getInformationsStateFromNewProps';
import { getTitleLabel, getSubtitle, getSourceLabel } from './getHeaderProps';
import { stringifySubtitle } from './getStringifiedSubtitle';
/*
 - width/height -> initial undefined, if undefined or blank, replaced by options.base
*/

export const toSingularity = ({ data, type }, state) => {
  switch(type) {
    case SCATTER:
      return toScatterSingularity(state);
    case H_SYMBOL: case V_SYMBOL:
      return toSymbolSingularity(data, state);
    case STACKED:
      return toStackedSingularity(state);
    default:
      return null;
  }
};

export const toChartData = (props, state) => {
  return ({
    ...toHeader(props, state),
    frequency: props.type === TIMELINE ? timelineFrequency(props.data) : null,
    series: series(
      props.data,
      props.type,
      R.pick(['highlight', 'baseline'], state),
      toSingularity(props, state),
      props.map,
      props.display,
      props.formaterIds
    ),
    share: {
      source: props.sdmxUrl,
      focused: R.pick(['highlight', 'baseline'], state),
      chartDimension: toSingularity(props, state),
    }
  });
};

const toFloat = (value) => {
  const float = parseFloat(value);
  return isNaN(float) ? undefined : float;
}

const toOption = (stateValue, optionValue) => R.isNil(stateValue)
  ? optionValue : toFloat(stateValue);

export const toChartOptions = (props, state) => {
  const options = R.propOr({}, 'options', props);
  const base = R.propOr({}, 'base', options);
  const axis = R.propOr({}, 'axis', options);
  const axeX = R.propOr({}, 'x', axis);
  const axeY = R.propOr({}, 'y', axis);
  const linearX = R.propOr({}, 'linear', axeX);
  const linearY = R.propOr({}, 'linear', axeY);
  const tickX = R.propOr({}, 'tick', axeX);
  const pivotX = R.propOr({}, 'pivot', linearX);
  const pivotY = R.propOr({}, 'pivot', linearY);
  const map = R.propOr({}, 'map', props);
  return ({
    ...options,
    axis: {
      ...axis,
      x: {
        ...axeX,
        tick: {
          ...tickX,
          step: toOption(state.freqStep, tickX.step)
        },
        linear: {
          ...linearX,
          max: toOption(state.maxX, linearX.max),
          min: toOption(state.minX, linearX.min),
          pivot: {
            value: toOption(state.pivotX, pivotX.value)
          },
          step: toOption(
            props.type === TIMELINE ? 1 :  state.stepX,
            linearX.step
          ),
        }
      },
      y: {
        ...axeY,
        linear: {
          ...linearY,
          max: toOption(state.maxY, linearY.max),
          min: toOption(state.minY, linearY.min),
          pivot: {
            value: toOption(state.pivotY, pivotY.value)
          },
          step: toOption(state.stepY, linearY.step),
        }
      }
    },
    map: R.pick(['projection', 'scale'], map),
    base: {
      ...base,
      height: toOption(state.height, base.height),
      width: toOption(state.width, base.width),
    },
  });
};

export const initialState = {
  highlight: [],
  baseline: [],
  width: undefined,
  height: undefined,
  freqStep: undefined,
  scatterDimension: undefined,
  scatterX: undefined,
  scatterY: undefined,
  symbolDimension: undefined,
  stackedDimension: undefined,
  stackedMode: undefined,
  id: undefined,
  type: undefined,
};

export const stateFromNewProps = (props, state) => ({
  ...sampleFocusStateFromNewProps(props, state),
  ...scatterStateFromNewProps(props, state),
  ...symbolStateFromNewProps(props, state),
  ...stackedStateFromNewProps(props, state),
  ...linearStateFromNewProps(props.type),
  ...getInformationsStateFromNewProps(props, state),
  width: R.prop('width', state),
  height: R.prop('height', state),
  freqStep: props.type === TIMELINE
    ? R.prop('freqStep', state) || '1' : undefined,
  type: R.prop('type', props),
});

const propertiesParsers = {
  highlight: onChangeFocusSelection,
  baseline: onChangeFocusSelection,
  width: (value) => isNaturalInt(value) ? value : undefined,
  height: (value) => isNaturalInt(value) ? value : undefined,
  freqStep: (value) => isNaturalInt(value) ? value : undefined,
  title: R.identity,
  subtitle: R.identity,
  sourceLabel: R.identity,
  scatterDimension: optionParser,
  scatterX: optionParser,
  scatterY: optionParser,
  symbolDimension: optionParser,
  stackedDimension: optionParser,
  stackedMode: optionParser,
  maxX: (value) => isInt(value) ? value : undefined,
  maxY: (value) => isInt(value) ? value : undefined,
  minX: (value) => isInt(value) ? value : undefined,
  minY: (value) => isInt(value) ? value : undefined,
  pivotX: (value) => isInt(value) ? value : undefined,
  pivotY: (value) => isInt(value) ? value : undefined,
  stepX: (value) => isNaturalInt(value) ? value : undefined,
  stepY: (value) => isNaturalInt(value) ? value : undefined,
}

export const onChangeProperties = (props, state, onChange) => (_newProperties) => {
  const apply = R.is(Function, onChange) ? onChange : R.identity;
  const newProperties = R.mapObjIndexed(
    (value, key) => {
      const _parser = R.prop(key, propertiesParsers);
      const parser = R.is(Function, _parser) ? _parser : R.identity;
      return parser(value); 
    },
    _newProperties
  );
  const nextState = stateFromNewProps(props, { ...state, ...newProperties });
  apply(nextState);
}

export const toProperties = (props, state, onChange) => ({
  ...toLinearProperties(props, state, onChange),
  highlight: {
    id: 'highlight',
    isActive: hasFocus(props.type),
    onChange: R.pipe(
      onChangeFocusSelection,
      selection => {
        onChange({ highlight: selection });
      }
    ),
    options: focusOptions(
      props,
      state,
      'baseline',
      toSingularity(props, state)
    ),
    value: R.prop('highlight', state),
  },
  baseline: {
    id: 'baseline',
    isActive: hasFocus(props.type),
    onChange: R.pipe(
      onChangeFocusSelection,
      selection => onChange({ baseline: selection })
    ),
    options: focusOptions(
      props,
      state,
      'highlight',
      toSingularity(props, state)
    ),
    value: R.prop('baseline', state),
  },
  width: {
    id: 'width',
    isActive: isChart(props.type),
    onChange: (value) => onChange({
      width: isNaturalInt(value) ? value : undefined
    }),
    value: R.prop('width', state),
  },
  height: {
    id: 'height',
    isActive: isChart(props.type),
    onChange: (value) => onChange({
      height: isNaturalInt(value) ? value : undefined
    }),
    value: R.prop('height', state),
  },
  freqStep: {
    id: 'freqStep',
    isActive: props.type === TIMELINE,
    onChange: (value) => onChange({
      freqStep: isNaturalInt(value) ? String(value) : undefined
    }),
    value: R.prop('freqStep', state),
  },
  title: {
    id: 'title',
    isActive: isChart(props.type),
    onChange: (value) => onChange({ title: value }),
    value: getTitleLabel(props, state),
    onReset: R.isNil(state.title)
      ? null : () => onChange({ title: null })
  },
  subtitle: {
    id: 'subtitle',
    isActive: isChart(props.type),
    onChange: (value) => onChange({ subtitle: value }),
    value: R.pipe(getSubtitle, stringifySubtitle)(props, state),
    onReset: R.isNil(state.subtitle)
      ? null : () => onChange({ subtitle: null })
  },
  source: {
    id: 'source',
    isActive: isChart(props.type),
    onChange: (value) => onChange({ sourceLabel: value }),
    value: getSourceLabel(props, state),
    onReset: R.isNil(state.sourceLabel)
      ? null : () => onChange({ sourceLabel: null })
  },
  scatterDimension: {
    id: 'scatterDimension',
    isActive: isScatter(props.type),
    onChange: R.pipe(
      optionParser,
      onChangeScatterDim(props, state),
      onChange
    ),
    options: chartDimensionOptions(props, isScatter),
    value: chartDimensionToOption('scatterDimension')(props, state),
  },
  scatterX: {
    id: 'scatterX',
    isActive: isScatter(props.type),
    onChange: R.pipe(
      optionParser,
      onChangeScatterVal(props, state, 'scatterX', 'scatterY'),
      onChange
    ),
    options: scatterValueOptions(props, state),
    value: scatterStateValueToOption('scatterX', props, state),
  },
  scatterY: {
    id: 'scatterY',
    isActive: isScatter(props.type),
    onChange: R.pipe(
      optionParser,
      onChangeScatterVal(props, state, 'scatterY', 'scatterX'),
      onChange
    ),
    options: scatterValueOptions(props, state),
    value: scatterStateValueToOption('scatterY', props, state),
  },
  symbolDimension: {
    id: 'symbolDimension',
    isActive: isSymbol(props.type),
    options: chartDimensionOptions(props, isSymbol),
    onChange: R.pipe(
      optionParser,
      onChangeSymbolDim(props),
      onChange
    ),
    value: chartDimensionToOption('symbolDimension')(props, state)
  },
  stackedDimension: {
    id: 'stackedDimension',
    isActive: isStacked(props.type),
    options: chartDimensionOptions(props, isStacked),
    onChange: R.pipe(
      optionParser,
      onChangeStackedDimension(props, state),
      onChange,
    ),
    value: chartDimensionToOption('stackedDimension')(props, state)
  },
  stackedMode: {
    id: 'stackedMode',
    isActive: isStacked(props.type),
    options: stackedModeOptions,
    onChange: R.pipe(
      optionParser,
      onChangeStackedMode,
      onChange
    ),
    value: R.prop('stackedMode', state)
  },
  logo: {
    id: 'logo',
    isActive: isChart(props.type),
    checked: !state.withLogo,
    onChange: () => onChange({ withLogo: !state.withLogo })
  },
  copyright: {
    id: 'copyright',
    isActive: isChart(props.type),
    checked: !state.withCopyright,
    onChange: () => onChange({ withCopyright: !state.withCopyright })
  }
});
