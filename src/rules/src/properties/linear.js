import * as R from 'ramda';
import {
  BAR,
  H_SYMBOL,
  ROW,
  SCATTER,
  STACKED,
  TIMELINE,
  TYPES,
  V_SYMBOL,
} from '../constants';
import { isNumber, isPositiveNumber } from './utils';
import { isStacked } from './stacked';

const hasXLinear = (type) => R.has(type, R.pick([H_SYMBOL, ROW, SCATTER], TYPES));

const hasYLinear = (type) => R.has(type, R.pick([BAR, SCATTER, STACKED, TIMELINE, V_SYMBOL], TYPES));

const xInitialState = {
  maxX: undefined,
  minX: undefined,
  pivotX: undefined,
  stepX: undefined,
};

const yInitialState = {
  maxY: undefined,
  minY: undefined,
  pivotY: undefined,
  stepY: undefined,
};

export const initialState = { ...xInitialState, ...yInitialState };

export const linearStateFromNewProps = (type) => {
  const xLinearState = hasXLinear(type) ? {} : xInitialState;
  const YLinearState = hasYLinear(type) ? {} : yInitialState;
  return ({
    ...xLinearState,
    ...R.when(
      R.always(type === STACKED),
      R.assoc('pivotY', 0)
    )(YLinearState),
  });
};

export const toProperties = (props, state, onChange) => ({
  maxX: {
    id: 'maxX',
    isActive: hasXLinear(props.type),
    onChange: (value) => onChange({
      maxX: isNumber(value) ? value : undefined
    }),
    value: R.prop('maxX', state)
  },
  maxY: {
    id: 'maxY',
    isActive: hasYLinear(props.type),
    onChange: (value) => onChange({
      maxY: isNumber(value) ? value : undefined
    }),
    value: R.prop('maxY', state)
  },
  minX: {
    id: 'minX',
    isActive: hasXLinear(props.type),
    onChange: (value) => onChange({
      minX: isNumber(value) ? value : undefined
    }),
    value: R.prop('minX', state)
  },
  minY: {
    id: 'minY',
    isActive: hasYLinear(props.type),
    onChange: (value) => onChange({
      minY: isNumber(value) ? value : undefined
    }),
    value: R.prop('minY', state)
  },
  pivotX: {
    id: 'pivotX',
    isActive: hasXLinear(props.type),
    onChange: (value) => onChange({
      pivotX: isNumber(value) ? value : undefined
    }),
    value: R.when(
      v => R.isNil(v) && props.type === ROW,
      R.always(0)
    )(R.prop('pivotX', state))
  },
  pivotY: {
    id: 'pivotY',
    isActive: (hasYLinear(props.type)  && !(isStacked(props.type))),
    onChange: (value) => onChange({
      pivotY: isNumber(value) ? value : undefined
    }),
    value: R.when(
      v => (R.isNil(v) && R.propEq('type', BAR)(props)) || R.propEq('type', STACKED)(props),
      R.always(0)
    )(R.prop('pivotY', state))
  },
  stepX: {
    id: 'stepX',
    isActive: hasXLinear(props.type),
    onChange: (value) => onChange({
      stepX: isPositiveNumber(value) ? value : undefined
    }),
    value: R.prop('stepX', state)
  },
  stepY: {
    id: 'stepY',
    isActive: hasYLinear(props.type),
    onChange: (value) => onChange({
      stepY: isPositiveNumber(value) ? value : undefined
    }),
    value: R.prop('stepY', state)
  },
});
