import * as R from 'ramda';
import {
  getDefaultSubtitle,
  getDefaultTitle,
  getHeaderUnits,
  getSubtitleFlags,
  getTitleFlags
} from '../header';
import { isChart } from './utils';

const isCustomInvalid = ({ type }) => (value) => R.isNil(value) || !isChart(type);

export const getTitleLabel = (props, state) => R.when(
  isCustomInvalid({ type: props.type }),
  R.always(getDefaultTitle({ dataflow: props.dataflow, display: props.display }))
)(state.title);

export const getSubtitle = (props, state) => R.ifElse(
  isCustomInvalid({type: props.type }),
  R.always(getDefaultSubtitle({
    customAttributes: props.customAttributes,
    dimensions: state.dimensions,
    display: props.display,
    units: state.units
  })),
  (label) => {
    const flags = getSubtitleFlags({
      customAttributes: props.customAttributes,
      dimensions: state.dimensions,
      display: props.display,
      units: state.units
    });
    return ([{ label, flags }]);
  }
)(state.subtitle);

export const getSourceLabel = (props, state) => R.when(
  isCustomInvalid({ type: props.type }),
  R.always(getDefaultTitle({ dataflow: props.dataflow, display: props.display }))
)(state.sourceLabel);

export const getHeaderProps = (props, state) => {
  const title = {
    label: getTitleLabel(props, state),
    flags: getTitleFlags({
      customAttributes: props.customAttributes,
      dataflowAttributes: state.dataflowAttributes,
      display: props.display,
    })
  };

  const subtitle = getSubtitle(props, state);

  const uprs = getHeaderUnits({ display: props.display, units: state.units });

  return ({ subtitle, title, uprs });
};

export const getFooterProps = (props, state) => ({
  withCopyright: isChart(props.type) ? state.withCopyright : true,
  withLogo: isChart(props.type) ? state.withLogo : true,
  sourceLabel: getSourceLabel(props, state)
});
