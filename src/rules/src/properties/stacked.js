import * as R from 'ramda';
import { PERCENT, STACKED, VALUES } from '../constants';
import {
  getDimensionsWithValues,
  getGroupedDimensions,
  getPropertyDimension,
  toState
} from './utils';
import { focusStateFromNewProps } from './focus';

const getStackedDimension = getPropertyDimension('stackedDimension');

const stackedModes = { [PERCENT]: PERCENT, [VALUES]: VALUES };

export const isStacked = (type) => type === STACKED;

const toStackedState = (dimension, mode) => ({
  stackedDimension: toState(dimension),
  stackedMode: mode,
});

export const toStackedSingularity = (state) => ({
  id: R.prop('stackedDimension', state),
  mode: R.prop('stackedMode', state)
});

export const onChangeStackedDimension = ({ data }, state) => (value) => {
  const dimension = getStackedDimension(data, { stackedDimension: value });
  if (R.isNil(dimension)) {
    return ({});
  }
  return ({
    stackedDimension: toState(dimension),
    highlight: [],
    baseline: [],
  })
};

export const stackedModeOptions = [{ value: VALUES }, { value: PERCENT }];

export const onChangeStackedMode = (value) => R.has(value, stackedModes)
  ? ({ stackedMode: value })
  : ({});

export const stackedStateFromNewProps = (props, state) => {
  const { data, type } = props;
  if (!isStacked(type)) {
    return toStackedState(undefined, undefined);
  }
  const dimensions = getDimensionsWithValues(data);
  if (R.isEmpty(dimensions)) {
    return { ...toStackedState(undefined, undefined), baseline: [], highlight: [] };
  }

  let dimension = getStackedDimension(data, state);
  if (R.isNil(dimension)) {
    const { area, time, other } = getGroupedDimensions(data);
    if (!R.isNil(area) && !R.isEmpty(area)) {
      dimension = R.head(area);
    }
    else if (!R.isNil(time) && !R.isEmpty(time)) {
      dimension = R.head(time)
    }
    else {
      dimension = R.head(other);
    }
  }
  const _mode = R.prop('stackedMode', state);
  const mode = R.has(_mode, stackedModes) ? _mode : VALUES;
  const stackedState = toStackedState(dimension, mode);
  return ({
    ...stackedState,
    ...focusStateFromNewProps(
      props,
      toStackedSingularity(stackedState), 
      state
    )
  });
}
