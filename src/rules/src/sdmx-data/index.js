import * as R from 'ramda';

export const LAYOUT_ROW = 'LAYOUT_ROW';
export const LAYOUT_COLUMN = 'LAYOUT_COLUMN';
export const LAYOUT_ROW_SECTION = 'LAYOUT_ROW_SECTION';

const ROWS = 'rows';
const HEADER = 'header';
const SECTIONS = 'sections';

const ORDER = 'ORDER';
const NOT_DISPLAYED = 'NOT_DISPLAYED';

const getType = R.propOr({}, 'type');
const getIds = (stringIds = '') => R.pipe(
  R.append(R.split(',', stringIds)),
  R.flatten
);
const getPivot = (key, stringIds) => R.over(R.lensProp(key), getIds(stringIds));

export const setAnnotationsLayout = (stringIds, layout) => R.reduce((acc, annotation) => {
  if (R.equals(getType(annotation), LAYOUT_ROW)) return getPivot(ROWS, stringIds)(acc);
  if (R.equals(getType(annotation), LAYOUT_COLUMN)) return getPivot(HEADER, stringIds)(acc);
  if (R.equals(getType(annotation), LAYOUT_ROW_SECTION)) return getPivot(SECTIONS, stringIds)(acc);
  return acc;
}, layout);

export const setIndexPositions = locale => R.reduce((acc, dimAnnotation) => {
  if (R.equals(getType(dimAnnotation), ORDER)) {
    return {
      ...acc,
      __indexPosition: R.pipe(
        R.pathOr({}, ['texts', locale]),
        R.ifElse(R.isEmpty, R.always(null), Number)
      )(dimAnnotation)
    };
  }
  return acc;
}, {});

export const getRelationnalAnnotations = annotationIndexes => annotations => R.map(annotationIndex =>
  R.propOr({}, annotationIndex)(annotations)
)(annotationIndexes);

export const getPriorityIndexPosition = (implicitOrder = 0) => R.over(
  R.lensProp('__indexPosition'),
  R.ifElse(R.isNil, R.always(implicitOrder), R.identity),
);

export const hiddenFormat = { isHidden: true }

export const getIsHidden = R.reduce((acc, annotation) => {
  if (R.equals(getType(annotation), NOT_DISPLAYED)) return hiddenFormat;
  return acc;
}, {});