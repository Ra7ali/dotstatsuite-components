import { get, head, find, isNil } from 'lodash';
import { isAreaDimension, isTimeDimension } from '..';

export default (dimensions, singularity) => {
  const mode = get(singularity, 'mode') === 'percent' ? 'percent' : 'values';
  let serieDimension = null;
  const id = get(singularity, 'id');
  if (!isNil(id)) {
    serieDimension = find(dimensions, { id });
  }
  const areaDimension = find(dimensions, d => isAreaDimension(d.id));
  const timeDimension = find(dimensions, d => isTimeDimension(d.id));

  serieDimension = serieDimension || areaDimension || timeDimension || head(dimensions);

  return ({
    mode: mode,
    id: get(serieDimension, 'id', null)
  });
}