import { find, findKey, get, head, includes, isUndefined, size } from 'lodash';
import * as R from 'ramda';
import { DEFAULT_MAJOR_DIMENSIONS } from '../constants';
import { isFreqDimension, isFreqAttribute, isTimeDimension } from '../';

const getFrequencyArtefact = (dimensions, attributes) => {
  const freq_dimension = find(dimensions, d => isFreqDimension(d.role));
  if (isUndefined(freq_dimension)) {
    const freq_attr = find(attributes, a => isFreqAttribute(a.role));
    if (isUndefined(freq_attr)) {
      return null;
    }
    return ({ type: 'attribute', artefact: freq_attr });
  }
  return ({ type: 'dimension', artefact: freq_dimension });
}

export const deduceFreqFromTimePeriod = (timeDimension) => {
  if (R.isNil(timeDimension)) {
    return null;
  }
  const values = R.prop('values', timeDimension);
  if (R.isNil(values) || R.length(values) < 2) {
    return null;
  }
  return R.cond([
    [R.pipe(R.match(/\d{4}-0?[1-9]|10|11|12$/), R.complement(R.isEmpty)), R.always('monthly')],
    [R.pipe(R.match(/\d{4}-Q[1-4]/), R.complement(R.isEmpty)), R.always('quarterly')],
    [R.pipe(R.match(/\d{4}/), R.complement(R.isEmpty)), R.always('annual')],
    [R.T, R.always(null)]
  ])(R.path(['0', 'id'], values));
};

export default ({ dimensions, attributes }, defaultFrequency) => {
  const frequency = getFrequencyArtefact(dimensions, attributes);
  if (!frequency) {
    const timeDimension = find(dimensions, d => isTimeDimension(d.role));
    return deduceFreqFromTimePeriod(timeDimension);
  }
  const frequencyValues = get(frequency, 'artefact.values');
  if (size(frequencyValues) !== 1)
    return null;
  const frequencyValue = head(frequencyValues);
  const standardFrequencyValues = get(DEFAULT_MAJOR_DIMENSIONS, `FREQ.${frequency.type}.values`, {});
  const matchedValue = findKey(standardFrequencyValues, sFV => includes(sFV.id, frequencyValue.id));
  if (isUndefined(matchedValue))
    return null;
  return matchedValue;
}

