import { find, get, groupBy, has, head, isUndefined, join, keyBy, map, reduce, split, sortBy, sumBy, take } from 'lodash';
import { isTimeDimension } from '../';
import { dimensionValueDisplay } from '../dimension-utils';
import { getObservationFormater } from '../observation-formater';

const flatDatapoints = (observations, dimensions, dimensionsWithValuesIndexedById, serieDimensionId, display, formaterAttrs) => {
  return map(
    observations,
    (observationValue, observationKey) => {
      let x = null;
      const y = Number(head(observationValue));
      const formater = getObservationFormater(observationValue, formaterAttrs);
      const splittedKey = split(observationKey, ':');
      let layerKey = [];
      const layerDimensions = reduce(
        splittedKey,
        (memo, dimensionValueIndex, dimensionIndex) => {
          const dimension = get(dimensions, `[${dimensionIndex}]`);
          if (has(dimensionsWithValuesIndexedById, dimension.id)) {
            const dimensionValue = {
              ...get(dimension, `values[${dimensionValueIndex}]`, {}),
              index: dimensionValueIndex,
              label: dimensionValueDisplay(display)(get(dimension, `values[${dimensionValueIndex}]`, {}))
            }
            if (dimension.id === serieDimensionId) {
              x = dimensionValue;
            }
            else {
              memo.push(dimensionValue);
              layerKey.push(dimensionValueIndex);
            }
          }
          return memo;
        },
        []
      );
      return ({ formater, x, y, layerDimensions, layerKey: join(layerKey, ':') });
    }
  );
};

const serie = (observations, dimensions, dimensionsWithValuesIndexedById, serieDimensionId, display, formaterAttrs) => {
  const serieDimension = find(dimensionsWithValuesIndexedById, d => d.id === serieDimensionId);
  if (isUndefined(serieDimension)) {
    return [];
  }
  const _flatDatapoints = flatDatapoints(observations, dimensions, dimensionsWithValuesIndexedById, serieDimensionId, display, formaterAttrs);
  const layeredDatapoints = groupBy(
    _flatDatapoints,
    dp => dp.layerKey
  ); // { [layeredKey]: [dps] }
  const layerFullSeries = map(
    layeredDatapoints,
    (datapoints, layerKey) => ({ layerDimensions: get(head(datapoints), 'layerDimensions'), layerKey })
  ); // [{ layerDimensions, layerKey }]
  const layerSeries = take(sortBy(layerFullSeries, layer => layer.layerKey), 5);
  const layeredDatapointsKeyedByX = reduce(
    layeredDatapoints,
    (memo, layerDps, key) => {
      const dps = keyBy(layerDps, dp => get(dp, 'x.id', ''));
      return ({
        ...memo,
        [`${key}`]: dps
      });
    },
    {}
  ); // { [layeredKey]: { [x.id]: dp } }
  const datapoints = map(
    serieDimension.values,
    (serieValue, index) => {
      const serieValueId = serieValue.id;
      const x = dimensionValueDisplay(display)(serieValue);
      const y = map(
        layerSeries,
        ({ layerKey }) => get(layeredDatapointsKeyedByX, `${layerKey}.${serieValueId}.y`, null)
      );
      const formaters = map(
        layerSeries,
        ({ layerKey }) => get(layeredDatapointsKeyedByX, `${layerKey}.${serieValueId}.formater`, null)
      );
      return ({ category: x, formaters, index, x, y, key: serieValueId });
    }
  );
  return ({
    datapoints,
    layerSeries: map(
      layerSeries,
      layer => ({
        id: join(map(layer.layerDimensions, l => l.id), ' - '),
        label: join(map(layer.layerDimensions, l => l.label), ' - '),
      })
    )
  });
};

const percentSerie = (serie) => {
  return [{
    layerSeries: serie.layerSeries,
    datapoints: map(
      serie.datapoints,
      dp => {
        const total = sumBy(dp.y, val => Math.abs(val));
        if (!total) {
          return dp;
        }
        return ({
          ...dp,
          y: map(dp.y, value => (Math.abs(value) / total) * 100),
          formaters: null,
        });
      }
    )
  }];
};

const serieSortedByTotal = (serie) => {
  return [{
    layerSeries: serie.layerSeries,
    datapoints: sortBy(
      serie.datapoints,
      dp => sumBy(dp.y, val => val > 0 ? (-1 * val) : 0)
    )
  }];
};

const serieSortedByDataOrder = (serie) => [{ //to robustize with date generation, as in timeline series
  layerSeries: serie.layerSeries,
  datapoints: sortBy(
    serie.datapoints,
    dp => dp.index
  )
}];

export default ({ observations, dimensions }, dimensionsWithValuesIndexedById, singularity, display, formaterAttrs) => {
  const mode = get(singularity, 'mode', 'values');
  const serieDimensionId = get(singularity, 'id', null);
  const _serie = serie(observations, dimensions, dimensionsWithValuesIndexedById, serieDimensionId, display, formaterAttrs);
  if (mode === 'percent') {
    return percentSerie(_serie);
  }
  else if (isTimeDimension(serieDimensionId)) {
    return serieSortedByDataOrder(_serie);
  }
  return serieSortedByTotal(_serie);
};
