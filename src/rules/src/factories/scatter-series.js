import {
  get, reject, map, reduce, head, findIndex, split, includes, has, each, size, find, keys,
  isNil, filter
} from 'lodash';
import { dimensionValueDisplay } from '../dimension-utils';
import { getFormatedObservation } from '../observation-formater';

export function dimensionValueIsValid(dimensionValue, chartDimension) {
  return includes(
    map(['x', 'y'], k => get(chartDimension, `${k}Id`)),
    get(dimensionValue, 'id')
  );
};

export function getDimensionValues(
  dimensionValuesIndexes, dimensions, dimensionsWithValuesIndexedById, chartDimensionId, display
) {
  const dimensionsWithValuesIds = keys(dimensionsWithValuesIndexedById);
  return reduce(
    dimensionValuesIndexes,
    (m, dvi, i) => {
      const dimension = get(dimensions, `[${i}]`);
      const dimensionId = get(dimension, 'id');
      const dimensionValue = get(dimension, `values[${dvi}]`);
      if (dimensionId !== chartDimensionId && includes(dimensionsWithValuesIds, dimensionId)) {
        m[dimensionId] = { ...dimensionValue, name: dimensionValueDisplay(display)(dimensionValue) };
      }
      return m;
    },
    {}
  );
};

export function getDimensionValueKey(chartDimension, dimensionValue) {
  return get(chartDimension, 'xId') === get(dimensionValue, 'id') ? 'x' : 'y';
};

export function datapoints(observations, dimensions, dimensionsWithValuesIndexedById, chartDimension, display, formaterAttrs) {
  const chartDimensionId = get(chartDimension, 'id');
  const chartDimensionIndex = findIndex(dimensions, { id: chartDimensionId });
  const datapoints = reduce(
    observations,
    (m, o, k) => {
      const oDimensionValuesIndexes = split(k, ':');
      const oDimensionValueIndex = oDimensionValuesIndexes[chartDimensionIndex];
      const oDimensionValue = get(
        dimensions,
        `[${chartDimensionIndex}].values[${oDimensionValueIndex}]`
      );
      if (dimensionValueIsValid(oDimensionValue, chartDimension)) {
        const oPartialKey = reject(
          oDimensionValuesIndexes,
          (o, i) => i === chartDimensionIndex
        ).join('');
        if (!has(m, oPartialKey)) {
          const dimensionValues = getDimensionValues(
            oDimensionValuesIndexes, dimensions, dimensionsWithValuesIndexedById, chartDimensionId, display
          );
          m[oPartialKey] = { dimensionValues };
        }
        const dimensionValueKey = getDimensionValueKey(chartDimension, oDimensionValue);
        m[oPartialKey][dimensionValueKey] = head(o);
        m[oPartialKey][`${dimensionValueKey}Format`] = getFormatedObservation(o, formaterAttrs);
      }
      return m;
    },
    {}
  );
  // it is possible to have null observations or incomplete datapoints (just an x or y)
  return filter(
    datapoints,
    dp => ((!isNil(dp.x)) && (!isNil(dp.y)))
  );
};

export const series = (observations, dimensions, dimensionsWithValuesIndexedById, chartDimension, display, formaterAttrs) => {
  // the 2 following lines may sound absurd but we could have 2 dimensions involved
  // and not only 1 as first specified...
  const xValues = get(dimensionsWithValuesIndexedById, `${get(chartDimension, 'id')}.values`, []);
  const yValues = get(dimensionsWithValuesIndexedById, `${get(chartDimension, 'id')}.values`, []);
  const xValue = find(xValues, { id: get(chartDimension, 'xId') });
  const yValue = find(yValues, { id: get(chartDimension, 'yId') });
  return [{
    dimensionValues: {
      x: { ...xValue, name: dimensionValueDisplay(display)(xValue) },
      y: { ...yValue, name: dimensionValueDisplay(display)(yValue) }
    },
    datapoints: datapoints(observations, dimensions, dimensionsWithValuesIndexedById, chartDimension, display, formaterAttrs)
  }]
};

export default ({ observations, dimensions }, dimensionsWithValuesIndexedById, chartDimension, display, formaterAttrs) => (
  series(observations, dimensions, dimensionsWithValuesIndexedById, chartDimension, display, formaterAttrs)
);
