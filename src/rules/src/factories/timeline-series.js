import {
  concat, filter, findIndex, get, has, head, map, reduce, size, sortBy, split, keys, includes
} from 'lodash';
import { FREQ_A, FREQ_Q, FREQ_M } from '../constants';
import { isTimeDimension } from '../';
import { categoryDisplay, dimensionValueDisplayAt } from '../dimension-utils';
import { getFormatedObservation } from '../observation-formater';

const getLineCategory = (
  oDimensionValuesIndexes, dimensions, timePeriodDimensionIndex, dimensionsWithValuesIndexedById
) => (
  reduce(
    oDimensionValuesIndexes,
    (memo, dimensionValueIndex, dimensionIndex) => {
      const dimension = get(dimensions, `[${dimensionIndex}]`, {});
      if ((dimensionIndex !== timePeriodDimensionIndex) && (has(dimensionsWithValuesIndexedById, dimension.id))) {
        return concat(
          memo, get(dimensions, `[${dimensionIndex}].values[${dimensionValueIndex}].name`, '')
        );
      }
      return memo;
    },
    []
  ).join(' - ')
);

const getLineCategoryId = (
  oDimensionValuesIndexes, dimensions, timePeriodDimensionIndex, dimensionsWithValuesIndexedById
) => (
  reduce(
    oDimensionValuesIndexes,
    (memo, dimensionValueIndex, dimensionIndex) => {
      const dimension = get(dimensions, `[${dimensionIndex}]`, {});
      if ((dimensionIndex !== timePeriodDimensionIndex) && (has(dimensionsWithValuesIndexedById, dimension.id))) {
        return concat(
          memo, get(dimensions, `[${dimensionIndex}].values[${dimensionValueIndex}].id`, '')
        );
      }
      return memo;
    },
    []
  ).join(' - ')
);

// u. as f.
export function getDateFromTimePeriodPeriodAndFrequency(timePeriod, frequency) {
  if (!timePeriod || !frequency) return 0;

  switch(frequency) {
    case FREQ_A:
      const am = timePeriod.id.match(/(\d{4})/);
      if (!am) return 0;
      const [a, ayear] = am;
      return new Date(ayear, 0, 1);
    case FREQ_Q:
      const qm = timePeriod.id.match(/(\d{4})-Q([1-4])/);
      if (!qm) return 0;
      const [q, qyear, quarter] = qm;
      return new Date(qyear, (quarter - 1)*3, 1);
    case FREQ_M:
      const mm = timePeriod.id.match(/(\d{4})-0?([1-9]|10|11|12)$/);
      if (!mm) return 0;
      const [m, myear, month] = mm;
      return new Date(myear, month-1, 1);
    default: return 0;
  }
};

const getDimensionValues = (splitObservationKey, dimensions, dimensionsWithValuesIndexedById, rejectedId, display) => (
  reduce(
    splitObservationKey,
    (memo, dimensionValueIndex, dimensionIndex) => {
      const dimension = get(dimensions, `[${dimensionIndex}]`, {});
      if (has(dimensionsWithValuesIndexedById, dimension.id) && dimension.id !== rejectedId) {
        const dimensionValue = get(dimension, `values[${dimensionValueIndex}]`, {});
        memo[dimensionValue.id] = {
          ...dimensionValue,
          name: dimensionValueDisplayAt(dimension, dimensionValueIndex, display)
        }
      }
      return memo;
    },
    {}
  )
);

const series = (
  observations, dimensions,
  dimensionsWithValuesIndexedById,
  timePeriodDimensionIndex, frequency,
  display, formaterAttrs
) => {
  const rawSeries = reduce(
    observations,
    (memo, o, k) => {
      const oDimensionValuesIndexes = split(k, ':');

      const timePeriodDimension = get(dimensions, `[${timePeriodDimensionIndex}]`);
      const timePeriodDimensionValueIndex = oDimensionValuesIndexes[timePeriodDimensionIndex];
      const timePeriod = get(timePeriodDimension, `values[${timePeriodDimensionValueIndex}]`);

      const datapoint = {
        x: getDateFromTimePeriodPeriodAndFrequency(timePeriod, frequency),
        y: head(o),
        formatedValue: getFormatedObservation(o, formaterAttrs),
        dimensionValues: getDimensionValues(
          oDimensionValuesIndexes, dimensions, dimensionsWithValuesIndexedById, timePeriodDimension.id, display
        ),
        //index: Number(timePeriodDimensionValueIndex)
      };

      const category = categoryDisplay(
        oDimensionValuesIndexes, dimensions, dimensionsWithValuesIndexedById, timePeriodDimension.id, display
      );
      const key = categoryDisplay(
        oDimensionValuesIndexes, dimensions, dimensionsWithValuesIndexedById, timePeriodDimension.id, 'code'
      );
      const serieIndex = findIndex(memo, serie => serie.key === key);

      if (serieIndex !== -1) memo[serieIndex].datapoints.push(datapoint);
      else memo.push({ datapoints: [datapoint], category, key });

      return memo;
    },
    []
  );
  return map(
    rawSeries,
    serie => ({
      ...serie,
      datapoints: sortBy(
        serie.datapoints,
        datapoint => datapoint.x
      )
    })
  );
};

export default ({ observations, dimensions }, dimensionsWithValuesIndexedById, frequency, display, formaterAttrs) => {
  const timePeriodDimensionIndex = findIndex(dimensions, d => isTimeDimension(d.role));
  const timePeriodDimension = get(dimensions, `[${timePeriodDimensionIndex}]`);
  const timePeriodDimensionHasManyPeriods = !has(dimensionsWithValuesIndexedById, get(timePeriodDimension, 'id'));
  if ((timePeriodDimensionIndex === -1) || timePeriodDimensionHasManyPeriods) return [];

  if (!frequency) return [];
  
  return series(
    observations, dimensions,
    dimensionsWithValuesIndexedById,
    timePeriodDimensionIndex, frequency, display, formaterAttrs
  );
};
