import * as R from 'ramda';

export const getConfirmedSeriesAttributesIds = (attributes, layoutIds) => R.reduce(
  (acc, attribute) => {
    const targetDimensions = R.pathOr([], ['relationship', 'dimensions'], attribute);

    const headerTargets = R.intersection(targetDimensions, R.propOr([], 'header', layoutIds));
    const sectionsAndRows = R.pipe(
      R.props(['sections', 'rows']),
      R.map(R.when(R.isNil, R.always([]))),
      R.unnest
    )(layoutIds);
    const rowsTargets = R.intersection(targetDimensions, sectionsAndRows);

    const isValid = R.pipe(
      R.reject(R.isEmpty),
      R.length,
      R.equals(1),
    )([headerTargets, rowsTargets]);

    return R.when(
      R.always(isValid),
      R.append(attribute.id)
    )(acc);
  },
  []
)(attributes);
