import * as R from 'ramda';
import { getFlags } from './getCells';
import { dimensionValueDisplay } from '../../dimension-utils';

export const getLayoutSerieAttributes  = (layoutSerie, seriesAttributes) => {
  const length = R.length(layoutSerie);
  if (length === 0 || length === 1) {
    return null;
  }

  const serieIndexes = R.reduce(
    (acc, entry) => {
      const dimIndex = R.pathOr(null, ['dimension', '__index'], entry);
      const valIndex = R.pathOr(null, ['value', '__index'], entry);

      if (R.isNil(dimIndex) || R.isNil(valIndex)) {
        return acc;
      }

      return R.assoc(String(dimIndex), String(valIndex))(acc);
    },
    {},
    layoutSerie
  );

  return R.pipe(
    R.filter(
      R.pipe(
        R.propOr([], 'serie'),
        R.addIndex(R.all)((valIndex, dimIndex) => {
          if (valIndex === 'x') {
            return true;
          }
          return R.propEq(String(dimIndex), String(valIndex), serieIndexes);
        }),
      ),
    )
  )(seriesAttributes);
};

const getFlagsFromSeriesAttributes = (display, customAttributes) => R.pipe(
  R.values,
  R.pluck('attributes'),
  R.mergeAll,
  (attributes) => getFlags(customAttributes, dimensionValueDisplay(display))({ attributes })
);

const formatCell = (display, customAttributes) => R.converge(
  (label, flags, others) => ({ label, flags, ...others }),
  [
    dimensionValueDisplay(display),
    getFlags(customAttributes, dimensionValueDisplay(display)),
    R.pick(['parents', 'unitsLabel'])
  ]
);

export const getSublayoutDataCells = (display, customAttributes) => R.map(
  R.evolve({
    dimension: formatCell(display, customAttributes),
    value: formatCell(display, customAttributes)
  })
);

const getSerieKey = R.pipe(R.reduce(
  (acc, entry) => {
    const index = R.pathOr(null, ['value', '__index'], entry);
    if (R.isNil(index)) {
      return acc;
    }
    return R.append(index, acc);
  },
  []
), R.join(':'));

export const getLayoutDataWithFlags = (seriesAttributes, display, customAttributes) => {
  const formatSublayout = getAttributes => R.converge(
    (data, key, flags) => ({ data, key, flags }),
    [
      getSublayoutDataCells(display, customAttributes),
      getSerieKey,
      R.pipe(getAttributes, getFlagsFromSeriesAttributes(display, customAttributes))
    ]
  );

  return R.evolve({
  headerData: R.map(formatSublayout(data => getLayoutSerieAttributes(data, seriesAttributes))),
  sectionsData: R.map(
    sectionData => {
      const sectionSerie = R.head(sectionData);
      const sectionAttributes = getLayoutSerieAttributes(sectionSerie, seriesAttributes);
      return ([
        formatSublayout(() => sectionAttributes)(sectionSerie),
        R.map(
          formatSublayout(rowData => getLayoutSerieAttributes(R.concat(sectionSerie, rowData), R.omit(R.keys(sectionAttributes), seriesAttributes)))
        )(R.last(sectionData))
      ]);
    }
  )
});
}
