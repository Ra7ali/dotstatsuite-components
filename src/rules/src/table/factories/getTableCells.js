import * as R from 'ramda';
import { getCells } from './getCells';
import { getCuratedCells } from './getCuratedCells';

export const getTableCells = (data, layoutIds, display, customAttributes) => {
  const cells = getCells(data, layoutIds, display, customAttributes);

  const layout = R.pipe(
    R.mergeRight({ header: [], sections: [], rows: [] }),
    R.mapObjIndexed(
      R.flip(R.props)(R.pathOr({}, ['dimensions', 'many'], data))
    )
  )(layoutIds);

  return getCuratedCells({ layout, observations: cells, shape: ['header', 'sections', 'rows'] });
};
