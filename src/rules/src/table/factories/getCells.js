import * as R from 'ramda';
import numeral from 'numeral';
import { dimensionValueDisplay } from '../../dimension-utils';

const getAttrLabel = (display) => (attribute) => `${display(attribute)}: ${display(R.propOr({}, 'value', attribute))}`;

export const getFlags = (customAttributesIds, display, unitsProps) => ({ attributes={}, units={} }) => {
  const refinedAttributes = R.omit(R.propOr([], 'rejected', customAttributesIds), attributes);
  const flags = R.pipe(R.pick(R.propOr([], 'flags', customAttributesIds)), R.values)(refinedAttributes);
  const footnotes = R.pipe(R.omit(R.propOr([], 'flags', customAttributesIds)), R.values)(refinedAttributes);

  const unitsDisplay = R.prop('unitsDisplay', unitsProps);
  const rejectedUnitsValueIds = R.propOr([], 'rejectedValueIds', unitsProps);

  const separatedUnitsFootnotes = R.ifElse(
    R.always(unitsDisplay === 'cells'),
    R.always([]),
    (units) => {
      const unitsSerie = R.pathOr({}, ['unitsSeries', units.serieKey], unitsProps);
      const validAttrs = R.omit(R.keys(unitsSerie), units);
      return R.pipe(
        R.props(R.propOr([], 'unitsDefinitionCodes', unitsProps)),
        R.reject(entry => {
          return (R.isNil(entry) || R.includes(R.path(['value', 'id'], entry), rejectedUnitsValueIds));
        })
      )(validAttrs);
    }
  )(units);

  const fullUnitsFootnote = R.ifElse(
    R.always(unitsDisplay === 'cells'),
    (units) => {
      if (R.isEmpty(units)) {
        return ({});
      }
      const unitsLabel = display(unitsProps.unitDimension);
      const valuesLabel = R.pipe(
        R.omit(R.propOr([], 'layoutDimsIds', unitsProps)),
        R.props(unitsProps.unitsDefinitionCodes),
        R.reduce(
          (values, entry) => {
            if (R.isNil(entry) || R.includes(R.path(['value', 'id'], entry), rejectedUnitsValueIds)) {
              return values
            }
            return R.append(display(entry.value), values);
          },
          []
        ),
        R.join(', ')
      )(units);
      if (R.isNil(valuesLabel) || R.isEmpty(valuesLabel)) {
        return ({});
      }
      return ({ label: `${unitsLabel}: ${valuesLabel}` });
    },
    R.always(null)
  )(units);

  return R.useWith(
    R.concat,
    [
      R.map(
        R.converge((code, label) => ({ code, label }), [R.path(['value', 'id']), getAttrLabel(display)])
      ),
      R.pipe(
        R.flip(R.concat)(separatedUnitsFootnotes),
        R.map(R.pipe(getAttrLabel(display), label => ({ label }))),
        R.when(
          R.always(unitsDisplay === 'cells' && !R.isEmpty(fullUnitsFootnote)),
          R.append(fullUnitsFootnote)
        )
      )
    ]
  )(flags, footnotes);
};

const isValidNumber = R.both(R.is(Number), R.complement(R.equals(NaN)));

export const getValue = (observation) => {
  const scale = Number(observation.prefscale);
  const formatScale = R.ifElse(
    R.always(isValidNumber(scale)),
    value => numeral(value).multiply(Math.pow(10, -1 * Number(scale))).value(),
    R.identity
  );

  const formatSeparator = value => numeral(value).format(`0,0.[0000000]`);

  const decimals = Number(observation.decimals);
  const formatDecimals = R.ifElse(
    R.always(isValidNumber(decimals)),
    value => numeral(value).format(
      R.ifElse(R.equals(0), R.always('0,0'), d => `0,0.${R.join('', R.times(R.always('0'), d))}`)(decimals)
    ),
    formatSeparator
  );

  const res = R.ifElse(
    isValidNumber,
    R.pipe(
      formatScale,
      formatDecimals,
    ),
    R.always('..')
  )(observation.value);
  return res;
};

export const getCells = (data, display, customAttributes, unitsProps) => R.mapObjIndexed(
  (observation) => R.pipe(
    R.set(
      R.lensProp('flags'),
      getFlags(
        customAttributes, dimensionValueDisplay(display), unitsProps
      )(R.pick(['attributes', 'units'], observation))
    ),
    R.set(R.lensProp('intValue'), R.prop('value')(observation)),
    R.set(
      R.lensProp('value'),
      getValue(observation)
    )
  )({}),
  R.propOr({}, 'observations', data)
);
