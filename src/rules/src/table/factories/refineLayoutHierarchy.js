import * as R from 'ramda';

const refineParents = (parents, indexed) => R.pipe(
  R.converge(
    R.splitAt,
    [
      R.pipe(R.findLastIndex(R.flip(R.has)(indexed)), R.inc),
      R.identity
    ]
  ),
  ([presents, absents]) => {
    const lastPresentParentId = R.last(presents);
    return [
      R.ifElse(
        R.isNil,
        R.always([]),
        R.converge(R.append, [R.identity, R.pipe(R.flip(R.prop)(indexed), R.path(['value', 'parents']))])
      )(lastPresentParentId),
      absents
    ];
  }
)(parents);

const pushRow = (row, last, acc) => ({
  refined: R.append(row, acc.refined),
  indexed: R.assoc(R.path(['value', 'id'], last), last, acc.indexed)
});

export const refineLayoutHierarchy = (sectionsData, lastDimension) => {
  const lastDimIndexedValues = R.pipe(
    R.propOr([], 'values'),
    R.indexBy(R.prop('id'))
  )(lastDimension);
  return R.map(
    (sectionData) => R.pipe(
      R.last,
      R.reduce(
        (acc, row) => {
          const last = R.last(row);
          if (R.anyPass([R.isNil, R.isEmpty])(R.path(['value', 'parents'], last))) {
            return pushRow(row, last, acc);
          }
          const [presents, absents] = refineParents(R.path(['value', 'parents'], last), acc.indexed);
          const name = R.pipe(
            R.map(
              R.pipe(R.flip(R.prop)(lastDimIndexedValues), R.prop('name'))),
            R.append(R.path(['value', 'name'], last)),
            R.join(' > ')
          )(absents);
          const refinedLast = R.over(
            R.lensProp('value'),
            R.pipe(R.assoc('name', name), R.assoc('parents', presents)),
            last
          );
          const refinedRow = R.pipe(R.dropLast(1), R.append(refinedLast))(row);
          return pushRow(refinedRow, refinedLast, acc);
        },
        { refined: [], indexed: {} }
      ),
      ({ refined }) => ([R.head(sectionData), refined])
    )(sectionData),
    sectionsData
  );
};
