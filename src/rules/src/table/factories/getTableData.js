import * as R from 'ramda';
import { withFlatHierarchy } from '@sis-cc/dotstatsuite-sdmxjs';
import { getCells } from './getCells';
import { getConfirmedSeriesAttributesIds } from './getConfirmedSeriesAttributesIds';
import { getCuratedCells } from './getCuratedCells';
import { getLayoutData } from './getLayoutData';
import { getLayoutDataWithFlags } from './getLayoutWithFlags';
import { refineLayoutHierarchy } from './refineLayoutHierarchy';
import { isTimeDimension } from '../../dimension-utils';

import { getUnitsInLayout } from '../units/getUnitsinLayout';
import { appendUnitsInLayoutData } from '../units/appendUnitsInLayoutData';
import { cleanUnitsInLayoutData } from '../units/cleanUnitsInLayoutData';


export const getTableProps = (data, layoutIds, display, customAttributes, limit, isTimeInverted) => {
  const seriesAttributes = R.propOr({}, 'seriesAttributes', data);

  const confirmedSeriesAttributesIds = getConfirmedSeriesAttributesIds(
    seriesAttributes,
    layoutIds
  );

  const {
    rejectedValueIds,
    unitDimension,
    unitsAttachmentSeriesIds,
    unitsDefinitionCodes,
    unitsSeries,
  } = R.propOr({}, 'units', data);
  const {
    unitsLevelDisplay,
    unitsAttachmentIndexesInLayout,
    unitsIndexesInLayout
  } = getUnitsInLayout({ layoutIds, unitsAttachmentSeriesIds, unitsDefinitionCodes, unitsSeries });

  const layoutDimsIds = R.pipe(R.pathOr({}, ['dimensions', 'many']), R.keys)(data);

  const cells = getCells(
    data,
    display,
    R.assoc('rejected', confirmedSeriesAttributesIds, customAttributes),
    {
      unitsDefinitionCodes, unitsSeries, unitsDisplay: unitsLevelDisplay,
      unitDimension, rejectedValueIds, layoutDimsIds
    }
  );

  const dimensions = R.mapObjIndexed(
    R.pipe(
      R.when( // invert TimeDimension Values
        R.allPass([R.pipe(R.prop('id'), isTimeDimension), R.always(isTimeInverted)]),
        (dim) => R.over(
          R.lensProp('values'),
          R.map(R.over(R.lensProp('__indexPosition'), R.negate))
        )(dim), 
      ),
      (dim) => R.over( // sort values
        R.lensProp('values'),
        R.sortBy(R.prop('__indexPosition'))
      )(dim),
      R.when( // refine hierarchy on last row
        R.pipe(R.prop('id'), R.equals(R.last(R.propOr([], 'rows', layoutIds)))),
        withFlatHierarchy
      )
    ),
    R.pathOr({}, ['dimensions', 'many'], data)
  );

  const seriesAttributesValues = R.propOr({}, 'seriesAttributesValues', data);
  const layout = R.pipe(
    R.mergeRight({ header: [], sections: [], rows: [] }),
    R.mapObjIndexed(R.flip(R.props)(dimensions)),
  )(layoutIds);

  const partialUnitSerieIndexes = R.pipe(
    R.pathOr(0, ['dimensions', 'length']),
    R.times(R.always('x'))
  )(data);

  const layoutData = R.pipe(
    getLayoutData,
    R.over(
      R.lensProp('sectionsData'),
      sectionsData => refineLayoutHierarchy(sectionsData, R.last(R.prop('rows', layout)))
    ),
    appendUnitsInLayoutData({
      display, unitDimension, unitsDisplay: unitsLevelDisplay, unitsSeries,
      unitsDefinitionCodes, unitsIndexes: unitsAttachmentIndexesInLayout,
      partialUnitSerieIndexes, rejectedValueIds, layoutIds
    }),
    getLayoutDataWithFlags(seriesAttributesValues, display, customAttributes),
    cleanUnitsInLayoutData({ unitsDisplay: unitsLevelDisplay, unitsLayoutIndexes: unitsIndexesInLayout })
  )({ layout, observations: cells, limit: R.when(R.isNil, R.always(0))(limit) });
  return ({
    cells: getCuratedCells({ layout, observations: cells, shape: ['header', 'sections', 'rows'] }),
    layout,
    ...layoutData
  });
};
