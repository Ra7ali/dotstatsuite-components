import * as R from 'ramda';
import { getSortedLayoutData } from './getSortedLayoutData';
import { getCuratedCells } from './getCuratedCells';

const layoutDataToKey = R.pipe(
  R.addIndex(R.map)((entry, index) => ({ ...entry, f: R.path([index, 'value', '__index']) })),
  R.map(R.prop('f')),
  getters => R.converge((...results) => R.join(':', results), getters), 
);

export const refineSectionsData = (sectionsData, extractedKeys, curatedData, dataToKey) => R.pipe(
  R.map(
    (section) => {
      const sectionKey = dataToKey.sections(R.head(section));
      return R.over(
        R.lensIndex(1),
        R.filter(
          rowData => {
            const rowKey = dataToKey.rows(rowData);
            return R.pipe(
              R.path([sectionKey, rowKey]),
              R.omit(extractedKeys),
              R.isEmpty,
              R.not
            )(curatedData);
          }
        )
      )(section);
    }
  ),
  R.filter(
    R.pipe(R.nth(1), R.isEmpty, R.not)
  )
)(sectionsData);

export const refineHeaderData = (headerData, extractedKeys, curatedData, dataToKey) => R.filter( // extracted : { [sectionKey]: [...rowKeys] }
  (header) => {
    const headerKey = dataToKey.header(header);
    return R.pipe(
      R.prop(headerKey),
      (sections) => {
        return R.pickBy(
          (rows, section) => R.pipe(
            R.omit(R.propOr([], section, extractedKeys)),
            R.isEmpty,
            R.not
          )(rows),
          sections
        );
      },
      R.isEmpty,
      R.not
    )(curatedData);
  }
)(headerData);

export const truncateSectionRows = (n, sectionsData, dataToKey) => {
  const [rest, extracted] = R.pipe(
    R.last, // last section
    R.nth(1), // rows
    R.splitAt(R.negate(n)) // truncate from the end
  )(sectionsData);

  const truncatedData = R.isEmpty(rest)
    ? R.dropLast(1, sectionsData) // remove section if no more rows
    : R.adjust(-1, R.adjust(1, R.always(rest)), sectionsData)

  const sectionKey = dataToKey.sections(R.pipe(R.last, R.head)(sectionsData));
  const rowKeys = R.map(dataToKey.rows, extracted);

  const extractedLength = R.length(extracted);
  if (extractedLength < n) {
    const { truncated, extractedKeys } = truncateSectionRows(
      n - extractedLength,
      truncatedData,
      dataToKey
    );
    return ({ truncated, extractedKeys: { ...extractedKeys, [sectionKey]: rowKeys } });
  }

  return ({ truncated: truncatedData, extractedKeys: { [sectionKey]: rowKeys } });
};

export const truncateHeader = (n, headerData, dataToKey) => {
  const [truncated, extracted] = R.splitAt(R.negate(n))(headerData);
  return ({ truncated, extractedKeys: R.map(dataToKey.header, extracted) });
};

const refineLength = length => length === 0 ? 1 : length;

const truncateLayout = (isVertical) => R.ifElse(
  R.always(isVertical),
  truncateSectionRows,
  truncateHeader,
);

const refineLayout = (isVertical) => R.ifElse(
  R.always(isVertical),
  refineHeaderData,
  refineSectionsData
)

const getShape = isVertical => isVertical ? ['header', 'sections', 'rows'] : ['sections', 'rows', 'header'];

const getRefinedLayout = (isVertical, truncated, refined) => R.ifElse(
  R.always(isVertical),
  R.pipe(R.assoc('sectionsData', truncated), R.assoc('headerData', refined)),
  R.pipe(R.assoc('headerData', truncated), R.assoc('sectionsData', refined)),
)({});

const segregateLayout = (isVertical) => R.ifElse(
  R.always(isVertical),
  R.props(['sectionsData', 'headerData']),
  R.props(['headerData', 'sectionsData'])
);

export const getLayoutData = ({ layout, observations, limit }) => {
  const layoutData = getSortedLayoutData(layout, observations);
  const headerLength = R.pipe(R.prop('headerData'), R.length, refineLength)(layoutData);
  const rowsLength = R.pipe(
    R.prop('sectionsData'),
    R.reduce(
      R.useWith(R.add, [R.identity, R.pipe(R.nth(1), R.length)]),
      0
    ),
    refineLength
  )(layoutData);
  const cellsCount = R.multiply(rowsLength, headerLength);
  const excess = cellsCount - limit;

  if (R.isNil(limit) || limit === 0 || excess <= 0) {
    return (layoutData);
  }

  const isVertical = rowsLength > headerLength;

  const [toTruncate, toRefine] = segregateLayout(isVertical)(layoutData);

  const cutLength = R.pipe(
    R.ifElse(R.identity, R.always(headerLength), R.always(rowsLength)),
    R.divide(excess),
    Math.ceil
  )(isVertical);

  const dataToKey = R.mapObjIndexed(layoutDataToKey, layout);

  const { truncated, extractedKeys } = truncateLayout(isVertical)(cutLength, toTruncate, dataToKey);

  const curatedData = R.pipe(
    getShape,
    (shape) => getCuratedCells({ layout, observations, shape })
  )(isVertical);

  const refined = refineLayout(isVertical)(toRefine, extractedKeys, curatedData, dataToKey);

  return getRefinedLayout(isVertical, truncated, refined);
};
