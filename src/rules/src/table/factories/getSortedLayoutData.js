import * as R from 'ramda';

const getSublayoutIndexToPositionPivots = dimensions => R.map(
  (dimension) => {
    const indexedPositions = R.addIndex(R.reduce)( // => [(entryIndex) : { ...val, __index }] => { [__index]: entryIndex }
      (acc, value, index) => ({
        ...acc,
        [R.prop('__index', value)]: index
      }),
      {},
      R.prop('values', dimension)
    );

    return (splitKey) => R.pipe(
      R.nth(R.prop('__index', dimension)),
      index => R.prop(index, indexedPositions)
    )(splitKey);
  },
  dimensions
);

const coordinatesCompare = (coordinates1, coordinates2) => {
  return R.pipe(
    R.zipWith((a, b) => a - b),
    (diffs) => R.find(diff => diff !== 0, diffs) || 0,
  )(coordinates1, coordinates2);
};

const coordinatesToSublayoutData = (sublayout, coordinates) => R.addIndex(R.map)(
  (valuePosition, dimensionIndex) => {
    const sublayoutDimension = R.nth(dimensionIndex, sublayout);
    const value = R.path(['values', valuePosition], sublayoutDimension);
    return ({
      dimension: R.pick(['id', 'name', '__index'], sublayoutDimension),
      value,
    });
  },
  coordinates
);

export const getSortedLayoutData = (layout, observations) => {
  const headersPivots = getSublayoutIndexToPositionPivots(R.prop('header', layout));
  const rowsPivots = getSublayoutIndexToPositionPivots(R.prop('rows', layout));
  const sectionsPivots = getSublayoutIndexToPositionPivots(R.prop('sections', layout));

  const rowsSize = R.length(R.prop('rows', layout));
  const sectionsSize = R.length(R.prop('sections', layout));

  const sortedLayoutPositions = R.pipe(
    R.keys,
    R.map(
      key => {
        const splitKey = R.split(':', key);
        return R.converge((...rest) => rest, [...sectionsPivots, ...rowsPivots, ...headersPivots])(splitKey);
      }
    ), 
    R.sort(coordinatesCompare),
    R.map(
      (coordinates) => {
        const [sections, tail] = R.splitAt(sectionsSize, coordinates);
        const [rows, headers] = R.splitAt(rowsSize, tail);
        return { headers, rows, sections };
      }
    )
  )(observations);

  const headerData = R.pipe(
    R.pluck('headers'),
    R.uniq,
    R.sort(coordinatesCompare),
    R.map(
      (coordinates) => coordinatesToSublayoutData(R.prop('header', layout), coordinates)
    )
  )(sortedLayoutPositions);

  const sectionsData = R.pipe(
    R.groupWith((a, b) => R.equals(R.prop('sections', a), R.prop('sections', b))),
    R.map(
      (entries) => {
        const sectionData = coordinatesToSublayoutData(R.prop('sections', layout), R.prop('sections', R.head(entries)));
        const rowsData = R.map(
          (coordinates) => coordinatesToSublayoutData(R.prop('rows', layout), coordinates),
          R.uniq(R.pluck('rows', entries))
        );
        return [sectionData, rowsData];
      }
    )
  )(sortedLayoutPositions);

  return { headerData, sectionsData };
};