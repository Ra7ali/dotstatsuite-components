import * as R from 'ramda';

/*

example shape = ['header', 'sections', 'rows']

*/

const getPivotKey = indexes => R.pipe(
  R.props(indexes),
  R.join(':')
);

const getPivots = (layout, shape) => R.pipe(
  R.props(shape),
  R.map(R.pipe(R.pluck('__index'), getPivotKey))
)(layout);

const indexWithPivots = (pivots, observations) => R.pipe(
  R.keys,
  R.reduce(
    (acc, obsKey) => {
      const splitKey = R.split(':', obsKey);
      const path = R.map(
        pivot => pivot(splitKey),
        pivots
      );
      const obs = R.prop(obsKey, observations);
      return R.over(
        R.lensPath(path),
        R.ifElse(R.isNil, R.always([obs]), R.append(obs))
      )(acc);
    },
    {}
  )
)(observations);

export const getCuratedCells = ({ layout, observations, shape }) => {
  const pivots = getPivots(layout, shape);
  return indexWithPivots(pivots, observations);
};
