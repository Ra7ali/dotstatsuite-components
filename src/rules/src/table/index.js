export { prepareData } from './preparators/prepareData';
export { getCells, getFlags } from './factories/getCells';
export { getTableCells } from './factories/getTableCells';
export { getTableProps } from './factories/getTableData';
export getTableShareData from './share';
