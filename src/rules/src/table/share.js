import * as R from 'ramda';

const getTableShareData = ({
  tableProps,
  source,
  config,
  display,
  customAttributes,
  dataflowId,
  layoutIds,
  cellsLimit,
  isNotATable,
  isTimeInverted,
  range
}) => {
  return {
    config: {
      dataflowId,
      ...R.pick(['fonts', 'locale', 'logo', 'owner', 'terms', 'sourceHeaders'], config),
    },
    data: {
      ...tableProps,
      cellsLimit,
      customAttributes,
      layoutIds,
      range,
      isTimeInverted,
      isNotATable,
      share: { display, source }
    }
  };
};

const refineSnapshotData = R.pipe(
  R.dissocPath(['config', 'sourceHeaders']),
  R.dissocPath(['config', 'locale']),
  R.dissocPath(['config', 'dataflowId']),
  R.dissocPath(['data', 'share']),
  R.dissocPath(['data', 'cellsLimit']),
  R.dissocPath(['data', 'isTimeInverted']),
  R.dissocPath(['data', 'customAttributes']),
  R.dissocPath(['data', 'layoutIds']),
  R.dissocPath(['data', 'header', 'disclaimer']),
);

const refineLatestData = R.pipe(
  R.over(
    R.lensProp('data'),
    R.pick(['footnotes', 'cellsLimit', 'customAttributes', 'layoutIds', 'isTimeInverted', 'share'])
  )
);

export default ({ shareMode, ...rest }) => R.pipe(
  getTableShareData,
  R.ifElse(
    R.always(shareMode === 'latest'),
    refineLatestData,
    refineSnapshotData
  )
)(rest);
