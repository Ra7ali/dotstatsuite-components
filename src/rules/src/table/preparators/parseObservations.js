import * as R from 'ramda';
import { getAttributeValue } from './getAttributeValue';
/*
  unitsArtefacts = { dimensions: { series: {} }, attributes: { series: {} } }; 
*/
const getUnitsValues = (indexes, getDisplay) => R.mapObjIndexed(
  (codelist) => {
    const valueIndex = R.prop(codelist.index, indexes);
    if (R.isNil(valueIndex) || !getDisplay(codelist)) {
      return null;
    }
    const value = R.path(['values', Number(valueIndex)], codelist);
    if (R.isNil(value) || !getDisplay({ ...value, parent: codelist.id })) {
      return null;
    }
    return ({
      ...R.pick(['id', 'name'], codelist),
      value: R.when(R.complement(R.isNil), R.assoc('index', Number(valueIndex)))(value)
    });
  }
);
const getObservationUnits = ({ dimValuesIndexes, attrValuesIndexes, getDisplay }) => R.converge(
  R.merge,
  [
    R.pipe(
      R.pathOr({}, ['dimensions', 'series']),
      getUnitsValues(dimValuesIndexes, getDisplay)
    ),
    R.pipe(
      R.pathOr({}, ['attributes', 'series']),
      getUnitsValues(attrValuesIndexes, getDisplay)
    ),
  ]
);

export const parseObservations = (observations, obsAttributes, formatAttributes, unitsArtefacts, getDisplay) => R.mapObjIndexed(
  (obs, key) => {
    const dimValuesIndexes = R.split(':', key);
    const value = R.head(obs);
    const attrValuesIndexes = R.tail(obs);
    const units = getObservationUnits({ dimValuesIndexes, attrValuesIndexes, getDisplay })(unitsArtefacts);
    const attributes = R.mapObjIndexed(
      (attribute) => {
        return getAttributeValue(attribute, R.nth(attribute.index, attrValuesIndexes));
      },
      obsAttributes
    );

    const format = R.mapObjIndexed(
      (attribute) => {
        const attributeValue = getAttributeValue(attribute, R.nth(attribute.index, attrValuesIndexes));
        return R.when(R.complement(R.isNil), R.path(['value', 'id']))(attributeValue);
      },
      formatAttributes
    );
    return ({
      ...format,
      value,
      key,
      dimValuesIndexes,
      attrValuesIndexes,
      attributes: R.filter(R.complement(R.isNil), attributes),
      units
    });
  },
  observations
);
