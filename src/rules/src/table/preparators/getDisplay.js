import * as R from 'ramda';

export const getDisplay = ({ noDisplayIndexes = [], notDisplayedCodes = {} }) => {
  const annotationsValidator = R.pipe(
    R.propOr([], 'annotations'),
    R.intersection(noDisplayIndexes),
    R.isEmpty
  );

  const blacklistValidator = R.ifElse(
    R.has('values'),
    ({ id }) => R.anyPass([
      R.complement(R.has)(id),
      R.hasPath([id, 'values'])
    ])(notDisplayedCodes),
    ({ id, parent }) => R.complement(R.hasPath)([parent, 'values', id], notDisplayedCodes)
  );

  return R.allPass([annotationsValidator, blacklistValidator]);
};
