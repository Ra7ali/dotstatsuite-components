import * as R from 'ramda';

export const parseAttributesValuesFromObservations = (observations, registers) => R.pipe(
  R.values,
  R.reduce(
    (acc, observation) => {
      const observationRegisters = R.map(
        getRegister => getRegister(observation),
        registers
      );

      return R.pipeWith(R.flip(R.applyTo), observationRegisters)(acc);
    },
    {}
  )
)(observations);
