import * as R from 'ramda';
import { refineObservationsAttributesValues } from './refineObservationsAttributesValues';
import { isTimeDimension } from '../../dimension-utils';

export const getManyValuesDimensions = (dimensionsByIds, dimensionsIds, attributeValues, attributes) => R.mapObjIndexed(
  (dimension) => {
    const dimensionIndex = R.prop(dimension.id, dimensionsIds);
    const values = R.addIndex(R.map)(
      (value, valueIndex) => {
        const valueAttributes = refineObservationsAttributesValues(attributes)
          (R.propOr({}, `${dimensionIndex}:${valueIndex}`, attributeValues));
        return ({
          ...R.pick(['id', 'name', '__indexPosition', 'parent'], value),
          attributes: valueAttributes,
          __index: valueIndex, 
        });
      }, dimension.values);
    return R.assoc('values', values, dimension);
  },
  R.pick(R.keys(dimensionsIds), dimensionsByIds)
);
