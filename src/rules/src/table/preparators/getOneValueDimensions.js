import * as R from 'ramda';
import { getAttributeValue } from './getAttributeValue';

export const getOneValueDimensions = (dimensions, attributesIds, attributes, getDisplay) => R.map(
  (dimension) => {
    const dimensionAttributes = R.pipe(
      R.mapObjIndexed(attr => getAttributeValue(attr, 0)),
      R.filter(R.complement(R.isNil))
    )(R.pick(R.propOr([], dimension.id, attributesIds), attributes));

    const display = getDisplay(dimension);

    const value = R.pipe(
      R.path(['values', 0]),
      R.assoc('attributes', dimensionAttributes),
      (val) => R.assoc(
        'display',
        getDisplay({ ...val, parent: dimension.id }),
        val
      )
    )(dimension);

    return R.pipe(
      R.assoc(
        'display',
        display
      ),
      R.assocPath(
        ['values', 0],
        value
      )
    )(dimension);
  },
  dimensions
);
