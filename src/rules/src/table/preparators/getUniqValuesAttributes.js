import * as R from 'ramda';

/*
 for Dataflow attributes and 'single values' dimensions
*/

export const getUniqValuesAttributes = (attributesIds, attributes, noDisplayIndexes) => R.pipe(
  R.pick(attributesIds),
  R.mapObjIndexed(
    (attribute) => {
      const value = R.head(attribute.values);
      const annotationsIndexes = R.propOr([], 'annotations', value);
      const noDisplayValueIndexes = R.intersection(annotationsIndexes, noDisplayIndexes);
      if (!R.isEmpty(noDisplayValueIndexes)) {
        return null;
      }
      return ({
        ...R.pick(['id', 'name'], attribute),
        value: R.head(attribute.values)
      });
    }
  ),
  R.filter(R.pipe(R.isNil, R.not))
)(attributes);
