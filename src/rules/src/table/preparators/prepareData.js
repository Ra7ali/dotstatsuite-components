import * as R from 'ramda';
import { parseDimensionsIds } from './parseDimensionsIds';
import { getNoDisplayAnnotationsIndexes } from './getNoDisplayAnnotationsIndexes';
import { getNoDisplayCodes } from './getNoDisplayCodes';
import { getDisplay } from './getDisplay';
import { parseAttributes } from './parseAttributes';
import { getDimensionsAttributesRegisters } from './getDimensionsAttributesRegisters';
import { parseAttributesValuesFromObservations } from './parseAttributesValuesFromObservations';
import { parseObservations } from './parseObservations';
import { getOneValueDimensions } from './getOneValueDimensions';
import { getManyValuesDimensions } from './getManyValuesDimensions';
import { getAttributeValue } from './getAttributeValue';
import { refineSeriesAttributesValues } from './refineSeriesAttributesValues';

import { getUnitsDefinition } from '../units/getUnitsDefinition';
import { getUnitsArtefacts } from '../units/getUnitsArtefacts';
import { getHeaderUnits } from '../units/getHeaderUnits';
import { getAttachmentSeriesIndexes } from '../units/getAttachmentSeriesIndexes';
import { getUnitsSeriesOccurences } from '../units/getUnitsSeriesOccurences';
import { refineDimSeriesUnits } from '../units/refineDimSeriesUnits';

export const prepareData = (sdmxJson, customAttributes, unitsProps={}) => {
  const {
    defaultCodes=[],
    annotationsDefinitionCodes={},
    rejectedValueIds=[],
  } = R.pick(['defaultCodes', 'annotationsDefinitionCodes', 'rejectedValueIds'], unitsProps);
  const unitsDefinitionCodes = getUnitsDefinition({
    defaultCodes, annotationsDefinitionCodes, data: sdmxJson
  });
  const _customAttributes = R.assoc('units', unitsDefinitionCodes, customAttributes);
  const dimensions = R.pipe(
    R.pathOr([], ['data', 'structure', 'dimensions', 'observation']),
    R.addIndex(R.map)((dim, index) => R.assoc('__index', index, dim))
  )(sdmxJson);

  const dimensionsIndexedByIds = R.indexBy(R.prop('id'), dimensions);

  const parsedDimensionsIds = parseDimensionsIds(dimensions);

  const annotations = R.pathOr([], ['data', 'structure', 'annotations'], sdmxJson);

  const noDisplayIndexes = getNoDisplayAnnotationsIndexes(annotations);
  const notDisplayedCodes = getNoDisplayCodes({
    noDisplayIndexes,
    annotations,
    datasetIndexes: R.pathOr([], ['data', 'dataSets', 0, 'annotations'], sdmxJson)
  });
  const _getDisplay = getDisplay({ noDisplayIndexes, notDisplayedCodes });

    //------------------------------------------------------------------
    const unitsArtefacts = getUnitsArtefacts({ unitsDefinitionCodes, data: sdmxJson, getDisplay: _getDisplay });
    const headerUnits = getHeaderUnits({ unitsArtefacts, unitsDefinitionCodes, rejectedValueIds });
    const _dimensions = R.pipe(
      R.pathOr([], ['data', 'structure', 'dimensions', 'observation']),
      R.addIndex(R.map)((dim, index) => R.assoc('index', index, dim)),
      R.indexBy(R.prop('id'))
    )(sdmxJson);
    const attachmentSeriesIndexes = getAttachmentSeriesIndexes({ dimensions: _dimensions, unitsArtefacts });
  //------------------------------------------------------------------

  const { attributesIndexedByIds, attributesIdsIndexedByTargets } = parseAttributes({
    attributes: R.concat(
      R.pathOr([], ['data', 'structure', 'attributes', 'observation'], sdmxJson),
      R.pathOr([], ['data', 'structure', 'attributes', 'dataSet'], sdmxJson),
    ),
    parsedDimensionsIds,
    getDisplay: _getDisplay,
    customAttributes: _customAttributes,
  });
  
  const seriesAttributesIds = R.propOr([], 'series', attributesIdsIndexedByTargets);
  const observationsAttributesIds = R.propOr([], 'observations', attributesIdsIndexedByTargets);
  const observations = parseObservations(
    R.pathOr({}, ['data', 'dataSets', 0, 'observations'], sdmxJson),
    R.pick(R.concat(seriesAttributesIds, observationsAttributesIds), attributesIndexedByIds),
    R.pipe(
      R.pick(['prefscale', 'decimals']),
      R.mapObjIndexed(R.flip(R.prop)(attributesIndexedByIds))
    )(attributesIdsIndexedByTargets),
    unitsArtefacts,
    _getDisplay
  );
  //------------------------------------------------------------------
  const { rawUnitsSeries, obs } = getUnitsSeriesOccurences({
    unitsArtefacts, attachmentSeriesIndexes
  })(observations);
  const unitsSeries = refineDimSeriesUnits({
    unitsArtefacts: R.merge(
      R.pathOr({}, ['dimensions', 'series'], unitsArtefacts),
      R.pathOr({}, ['attributes', 'series'], unitsArtefacts)
    ),
    rejectedValueIds
  })(rawUnitsSeries);
  const unitsAttachmentSeriesIds = R.pipe(
    R.props(attachmentSeriesIndexes),
    R.pluck('id')
  )(dimensions);
  //------------------------------------------------------------------   
  const attributesRegisters = getDimensionsAttributesRegisters(
    attributesIdsIndexedByTargets,
    attributesIndexedByIds
  );

  const dimensionsAttributesValues = parseAttributesValuesFromObservations( // => { series: { key: { attrId: [...indexes] } }, dimensions: {} }
    observations,
    attributesRegisters
  );

  const dataflowAttributes = R.mapObjIndexed(
    attribute => getAttributeValue(attribute, 0),
    R.pipe(
      R.pick(
        R.propOr([], 'dataflow', attributesIdsIndexedByTargets),
      ),
      R.filter(R.complement(R.isNil))
    )(attributesIndexedByIds)
  );

  const oneValueDimensions = getOneValueDimensions(
    R.pipe(
      R.pick(R.keys(parsedDimensionsIds.oneValue)),
      R.omit(unitsDefinitionCodes)
    )(dimensionsIndexedByIds),
    attributesIdsIndexedByTargets.oneValueDimensions,
    attributesIndexedByIds,
    _getDisplay
  );

  const manyValuesDimensions = getManyValuesDimensions(
    dimensionsIndexedByIds,
    parsedDimensionsIds.manyValues,
    dimensionsAttributesValues.dimensions,
    attributesIndexedByIds,
  );

  const seriesAttributesValues = refineSeriesAttributesValues(
    dimensionsAttributesValues.series,
    attributesIndexedByIds,
  );
  return ({
    dimensions: {
      one: oneValueDimensions,
      many: manyValuesDimensions,
      length: R.length(dimensions)
    },
    units: {
      headerUnits,
      rejectedValueIds,
      unitsDefinitionCodes,
      unitsAttachmentSeriesIds,
      unitsSeries,
    },
    observations: obs,
    dataflowAttributes,
    dataflowName: R.path(['data', 'structure', 'name'], sdmxJson),
    seriesAttributes: R.props(seriesAttributesIds, attributesIndexedByIds),
    seriesAttributesValues
  });
};
