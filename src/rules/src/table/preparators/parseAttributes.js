import * as R from 'ramda';

const registerAttrId = (path, id) => R.over(
  R.lensPath(R.prepend('attributesIdsIndexedByTargets', path)),
  R.ifElse(R.isNil, R.always([id]), R.append(id))
);

const registerAttr = (attr) => R.set(
  R.lensPath(['attributesIndexedByIds', attr.id]),
  attr
);

const getAttributeRegisters = (attribute, parsedDimensionsIds, customAttributes) => {
  if (R.propEq('id', customAttributes.prefscale, attribute)) {
    return [registerAttrId(['prefscale'], attribute.id), registerAttr(attribute)];
  }
  if (R.propEq('id', customAttributes.decimals, attribute)) {
    return [registerAttrId(['decimals'], attribute.id), registerAttr(attribute)];
  }
  if (R.hasPath(['relationship', 'none'], attribute)) {
    return [registerAttrId(['dataflow'], attribute.id), registerAttr(attribute)];
  }
  if (R.pathEq(['relationship', 'primaryMeasure'], 'OBS_VALUE', attribute)) {
    return [registerAttrId(['observations'], attribute.id), registerAttr(attribute)];
  } 
  const targetDimensions = R.pathOr([], ['relationship', 'dimensions'], attribute);
  if (R.isEmpty(targetDimensions)) {
    return [];
  }
  if (R.length(targetDimensions) === 1) {
    return R.ifElse(
      R.flip(R.has)(parsedDimensionsIds.oneValue),
      R.always([
        registerAttrId(['oneValueDimensions', R.head(targetDimensions)], attribute.id),
        registerAttr(attribute)
      ]),
      R.always([
        registerAttrId(['manyValuesDimensions'], attribute.id),
        registerAttr(R.assoc(
          'targetIndex',
          R.prop(R.head(targetDimensions), parsedDimensionsIds.manyValues),
          attribute
        ))
      ])
    )(R.head(targetDimensions));
  }
  const manyValuesTargetDimensions = R.without(R.keys(parsedDimensionsIds.oneValue), targetDimensions);
  const size = R.length(manyValuesTargetDimensions);
  if (size === 0) {
    return [registerAttrId(['dataflow'], attribute.id), registerAttr(attribute)];
  }
  if (size === 1) {
    return [
      registerAttrId(['manyValuesDimensions'], attribute.id),
      registerAttr(R.assoc(
        'targetIndex',
        R.prop(R.head(manyValuesTargetDimensions), parsedDimensionsIds.manyValues),
        attribute
      ))
    ];
  }
  return [
    registerAttrId(['series'], attribute.id),
    registerAttr(
      R.assoc(
        'targetIndexes',
        R.reduce(
          (acc, dimId) => {
            const index = R.prop(dimId, parsedDimensionsIds.manyValues);
            return R.assoc(String(index), index, acc);
          },
          {},
          manyValuesTargetDimensions
        ),
        attribute
      )
    )
  ]
};

export const parseAttributes = ({ attributes, parsedDimensionsIds, customAttributes, getDisplay }) => R.addIndex(R.reduce)(
  (acc, attribute, attributeIndex) => {
    const values = R.propOr([], 'values', attribute);

    if (R.isEmpty(values) || !getDisplay(attribute) || R.includes(attribute.id, R.propOr([], 'units', customAttributes))) {
      return acc;
    }

    const registers = getAttributeRegisters(
      R.pipe(
        R.assoc('index', attributeIndex),
        R.over(
          R.lensProp('values'),
          R.map((value) => {
            if (!getDisplay({ ...value, parent: attribute.id })) {
              return null;
            }
            return value;
          })
        )
      )(attribute),
      parsedDimensionsIds,
      customAttributes,
    );

    return R.pipeWith(R.flip(R.applyTo), registers)(acc);
  },
  { attributesIndexedByIds: {}, attributesIdsIndexedByTargets: {} },
  attributes
);
