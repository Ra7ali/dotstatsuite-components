import * as R from 'ramda';

export const getAttributeValue = (attribute, valueIndex) => {
  const value = R.pipe(
    R.propOr([], 'values'),
    R.nth(valueIndex)
  )(attribute);

  if (R.isNil(value)) {
    return null;
  }

  return ({
    ...R.pick(['id', 'name'], attribute),
    value: R.pick(['id', 'name'], value)
  });
};
