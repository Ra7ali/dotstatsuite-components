import * as R from 'ramda';
import { getAttributeValue } from './getAttributeValue';
/*
  After parsing series attributes or 'many values' dimensions attributes from observations,
  filter to keep only single uniq non null occurences, and return the corresponding attribute value
*/


export const refineObservationsAttributesValues = (attributesByIds) => R.pipe(
  R.mapObjIndexed(
    (indexes, attributeId) => {
      const uniqed = R.uniq(indexes);
      if (R.length(uniqed) !== 1) {
        return null;
      }
      const valueIndex = R.head(uniqed);

      return getAttributeValue(R.propOr({}, attributeId, attributesByIds), valueIndex);
    }
  ),
  R.filter(R.complement(R.isNil))
);
