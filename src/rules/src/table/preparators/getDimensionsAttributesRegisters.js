import * as R from 'ramda';

const getSerieRegisterPath = (attribute, obsDimValuesIndexes) => {
  const serieKey = R.pipe(
    R.addIndex(R.map)(
      (dimensionValueIndex, dimensionIndex) => R.when(
        R.always(!R.has(String(dimensionIndex), attribute.targetIndexes)),
        R.always('x')
      )(dimensionValueIndex)
    ),
    R.join(':')
  )(obsDimValuesIndexes);

  return ['series', serieKey, attribute.id];
};

const getDimensionRegisterPath = (attribute, obsDimValuesIndexes) => {
  const dimensionIndex = attribute.targetIndex;
  const dimensionValueIndex = R.nth(dimensionIndex, obsDimValuesIndexes);

  return ['dimensions', `${dimensionIndex}:${dimensionValueIndex}`, attribute.id];
};

const getAttributesRegisters = (getPath) => R.map(
  (attribute) => (observation) => {
    const valueIndex = R.nth(attribute.index, R.propOr([], 'attrValuesIndexes', observation));

    const registerPath = getPath(attribute, R.propOr([], 'dimValuesIndexes', observation));

    return R.over(
      R.lensPath(registerPath),
      R.ifElse(R.isNil, R.always([valueIndex]), R.append(valueIndex))
    );
  }
);

export const getDimensionsAttributesRegisters = (attributesIds, attributesByIds) => R.converge(
  R.concat,
  [
    R.pipe(
      R.propOr([], 'series'),
      R.flip(R.props)(attributesByIds),
      getAttributesRegisters(getSerieRegisterPath)
    ),
    R.pipe(
      R.propOr([], 'manyValuesDimensions'),
      R.flip(R.props)(attributesByIds),
      getAttributesRegisters(getDimensionRegisterPath)
    )
  ]
)(attributesIds);
