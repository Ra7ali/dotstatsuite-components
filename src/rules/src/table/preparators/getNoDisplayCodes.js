import * as R from 'ramda';

export const getNoDisplayCodes = ({ noDisplayIndexes, annotations, datasetIndexes }) => {
  const datasetLevelNoDisplayIndexes = R.intersection(noDisplayIndexes, datasetIndexes);

  return R.pipe(
    R.nth(R.head(datasetLevelNoDisplayIndexes)),
    R.when(R.isNil, R.always({})),
    // duplicate from sdmxjs
    R.propOr('', 'title'),
    R.split(','),
    R.reduce((acc, entry) => {
      if (R.isEmpty(entry)) {
        return acc;
      }
      const parse = R.split('=', entry);
      const id = R.head(parse);
      const values = R.ifElse(
        R.pipe(R.length, R.equals(1)),
        R.always(null),
        R.pipe(R.last, R.split('+'), R.reject(R.isEmpty), R.indexBy(R.identity)),
      )(parse);
      return R.pipe(
        R.set(R.lensProp(id), { id }),
        R.when(
          R.always(!R.isNil(values) && !R.isEmpty(values)),
          R.set(R.lensPath([id, 'values']), values),
        ),
      )(acc);
    }, {})
  )(annotations);
};
