import * as R from 'ramda';

export const parseDimensionsIds = R.addIndex(R.reduce)(
  (acc, dimension, dimensionIndex) => {
    const values = R.propOr([], 'values', dimension);
    const size = R.length(values);
    if (R.equals(0, size)) {
      return acc;
    }
    const target = R.equals(1, size) ? 'oneValue' : 'manyValues';
    return R.set(
      R.lensPath([target, dimension.id]),
      dimensionIndex
    )(acc);
  },
  { manyValues: {}, oneValue: {} }
);
