import * as R from 'ramda';
import { refineObservationsAttributesValues } from './refineObservationsAttributesValues';

export const refineSeriesAttributesValues = (seriesAttributeIndexes, attributesByIds) => R.pipe(
  R.mapObjIndexed(
    (serieAttributes, serieKey) => ({
      attributes: refineObservationsAttributesValues(attributesByIds)(serieAttributes),
      serie: R.split(':', serieKey)
    })
  ),
)(seriesAttributeIndexes);
