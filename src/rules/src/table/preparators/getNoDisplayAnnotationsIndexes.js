import * as R from 'ramda';

export const getNoDisplayAnnotationsIndexes = R.addIndex(R.reduce)(
  (acc, annotation, annotationIndex) => R.when(
    R.always(R.pipe(R.prop('type'), R.equals('NOT_DISPLAYED'))(annotation)),
    R.append(annotationIndex)
  )(acc),
  []
);
