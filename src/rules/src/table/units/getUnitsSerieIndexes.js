import * as R from 'ramda';

//=> [{ dimension: { __index: 0 }, value: { __index: 0 } }] x { '0:x:x:x': {} }
export const getUnitsSerieIndexes = ({ unitsIndexes, partialUnitSerieIndexes }) => (layoutDataEntry) => R.reduce(
  (pUSI, unitIndex) => R.update(
    R.path([unitIndex, 'dimension', '__index'], layoutDataEntry),
    R.path([unitIndex, 'value', '__index'], layoutDataEntry),
    pUSI
  ),
  partialUnitSerieIndexes,
  unitsIndexes
);
