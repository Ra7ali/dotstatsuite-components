import * as R from 'ramda';

export const getUnitsCodes = ({ data, unitsDefinition }) => {
  const dimensionsCodes = R.pipe(
    R.pathOr([], ['data', 'structure', 'dimensions', 'observation']),
    R.indexBy(R.prop('id')),
    R.props(unitsDefinition),
    R.pluck('id')
  )(data);

  const attributesCodes = R.pipe(
    R.pathOr({}, ['data', 'structure', 'attributes']),
    R.mapObjIndexed(
      R.pipe(
        R.filter(R.pipe(R.prop('id'), R.flip(R.includes)(unitsDefinition))),
        R.pluck('id')
      )
    )
  )(data);

  return ({ dimensions: dimensionsCodes, attributes: attributesCodes });
};
