import * as R from 'ramda';

export const getUnitsDisplay = ({ unitsAttachmentSeriesIds, layoutIds }) => {
  if (R.isEmpty(unitsAttachmentSeriesIds)) {
    return 'cells';
  }
  const headerIntersections = R.intersection(layoutIds.header, unitsAttachmentSeriesIds);
  if (R.length(headerIntersections) === R.length(unitsAttachmentSeriesIds)) {
    return 'header';
  }
  if (!R.isEmpty(headerIntersections)) {
    return 'cells';
  }
  const sectionsIntersections = R.intersection(layoutIds.sections, unitsAttachmentSeriesIds);
  if (R.length(sectionsIntersections) === R.length(unitsAttachmentSeriesIds)) {
    return 'sections';
  }
  return 'rows';
};
