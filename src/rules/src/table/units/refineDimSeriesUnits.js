import * as R from 'ramda';

export const refineDimSeriesUnits = ({
  unitsArtefacts, // careful, here dimensions and attributes are merged,
  rejectedValueIds=[]
}) => R.pipe(
  R.mapObjIndexed(
    ({ observations, ...unitsIndexes }) => R.pipe(
      R.filter(R.allPass([
        occurences => R.length(occurences) === observations,
        occurences => R.pipe(R.uniq, R.length, R.equals(1))(occurences)
      ])),
      R.mapObjIndexed(
        (indexes, artefactId) => {
          const artefact = R.propOr({}, artefactId, unitsArtefacts);
          const value = R.pathOr({}, ['values', R.head(indexes)], artefact);
          return ({ ...R.pick(['id', 'name'], artefact), value });
        }
      ),
      R.reject(
        R.pipe(R.prop('value'), R.anyPass([R.isEmpty, val => R.includes(val.id, rejectedValueIds)]))
      )
    )(unitsIndexes)
  ),
  R.reject(R.isEmpty)
);
