import * as R from 'ramda';

export const getHeaderUnits = ({
  rejectedValueIds=[],
  unitsArtefacts, // { dimensions: { header: { id: dim, ... }, series: {} }, attributes: { header: {}, series: {} } }
  unitsDefinitionCodes=[]
}) => {
  const artefacts = R.converge(
    R.merge,
    [
      R.pathOr({}, ['dimensions', 'header']),
      R.pathOr({}, ['attributes', 'header'])
    ]
  )(unitsArtefacts);

  return R.pipe(
    R.props(unitsDefinitionCodes),
    R.reduce(
      (acc, entry) => {
        if (R.isNil(entry) || R.includes(R.path(['values', 0, 'id'], entry), rejectedValueIds)) {
          return acc;
        }
        return R.append(
          { ...R.pick(['id', 'name'], entry), value: R.path(['values', 0], entry) },
          acc
        );
      },
      []
    )
  )(artefacts);
};
