import * as R from 'ramda';
import { appendUnitsInLayoutDataEntry } from './appendUnitsInLayoutDataEntry';
import { getUnitsSerieIndexes } from './getUnitsSerieIndexes';

export const appendUnitsInLayoutData = ({
  display, // label || code || both
  layoutIds,
  rejectedValueIds,
  unitDimension,
  unitsDisplay,
  unitsSeries,
  unitsDefinitionCodes,
  unitsIndexes, // { header: [], sections: [], rows: [] }
  partialUnitSerieIndexes, // here it is blank (only 'x' in the array)
}) => (layoutData) => {
  switch (unitsDisplay) {
    case 'header':
      return R.evolve({
        'headerData': R.map(appendUnitsInLayoutDataEntry({
          display, unitDimension, unitsSeries, unitsDefinitionCodes,
          unitsIndexes: unitsIndexes.header, partialUnitSerieIndexes,
          rejectedValueIds
        }))
      }, layoutData);
    case 'sections':
      return R.evolve({
        'sectionsData': R.map(
          ([section, rows]) => ([
            appendUnitsInLayoutDataEntry({
              display, unitDimension, unitsSeries, unitsDefinitionCodes,
              unitsIndexes: unitsIndexes.sections, partialUnitSerieIndexes,
              rejectedValueIds
            })(section),
          rows])
        )
      }, layoutData);
    case 'rows':
      return R.evolve({
        'sectionsData': R.map(([section, rows]) => {
          const sectionSerieIndexes = getUnitsSerieIndexes({
            unitsIndexes: unitsIndexes.sections,
            partialUnitSerieIndexes
          })(section);
          const unitsCodes = R.without(layoutIds.sections, unitsDefinitionCodes);
          const unitsInRows = R.map(appendUnitsInLayoutDataEntry({
            display, unitDimension, unitsSeries, unitsDefinitionCodes: unitsCodes,
            unitsIndexes: unitsIndexes.rows, partialUnitSerieIndexes: sectionSerieIndexes,
            rejectedValueIds
          }))(rows);
          return [section, unitsInRows];
        })
      }, layoutData);
    default:
      return layoutData;
  }
};
