import * as R from 'ramda';

export const getUnitsDefinition = ({
  data, // sdmxJson
  defaultCodes, // [...codelistIds] 
  annotationsDefinitionCodes // { concepts: [] }
}) => {
  const conceptsCodes = R.propOr([], 'concepts', annotationsDefinitionCodes);
  if (R.anyPass([R.isNil, R.isEmpty])(conceptsCodes)) {
    return defaultCodes;
  }

  const datasetAnnotations = R.converge(
    R.props,
    [
      R.pathOr([], ['data', 'dataSets', 0, 'annotations']),
      R.pathOr([], ['data', 'structure', 'annotations'])
    ]
  )(data);

  const keyedDatasetAnnotations = R.indexBy(R.prop('type'), datasetAnnotations);
  
  const conceptsAnnotation = R.pipe(
    R.props(conceptsCodes),
    R.find(ann => !R.isNil(ann)),
    R.when(R.isNil, R.always({}))
  )(keyedDatasetAnnotations);

  return R.pipe(
    R.propOr('', 'title'),
    R.split(','),
    R.when(R.equals(['']), R.always(defaultCodes))
  )(conceptsAnnotation);
};