import * as R from 'ramda';

export const getUnitsSeriesOccurences = ({ attachmentSeriesIndexes }) => R.pipe(
  R.values,
  R.reduce(
    (acc, observation) => {
      const serieKey = R.pipe(
        R.addIndex(R.map)(
          (valueIndex, dimIndex) => {
            if (R.includes(dimIndex, attachmentSeriesIndexes)) {
              return valueIndex;
            }
            return 'x';
          }
        ),
        R.join(':')
      )(observation.dimValuesIndexes);

      const serie = R.pathOr({}, ['rawUnitsSeries', serieKey], acc);

      const unitsOccurences = R.mapObjIndexed(
        R.ifElse(R.isNil, R.identity, R.path(['value', 'index'])),
        observation.units
      );

      let updatedSerie = serie;
      R.forEachObjIndexed(
        (valueIndex, unitId) => {
          updatedSerie = R.over(
            R.lensProp(unitId),
            R.ifElse(R.isNil, R.always([valueIndex]), R.append(valueIndex))
          )(updatedSerie)
        },
        unitsOccurences
      );
      updatedSerie = R.over(
        R.lensProp('observations'),
        R.ifElse(R.isNil, R.always(1), R.inc)
      )(updatedSerie);

      return R.evolve({
        rawUnitsSeries: R.assoc(serieKey, updatedSerie),
        obs: R.assoc(
          observation.key,
          R.assocPath(['units', 'serieKey'], serieKey, observation)
        )
      })(acc);
    },
    { rawUnitsSeries: {}, obs: {} }
  )
);
