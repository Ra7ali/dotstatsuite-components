import * as R from 'ramda';

export const getAttachmentSeriesIndexes = ({
  unitsArtefacts,
  dimensions // { id: { index, ...dim } }
}) => {
  const targettedDimensionsCodes = R.pipe(
    R.pathOr({}, ['attributes', 'series']),
    R.values,
    R.map(R.pathOr([], ['relationship', 'dimensions'])),
    R.unnest
  )(unitsArtefacts);

  const unitsDimensionsCodes = R.pipe(
    R.pathOr({}, ['dimensions', 'series']),
    R.reject(R.propOr(false, 'header')),
    R.keys,
  )(unitsArtefacts);

  return R.pipe(
    R.props(R.concat(unitsDimensionsCodes, targettedDimensionsCodes)),
    R.filter(dim => !R.isNil(dim)),
    R.pluck('index'),
    R.uniq
  )(dimensions);
};
