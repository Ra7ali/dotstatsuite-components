import * as R from 'ramda';

const formatUnitsInLayoutSerie = R.over(
  R.lensIndex(-1),
  (units) => {
    const label = R.path(['value', 'unitsLabel'], units);
    return R.assocPath(['value', 'label'], label, units);
  }
);

const removeUnitsDimensions = (indexes) =>  R.addIndex(R.reject)(
  (entry, index) => R.includes(index, indexes)
);

export const cleanUnitsInLayoutData = ({
  unitsDisplay, // sections, headers, rows or cells
  unitsLayoutIndexes, // indexes to remove from the layout data
}) => (layoutData) => {
  switch (unitsDisplay) {
    case 'header':
      return R.evolve({
        headerData: R.map(
          R.over(
            R.lensProp('data'),
            R.pipe(
              removeUnitsDimensions(unitsLayoutIndexes.header),
              formatUnitsInLayoutSerie
            )
          )
        )
      })(layoutData);
    case 'sections':
      return R.evolve({
        sectionsData: R.map(
          ([section, rows]) => {
            const updatedSection = R.over(
              R.lensProp('data'),
              R.pipe(
                removeUnitsDimensions(unitsLayoutIndexes.sections),
                formatUnitsInLayoutSerie
              )
            )(section);
            return ([updatedSection, rows]);
          }
        )
      })(layoutData);
    case 'rows':
      return R.evolve({
        sectionsData: R.map(
          ([section, rows]) => {
            const updatedRows = R.map(R.over(
              R.lensProp('data'),
              R.pipe(
                removeUnitsDimensions(unitsLayoutIndexes.rows),
                formatUnitsInLayoutSerie
              )
            ), rows);
            return ([section, updatedRows]);
          }
        )
      })(layoutData);
    default:
      return layoutData;
  }
};
