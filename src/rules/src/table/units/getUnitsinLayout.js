import * as R from 'ramda';

const getUnitsIndexesInLayoutEntry = (unitsIds) => R.addIndex(R.reduce)(
  (indexes, id, index) => {
    if (R.has(id, unitsIds)) {
      return R.append(index, indexes);
    }
    return indexes;
  },
  [],
);

export const getUnitsInLayout = ({
  layoutIds, // { header: [], sections: [], rows: [] }
  unitsAttachmentSeriesIds=[], // [ ...DIM_IDS ]
  unitsDefinitionCodes=[],
  unitsSeries={}
}) => {
  const defaultIndexes = { header: [], sections: [], rows: [] };
  if (R.isEmpty(unitsAttachmentSeriesIds) || R.isEmpty(unitsSeries)) {
    return {
      unitsLevelDisplay: 'cells',
      unitsAttachmentIndexesInLayout: defaultIndexes,
      unitsIndexesInLayout: defaultIndexes
    };
  }
  const unitsLength = R.length(unitsAttachmentSeriesIds);
  const keyedAttUnitsIds = R.indexBy(R.identity, unitsAttachmentSeriesIds);
  const keyedUnitsIds = R.indexBy(R.identity, unitsDefinitionCodes);
  const headerUnitsIndexes = getUnitsIndexesInLayoutEntry(keyedAttUnitsIds)(R.propOr([], 'header', layoutIds));
  if (R.length(headerUnitsIndexes) === unitsLength) {
    return {
      unitsLevelDisplay: 'header',
      unitsAttachmentIndexesInLayout: R.assoc('header', headerUnitsIndexes, defaultIndexes),
      unitsIndexesInLayout: R.assoc(
        'header',
        getUnitsIndexesInLayoutEntry(keyedUnitsIds)(R.propOr([], 'header', layoutIds)),
        defaultIndexes
      )
    };
  }
  if (!R.isEmpty(headerUnitsIndexes)) {
    return {
      unitsLevelDisplay: 'cells',
      unitsAttachmentIndexesInLayout: defaultIndexes,
      unitsIndexesInLayout: defaultIndexes
    };
  }
  const sectionsAttUnitsIndexes = getUnitsIndexesInLayoutEntry(keyedAttUnitsIds)(R.propOr([], 'sections', layoutIds));
  const sectionsUnitsIndexes = getUnitsIndexesInLayoutEntry(keyedUnitsIds)(R.propOr([], 'sections', layoutIds));
  if (R.length(sectionsAttUnitsIndexes) === unitsLength) {
    return {
      unitsLevelDisplay: 'sections',
      unitsAttachmentIndexesInLayout: R.assoc('sections', sectionsAttUnitsIndexes, defaultIndexes),
      unitsIndexesInLayout: R.assoc('sections', sectionsUnitsIndexes, defaultIndexes)
    }
  }
  const rowsAttUnitsIndexes = getUnitsIndexesInLayoutEntry(keyedAttUnitsIds)(R.propOr([], 'rows', layoutIds));
  const rowsUnitsIndexes = getUnitsIndexesInLayoutEntry(keyedUnitsIds)(R.propOr([], 'rows', layoutIds));
  return ({
    unitsLevelDisplay: 'rows',
    unitsAttachmentIndexesInLayout: R.pipe(
      R.assoc('sections', sectionsAttUnitsIndexes),
      R.assoc('rows', rowsAttUnitsIndexes)
    )(defaultIndexes),
    unitsIndexesInLayout: R.pipe(
      R.assoc('sections', sectionsUnitsIndexes),
      R.assoc('rows', rowsUnitsIndexes)
    )(defaultIndexes)
  });
};
