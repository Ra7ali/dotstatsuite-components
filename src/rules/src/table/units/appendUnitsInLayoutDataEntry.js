import * as R from 'ramda';
import { getUnitsSerieIndexes } from './getUnitsSerieIndexes';
import { dimensionValueDisplay } from '../../dimension-utils';

export const appendUnitsInLayoutDataEntry = ({
  display, // label || code || both
  rejectedValueIds=[],
  unitDimension, // { id: 'UNITS', name: 'Units' }; provided y DE
  unitsSeries, // all the units series parsed through observations
  unitsDefinitionCodes, // all units dimensions and attributes ids in array (used to guarantee the order of the concatenated data) 
  unitsIndexes, // indexes of all the dimensions attached to the units in the layout data entry
  partialUnitSerieIndexes // array of indexes to complete with units values indexes of the layout entry to retrieve tthe corresponding units serie
}) => (layoutDataEntry) => { // [{ dimension: {}, value: {} }]
  const unitsSerie = R.pipe(
    getUnitsSerieIndexes({ unitsIndexes, partialUnitSerieIndexes }),
    R.join(':'),
    R.flip(R.propOr({}))(unitsSeries)
  )(layoutDataEntry);
  const unitsData = {
    dimension: unitDimension,
    value: {
      id: 'VAL',
      unitsLabel: R.pipe(
        R.props(unitsDefinitionCodes),
        R.reduce(
          (label, unit) => {
            if (R.isNil(unit) || R.includes(R.path(['value', 'id'], unit), rejectedValueIds)) {
              return label;
            }
            const unitLabel = dimensionValueDisplay(display)(R.propOr({}, 'value', unit));
            return R.isEmpty(label)
              ? unitLabel
              : `${label}, ${unitLabel}`;
          },
          ''
        )
      )(unitsSerie)
    }
  };
  return R.append(unitsData, layoutDataEntry);
};
