import * as R from 'ramda';

export const getUnitsArtefacts = ({ unitsDefinitionCodes=[], data, getDisplay }) => {
  const dimensions = R.pathOr([], ['data', 'structure', 'dimensions', 'observation'], data);
  const parsedUnitsDimensions = R.addIndex(R.reduce)(
    (acc, dimension, index) => {
      if (!R.includes(dimension.id, unitsDefinitionCodes)) {
        return acc;
      }
      const length = R.pipe(R.propOr([], 'values'), R.length)(dimension);
      if (length === 1) {
        return R.evolve({ header: R.assoc(dimension.id, { ...dimension, index, header: true }) }, acc);
      }
      if (length > 1) {
        return R.evolve({ series: R.assoc(dimension.id, { ...dimension, index }) }, acc);
      }
      return acc;
    },
    { header: {}, series: {} },
    dimensions
  );

  const keyedDimensions = R.indexBy(R.prop('id'), dimensions);

  const parsedUnitsAttributes = R.converge(
    (datasetLevel, observationLevel) => R.over(
      R.lensProp('header'),
      R.merge(datasetLevel)
    )(observationLevel),
    [
      R.pipe(
        R.pathOr([], ['data', 'structure', 'attributes', 'dataSet']),
        R.filter(R.pipe(R.propOr([], 'values'), R.length, R.equals(1))),
        R.indexBy(R.prop('id')),
        R.pick(unitsDefinitionCodes),
        R.filter(getDisplay),
        R.mapObjIndexed(R.assoc('datasetLevel', true))
      ),
      R.pipe(
        R.pathOr([], ['data', 'structure', 'attributes', 'observation']),
        R.addIndex(R.reduce)(
          (acc, attr, index) => {
            if(!R.includes(attr.id, unitsDefinitionCodes) || R.length(R.propOr([], 'values', attr)) === 0
              || !getDisplay(attr)) {
              return acc;
            }
            if (R.path(['relationship', 'primaryMeasure'], attr) === 'OBS_VALUE') {
              return R.assocPath(['series', attr.id], { ...attr, index, observations: true }, acc);
            }
            const attachedDimensions = R.path(['relationship', 'dimensions'], attr);
            if (R.isNil(attachedDimensions)) {
              return acc;
            }
            const filteredAttachedDimensions = R.filter(
              (id) => {
                const dimension = R.propOr({}, id, keyedDimensions);
                const nValues = R.pipe(R.propOr([], 'values'), R.length)(dimension);
                return nValues > 1;
              },
              attachedDimensions
            );
            const level = R.isEmpty(filteredAttachedDimensions)
              ? 'header'
              : 'series';
            return R.assocPath(
              [level, attr.id],
              { ...attr, index, relationship: { dimensions: filteredAttachedDimensions } },
              acc
            );
          },
          { header: {}, series: {} }
        )
      )
    ]
  )(data);

  const hasSeries = R.pipe(
    R.all(R.isEmpty),
    R.not
  )([parsedUnitsDimensions.series, parsedUnitsAttributes.series]);

  return ({
    dimensions: R.evolve({
      header: R.when(R.always(hasSeries), R.always({})),
      series: R.when(R.always(hasSeries), R.merge(parsedUnitsDimensions.header))
    })(parsedUnitsDimensions),
    attributes: R.evolve({
      header: R.when(R.always(hasSeries), R.always({})),
      series: R.when(R.always(hasSeries), R.merge(parsedUnitsAttributes.header))
    })(parsedUnitsAttributes)
  });
};
