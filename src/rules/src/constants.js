export const BAR = 'BarChart';
export const ROW = 'RowChart';
export const SCATTER = 'ScatterChart';
export const TIMELINE = 'TimelineChart';
export const H_SYMBOL = 'HorizontalSymbolChart';
export const V_SYMBOL = 'VerticalSymbolChart';
export const STACKED = 'StackedBarChart';
export const CHORO = 'ChoroplethChart';
export const PERCENT = 'percent';
export const VALUES = 'values';
export const TYPES = {
  [BAR]: BAR,
  [ROW]: ROW,
  [SCATTER]: SCATTER,
  [TIMELINE]: TIMELINE,
  [H_SYMBOL]: H_SYMBOL,
  [V_SYMBOL]: V_SYMBOL,
  [STACKED]: STACKED,
  [CHORO]: CHORO,
};
export const FREQ_A = 'annual';
export const FREQ_S = 'semesterly';
export const FREQ_Q = 'quarterly';
export const FREQ_M = 'monthly';
export const DEFAULT_UPR_IDS = {
  U: ['UNIT', 'UNIT_MEASURE', 'UNIT_MEAS', 'UNITS'],
  P: ['UNIT_MULT', 'POWERCODE', 'POWER', 'MAGNITUDE', 'SCALE'],
  R: ['REF_PERIOD', 'REFERENCEPERIOD', 'BASE_PER']
};

export const DEFAULT_REJECTED_UPR_VALUES_IDS = {
  U: [],
  P: ['0'],
  R: []
};

export const DEFAULT_REJECTED_SUBTITLE_IDS = ['_L', '_T', '_Z'];

export const DEFAULT_MAJOR_DIMENSIONS = {
  FREQ: {
    dimension: {
      id: ['FREQ', 'FREQUENCY'],
      values: {
        [`${FREQ_A}`]: { id: ['A'] },
        [`${FREQ_S}`]: { id: ['S'] },
        [`${FREQ_Q}`]: { id: ['Q'] },
        [`${FREQ_M}`]: { id: ['M'] }
      }
    },
    attribute: {
      id: ['TIME_FORMAT'],
      values: {
        [`${FREQ_A}`]: { id: ['P1Y'] },
        [`${FREQ_S}`]: { id: ['P6M'] },
        [`${FREQ_Q}`]: { id: ['P3M'] },
        [`${FREQ_M}`]: { id: ['P1M'] }
      }
    }
  },
  AREA: {
    dimension: {
      id: ['REF_AREA', 'LOCATION', 'COUNTRY']
    }
  },
  TIME: {
    dimension: {
      id: ['TIME_PERIOD', 'TIME', 'YEAR', 'time']
    }
  }
}

export const DIMENSION_DISPLAY_PARTS = {
  DESCRIPTION: 'description',
  CODE: 'code',
  BOTH: 'both',
};

export const DEFAULT_MAJOR_ATTRIBUTES = {
  DECIMALS: {
    ID: 'DECIMALS',
    ROLE: 'DECIMALS'
  },
  SCALE: {
    ID: 'SCALE'
  }
};

export const V8_SDMX_JSON = "application/vnd.sdmx.data+json;version=1.0.0-wd";
