import { size, isEmpty, reduce, concat, filter, isEqual, differenceBy, omit, get, find } from 'lodash';
import memoizee from 'memoizee';
import { getDimensions } from './sdmx-utils';
import { isAreaDimension, isTimeDimension } from '../dimension-utils';
import { DIMENSION_DISPLAY_PARTS } from '../constants';

export * as sdmxUtils from './sdmx-utils';
export * as attributeUtils from './attribute-utils';

export function getDefaultLayout(sdmxJson) {
  const suitableDimensions = getSuitableDimensions(sdmxJson);
  const defaultXdim = [];

  const timeDim = find(suitableDimensions, x => isTimeDimension(x.id));
  if (!isEmpty(timeDim)) {
    defaultXdim.push(timeDim);
  } else {
    const firstNonCountryDim = find(suitableDimensions, x => !isAreaDimension(x.id));
    !isEmpty(firstNonCountryDim) ? defaultXdim.push(firstNonCountryDim) : null;
  }

  return {
    columns: defaultXdim,
    rows: differenceBy(suitableDimensions, defaultXdim, 'id')
  }
}

export function isConfigSuitable({ sdmxJson, config }) {
  if (!sdmxJson || !config) {
    return false;
  }

  const dimensions = getSuitableDimensions(sdmxJson);
  const configDimensions = concat(get(config, 'layout.rows'), get(config, 'layout.columns'));
  if (!isEqual(size(dimensions), size(configDimensions))) {
    return false;
  }

  return isEmpty(differenceBy(configDimensions, dimensions, 'id'));
}

function _getDefaultTableConfig(sdmxJson) {
  if (isEmpty(sdmxJson)) {
    return null;
  }
  return {
    layout: getDefaultLayout(sdmxJson),
    dimensionLabel: DIMENSION_DISPLAY_PARTS.DESCRIPTION,
  };
}
export const getDefaultTableConfig = memoizee(_getDefaultTableConfig);

function _getSuitableDimensions(sdmxJson) {
  return reduce(getDimensions(sdmxJson), (agg, dim) => {
    if (size(dim.values) > 1) {
      agg.push(omit(dim, ['values']));
    }
    return agg;
  }, []);
}
const getSuitableDimensions = memoizee(_getSuitableDimensions);
