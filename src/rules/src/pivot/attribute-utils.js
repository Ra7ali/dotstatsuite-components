import { reduce, get, find, difference, filter, map, concat, set, isEmpty, size } from 'lodash';
import { DEFAULT_UPR_IDS } from '../constants';

export function getByLayout(structure, layout, jsonAttributes) {
  const { columns, rows } = layout;
  const structureAttributes = get(structure, 'attributes');

  const columnDimensionIds = map(columns, x => x.id);
  const rowDimensionIds = map(rows, x => x.id);
  const layoutDimensionIds = concat(columnDimensionIds, rowDimensionIds);
  const allLayoutDimensions = concat(columns, rows);

  return reduce(structureAttributes, (agg, attr, key) => {
    const dimensionRelationshipInLayout = filter(get(attr, 'relationship.dimension', []), x => layoutDimensionIds.indexOf(x) > -1);

    if (!isEmpty(dimensionRelationshipInLayout)) {
      const jsonAttr = find(jsonAttributes.observation, x => x.id === attr.id);
      if (!isEmpty(jsonAttr) && size(get(jsonAttr, 'values')) > 1) {
        Object.assign(jsonAttr, {
          dimensionRelationship: reduce(dimensionRelationshipInLayout, (agg, item) => {
            const inLayout = find(allLayoutDimensions, x => x.id == item);
            if (inLayout) {
              agg.push({
                name: inLayout.name,
                id: inLayout.id,
              })
            }
            return agg;
          }, [])
        });

        if (difference(dimensionRelationshipInLayout, rowDimensionIds).length == 0) {
          agg.rows[attr.id] = jsonAttr;
        } else if (difference(dimensionRelationshipInLayout, columnDimensionIds).length == 0) {
          agg.columns[attr.id] = jsonAttr;
        } else {
          agg.cells[attr.id] = jsonAttr;
        }
      }
    }

    return agg;
  }, { structureAttributes, rows: {}, columns: {}, cells: {} });
}

export function splitToUprAndOther(attributes, uprAttributeIds = DEFAULT_UPR_IDS) {
  return reduce(attributes, (agg, attr) => {

    const uprAttr = reduce(uprAttributeIds, (uprAgg, uprIds, key) => {
      if (isEmpty(uprAgg) && uprIds.indexOf(attr.id) > -1) {
        set(uprAgg, key, attr);
      }
      return uprAgg;
    }, {});

    if (!isEmpty(uprAttr)) {
      Object.assign(agg.upr, uprAttr);
    } else {
      set(agg, `other.${attr.id}`, attr);
    }

    return agg;
  }, { upr: {}, other: {} });
}
