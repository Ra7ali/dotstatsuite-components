import { get, map, keys } from 'lodash';

export function getObservationKeysAsArrays(observations) {
  return map(keys(observations), x => x.split(':'));
}

export function getDimensions(sdmxJson) {
  const dimensions = get(sdmxJson, 'structure.dimensions.observation', []);

  return map(dimensions, (x, idx) => Object.assign(x, {
      index: idx,
      values: map(x.values, (y, idy) => Object.assign(y, { index: idy }))
    })
  );
}

export function getAttributes(sdmxJson) {
  // add 1 to index of attribute by default (in observation array there are always observation value at index 0)
  const addIndex = (attributes) => map(attributes, (x, idx) => Object.assign(x, {
      index: (1 + idx),
      values: map(x.values, (y, idy) => Object.assign(y, { index: idy }))
    })
  );

  return {
    dataSet: addIndex(get(sdmxJson, 'structure.attributes.dataSet', [])),
    observation: addIndex(get(sdmxJson, 'structure.attributes.observation', [])),
  }
}

export function getObservations(sdmxJson) {
  return get(sdmxJson, 'dataSets[0].observations', {});
}