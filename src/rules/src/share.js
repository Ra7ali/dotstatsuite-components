import * as R from 'ramda';
import { TIMELINE } from './constants';

/*
  Goal: return only relevant Data regarding latest or snapshot mode.
  Why: At this moment, still have two Share implementation:
    - V7: For Chart Generator, shareData defined in `oecd-charts` component, passed to `oecd-share`;
    - V8: For Data Explorer, shareData generated in the application, for share thunk action;
  Better have only one function for Both.
  Issue: for V7 share, `oecd-charts` doesn't hold share mode aspect, handle by `oecd-share`.
  Result: process in 2 times : First a complete generation, then a refining.
  A third function will be added, combining the 2 previous, for V8 use
  chartData = {
    footnotes,
    frequency,
    series,
    share: { focused, chartDimension }
    subtitle,
    title,
    uprs
  };
  config = {
    display,
    fonts,
    formaterIds,
    locale,
    logo,
    owner,
    source,
    sourceHeaders,
    terms
  };
*/

export const getShareData = (chartData, chartOptions, config, type, range) => ({
  config: R.pick(['fonts', 'locale', 'logo', 'owner', 'terms', 'sourceHeaders'], config),
  data: {
    ...R.pick(['footnotes', 'series', 'subtitle', 'title', 'uprs'], chartData),
    ...(type === TIMELINE ? { frequency: R.prop('frequency', chartData) } : {}),
    range,
    share: {
      ...R.pick(['display', 'formaterIds'], config),
      focused: R.path(['share', 'focused'], chartData),
      chartDimension: R.path(['share', 'chartDimension'], chartData),
      source: R.path(['share', 'source'], chartData),
    }
  },
  options: chartOptions,
});

export const refineShareData = (shareMode) => (shareData) => {
  const latestRefine = R.pipe(
    R.dissocPath(['data', 'series']),
    R.dissocPath(['data', 'uprs']),
    R.dissocPath(['data', 'frequency']),
    R.dissocPath(['data', 'range']),
  );
  const snapshotRefine = R.pipe(
    R.dissocPath(['config', 'sourceHeaders']),
    R.dissocPath(['data', 'share', 'display']),
    R.dissocPath(['data', 'share', 'formaterIds']),
    R.dissocPath(['data', 'share', 'source']),
    R.dissocPath(['data', 'share', 'chartDimension']),
  );
  return R.pipe(
    R.ifElse(
      () => shareMode === 'latest',
      latestRefine,
      snapshotRefine
    ),
    R.assocPath(['data', 'share', 'sharedate'], new Date().toISOString()),
  )(shareData)
};

export const getChartShareData = (chartData, chartOptions, config, type, shareMode, range = {}) => {
  return R.pipe(
    getShareData,
    refineShareData(shareMode)
  )(chartData, chartOptions, config, type, range);
};
