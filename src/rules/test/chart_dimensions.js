import { expect } from 'chai';
import eql from 'deep-eql';
import oecd_HEALTH_PROT from './oecd-HEALTH_PROT.json';
import oecd_HEALTH_PROT_NO_FREQ from './oecd-HEALTH_PROT-no-freq.json';
import oecd_HEALTH_PROT_WRONG_FREQ from './oecd-HEALTH_PROT-wrong-freq.json';
import oecd_KEI from './oecd-KEI.json';
import { scatterDimension, symbolDimension, timelineFrequency } from '../src';

describe('timeline frequency tests', () => {
  it('found in dimensions', () => {
    expect(timelineFrequency(oecd_KEI)).to.equal('annual');
  });
  it('found in attributes', () => {
    expect(timelineFrequency(oecd_HEALTH_PROT)).to.equal('annual');
  });
  it('no frequency to be found', () => {
    expect(timelineFrequency(oecd_HEALTH_PROT_NO_FREQ)).to.equal(null);
  });
  it('unknown frequency value', () => {
    expect(timelineFrequency(oecd_HEALTH_PROT_WRONG_FREQ)).to.equal(null);
  });
});

const expected_default_scatter = { id: 'SUBJECT', xId: 'PRINTO01', yId: 'PRMNTO01' };
describe('scatter dimension tests', () => {
  it('default', () => {
    eql(scatterDimension(oecd_KEI), expected_default_scatter);
  });
});

const expected_default_symbol = {
  id: 'SUBJECT',
   serie: [
    {
      id: 'PRINTO01',
      name: 'Industrial production, s.a.'
    },
    {
      id: 'PRMNTO01',
      name: 'Total manufacturing, s.a.'
    }
  ]
};
describe('symbol dimension tests', () => {
  it('default', () => {
    eql(symbolDimension(oecd_KEI), expected_default_symbol);
  });
});