import { expect } from 'chai';
import { SCATTER, BAR } from '../src/constants';
import oecd_KEI from './oecd-KEI.json';
import {
  scatterStateFromNewProps,
  scatterStateValueToOption,
  scatterValueOptions,
  onChangeScatterDim,
  onChangeScatterVal,
} from '../src/properties/scatter';

const undefinedState = {
  scatterDimension: undefined,
  scatterX: undefined,
  scatterY: undefined,
};

const scatterKEIDefaultState = {
  scatterDimension: 'SUBJECT',
  scatterX: 'PRINTO01',
  scatterY: 'PRMNTO01',
};

const scatterKEICustomState = {
  scatterDimension: 'LOCATION',
  scatterX: 'AUS',
  scatterY: 'AUT',
};

const scatterKEICustomState2 = {
  scatterDimension: 'LOCATION',
  scatterX: 'CZE',
  scatterY: 'FRA',
};

const scatterKEICustomStateWithoutValues = {
  scatterDimension: 'LOCATION',
  scatterX: undefined,
  scatterY: undefined,
};

const scatterKEICustomStateWithoutDim = {
  scatterDimension: undefined,
  scatterX: 'AUS',
  scatterY: 'AUT',
}; 

const scatterUnexistingState = {
  scatterDimension: 'undefined_dim',
  scatterX: 'undefined_val1',
  scatterY: 'undefined_val2'
};

// focus tested separately
const stateWithFocus = (state) => ({  ...state, highlight: [], baseline: [] });

const undefinedStateWithFocus = stateWithFocus(undefinedState);

const defaultKEIState = stateWithFocus(scatterKEIDefaultState);

const customKEIState = stateWithFocus(scatterKEICustomState);

const customKEIState2 = stateWithFocus(scatterKEICustomState2);

describe('scatterStateFromNewProps tests', () => {
  it('undefined state from no data', () => {
    const props = { type: SCATTER };
    expect(scatterStateFromNewProps(props, scatterKEIDefaultState)).to.deep.equal(undefinedStateWithFocus);
  });
  it('undefined state from not scatter type', () => {
    const props = { data: oecd_KEI, type: BAR };
    expect(scatterStateFromNewProps(props, scatterKEIDefaultState)).to.deep.equal(undefinedState);
  });
  it('default from no state', () => {
    const props = { data: oecd_KEI, type: SCATTER};
    expect(scatterStateFromNewProps(props, {})).to.deep.equal(defaultKEIState);
  });
  it('default from undefined state', () => {
    const props = { data: oecd_KEI, type: SCATTER };
    expect(scatterStateFromNewProps(props, undefinedState)).to.deep.equal(defaultKEIState);
  });
  it('default from unexisting state', () => {
    const props = { data: oecd_KEI, type: SCATTER };
    expect(scatterStateFromNewProps(props, scatterUnexistingState)).to.deep.equal(defaultKEIState);
  });
  it('default from default state', () => {
    const props = { data: oecd_KEI, type: SCATTER };
    expect(scatterStateFromNewProps(props, scatterKEIDefaultState)).to.deep.equal(defaultKEIState);
  });
  it('custom state from custom state', () => {
    const props = { data: oecd_KEI, type: SCATTER };
    expect(scatterStateFromNewProps(props, scatterKEICustomState)).to.deep.equal(customKEIState);
  });
  it('custom state without values', () => {
    const props = { data: oecd_KEI, type: SCATTER };
    expect(scatterStateFromNewProps(props, scatterKEICustomStateWithoutValues)).to.deep.equal(customKEIState)
  });
  it('default state from custom without dimension', () => {
    const props = { data: oecd_KEI, type: SCATTER };
    expect(scatterStateFromNewProps(props, scatterKEICustomStateWithoutDim)).to.deep.equal(defaultKEIState)
  });
});

describe('focus tests through scatterStateFromNewProps', () => {
  it('empty focus', () => {
    const props = { data: oecd_KEI, type: SCATTER };
    const state = { type: SCATTER, baseline: [], highlight: [] };
    expect(scatterStateFromNewProps(props, state)).to.deep.equal(defaultKEIState);
  });
  it('correct focus', () => {
    const props = { data: oecd_KEI, type: SCATTER };
    const state = {
      baseline: [
        {
          label: 'Country - France',
          value: { dimensionId: 'LOCATION', dimensionValueId: 'FRA' }
        }
      ],
      highlight: [
        {
          label: 'Country - Australia',
          value: { dimensionId: 'LOCATION', dimensionValueId: 'AUS' }
        },
        {
          label: 'Country - Austria',
          value: { dimensionId: 'LOCATION', dimensionValueId: 'AUT' }
        },
        {
          label: 'Country - Czech Republic',
          value: { dimensionId: 'LOCATION', dimensionValueId: 'CZE' }
        }
      ]
    };
    expect(scatterStateFromNewProps(props, { ...state, type: SCATTER })).to.deep.equal({ ...defaultKEIState, ...state });
  });
  it('all wrong focus', () => {
    const props = { data: oecd_KEI, type: SCATTER };
    const state = {
      baseline: [
        {
          label: 'RANDOM 1',
          value: { dimensionId: 'RANDOM_DIM', dimensionValueId: 'RANDOM_VAL1' }
        }
      ],
      highlight: [
        {
          label: 'RANDOM 2',
          value: { dimensionId: 'RANDOM_DIM', dimensionValueId: 'RANDOM_VAL2' }
        },
        {
          label: 'RANDOM 3',
          value: { dimensionId: 'RANDOM_DIM', dimensionValueId: 'RANDOM_VAL3' }
        },
        {
          label: 'RANDOM 4',
          value: { dimensionId: 'RANDOM_DIM', dimensionValueId: 'RANDOM_VAL4' }
        }
      ]
    };
    expect(scatterStateFromNewProps(props, { ...state, type: SCATTER })).to.deep.equal(defaultKEIState);
  });
  it('mix of correct and wrong', () => {
    const props = { data: oecd_KEI, type: SCATTER };
    const state = {
      type: SCATTER,
      baseline: [
        {
          label: 'Country - France',
          value: { dimensionId: 'LOCATION', dimensionValueId: 'FRA' }
        }
      ],
      highlight: [
        {
          label: 'RANDOM 2',
          value: { dimensionId: 'RANDOM_DIM', dimensionValueId: 'RANDOM_VAL2' }
        },
        {
          label: 'Country - Austria',
          value: { dimensionId: 'LOCATION', dimensionValueId: 'AUT' }
        },
        {
          label: 'RANDOM 4',
          value: { dimensionId: 'RANDOM_DIM', dimensionValueId: 'RANDOM_VAL4' }
        }
      ]
    };
    const expected = {
      baseline: [
        {
          label: 'Country - France',
          value: { dimensionId: 'LOCATION', dimensionValueId: 'FRA' }
        }
      ],
      highlight: [
        {
          label: 'Country - Austria',
          value: { dimensionId: 'LOCATION', dimensionValueId: 'AUT' }
        }
      ]
    };
    expect(scatterStateFromNewProps(props, state)).to.deep.equal({ ...defaultKEIState, ...expected });
  });
  it('code display', () => {
    const props = { data: oecd_KEI, type: SCATTER, display: 'code' };
    const state = {
      type: SCATTER,
      baseline: [
        {
          label: 'Country - France',
          value: { dimensionId: 'LOCATION', dimensionValueId: 'FRA' }
        }
      ],
      highlight: [
        {
          label: 'Country - Austria',
          value: { dimensionId: 'LOCATION', dimensionValueId: 'AUT' }
        }
      ]
    };
    const expected = {
      ...defaultKEIState,
      baseline: [
        {
          label: 'LOCATION - FRA',
          value: { dimensionId: 'LOCATION', dimensionValueId: 'FRA' }
        }
      ],
      highlight: [
        {
          label: 'LOCATION - AUT',
          value: { dimensionId: 'LOCATION', dimensionValueId: 'AUT' }
        }
      ]
    };
    expect(scatterStateFromNewProps(props, state)).to.deep.equal(expected);
  });
  it('both display', () => {
    const props = { data: oecd_KEI, type: SCATTER, display: 'both' };
    const state = {
      type: SCATTER,
      baseline: [
        {
          label: 'Country - France',
          value: { dimensionId: 'LOCATION', dimensionValueId: 'FRA' }
        }
      ],
      highlight: [
        {
          label: 'Country - Austria',
          value: { dimensionId: 'LOCATION', dimensionValueId: 'AUT' }
        }
      ]
    };
    const expected = {
      ...defaultKEIState,
      baseline: [
        {
          label: '(LOCATION) Country - (FRA) France',
          value: { dimensionId: 'LOCATION', dimensionValueId: 'FRA' }
        }
      ],
      highlight: [
        {
          label: '(LOCATION) Country - (AUT) Austria',
          value: { dimensionId: 'LOCATION', dimensionValueId: 'AUT' }
        }
      ]
    };
    expect(scatterStateFromNewProps(props, state)).to.deep.equal(expected);
  });
});

describe('onChangeScatterDim tests', () => {
  it('no data', () => {
    const props = { type: SCATTER };
    expect(onChangeScatterDim(props, defaultKEIState)('LOCATION')).to.deep.equal({});
  });
  it('no value', () => {
    const props = { data: oecd_KEI, type: SCATTER };
    expect(onChangeScatterDim(props, scatterKEIDefaultState)(undefined)).to.deep.equal({});
  });
  it('correct value', () => {
    const props = { data: oecd_KEI, type: SCATTER };
    expect(onChangeScatterDim(props, defaultKEIState)('LOCATION')).to.deep.equal(customKEIState);
  });
  it('non correct value', () => {
    const props = { data: oecd_KEI, type: SCATTER };
    expect(onChangeScatterDim(props, scatterKEIDefaultState)('UNDEFINED')).to.deep.equal({});
  });
});

describe('onChangeScatterVal tests', () => {
  const onChange = (props, state) => onChangeScatterVal(props, state, 'scatterX', 'scatterY');
  it('no data', () => {
    expect(onChange({ type: SCATTER }, defaultKEIState)('AUS')).to.deep.equal({});
  });
  it('not scatter type', () => {
    expect(onChange({ data: oecd_KEI }, defaultKEIState)('AUS')).to.deep.equal({});
  });
  it('no value', () => {
    const props = { data: oecd_KEI, type: SCATTER };
    expect(onChange(props, scatterKEIDefaultState)(undefined)).to.deep.equal({});
  });
  it('correct value', () => {
    const props = { data: oecd_KEI, type: SCATTER };
    const expected = { scatterX: 'CZE', scatterY: 'AUT' }
    expect(onChange(props, scatterKEICustomState)('CZE')).to.deep.equal(expected);
  });
  it('non correct value', () => {
    const props = { data: oecd_KEI, type: SCATTER };
    expect(onChange(props, scatterKEIDefaultState)('UNDEFINED')).to.deep.equal({});
  });
  it('same value as other', () => {
    const props = { data: oecd_KEI, type: SCATTER };
    const expected = { scatterX: 'FRA', scatterY: 'AUS' };
    expect(onChange(props, customKEIState2)('FRA')).to.deep.equal(expected);
  });
});

describe('scatterStateValueToOption tests', () => {
  it('no data', () => {
    const props = { type: SCATTER, display: 'label' };
    expect(scatterStateValueToOption('scatterX', props, defaultKEIState)).to.deep.equal(undefined);
  });
  it('no state', () => {
    const props = { data: oecd_KEI, type: SCATTER, display: 'label' };
    expect(scatterStateValueToOption('scatterX', props, undefined)).to.deep.equal(undefined);
  });
  it('not scatter type', () => {
    const props = { data: oecd_KEI, type: BAR, display: 'label' };
    expect(scatterStateValueToOption('scatterX', props, defaultKEIState)).to.deep.equal(undefined);
  });
  it('undefined state', () => {
    const props = { data: oecd_KEI, type: SCATTER, display: 'label' };
    expect(scatterStateValueToOption('scatterX', props, undefinedState)).to.deep.equal(undefined);
  });
  it('non existing dimension', () => {
    const props = { data: oecd_KEI, type: SCATTER, display: 'label' };
    expect(scatterStateValueToOption('scatterX', props, scatterUnexistingState)).to.deep.equal(undefined);
  });
  it('non existing property', () => {
    const props = { data: oecd_KEI, type: SCATTER, display: 'label' };
    expect(scatterStateValueToOption('scatterZ', props, scatterUnexistingState)).to.deep.equal(undefined);
  });
  it('non existing value', () => {
    const props = { data: oecd_KEI, type: SCATTER, display: 'label' };
    const state = { scatterDimension: 'SUBJECT', scatterX: 'random' }
    expect(scatterStateValueToOption('scatterX', props, state)).to.deep.equal(undefined);
  });
  it('label displayed', () => {
    const props = { data: oecd_KEI, type: SCATTER, display: 'label' };
    const expected = { label: 'Industrial production, s.a.', value: 'PRINTO01' };
    expect(scatterStateValueToOption('scatterX', props, defaultKEIState)).to.deep.equal(expected);
  });
  it('default label displayed from unknown display props', () => {
    const props = { data: oecd_KEI, type: SCATTER, display: 'random' };
    const expected = { label: 'Industrial production, s.a.', value: 'PRINTO01' };
    expect(scatterStateValueToOption('scatterX', props, defaultKEIState)).to.deep.equal(expected);
  });
  it('code displayed', () => {
    const props = { data: oecd_KEI, type: SCATTER, display: 'code' };
    const expected = { label: 'PRINTO01', value: 'PRINTO01' };
    expect(scatterStateValueToOption('scatterX', props, defaultKEIState)).to.deep.equal(expected);
  });
  it('both displayed', () => {
    const props = { data: oecd_KEI, type: SCATTER, display: 'both' };
    const expected = { label: '(PRINTO01) Industrial production, s.a.', value: 'PRINTO01' };
    expect(scatterStateValueToOption('scatterX', props, defaultKEIState)).to.deep.equal(expected);
  });
  it('scatterY', () => {
    const props = { data: oecd_KEI, type: SCATTER, display: 'code' };
    const expected = { label: 'PRMNTO01', value: 'PRMNTO01' };
    expect(scatterStateValueToOption('scatterY', props, defaultKEIState)).to.deep.equal(expected);
  });
  it('custom', () => {
    const props = { data: oecd_KEI, type: SCATTER, display: 'code' };
    const expected = { label: 'FRA', value: 'FRA' };
    expect(scatterStateValueToOption('scatterY', props, customKEIState2)).to.deep.equal(expected);
  })
});

describe('scatterValueOptions tests', () => {
  it('no data', () => {
    const props = { type: SCATTER, display: 'label' };
    expect(scatterValueOptions(props, defaultKEIState)).to.deep.equal([]);
  });
  it('no state', () => {
    const props = { data: oecd_KEI, type: SCATTER, display: 'label' };
    expect(scatterValueOptions(props, undefined)).to.deep.equal([]);
  });
  it('not scatter type', () => {
    const props = { data: oecd_KEI, type: BAR, display: 'label' };
    expect(scatterValueOptions(props, defaultKEIState)).to.deep.equal([]);
  });
  it('undefined state', () => {
    const props = { data: oecd_KEI, type: SCATTER, display: 'label' };
    expect(scatterValueOptions(props, undefinedState)).to.deep.equal([]);
  });
  it('non existing dimension', () => {
    const props = { data: oecd_KEI, type: SCATTER, display: 'label' };
    expect(scatterValueOptions(props, scatterUnexistingState)).to.deep.equal([]);
  });
  it('dimension with just one value', () => {
    const props = { data: oecd_KEI, type: SCATTER, display: 'label' };
    expect(scatterValueOptions(props, { symbolDimension: 'MEASURE' })).to.deep.equal([]);
  });
  it('label displayed', () => {
    const props = { data: oecd_KEI, type: SCATTER, display: 'label' };
    const expected = [
      { label: 'Industrial production, s.a.', value: 'PRINTO01' },
      { label: 'Total manufacturing, s.a.', value: 'PRMNTO01' }
    ];
    expect(scatterValueOptions(props, defaultKEIState)).to.deep.equal(expected);
  });
  it('default label displayed from unexisting display prop', () => {
    const props = { data: oecd_KEI, type: SCATTER, display: 'random' };
    const expected = [
      { label: 'Industrial production, s.a.', value: 'PRINTO01' },
      { label: 'Total manufacturing, s.a.', value: 'PRMNTO01' }
    ];
    expect(scatterValueOptions(props, defaultKEIState)).to.deep.equal(expected);
  });
  it('code displayed', () => {
    const props = { data: oecd_KEI, type: SCATTER, display: 'code' };
    const expected = [
      { label: 'PRINTO01', value: 'PRINTO01' },
      { label: 'PRMNTO01', value: 'PRMNTO01' }
    ];
    expect(scatterValueOptions(props, defaultKEIState)).to.deep.equal(expected);
  });
  it('both displayed', () => {
    const props = { data: oecd_KEI, type: SCATTER, display: 'both' };
    const expected = [
      { label: '(PRINTO01) Industrial production, s.a.', value: 'PRINTO01' },
      { label: '(PRMNTO01) Total manufacturing, s.a.', value: 'PRMNTO01' }
    ];
    expect(scatterValueOptions(props, defaultKEIState)).to.deep.equal(expected);
  });
  it('custom dimension', () => {
    const props = { data: oecd_KEI, type: SCATTER, display: 'label' };
    const expected = [
      { label: 'Australia', value: "AUS" },
      { label: 'Austria', value: 'AUT' },
      { label: 'Belgium', value: 'BEL' },
      { label: 'Canada', value: 'CAN' },
      { label: 'Czech Republic', value: 'CZE' },
      { label: 'Denmark', value: 'DNK' },
      { label: 'Finland', value: 'FIN' },
      { label: 'France', value: 'FRA' },
      { label: 'Chile', value: 'CHL' },
      { label: 'Estonia', value: 'EST' }
    ];
    expect(scatterValueOptions(props, customKEIState)).to.deep.equal(expected);
  });
});
