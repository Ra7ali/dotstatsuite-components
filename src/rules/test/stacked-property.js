import { expect } from 'chai';
import { BAR, PERCENT, STACKED, VALUES } from '../src/constants';
import {
  onChangeStackedDimension,
  onChangeStackedMode,
  stackedStateFromNewProps
} from '../src/properties/stacked';
import oecd_KEI from './oecd-KEI.json';

const undefinedState = {
  stackedDimension: undefined,
  stackedMode: undefined
};

const defaultState = {
  stackedDimension: 'LOCATION',
  stackedMode: VALUES,
};

const stateWithFocus = (state) => ({ ...state, baseline: [], highlight: [] });

const defaultStateWithFocus = stateWithFocus(defaultState);

describe('stackedStateFromNewProps tests', () => {
  it('undefined state with focus from no data', () => {
    const props = { type: STACKED };
    const expected = stateWithFocus(undefinedState);
    expect(stackedStateFromNewProps(props, defaultState)).to.deep.equal(expected);
  });
  it('undefined state without focus from non stacked type', () => {
    const props = { data: oecd_KEI, type: BAR };
    expect(stackedStateFromNewProps(props, defaultState)).to.deep.equal(undefinedState);
  });
  it('default state from no state', () => {
    const props = { data: oecd_KEI, type: STACKED };
    expect(stackedStateFromNewProps(props, undefined)).to.deep.equal(defaultStateWithFocus);
  });
  it('default state from undefined state', () => {
    const props = { data: oecd_KEI, type: STACKED };
    expect(stackedStateFromNewProps(props, undefinedState)).to.deep.equal(defaultStateWithFocus);
  });
  it('default state from default state', () => {
    const props = { data: oecd_KEI, type: STACKED };
    expect(stackedStateFromNewProps(props, defaultState)).to.deep.equal(defaultStateWithFocus);
  });
  it('default state from non existing mode', () => {
    const props = { data: oecd_KEI, type: STACKED };
    const state = { stackedDimension: 'LOCATION', stackedMode: 'RANDOM' }
    expect(stackedStateFromNewProps(props, state)).to.deep.equal(defaultStateWithFocus);
  });
  it('correct dimension but non existing mode', () => {
    const props = { data: oecd_KEI, type: STACKED };
    const state = { stackedDimension: 'SUBJECT', stackedMode: 'RANDOM' }
    const expected = stateWithFocus({ stackedDimension: 'SUBJECT', stackedMode: VALUES });
    expect(stackedStateFromNewProps(props, state)).to.deep.equal(expected);
  });
  it('non existing dimension but correct mode', () => {
    const props = { data: oecd_KEI, type: STACKED };
    const state = { stackedDimension: 'RANDOM', stackedMode: PERCENT }
    const expected = stateWithFocus({ stackedDimension: 'LOCATION', stackedMode: PERCENT });
    expect(stackedStateFromNewProps(props, state)).to.deep.equal(expected);
  });
  it('dimension with only one value', () => {
    const props = { data: oecd_KEI, type: STACKED };
    const state = { stackedDimension: 'MEASURE', stackedMode: VALUES }
    expect(stackedStateFromNewProps(props, state)).to.deep.equal(defaultStateWithFocus);
  });
});

describe('focus tests through stackedStateFromNewProps', () => {
  it('empty focus', () => {
    const props = { data: oecd_KEI, type: STACKED };
    const state = { type: STACKED, baseline: [], highlight: [] };
    expect(stackedStateFromNewProps(props, state)).to.deep.equal(defaultStateWithFocus);
  });
  it('correct focus', () => {
    const props = { data: oecd_KEI, type: STACKED };
    const state = {
      baseline: [
        {
          label: 'France',
          value: 'FRA'
        }
      ],
      highlight: [
        {
          label: 'Australia',
          value: 'AUS'
        },
        {
          label: 'Austria',
          value: 'AUT'
        },
        {
          label: 'Czech Republic',
          value: 'CZE'
        }
      ]
    };
    const expected = { ...defaultState, ...state }
    expect(stackedStateFromNewProps(props, { ...state, type: STACKED })).to.deep.equal(expected);
  });
  it('all wrong focus', () => {
    const props = { data: oecd_KEI, type: STACKED };
    const state = {
      type: STACKED,
      baseline: [
        {
          label: 'RANDOM 1',
          value: 'RANDOM_VAL1'
        }
      ],
      highlight: [
        {
          label: 'RANDOM 2',
          value: 'RANDOM_VAL2'
        },
        {
          label: 'RANDOM 3',
          value: 'RANDOM_VAL3'
        },
        {
          label: 'RANDOM 4',
          value: 'RANDOM_VAL4'
        }
      ]
    };
    expect(stackedStateFromNewProps(props, state)).to.deep.equal(defaultStateWithFocus);
  });
  it('mix of correct and wrong', () => {
    const props = { data: oecd_KEI, type: STACKED };
    const state = {
      type: STACKED,
      baseline: [
        {
          label: 'France',
          value: 'FRA'
        }
      ],
      highlight: [
        {
          label: 'RANDOM 2',
          value: 'RANDOM_VAL2'
        },
        {
          label: 'Austria',
          value: 'AUT'
        },
        {
          label: 'RANDOM 4',
          value: 'RANDOM_VAL4'
        }
      ]
    };
    const expected = {
      baseline: [
        {
          label: 'France',
          value: 'FRA'
        }
      ],
      highlight: [
        {
          label: 'Austria',
          value: 'AUT'
        }
      ]
    };
    expect(stackedStateFromNewProps(props, state)).to.deep.equal({ ...defaultState, ...expected });
  });
  it('code display', () => {
    const props = { data: oecd_KEI, type: STACKED, display: 'code' };
    const state = {
      type: STACKED,
      baseline: [
        {
          label: 'France',
          value: 'FRA'
        }
      ],
      highlight: [
        {
          label: 'Austria',
          value: 'AUT'
        }
      ]
    };
    const expected = {
      ...defaultState,
      baseline: [
        {
          label: 'FRA',
          value: 'FRA'
        }
      ],
      highlight: [
        {
          label: 'AUT',
          value: 'AUT'
        }
      ]
    };
    expect(stackedStateFromNewProps(props, state)).to.deep.equal(expected);
  });
  it('both display', () => {
    const props = { data: oecd_KEI, type: STACKED, display: 'both' };
    const state = {
      type: STACKED,
      baseline: [
        {
          label: 'France',
          value: 'FRA'
        }
      ],
      highlight: [
        {
          label: 'Austria',
          value: 'AUT'
        }
      ]
    };
    const expected = {
      ...defaultState,
      baseline: [
        {
          label: '(FRA) France',
          value: 'FRA'
        }
      ],
      highlight: [
        {
          label: '(AUT) Austria',
          value: 'AUT'
        }
      ]
    };
    expect(stackedStateFromNewProps(props, state)).to.deep.equal(expected);
  });
});

describe('onChangeStackedDimension tests', () => {
  it('no data', () => {
    expect(onChangeStackedDimension({})('LOCATION')).to.deep.equal({});
  });
  it('no value', () => {
    expect(onChangeStackedDimension({ data: oecd_KEI })(undefined)).to.deep.equal({});
  });
  it('non existing dimension', () => {
    expect(onChangeStackedDimension({ data: oecd_KEI })('RANDOM')).to.deep.equal({});
  });
  it('correct dimension', () => {
    const expected = stateWithFocus({ stackedDimension: 'SUBJECT' });
    expect(onChangeStackedDimension({ data: oecd_KEI })('SUBJECT')).to.deep.equal(expected);
  });
  it('dimension with only one value', () => {
    expect(onChangeStackedDimension({ data: oecd_KEI })('MEASURE')).to.deep.equal({});
  });
});

describe('onChangeStackedMode tests', () => {
  it('no value', () => {
    expect(onChangeStackedMode(undefined)).to.deep.equal({});
  });
  it('non existing mode', () => {
    expect(onChangeStackedMode('random')).to.deep.equal({});
  })
  it('values', () => {
    expect(onChangeStackedMode(VALUES)).to.deep.equal({ stackedMode: VALUES });
  });
  it('percent', () => {
    expect(onChangeStackedMode(PERCENT)).to.deep.equal({ stackedMode: PERCENT });
  });
});
