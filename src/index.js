import meta from '../package.json';
console.info(`${meta.name}@${meta.version}`);

import * as rules from './rules/src';
import * as bridgeD3React from './bridge-d3-react/src';

export { rules };
export { bridgeD3React };
//export * from ...

export { default as Chart } from './chart/src';
export { default as RulesDriver } from './rules-driver/src';
export { default as Share } from './share/src';
export { default as Viewer } from './viewer/src';