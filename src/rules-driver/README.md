# rules-driver

A component that take business logic regarding sdmx from 'rules' library, and exposes
it into props for view components.
(For now only charts from `charts` are supported).

## usage

```javascript
import { RulesDriver } from '@sis-cc/dotstatsuite-components';

<RulesDriver
  data={data}
  dataflowCode={dataflowCode}
  display={display}
  options={options}
  type={type}
  render={
    ({ chartData, chartOptions, onChange, properties }) => (
      <MyViewer
        chartData={chartData}
        chartOptions={chartOptions}
        onChange={onChange}
        properties={properties}
        type={type}
      />
    )
  }
/>
```

## props

### data
`[Object]`
sdmx json data

### dataflowCode
`[String]`
Sdmx code of the dataflow (not defined in sdmx data), used for `code` and `both` displays
of the default title.

### display
`[String]`
Way to display sdmx dimension and atribute values in Chart series and configs views, one of
- `label`
- `code`
- `both`

### options
`[Object]`
Chart engine default options. See `rcw-charts` props definition.

### type
`[String]`
Chart type, one of:
- `BarChart`
- `RowChart`
- `ScatterChart`
- `TimelineChart`
- `HorizontalSymbolChart`
- `VerticalSymbolChart`
- `StackedBarChart`

## render props

### chartData
`[Object]`
Chart derivated data. See `charts` data prop definition.

### chartOptions
`[Object]`
Chart engine derivated options.

### properties
`[Object]`
All property props required in order to render configs views allowing to
configure the current chart data and options.

```javascript
{
  baseline: Object,
  highlight: Object,
  width: Object,
  height: Object,
  title: Object,
  subtitle: Object,
  freqStep: Object,
  scatterDimension: Object,
  scatterX: Object,
  scatterY: Object,
  symbolDimension: Object,
  stackedDimension: Object,
  stackedMode: Object*,
  maxX: Object,
  maxY: Object,
  minX: Object,
  minY: Object,
  pivotX: Object,
  pivotY: Object,
  stepX: Object,
  stepY: Object,
}
```
Each of these properties follows the same definition:

```javascript
property: {
  id: String, // id of the property
  isActive: Boolean, // if the property is Active, regarding the chart type
  onChange: Function, // setter function in order to change the current value of the property
  options: // The complete set of acceptable alternative value if relevant,
    [{ label: String, value: String }] // for baseline, highlight, scatterDimension, scatterX, scatterY, symbolDimension, stackedDimension
    || undefined, // for width, height, title, subtitle, freqStep, maxX, maxY, minX, minY, pivotX, pivotY, stepX, stepY
  value: // the current value of the property
    String
    || { label: String, value: String } // for scatterDimension, scatterX, scatterY, symbolDimension, stackedDimension
    || [{ label: String, value: String }] // for baseline, highlight
}
```

`*` For `stackedMode` the property is not derivated from sdmx data, so options are fixed and always `[{ value: 'values' }, { value: 'percent' }]`,
without labels, as for value `{ value: 'values' || 'percent' }`, `values` being the default one.

### onChange
`[Function]`
Alternative to individual `properties` `onChange` setters. Can be used
to submit several `properties` at once. Useful for a submit form.
```javascript
onChange({ width: 500, height: 500, minY: 0, maxY: 5000, stepY: 500 });
```
