import React, { Component } from 'react';
import { render } from 'react-dom';
import { Button } from '@blueprintjs/core';
import '@blueprintjs/core/dist/blueprint.css';
import { find, prop, propEq } from 'ramda';
import RulesDriver from './';
import oecdData from './mocks/oecd.json';
import oecdTimeData from './mocks/oecd-time.json';
import Menu from './app/menu';
import Viewer1 from './app/viewer1';
import Viewer2 from './app/viewer2';

export default class App extends Component {
  constructor(props) {
    super(props);

    this.availableDatas = { oecdData, oecdTimeData };

    this.sources = [
      { label: 'oecd', value: 'oecdData', dataflowCode: 'KEI' },
      { label: 'oecd quarter', value: 'oecdTimeData', dataflowCode: 'KEI' }
    ];

    this.chartTypes = [
      { label: 'Bar', value: 'BarChart' },
      { label: 'Row', value: 'RowChart' },
      { label: 'Scatter', value: 'ScatterChart' },
      { label: 'Timeline', value: 'TimelineChart' },
      { label: 'H Symbol', value: 'HorizontalSymbolChart' },
      { label: 'V Symbol', value: 'VerticalSymbolChart' },
      { label: 'Stacked Bar', value: 'StackedBarChart' },
      { label: 'Non Chart Type', value: 'NonChartType' }
    ];

    this.viewerOptions = [
      { label: 'Old Classic OCC View', value: 'Viewer1' },
      { label: 'Alternative Split', value: 'Viewer2' },
    ];

    this.viewers = {
      Viewer1,
      Viewer2,
    };

    this.state = {
      data: oecdData,
      display: 'label',
      source: 'oecdData',
      type: 'BarChart',
      viewer: 'Viewer1',
    };

    this.onChangeData = this.onChangeData.bind(this);
    this.onChangeType = this.onChangeType.bind(this);
    this.onChangeViewer = this.onChangeViewer.bind(this);
    this.onChangeDisplay = this.onChangeDisplay.bind(this);
  };

  onChangeData(source) { 
    this.setState({ data: this.availableDatas[source], source });
  };

  onChangeType(type) {
    this.setState({ type });
  }

  onChangeViewer(viewer) {
    this.setState({ viewer });
  }

  onChangeDisplay(display) {
    this.setState({ display });
  }
  
  render() {
    const { data, source, type, viewer, display } = this.state;
    const displayOptions = [
      { label: 'label', value: 'label' },
      { label: 'code', value: 'code' },
      { label: 'both', value: 'both' }
    ];
    const displayProperties = {
      display: {
        isActive: true,
        label: 'Display as',
        onChange: this.onChangeDisplay,
        options: displayOptions,
        value: display,
      }
    };
    const dataflowCode = prop('dataflowCode', find(propEq('value', this.state.source), this.sources));
    const Viewer = this.viewers[viewer];
    return (
      <div>
        <nav className="pt-navbar pt-dark">
          <Menu
            align="left"
            items={this.viewerOptions}
            onChange={this.onChangeViewer}
            selected={viewer}
          />
          <span className="pt-navbar-divider"></span>
          <Menu
            align="right"
            items={this.chartTypes}
            onChange={this.onChangeType}
            selected={type}
          />
          <span className="pt-navbar-divider"></span>
          <Menu
            align="right"
            iconName="import"
            items={this.sources}
            onChange={this.onChangeData}
            selected={source}
          />
        </nav>
        <RulesDriver
          data={data}
          dataflowCode={dataflowCode}
          display={display}
          options={{ base: { height: 500 } }}
          type={type}
          render={
            ({ chartData, chartOptions, onChange, properties }) => (
              <Viewer
                chartData={chartData}
                chartOptions={chartOptions}
                onChange={onChange}
                properties={{ ...properties, ...displayProperties }}
                type={type}
              />
            )
          }
        />
      </div>
    );
  };
};
