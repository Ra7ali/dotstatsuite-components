import React from  'react';
import { Button } from '@blueprintjs/core';
import { map } from 'ramda';

const Menu = ({ align, iconName, items, onChange, selected }) => {
  return (
    <div className={`pt-navbar-group pt-align-${align}`}>
      {
        map(
          ({ label, value }) => (
            <Button
              className="pt-minimal"
              iconName={iconName}
              onClick={() => onChange(value)}
              active={value === selected}
              key={value}
              text={label}
            />
          ),
          items
        )
      }
    </div>
  );
};

export default Menu;
