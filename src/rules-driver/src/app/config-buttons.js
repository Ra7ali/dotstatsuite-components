import React from 'react';
import { map } from 'ramda';
import { Button, ButtonGroup } from '@blueprintjs/core';

export const ConfigButtons = ({ label, onChange, options, value }) => (
  <label className="pt-label pt-inline">
    {label}
    <ButtonGroup>
      {
        map(
          option => (
            <Button
              active={option.value === value}
              key={option.value}
              onClick={() => onChange(option.value)}
            >
              {option.label}
            </Button>
          ),
          options
        )
      }
    </ButtonGroup>
  </label>
);

export default ConfigButtons;
