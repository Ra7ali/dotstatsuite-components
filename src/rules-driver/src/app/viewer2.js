import React from 'react';
import Chart from './chart';
import ConfigT4 from './config-t4size';
import { path } from 'ramda';
import _ConfigSelect from './config-select';
import * as R from 'ramda';

const activeComponent = (Component) => ({ isActive, ...rest }) => isActive ? <Component {...rest} /> : null;

const ConfigSelect = activeComponent(_ConfigSelect);

const Viewer = ({ chartData, chartOptions, onChange, properties, type }) => (
  <div>
    <ConfigT4
      height={path(['height', 'value'], properties)}
      onChange={onChange}
      width={path(['width', 'value'], properties)}
    />
    <ConfigSelect
      {...properties.display}
      label="Display as"
      onChange={selection => properties.display.onChange(selection.value)}
      clearable={false}
      multi={false}
      value={R.find(R.propEq('value', properties.display.value), properties.display.options)}
    />
    <Chart
      data={chartData}
      options={chartOptions}
      type={type}
    />
    <ConfigSelect
      {...R.prop('scatterDimension', properties)}
      label="Scatter Dimension"
      multi={false}
      clearable={false}
    />
    <ConfigSelect
      {...R.prop('scatterX', properties)}
      label="Scatter Value For X Axis"
      multi={false}
      clearable={false}
    />
    <ConfigSelect
      {...R.prop('scatterY', properties)}
      label="Scatter Value For Y Axis"
      multi={false}
      clearable={false}
    />
    <ConfigSelect
      {...R.prop('symbolDimension', properties)}
      label="Symbol Dimension"
      multi={false}
      clearable={false}
    />
    <ConfigSelect
      {...R.prop('stackedDimension', properties)}
      label="Stacked Dimension"
      multi={false}
      clearable={false}
    />
    <ConfigSelect
      {...R.prop('highlight', properties)}
      label="Highlight Selection"
      multi={true}
      clearable={true}
    />
    <ConfigSelect
      {...R.prop('baseline', properties)}
      label="Baseline Selection"
      multi={false}
      clearable={true}
    />
  </div>
);

export default Viewer;
