import React from 'react';
import _ConfigSelect from './config-select';
import _ConfigInput from './config-input';
import _ConfigButtons from './config-buttons';
import ConfigAxis from './config-axis';
import { prop } from 'ramda';
import glamorous from 'glamorous';

const activeComponent = (Component) => ({ isActive, ...rest }) => isActive ? <Component {...rest} /> : null;

const ConfigSelect = activeComponent(_ConfigSelect);
const ConfigInput = activeComponent(_ConfigInput);
const ConfigButtons = activeComponent(_ConfigButtons);

const GDiv = glamorous.div({ width: '100%' });

const Axes = glamorous.div({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-between'
});

const Config = ({ properties }) => {
  return (
    <GDiv>
      <ConfigInput
        {...prop('title', properties)}
        isControlled={true}
        label="Title of the Chart"
        placeholder="Enter a chart title"
      />
      <ConfigInput
        {...prop('subtitle', properties)}
        isControlled={true}
        label="Subtitle of the Chart"
        placeholder="Enter a chart subtitle"
      />
      <ConfigInput
        {...prop('width', properties)}
        isControlled={false}
        label="Width of the Chart"
        leftIcon="arrows-horizontal"
        placeholder="Enter a numerical value"
      />
      <ConfigInput
        {...prop('height', properties)}
        isControlled={false}
        label="Height of the Chart"
        leftIcon="arrows-vertical"
        placeholder="Enter a numerical value"
      />
      <ConfigSelect
        {...prop('scatterDimension', properties)}
        label="Scatter Dimension"
        multi={false}
        clearable={false}
      />
      <ConfigSelect
        {...prop('scatterX', properties)}
        label="Scatter Value For X Axis"
        multi={false}
        clearable={false}
      />
      <ConfigSelect
        {...prop('scatterY', properties)}
        label="Scatter Value For Y Axis"
        multi={false}
        clearable={false}
      />
      <ConfigSelect
        {...prop('symbolDimension', properties)}
        label="Symbol Dimension"
        multi={false}
        clearable={false}
      />
      <ConfigSelect
        {...prop('stackedDimension', properties)}
        label="Stacked Dimension"
        multi={false}
        clearable={false}
      />
      <ConfigSelect
        {...prop('highlight', properties)}
        label="Highlight Selection"
        multi={true}
        clearable={true}
      />
      <ConfigSelect
        {...prop('baseline', properties)}
        label="Baseline Selection"
        multi={false}
        clearable={true}
      />
      <ConfigButtons
        {...prop('display', properties)}
      />
      <ConfigInput
        {...prop('freqStep', properties)}
        isControlled={false}
        label="Timeline Axis Frequency Step"
      />
      <ConfigAxis
        label="Axe X"
        max={prop('maxX', properties)}
        min={prop('minX', properties)}
        pivot={prop('pivotX', properties)}
        step={prop('stepX', properties)}
      />
      <ConfigAxis
        label="Axe Y"
        max={prop('maxY', properties)}
        min={prop('minY', properties)}
        pivot={prop('pivotY', properties)}
        step={prop('stepY', properties)}
      />
    </GDiv>
  );
};

export default Config;
