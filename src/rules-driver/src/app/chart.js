import React from 'react';
import OecdChart from '../../../chart/src';
import { chartTypes } from '../../../rules/src';
import { has } from 'ramda';
import glamorous from 'glamorous';

const Container = glamorous.div({
  marginBottom: 15,
  marginTop: 15
});

const Chart = ({ type, ...rest }) => (
  has(type, chartTypes)
    ? <Container><OecdChart {...rest} type={type} /></Container>
    : <Container>Not a Chart Type</Container>
);

export default Chart;