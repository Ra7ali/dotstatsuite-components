import React from 'react';
import { InputGroup } from '@blueprintjs/core';

const ConfigInput = ({ isControlled, label, leftIcon, onChange, placeholder, value }) => {
  const submit = e => onChange(e.target.value);

  const commit = e => {
    if (e.key === 'Enter') {
      e.preventDefault();
      submit(e);
    }
  }
  return (
    <label className="pt-label">
      {label}
      {
        isControlled
        ? <InputGroup
            value={value}
            leftIconName={leftIcon}
            onChange={submit}
            placeholder={placeholder}
          />
        : <InputGroup
            defaultValue={value}
            leftIconName={leftIcon}
            onKeyPress={commit}
            placeholder={placeholder}
          />
      }
    </label>
  );
};

export default ConfigInput;
