import React from 'react';
import Chart from './chart';
import Config from './config1';

const Viewer = ({ chartData, chartOptions, properties, type }) => {
  return (
    <div>
      <Chart
        data={chartData}
        options={chartOptions}
        type={type}
      />
      <Config properties={properties} />
   </div>
  )
};

export default Viewer;
