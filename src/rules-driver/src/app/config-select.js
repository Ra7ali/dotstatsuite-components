import React from 'react';
import Select from 'react-select';
import glamorous from 'glamorous';
import { head, is } from 'ramda';

const Container = glamorous.div({
  '& .Select-menu-outer': {
    zIndex: 5,
  },
});

const ConfigSelect = ({ label, multi, value, ...rest }) => (
  <Container>
    <label className="pt-label pt-inline">
      {label}
      <Select
        {...rest}
        multi={multi}
        value={is(Array, value) && !multi ? head(value) : value} 
      />
    </label>
  </Container>
);

export default ConfigSelect;
