import React from 'react';
import * as R from 'ramda';
import ConfigSelect from './config-select';

const  t4Options = [
  { label: 'oecd.org/T4: 3 columns (width 270 px)', value: 'T4/3' },
  { label: 'oecd.org/T4: 4 columns (width 370 px)', value: 'T4/4' },
  { label: 'oecd.org/T4: 6 columns (width 560 px)', value: 'T4/6' },
  { label: 'oecd.org/T4: 8 columns (width 760 px)', value: 'T4/8' },
  { label: 'oecd.org/T4: 9 columns (width 855 px)', value: 'T4/9' },
  { label: 'oecd.org/T4: 12 columns (width 1140 px)', value: 'T4/12' }
];
  
const t4Values = [
  { value: 'T4/3', width: 270, height: 203 },
  { value: 'T4/4', width: 370, height: 278 },
  { value: 'T4/6', width: 560, height: 420 },
  { value: 'T4/8', width: 760, height: 570 },
  { value: 'T4/9', width: 855, height: 642 },
  { value: 'T4/12', width: 1140, height: 855 }
];

const ConfigT4 = ({ height, onChange, width }) => {
  const _value = R.find(val => R.propEq('width', width, val) && R.propEq('height', height, val), t4Values);
  const value = R.isNil(_value) ? undefined : R.prop('value', _value);
  const option = R.find(opt => R.propEq('value', value, opt), t4Options);

  const onSelect = (selection) => {
    if (R.isNil(selection)) {
      onChange({ width: undefined, height: undefined });
    }
    else {
      const selectedValue = R.find(val => R.propEq('value', selection.value, val), t4Values);
      onChange(R.pick(['height', 'width'], selectedValue));
    }
  }

  return (
    <ConfigSelect
      label="T4 Formats"
      multi={false}
      clearable={true}
      value={option}
      onChange={onSelect}
      options={t4Options}
    />
  );   
};

export default ConfigT4;
