import React from 'react';
import _ConfigInput from './config-input';
import glamorous from 'glamorous';
import { Classes } from '@blueprintjs/core';

const Content = glamorous.div({
  display: 'flex',
  flexDirection: 'row',
  flexWrap: 'wrap',
  justifyContent: 'space-between',
});

const Callout = glamorous.div({ marginBottom: 10 });

const activeComponent = (Component) => ({ isActive, ...rest }) => isActive ? <Component {...rest} /> : null;

const ConfigInput = activeComponent(_ConfigInput);

const ConfigAxis = ({ label, max, min, pivot, step }) => {
  if (!max.isActive && !min.isActive && !pivot.isActive && !step.isActive) {
    return null;
  }
  return (
    <Callout>
      <div className={Classes.CALLOUT}>
        <h5>{label}</h5>
        <Content>
          <ConfigInput
            {...min}
            label="Min Value"
            isControlled={false}
          />
            <ConfigInput
            {...max}
            label="Max Value"
            isControlled={false}
          />
          <ConfigInput
            {...pivot}
            label="Pivot Value"
            isControlled={false}
          />
          <ConfigInput
            {...step}
            label="Step Value"
            isControlled={false}
          />
        </Content>
      </div>
    </Callout>
  );
};

export default ConfigAxis;
