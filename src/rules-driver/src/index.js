import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  initialState,
  isNonIdealState,
  stateFromNewProps,
  onChangeProperties,
  toProperties,
  toChartData,
  toChartOptions,
} from '../../rules/src';
import { getHeaderProps, getFooterProps } from '../../rules/src/properties/getHeaderProps';

class RulesDriver extends Component {
  constructor(props) {
    super(props);
    this.state = initialState;
  };

  componentWillPrepare = (props, state) => {
    this.setState(stateFromNewProps(props, state));
  };

  componentWillMount = () => {
    this.componentWillPrepare(this.props, this.state);
  };

  componentWillReceiveProps = (props) => {
    this.componentWillPrepare(props, this.state);
  };

  onChange = (newState) => {
    this.setState(newState);
  }

  render = () => {
    return this.props.render({
      chartData: toChartData(this.props, this.state),
      chartOptions: toChartOptions(this.props, this.state),
      headerProps: getHeaderProps(this.props, this.state),
      footerProps: getFooterProps(this.props, this.state),
      isNonIdealState: isNonIdealState(this.props.data, this.props.type),
      onChange: onChangeProperties(this.props, this.state, this.onChange),
      properties: toProperties(this.props, this.state, this.onChange)
    }); 
  }; 
};

RulesDriver.propTypes = {
  data: PropTypes.object,
  dataflow: PropTypes.object,
  display: PropTypes.string,
  formaterIds: PropTypes.object,
  options: PropTypes.object,
  type: PropTypes.string.isRequired,
};

export default RulesDriver;
