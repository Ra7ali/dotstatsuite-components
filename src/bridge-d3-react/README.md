# bridge-d3-react

Wrapper of [d3-charts](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-d3-charts) that expose charts as React Components.

## Usage

```javascript
import React from 'react';
import { render } from 'react-dom';
import { bridgeD3React } from '@sis-cc/dotstatsuite-components';

const log = options => console.log(options);

const BarChart = bridgeD3React.BarChart;

render(
  <BarChart data={ data } getInitialOptions={ log } />,
  document.getElementById('root')
);
```

## Available charts

All charts of `d3-charts`.

## API (out of the box usage)

### methods

#### `getInitialOptions(options)`

Give options used by the chart engine to draw a chart (superset of user options).

### props

#### `options`

Same as `d3-charts`.

#### `data`

Same as `d3-charts`.
