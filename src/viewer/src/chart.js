import React from 'react';
import { branch, compose, renderComponent, withProps } from 'recompose';
import * as R from 'ramda';
import glamorous from 'glamorous';
import { NoData } from '@sis-cc/dotstatsuite-visions';
import * as charts from '../../bridge-d3-react/src';
import { getOptions, isScatter } from '../../chart/src/utils';
import filterSeriesRegardingDimensions from '../../chart/src/utils/filterSeries';
import { AxisLegend } from '../../chart/src/glamorous_components';
import { isChartNoData } from './utils';

const Container = glamorous.div({
  backgroundColor: 'none',
  overflow: 'hidden',
  position: 'relative',
  direction: 'ltr !important',
}, ({ type }) => {
  if (!isScatter(type))
    return ({});
  return ({
    '& .rcw-chart__chart__line--x:last-of-type': {
      visibility: 'hidden',
    },
    '& .rcw-chart__chart__line--y:last-of-type': {
      visibility: 'hidden',
    },
    '& .rcw-chart__chart__axis--x': {
      '& .tick:last-of-type': {
        visibility: 'hidden',
      }
    },
    '& .rcw-chart__chart__axis--y': {
      '& .tick:last-of-type': {
        visibility: 'hidden',
      }
    },
  });
});

const Chart = ({ options, getEngineOptions, fonts, series, type, width }) => {
  const ChartClass = charts[type];
  return (
    <Container type={type}>
      <ChartClass
        data={series}
        getInitialOptions={getEngineOptions}
        options={options}
      />
      {
        (R.is(Number, width) && width < 370)
        ? null
        : <AxisLegend axis="y" data={{ series }} fonts={R.propOr({}, 'axisLegend', fonts)} type={type} />
      }
    </Container>
  );
};

export default compose(
  withProps(({ heightOffsets, options }) => ({
    options: R.over( // adjust svg height regarding header and footer height
      R.lensPath(['base', 'height']),
      R.pipe(h => h - heightOffsets.header - heightOffsets.footer - 22, R.when(R.gt(0), R.always(300))), // 22 = border plus header margins
      options
    )
  })),
  branch(
    isChartNoData,
    renderComponent(({ noData, options, fixedHeight }) => {
      if (R.isNil(fixedHeight)) {
        return (<NoData message={noData || 'No Data'} />);
      }
      const height = R.path(['base', 'height'], options);
      const style = R.equals(0, height) ? { height: 'auto' } : { height };
      return (
        <div style={style}>
          <NoData message={noData || 'No Data'} />
        </div>
      );
    })
  ),
  withProps(({ data, options, type }) => ({
    series: filterSeriesRegardingDimensions(data.series, type, options), // responsive rule only display highlighted data
  })),
)(Chart);
