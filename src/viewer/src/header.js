import React from 'react';
import sizeMe from 'react-sizeme';
import { DataHeader as Header } from '@sis-cc/dotstatsuite-visions';

export default sizeMe({ noPlaceholder: true, monitorHeight: true, monitorWidth: false })(Header);
