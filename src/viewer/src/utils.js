import * as R from 'ramda';

export const isChartNoData = R.anyPass([
  R.pipe(R.path(['data', 'series']), R.anyPass([R.isNil, R.isEmpty])),
  R.allPass([
    R.pipe(R.prop('type'), R.equals('ChoroplethChart'), R.not),
    R.pipe(R.path(['data', 'series', 0, 'datapoints']), R.anyPass([R.isNil, R.isEmpty]))
  ])
]);