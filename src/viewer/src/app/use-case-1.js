import React, { Component } from 'react';
import { render } from 'react-dom';
import Viewer from '../';
import stackedData from '../../../chart/test/mocks/stacked-format';

export default ({ config, style, options }) => {
  const chartData = stackedData();
  const chartOptions = options;
  const footerProps = config;
  const headerProps = {
    title: { label: 'Science performance (PISA)' },
    subtitle: [{ label: 'Mean score 2015, Annual incl. OECD, all countries' }],
  };
  return (
    <div style={style}>
      <Viewer
        chartData={chartData}
        chartOptions={chartOptions}
        headerProps={headerProps}
        footerProps={footerProps}
        type="StackedBarChart"
      />
    </div>
  );
};
