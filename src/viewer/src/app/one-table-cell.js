import React, { Component } from 'react';
import { render } from 'react-dom';
import Viewer from '../';

const cells = {
  "": {
    "": {
      "": [
        {
          "flags": [
            {
              "code": "A",
              "label": "[OBS_STATUS]: [A]"
            },
            {
              "code": "F",
              "label": "[CONF_STATUS]: [F]"
            }
          ],
          "intValue": 2,
          "value": "2"
        }
      ]
    }
  }
}

const headerData = [
  {
    "data": [],
    "key": "",
    "flags": []
  }
];

const sectionsData = [
  [
    {
      "data": [],
      "key": "",
      "flags": []
    },
    [
      {
        "data": [],
        "key": "",
        "flags": []
      }
    ]
  ]
]

import { ThemeProvider } from 'glamorous';
import { mainTheme } from '@sis-cc/dotstatsuite-ui-components';

export default ({ config, style, options, isRtl }) => {
  const footerProps = config;
  const headerProps = {
    title: { label: 'Table Data' },
  };
  const tableProps = { headerData, sectionsData, cells }
  return (
    <div style={style}>
      <ThemeProvider theme={mainTheme}>
        <Viewer
          tableProps={tableProps}
          headerProps={headerProps}
          footerProps={footerProps}
          type="table"
          isRtl={isRtl}
        />
      </ThemeProvider>
    </div>
  );
};