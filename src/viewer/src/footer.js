import React from 'react';
import { compose, withProps } from 'recompose';
import * as R from 'ramda';
import sizeMe from 'react-sizeme';
import SisFooter from '@sis-cc/dotstatsuite-ui-footer';
import FocusLegend from '../../chart/src/components/focus-legend';
import SymbolLegend from '../../chart/src/components/symbol-legend';
import StackedLegend from '../../chart/src/components/stacked-legend';
import ChoroLegend from '../../chart/src/components/choro-legend';
import { isSymbol, isStackedBar, isChoropleth } from '../../chart/src/utils';
import { AxisLegend } from '../../chart/src/glamorous_components';
import { isChartNoData } from './utils';

export default compose(
  sizeMe({ noPlaceholder: true, monitorHeight: true, monitorWidth: false }),
  withProps(({ chartData, fonts, legendProps, options, type, width }) => {
    if (type === 'table' || isChartNoData({ data: chartData, type })) {
      return ({ fonts: R.propOr({}, 'footer', fonts) });
    }
    const symbols = R.prop('symbols', legendProps);
    const legendFonts = R.pathOr({}, ['chart', 'chartLegend'], fonts);
    const symbolLegend = isSymbol(type) && !R.anyPass([R.isNil, R.isEmpty])(symbols)
      ? <SymbolLegend
          data={chartData}
          fonts={legendFonts}
          symbols={symbols}
          type={type}
        />
      : null;

    const layerSeries = R.prop('layerSeries', legendProps);
    const stackedLegend = isStackedBar(type) && !R.anyPass([R.isNil, R.isEmpty])(layerSeries)
      ? <StackedLegend
          fonts={legendFonts}
          layerSeries={layerSeries}
          type={type}
        />
      : null;
    const choroLegend = isChoropleth
      ? <ChoroLegend
          data={chartData}
          fonts={legendFonts}
          options={options}
          type={type}
          width={width}
        />
      : null;

    const legend =
      <div>
        <AxisLegend axis="x" data={chartData} fonts={R.pathOr({}, ['chart', 'axisLegend'], fonts)} type={type} />
        <FocusLegend data={chartData} fonts={legendFonts} type={type} width={width} />
        {symbolLegend}
        {stackedLegend}
        {choroLegend}
      </div>;

    return ({ legend, fonts: R.propOr({}, 'footer', fonts) });
  })
)(SisFooter);
