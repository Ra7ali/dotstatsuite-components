import React from 'react';
import * as R from 'ramda';
import { compose, withState, withProps } from 'recompose';
import sizeMe from 'react-sizeme';
import { Loading, NoData, Table, TableHtml5 } from '@sis-cc/dotstatsuite-visions';
import glamorous from 'glamorous';
import { getOptions as getChartOptions } from '../../chart/src/utils';
import Header from './header';
import Chart from './chart';
import Footer from './footer';

const Container = glamorous.div({
  borderColor: '#0297C9',
  borderBottomWidth: 1,
  borderLeftWidth: 0,
  borderTopWidth: 1,
  borderRightWidth: 0,
  borderStyle: 'solid',
  '& svg text::selection': {
    background: 'none'
  }}, ({ type, fixedWidth, fixedHeight }) => R.ifElse(
    R.always(type === 'table'),
    R.always({
      minWidth: '100%',
      minHeight: '100%'
    }),
    R.always({
      width: fixedWidth || '100%',
      height: fixedHeight || '100%',
      overflow: 'hidden'
    })
  )
);

const ViewContent = ({ loading, noData = 'No Data', hasAccessibility, type, fonts, width, ...rest }) => {
  if (loading) return <Loading message={loading} />;
  
  if (type === 'table') {
    const tableProps = R.propOr({}, 'tableProps', rest);
    const hasNoObs = R.pipe(R.prop('cells'), R.anyPass([R.isNil, R.isEmpty]))(tableProps);
    if (hasNoObs) return <NoData message={noData} />;

    const TableComponent = hasAccessibility ? TableHtml5 : Table;
    return <TableComponent isRtl={R.prop('isRtl', rest)} {...tableProps} />;
  }

  return (
    <Chart
      data={R.prop('chartData', rest)}
      fixedHeight={R.prop('fixedHeight', rest)}
      fonts={R.propOr({}, 'chart', fonts)}
      getEngineOptions={R.prop('getEngineOptions', rest)}
      heightOffsets={R.prop('heightOffsets', rest)}
      noData={noData}
      options={R.prop('chartOptions', rest)}
      type={type}
      width={width}
    />
  );
};

const Viewer = ({ fonts, setFooterOffset, setHeaderOffset, size, type, ...rest }) => {
  return (
    <Container type={type} {...R.pick(['fixedWidth', 'fixedHeight'], rest)}>
      <Header
        onSize={size => setHeaderOffset(size.height)}
        {...R.propOr({}, 'headerProps', rest)}
      />
      <ViewContent fonts={fonts} type={type} width={size.width} {...rest} />
      <Footer
        fonts={fonts}
        onSize={size => setFooterOffset(size.height)}
        width={size.width}
        type={type}
        chartData={R.prop('chartData', rest)}
        {...R.propOr({}, 'footerProps', rest)}
      />
    </Container>
  );
};

export default compose(
  sizeMe({ noPlaceholder: true, monitorWidth: true }),
  withState(
    'heightOffsets',
    'setHeightOffsets',
    { footer: undefined, header: undefined }
  ),
  withState(
    'engineOptions',
    'getEngineOptions',
    undefined
  ),
  withProps(({ engineOptions, getEngineOptions, heightOffsets, setHeightOffsets }) => ({
    heightOffsets: R.evolve({
      footer: R.when(R.isNil, R.always(0)),
      header: R.when(R.isNil, R.always(0))
    },heightOffsets),
    setFooterOffset: offset => setHeightOffsets(R.assoc('footer', offset)),
    setHeaderOffset: offset => setHeightOffsets(R.assoc('header', offset)),
    getEngineOptions: R.isNil(engineOptions) ? getEngineOptions : null,
  })),
  withProps( // inject responsive size if needed for chart
    ({ chartOptions, engineOptions, footerProps, heightOffsets, size, type }) => {
      if (type === 'table') {
        return ({});
      }
      const offsets = heightOffsets.header + heightOffsets.footer;
      return ({
        chartOptions: R.pipe(
          R.over(R.lensPath(['base', 'width']), R.when(R.anyPass([R.isNil, isNaN]), R.always(size.width || 0))),
          R.over(R.lensPath(['base', 'height']), R.when(R.anyPass([R.isNil, isNaN]), R.always(0)))
        )(chartOptions),
        fixedWidth: R.path(['base', 'width'], chartOptions),
        fixedHeight: R.path(['base', 'height'], chartOptions),
        footerProps: R.pipe(
          R.assocPath(['legendProps', 'layerSeries'], R.pathOr([], ['serie', 'stacked', 'layerSeries'], engineOptions)),
          R.assocPath(['legendProps', 'symbols'], R.pathOr({}, ['serie', 'symbol'], engineOptions))
        )(footerProps)
      });
  }),
  withProps( // compute all chart options regarding oecd design
    ({ chartData, chartOptions, fonts, type }) => {
      if (type === 'table') {
        return ({});
      }
      return ({
        chartOptions: getChartOptions(chartOptions, chartData, type, { fonts })
      });
  })
)(Viewer);
