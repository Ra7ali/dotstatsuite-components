import React from 'react';
import { render } from 'react-dom';
//import ChartApp from './chart/src/app';
//import RulesDriverApp from './rules-driver/src/app';
//import ShareApp from './share/src/app';
import ViewerApp from './viewer/src/app';

const App = () => (
  <React.Fragment>
    {/*<ChartApp />*/}
    {/*<RulesDriverApp />*/}
    {/*<ShareApp />*/}
    <ViewerApp />
  </React.Fragment>
);

render(<App />, document.getElementById('root'));
